drop foreign table if exists sm_account_account;	create FOREIGN TABLE sm_account_account("id" int4 NOT NULL
	,"create_uid" int4
	,"code" varchar(64) NOT NULL
	,"create_date" timestamp
	,"reconcile" bool
	,"name" varchar NOT NULL
	,"deprecated" bool
	,"company_id" int4 NOT NULL
	,"write_uid" int4
	,"currency_id" int4
	,"note" text
	,"internal_type" varchar
	,"write_date" timestamp
	,"user_type_id" int4 NOT NULL
	,"last_time_entries_checked" timestamp
	,"group_id" int4
	,"centralized" bool
	,"internal_group" varchar
	,"asset_profile_id" int4
	,"_api_external_id" int4
	,"external_id_sequence_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_account');
drop foreign table if exists sm_account_account_account_group_rel;	create FOREIGN TABLE sm_account_account_account_group_rel("account_group_id" int4 NOT NULL
	,"account_account_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_account_account_group_rel');
drop foreign table if exists sm_account_account_account_tag;	create FOREIGN TABLE sm_account_account_account_tag("account_account_id" int4 NOT NULL
	,"account_account_tag_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_account_account_tag');
drop foreign table if exists sm_account_account_aged_partner_balance_wizard_rel;	create FOREIGN TABLE sm_account_account_aged_partner_balance_wizard_rel("aged_partner_balance_wizard_id" int4 NOT NULL
	,"account_account_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_account_aged_partner_balance_wizard_rel');
drop foreign table if exists sm_account_account_financial_report;	create FOREIGN TABLE sm_account_account_financial_report("report_line_id" int4 NOT NULL
	,"account_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_account_financial_report');
drop foreign table if exists sm_account_account_financial_report_type;	create FOREIGN TABLE sm_account_account_financial_report_type("report_id" int4 NOT NULL
	,"account_type_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_account_financial_report_type');
drop foreign table if exists sm_account_account_general_ledger_report_wizard_rel;	create FOREIGN TABLE sm_account_account_general_ledger_report_wizard_rel("general_ledger_report_wizard_id" int4 NOT NULL
	,"account_account_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_account_general_ledger_report_wizard_rel');
drop foreign table if exists sm_account_account_open_items_report_wizard_rel;	create FOREIGN TABLE sm_account_account_open_items_report_wizard_rel("open_items_report_wizard_id" int4 NOT NULL
	,"account_account_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_account_open_items_report_wizard_rel');
drop foreign table if exists sm_account_account_report_aged_partner_balance_rel;	create FOREIGN TABLE sm_account_account_report_aged_partner_balance_rel("report_aged_partner_balance_id" int4 NOT NULL
	,"account_account_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_account_report_aged_partner_balance_rel');
drop foreign table if exists sm_account_account_report_general_ledger_rel;	create FOREIGN TABLE sm_account_account_report_general_ledger_rel("report_general_ledger_id" int4 NOT NULL
	,"account_account_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_account_report_general_ledger_rel');
drop foreign table if exists sm_account_account_report_open_items_rel;	create FOREIGN TABLE sm_account_account_report_open_items_rel("report_open_items_id" int4 NOT NULL
	,"account_account_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_account_report_open_items_rel');
drop foreign table if exists sm_account_account_report_trial_balance_account_rel;	create FOREIGN TABLE sm_account_account_report_trial_balance_account_rel("report_trial_balance_account_id" int4 NOT NULL
	,"account_account_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_account_report_trial_balance_account_rel');
drop foreign table if exists sm_account_account_report_trial_balance_rel;	create FOREIGN TABLE sm_account_account_report_trial_balance_rel("report_trial_balance_id" int4 NOT NULL
	,"account_account_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_account_report_trial_balance_rel');
drop foreign table if exists sm_account_account_tag;	create FOREIGN TABLE sm_account_account_tag("id" int4 NOT NULL
	,"create_uid" int4
	,"applicability" varchar NOT NULL
	,"create_date" timestamp
	,"name" varchar NOT NULL
	,"color" int4
	,"write_uid" int4
	,"write_date" timestamp
	,"active" bool)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_account_tag');
drop foreign table if exists sm_account_account_tag_account_tax_template_rel;	create FOREIGN TABLE sm_account_account_tag_account_tax_template_rel("account_tax_template_id" int4 NOT NULL
	,"account_account_tag_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_account_tag_account_tax_template_rel');
drop foreign table if exists sm_account_account_tax_default_rel;	create FOREIGN TABLE sm_account_account_tax_default_rel("account_id" int4 NOT NULL
	,"tax_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_account_tax_default_rel');
drop foreign table if exists sm_account_account_template;	create FOREIGN TABLE sm_account_account_template("id" int4 NOT NULL
	,"create_uid" int4
	,"code" varchar(64) NOT NULL
	,"create_date" timestamp
	,"reconcile" bool
	,"name" varchar NOT NULL
	,"chart_template_id" int4
	,"write_uid" int4
	,"currency_id" int4
	,"note" text
	,"nocreate" bool
	,"write_date" timestamp
	,"user_type_id" int4 NOT NULL
	,"group_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_account_template');
drop foreign table if exists sm_account_account_template_account_tag;	create FOREIGN TABLE sm_account_account_template_account_tag("account_account_template_id" int4 NOT NULL
	,"account_account_tag_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_account_template_account_tag');
drop foreign table if exists sm_account_account_template_tax_rel;	create FOREIGN TABLE sm_account_account_template_tax_rel("account_id" int4 NOT NULL
	,"tax_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_account_template_tax_rel');
drop foreign table if exists sm_account_account_trial_balance_report_wizard_rel;	create FOREIGN TABLE sm_account_account_trial_balance_report_wizard_rel("trial_balance_report_wizard_id" int4 NOT NULL
	,"account_account_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_account_trial_balance_report_wizard_rel');
drop foreign table if exists sm_account_account_type;	create FOREIGN TABLE sm_account_account_type("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"name" varchar NOT NULL
	,"write_uid" int4
	,"note" text
	,"write_date" timestamp
	,"include_initial_balance" bool
	,"type" varchar NOT NULL
	,"internal_group" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_account_type');
drop foreign table if exists sm_account_account_type_general_ledger_report_wizard_rel;	create FOREIGN TABLE sm_account_account_type_general_ledger_report_wizard_rel("general_ledger_report_wizard_id" int4 NOT NULL
	,"account_account_type_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_account_type_general_ledger_report_wizard_rel');
drop foreign table if exists sm_account_account_type_rel;	create FOREIGN TABLE sm_account_account_type_rel("journal_id" int4 NOT NULL
	,"account_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_account_type_rel');
drop foreign table if exists sm_account_aged_trial_balance;	create FOREIGN TABLE sm_account_aged_trial_balance("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"period_length" int4 NOT NULL
	,"date_from" date
	,"result_selection" varchar NOT NULL
	,"company_id" int4
	,"write_date" timestamp
	,"date_to" date
	,"write_uid" int4
	,"target_move" varchar NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_aged_trial_balance');
drop foreign table if exists sm_account_aged_trial_balance_account_journal_rel;	create FOREIGN TABLE sm_account_aged_trial_balance_account_journal_rel("account_aged_trial_balance_id" int4 NOT NULL
	,"account_journal_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_aged_trial_balance_account_journal_rel');
drop foreign table if exists sm_account_analytic_account;	create FOREIGN TABLE sm_account_analytic_account("id" int4 NOT NULL
	,"code" varchar
	,"create_date" timestamp
	,"name" varchar NOT NULL
	,"create_uid" int4
	,"message_last_post" timestamp
	,"company_id" int4
	,"write_uid" int4
	,"write_date" timestamp
	,"account_type" varchar
	,"partner_id" int4
	,"use_tasks" bool
	,"use_issues" bool
	,"openupgrade_legacy_10_0_account_type" varchar
	,"active" bool
	,"contract_type" varchar
	,"pricelist_id" int4
	,"recurring_rule_type" varchar
	,"recurring_invoicing_type" varchar
	,"recurring_interval" int4
	,"journal_id" int4
	,"contract_template_id" int4
	,"date_start" date
	,"date_end" date
	,"recurring_invoices" bool
	,"recurring_next_date" date
	,"user_id" int4
	,"payment_mode_id" int4
	,"invoicing_sales" bool
	,"skip_zero_qty" bool
	,"department_id" int4
	,"parent_left" int4
	,"parent_right" int4
	,"parent_id" int4
	,"message_main_attachment_id" int4
	,"group_id" int4
	,"parent_path" varchar
	,"complete_name" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_analytic_account');
drop foreign table if exists sm_account_analytic_account_general_ledger_report_wizard_rel;	create FOREIGN TABLE sm_account_analytic_account_general_ledger_report_wizard_rel("general_ledger_report_wizard_id" int4 NOT NULL
	,"account_analytic_account_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_analytic_account_general_ledger_report_wizard_rel');
drop foreign table if exists sm_account_analytic_account_report_general_ledger_rel;	create FOREIGN TABLE sm_account_analytic_account_report_general_ledger_rel("report_general_ledger_id" int4 NOT NULL
	,"account_analytic_account_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_analytic_account_report_general_ledger_rel');
drop foreign table if exists sm_account_analytic_account_tag_rel;	create FOREIGN TABLE sm_account_analytic_account_tag_rel("account_id" int4 NOT NULL
	,"tag_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_analytic_account_tag_rel');
drop foreign table if exists sm_account_analytic_chart;	create FOREIGN TABLE sm_account_analytic_chart("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"from_date" date
	,"to_date" date
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_analytic_chart');
drop foreign table if exists sm_account_analytic_default;	create FOREIGN TABLE sm_account_analytic_default("id" int4 NOT NULL
	,"sequence" int4
	,"analytic_id" int4
	,"product_id" int4
	,"partner_id" int4
	,"user_id" int4
	,"company_id" int4
	,"date_start" date
	,"date_stop" date
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_analytic_default');
drop foreign table if exists sm_account_analytic_default_account_analytic_tag_rel;	create FOREIGN TABLE sm_account_analytic_default_account_analytic_tag_rel("account_analytic_default_id" int4 NOT NULL
	,"account_analytic_tag_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_analytic_default_account_analytic_tag_rel');
drop foreign table if exists sm_account_analytic_dimension;	create FOREIGN TABLE sm_account_analytic_dimension("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"code" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_analytic_dimension');
drop foreign table if exists sm_account_analytic_distribution;	create FOREIGN TABLE sm_account_analytic_distribution("id" int4 NOT NULL
	,"account_id" int4 NOT NULL
	,"percentage" float8 NOT NULL
	,"tag_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_analytic_distribution');
drop foreign table if exists sm_account_analytic_group;	create FOREIGN TABLE sm_account_analytic_group("id" int4 NOT NULL
	,"parent_path" varchar
	,"name" varchar NOT NULL
	,"description" text
	,"parent_id" int4
	,"complete_name" varchar
	,"company_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_analytic_group');
drop foreign table if exists sm_account_analytic_line;	create FOREIGN TABLE sm_account_analytic_line("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"account_id" int4 NOT NULL
	,"company_id" int4 NOT NULL
	,"write_uid" int4
	,"date" date NOT NULL
	,"amount" numeric NOT NULL
	,"unit_amount" float8
	,"write_date" timestamp
	,"user_id" int4
	,"partner_id" int4
	,"name" varchar NOT NULL
	,"code" varchar(8)
	,"currency_id" int4
	,"ref" varchar
	,"general_account_id" int4
	,"move_id" int4
	,"product_id" int4
	,"product_uom_id" int4
	,"amount_currency" numeric
	,"so_line" int4
	,"department_id" int4
	,"account_department_id" int4
	,"agreement_id" int4
	,"openupgrade_legacy_12_0_company_id" int4
	,"group_id" int4
	,"task_id" int4
	,"project_id" int4
	,"employee_id" int4
	,"ticket_id" int4
	,"ticket_partner_id" int4
	,"holiday_id" int4
	,"date_time" timestamp
	,"timesheet_invoice_type" varchar
	,"timesheet_invoice_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_analytic_line');
drop foreign table if exists sm_account_analytic_line_tag_rel;	create FOREIGN TABLE sm_account_analytic_line_tag_rel("line_id" int4 NOT NULL
	,"tag_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_analytic_line_tag_rel');
drop foreign table if exists sm_account_analytic_tag;	create FOREIGN TABLE sm_account_analytic_tag("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"name" varchar NOT NULL
	,"color" int4
	,"write_uid" int4
	,"write_date" timestamp
	,"active" bool
	,"active_analytic_distribution" bool
	,"company_id" int4
	,"analytic_dimension_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_analytic_tag');
drop foreign table if exists sm_account_analytic_tag_account_asset_profile_rel;	create FOREIGN TABLE sm_account_analytic_tag_account_asset_profile_rel("account_asset_profile_id" int4 NOT NULL
	,"account_analytic_tag_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_analytic_tag_account_asset_profile_rel');
drop foreign table if exists sm_account_analytic_tag_account_asset_rel;	create FOREIGN TABLE sm_account_analytic_tag_account_asset_rel("account_asset_id" int4 NOT NULL
	,"account_analytic_tag_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_analytic_tag_account_asset_rel');
drop foreign table if exists sm_account_analytic_tag_account_invoice_line_rel;	create FOREIGN TABLE sm_account_analytic_tag_account_invoice_line_rel("account_invoice_line_id" int4 NOT NULL
	,"account_analytic_tag_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_analytic_tag_account_invoice_line_rel');
drop foreign table if exists sm_account_analytic_tag_account_invoice_tax_rel;	create FOREIGN TABLE sm_account_analytic_tag_account_invoice_tax_rel("account_invoice_tax_id" int4 NOT NULL
	,"account_analytic_tag_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_analytic_tag_account_invoice_tax_rel');
drop foreign table if exists sm_account_analytic_tag_account_move_line_rel;	create FOREIGN TABLE sm_account_analytic_tag_account_move_line_rel("account_move_line_id" int4 NOT NULL
	,"account_analytic_tag_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_analytic_tag_account_move_line_rel');
drop foreign table if exists sm_account_analytic_tag_account_reconcile_model_rel;	create FOREIGN TABLE sm_account_analytic_tag_account_reconcile_model_rel("account_reconcile_model_id" int4 NOT NULL
	,"account_analytic_tag_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_analytic_tag_account_reconcile_model_rel');
drop foreign table if exists sm_account_analytic_tag_contract_line_rel;	create FOREIGN TABLE sm_account_analytic_tag_contract_line_rel("contract_line_id" int4 NOT NULL
	,"account_analytic_tag_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_analytic_tag_contract_line_rel');
drop foreign table if exists sm_account_analytic_tag_general_ledger_report_wizard_rel;	create FOREIGN TABLE sm_account_analytic_tag_general_ledger_report_wizard_rel("general_ledger_report_wizard_id" int4 NOT NULL
	,"account_analytic_tag_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_analytic_tag_general_ledger_report_wizard_rel');
drop foreign table if exists sm_account_analytic_tag_hr_expense_rel;	create FOREIGN TABLE sm_account_analytic_tag_hr_expense_rel("hr_expense_id" int4 NOT NULL
	,"account_analytic_tag_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_analytic_tag_hr_expense_rel');
drop foreign table if exists sm_account_analytic_tag_mis_budget_by_account_item_rel;	create FOREIGN TABLE sm_account_analytic_tag_mis_budget_by_account_item_rel("mis_budget_by_account_item_id" int4 NOT NULL
	,"account_analytic_tag_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_analytic_tag_mis_budget_by_account_item_rel');
drop foreign table if exists sm_account_analytic_tag_mis_budget_item_rel;	create FOREIGN TABLE sm_account_analytic_tag_mis_budget_item_rel("mis_budget_item_id" int4 NOT NULL
	,"account_analytic_tag_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_analytic_tag_mis_budget_item_rel');
drop foreign table if exists sm_account_analytic_tag_mis_report_instance_period_rel;	create FOREIGN TABLE sm_account_analytic_tag_mis_report_instance_period_rel("mis_report_instance_period_id" int4 NOT NULL
	,"account_analytic_tag_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_analytic_tag_mis_report_instance_period_rel');
drop foreign table if exists sm_account_analytic_tag_mis_report_instance_rel;	create FOREIGN TABLE sm_account_analytic_tag_mis_report_instance_rel("mis_report_instance_id" int4 NOT NULL
	,"account_analytic_tag_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_analytic_tag_mis_report_instance_rel');
drop foreign table if exists sm_account_analytic_tag_purchase_order_line_rel;	create FOREIGN TABLE sm_account_analytic_tag_purchase_order_line_rel("purchase_order_line_id" int4 NOT NULL
	,"account_analytic_tag_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_analytic_tag_purchase_order_line_rel');
drop foreign table if exists sm_account_analytic_tag_report_general_ledger_rel;	create FOREIGN TABLE sm_account_analytic_tag_report_general_ledger_rel("report_general_ledger_id" int4 NOT NULL
	,"account_analytic_tag_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_analytic_tag_report_general_ledger_rel');
drop foreign table if exists sm_account_analytic_tag_sale_order_line_rel;	create FOREIGN TABLE sm_account_analytic_tag_sale_order_line_rel("sale_order_line_id" int4 NOT NULL
	,"account_analytic_tag_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_analytic_tag_sale_order_line_rel');
drop foreign table if exists sm_account_analytic_tag_stock_move_rel;	create FOREIGN TABLE sm_account_analytic_tag_stock_move_rel("stock_move_id" int4 NOT NULL
	,"account_analytic_tag_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_analytic_tag_stock_move_rel');
drop foreign table if exists sm_account_analytic_tag_stock_scrap_rel;	create FOREIGN TABLE sm_account_analytic_tag_stock_scrap_rel("stock_scrap_id" int4 NOT NULL
	,"account_analytic_tag_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_analytic_tag_stock_scrap_rel');
drop foreign table if exists sm_account_asset;	create FOREIGN TABLE sm_account_asset("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"code" varchar(32)
	,"company_currency_id" int4
	,"company_id" int4 NOT NULL
	,"note" text
	,"profile_id" int4 NOT NULL
	,"date_start" date NOT NULL
	,"state" varchar NOT NULL
	,"active" bool
	,"partner_id" int4
	,"method" varchar NOT NULL
	,"method_number" int4
	,"openupgrade_legacy_12_0_method_period" int4
	,"method_end" date
	,"method_progress_factor" numeric
	,"method_time" varchar NOT NULL
	,"prorata" bool
	,"salvage_value" numeric
	,"invoice_id" int4
	,"message_last_post" timestamp
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"move_end_period" bool
	,"method_percentage" numeric
	,"annual_percentage" numeric
	,"start_depreciation_date" date
	,"openupgrade_legacy_12_0_method_number" int4
	,"openupgrade_legacy_12_0_method_time" varchar
	,"purchase_value" float8 NOT NULL
	,"depreciation_base" numeric
	,"value_residual" numeric
	,"value_depreciated" numeric
	,"date_remove" date
	,"method_period" varchar NOT NULL
	,"days_calc" bool
	,"use_leap_years" bool
	,"account_analytic_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_asset');
drop foreign table if exists sm_account_asset_compute;	create FOREIGN TABLE sm_account_asset_compute("id" int4 NOT NULL
	,"date_end" date NOT NULL
	,"note" text
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_asset_compute');
drop foreign table if exists sm_account_asset_group;	create FOREIGN TABLE sm_account_asset_group("id" int4 NOT NULL
	,"parent_path" varchar
	,"name" varchar(64) NOT NULL
	,"code" varchar
	,"company_id" int4 NOT NULL
	,"parent_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_asset_group');
drop foreign table if exists sm_account_asset_group_rel;	create FOREIGN TABLE sm_account_asset_group_rel("asset_id" int4 NOT NULL
	,"group_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_asset_group_rel');
drop foreign table if exists sm_account_asset_line;	create FOREIGN TABLE sm_account_asset_line("id" int4 NOT NULL
	,"name" varchar
	,"sequence" int4
	,"asset_id" int4 NOT NULL
	,"amount" numeric NOT NULL
	,"remaining_value" numeric
	,"depreciated_value" numeric
	,"line_date" date NOT NULL
	,"move_id" int4
	,"move_check" bool
	,"move_posted_check" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"previous_id" int4
	,"line_days" int4
	,"type" varchar
	,"init_entry" bool)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_asset_line');
drop foreign table if exists sm_account_asset_profile;	create FOREIGN TABLE sm_account_asset_profile("id" int4 NOT NULL
	,"active" bool
	,"name" varchar NOT NULL
	,"account_analytic_id" int4
	,"account_asset_id" int4 NOT NULL
	,"account_depreciation_id" int4 NOT NULL
	,"account_expense_depreciation_id" int4 NOT NULL
	,"journal_id" int4 NOT NULL
	,"company_id" int4 NOT NULL
	,"method" varchar NOT NULL
	,"method_number" int4
	,"openupgrade_legacy_12_0_method_period" int4
	,"method_progress_factor" numeric
	,"method_time" varchar NOT NULL
	,"method_end" date
	,"prorata" bool
	,"open_asset" bool
	,"group_entries" bool
	,"type" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"method_percentage" numeric
	,"openupgrade_legacy_12_0_method_number" int4
	,"openupgrade_legacy_12_0_method_time" varchar
	,"note" text
	,"account_plus_value_id" int4
	,"account_min_value_id" int4
	,"account_residual_value_id" int4
	,"method_period" varchar NOT NULL
	,"days_calc" bool
	,"use_leap_years" bool
	,"asset_product_item" bool
	,"annual_percentage" numeric)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_asset_profile');
drop foreign table if exists sm_account_asset_profile_group_rel;	create FOREIGN TABLE sm_account_asset_profile_group_rel("profile_id" int4 NOT NULL
	,"group_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_asset_profile_group_rel');
drop foreign table if exists sm_account_asset_recompute_trigger;	create FOREIGN TABLE sm_account_asset_recompute_trigger("id" int4 NOT NULL
	,"reason" varchar NOT NULL
	,"company_id" int4 NOT NULL
	,"date_trigger" timestamp
	,"date_completed" timestamp
	,"state" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_asset_recompute_trigger');
drop foreign table if exists sm_account_asset_remove;	create FOREIGN TABLE sm_account_asset_remove("id" int4 NOT NULL
	,"date_remove" date NOT NULL
	,"force_date" date
	,"sale_value" float8
	,"account_sale_id" int4
	,"account_plus_value_id" int4
	,"account_min_value_id" int4
	,"account_residual_value_id" int4
	,"posting_regime" varchar NOT NULL
	,"note" text
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_asset_remove');
drop foreign table if exists sm_account_asset_report;	create FOREIGN TABLE sm_account_asset_report("id" int4
	,"depreciation_date" date
	,"date" date
	,"gross_value" float8
	,"depreciation_value" numeric
	,"posted_value" numeric
	,"unposted_value" numeric
	,"asset_id" int4
	,"move_check" bool
	,"asset_profile_id" int4
	,"partner_id" int4
	,"state" varchar
	,"depreciation_count" int8
	,"company_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_asset_report');
drop foreign table if exists sm_account_balance_report;	create FOREIGN TABLE sm_account_balance_report("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"display_account" varchar NOT NULL
	,"date_from" date
	,"company_id" int4
	,"write_date" timestamp
	,"date_to" date
	,"write_uid" int4
	,"target_move" varchar NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_balance_report');
drop foreign table if exists sm_account_balance_report_journal_rel;	create FOREIGN TABLE sm_account_balance_report_journal_rel("account_id" int4 NOT NULL
	,"journal_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_balance_report_journal_rel');
drop foreign table if exists sm_account_bank_accounts_wizard;	create FOREIGN TABLE sm_account_bank_accounts_wizard("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"bank_account_id" int4 NOT NULL
	,"acc_name" varchar NOT NULL
	,"write_uid" int4
	,"currency_id" int4
	,"write_date" timestamp
	,"account_type" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_bank_accounts_wizard');
drop foreign table if exists sm_account_bank_statement;	create FOREIGN TABLE sm_account_bank_statement("id" int4 NOT NULL
	,"create_date" timestamp
	,"date_done" timestamp
	,"balance_start" numeric
	,"company_id" int4
	,"write_uid" int4
	,"create_uid" int4
	,"user_id" int4
	,"message_last_post" timestamp
	,"journal_id" int4 NOT NULL
	,"state" varchar NOT NULL
	,"cashbox_start_id" int4
	,"cashbox_end_id" int4
	,"total_entry_encoding" numeric
	,"write_date" timestamp
	,"date" date NOT NULL
	,"difference" numeric
	,"name" varchar
	,"balance_end" numeric
	,"balance_end_real" numeric
	,"reference" varchar
	,"message_main_attachment_id" int4
	,"accounting_date" date)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_bank_statement');
drop foreign table if exists sm_account_bank_statement_cashbox;	create FOREIGN TABLE sm_account_bank_statement_cashbox("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_bank_statement_cashbox');
drop foreign table if exists sm_account_bank_statement_closebalance;	create FOREIGN TABLE sm_account_bank_statement_closebalance("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_date" timestamp
	,"write_uid" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_bank_statement_closebalance');
drop foreign table if exists sm_account_bank_statement_import;	create FOREIGN TABLE sm_account_bank_statement_import("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"data_file" bytea NOT NULL
	,"write_uid" int4
	,"write_date" timestamp
	,"filename" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_bank_statement_import');
drop foreign table if exists sm_account_bank_statement_import_journal_creation;	create FOREIGN TABLE sm_account_bank_statement_import_journal_creation("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"journal_id" int4 NOT NULL
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_bank_statement_import_journal_creation');
drop foreign table if exists sm_account_bank_statement_line;	create FOREIGN TABLE sm_account_bank_statement_line("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"statement_id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"sequence" int4
	,"partner_name" varchar
	,"ref" varchar
	,"currency_id" int4
	,"company_id" int4
	,"write_uid" int4
	,"note" text
	,"date" date NOT NULL
	,"amount" numeric
	,"write_date" timestamp
	,"journal_id" int4
	,"amount_currency" numeric
	,"partner_id" int4
	,"bank_account_id" int4
	,"account_id" int4
	,"unique_import_id" varchar
	,"move_name" varchar
	,"account_number" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_bank_statement_line');
drop foreign table if exists sm_account_banking_mandate;	create FOREIGN TABLE sm_account_banking_mandate("id" int4 NOT NULL
	,"format" varchar NOT NULL
	,"type" varchar
	,"partner_bank_id" int4
	,"partner_id" int4
	,"company_id" int4 NOT NULL
	,"unique_mandate_reference" varchar
	,"signature_date" date
	,"last_debit_date" date
	,"state" varchar
	,"message_last_post" timestamp
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"recurrent_sequence_type" varchar
	,"scheme" varchar
	,"display_name" varchar
	,"message_main_attachment_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_banking_mandate');
drop foreign table if exists sm_account_cash_rounding;	create FOREIGN TABLE sm_account_cash_rounding("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"rounding" float8 NOT NULL
	,"strategy" varchar NOT NULL
	,"account_id" int4
	,"rounding_method" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_cash_rounding');
drop foreign table if exists sm_account_cashbox_line;	create FOREIGN TABLE sm_account_cashbox_line("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"number" int4
	,"cashbox_id" int4
	,"write_uid" int4
	,"write_date" timestamp
	,"coin_value" numeric NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_cashbox_line');
drop foreign table if exists sm_account_chart_template;	create FOREIGN TABLE sm_account_chart_template("id" int4 NOT NULL
	,"bank_account_code_prefix" varchar NOT NULL
	,"cash_account_code_prefix" varchar NOT NULL
	,"create_date" timestamp
	,"write_uid" int4
	,"currency_id" int4 NOT NULL
	,"visible" bool
	,"property_account_receivable_id" int4
	,"property_stock_valuation_account_id" int4
	,"create_uid" int4
	,"complete_tax_set" bool
	,"property_stock_account_output_categ_id" int4
	,"openupgrade_legacy_12_0_transfer_account_id" int4
	,"openupgrade_legacy_12_0_company_id" int4
	,"expense_currency_exchange_account_id" int4
	,"parent_id" int4
	,"property_account_income_categ_id" int4
	,"property_stock_account_input_categ_id" int4
	,"property_account_income_id" int4
	,"property_account_expense_categ_id" int4
	,"write_date" timestamp
	,"use_anglo_saxon" bool
	,"code_digits" int4 NOT NULL
	,"name" varchar NOT NULL
	,"property_account_expense_id" int4
	,"property_account_payable_id" int4
	,"income_currency_exchange_account_id" int4
	,"transfer_account_code_prefix" varchar NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_chart_template');
drop foreign table if exists sm_account_common_account_report;	create FOREIGN TABLE sm_account_common_account_report("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"display_account" varchar NOT NULL
	,"date_from" date
	,"company_id" int4
	,"write_date" timestamp
	,"date_to" date
	,"write_uid" int4
	,"target_move" varchar NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_common_account_report');
drop foreign table if exists sm_account_common_account_report_account_journal_rel;	create FOREIGN TABLE sm_account_common_account_report_account_journal_rel("account_common_account_report_id" int4 NOT NULL
	,"account_journal_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_common_account_report_account_journal_rel');
drop foreign table if exists sm_account_common_journal_report;	create FOREIGN TABLE sm_account_common_journal_report("id" int4 NOT NULL
	,"create_uid" int4
	,"date_from" date
	,"company_id" int4 NOT NULL
	,"create_date" timestamp
	,"write_date" timestamp
	,"date_to" date
	,"amount_currency" bool
	,"write_uid" int4
	,"target_move" varchar NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_common_journal_report');
drop foreign table if exists sm_account_common_journal_report_account_journal_rel;	create FOREIGN TABLE sm_account_common_journal_report_account_journal_rel("account_common_journal_report_id" int4 NOT NULL
	,"account_journal_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_common_journal_report_account_journal_rel');
drop foreign table if exists sm_account_common_partner_report;	create FOREIGN TABLE sm_account_common_partner_report("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"date_from" date
	,"result_selection" varchar NOT NULL
	,"company_id" int4
	,"write_date" timestamp
	,"date_to" date
	,"write_uid" int4
	,"target_move" varchar NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_common_partner_report');
drop foreign table if exists sm_account_common_partner_report_account_journal_rel;	create FOREIGN TABLE sm_account_common_partner_report_account_journal_rel("account_common_partner_report_id" int4 NOT NULL
	,"account_journal_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_common_partner_report_account_journal_rel');
drop foreign table if exists sm_account_common_report;	create FOREIGN TABLE sm_account_common_report("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"date_from" date
	,"company_id" int4 NOT NULL
	,"write_date" timestamp
	,"date_to" date
	,"write_uid" int4
	,"target_move" varchar NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_common_report');
drop foreign table if exists sm_account_common_report_account_journal_rel;	create FOREIGN TABLE sm_account_common_report_account_journal_rel("account_common_report_id" int4 NOT NULL
	,"account_journal_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_common_report_account_journal_rel');
drop foreign table if exists sm_account_config_settings;	create FOREIGN TABLE sm_account_config_settings("id" int4 NOT NULL
	,"create_date" timestamp
	,"module_account_asset" bool
	,"write_uid" int4
	,"module_account_accountant" bool
	,"has_chart_of_accounts" bool
	,"create_uid" int4
	,"complete_tax_set" bool
	,"template_transfer_account_id" int4
	,"module_account_bank_statement_import_qif" bool
	,"module_account_budget" bool
	,"company_id" int4 NOT NULL
	,"group_multi_currency" bool
	,"group_proforma_invoices" bool
	,"module_account_plaid" bool
	,"has_default_company" bool
	,"purchase_tax_rate" float8
	,"module_account_bank_statement_import_ofx" bool
	,"default_purchase_tax_id" int4
	,"group_analytic_accounting" bool
	,"write_date" timestamp
	,"sale_tax_rate" float8
	,"module_account_batch_deposit" bool
	,"module_account_yodlee" bool
	,"module_account_tax_cash_basis" bool
	,"chart_template_id" int4
	,"default_sale_tax_id" int4
	,"sale_tax_id" int4
	,"module_account_sepa" bool
	,"module_account_reports" bool
	,"module_l10n_us_check_printing" bool
	,"module_account_reports_followup" bool
	,"purchase_tax_id" int4
	,"module_payment_paypal" bool
	,"module_payment_buckaroo" bool
	,"module_payment_adyen" bool
	,"module_payment_ogone" bool
	,"module_payment_transfer" bool
	,"group_analytic_account_for_sales" bool
	,"module_account_bank_statement_import_csv" bool
	,"group_warning_account" int4
	,"module_account_deferred_revenue" bool
	,"group_analytic_account_for_purchases" bool)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_config_settings');
drop foreign table if exists sm_account_financial_report;	create FOREIGN TABLE sm_account_financial_report("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"name" varchar NOT NULL
	,"level" int4
	,"sequence" int4
	,"style_overwrite" int4
	,"sign" int4 NOT NULL
	,"parent_id" int4
	,"write_date" timestamp
	,"account_report_id" int4
	,"display_detail" varchar
	,"write_uid" int4
	,"type" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_financial_report');
drop foreign table if exists sm_account_financial_year_op;	create FOREIGN TABLE sm_account_financial_year_op("id" int4 NOT NULL
	,"company_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_financial_year_op');
drop foreign table if exists sm_account_fiscal_position;	create FOREIGN TABLE sm_account_fiscal_position("id" int4 NOT NULL
	,"create_uid" int4
	,"country_group_id" int4
	,"create_date" timestamp
	,"zip_to" int4
	,"name" varchar NOT NULL
	,"sequence" int4
	,"country_id" int4
	,"company_id" int4
	,"auto_apply" bool
	,"write_uid" int4
	,"note" text
	,"zip_from" int4
	,"write_date" timestamp
	,"vat_required" bool
	,"active" bool)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_fiscal_position');
drop foreign table if exists sm_account_fiscal_position_account;	create FOREIGN TABLE sm_account_fiscal_position_account("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"position_id" int4 NOT NULL
	,"write_uid" int4
	,"account_dest_id" int4 NOT NULL
	,"write_date" timestamp
	,"account_src_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_fiscal_position_account');
drop foreign table if exists sm_account_fiscal_position_account_template;	create FOREIGN TABLE sm_account_fiscal_position_account_template("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"position_id" int4 NOT NULL
	,"write_uid" int4
	,"account_dest_id" int4 NOT NULL
	,"write_date" timestamp
	,"account_src_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_fiscal_position_account_template');
drop foreign table if exists sm_account_fiscal_position_res_country_state_rel;	create FOREIGN TABLE sm_account_fiscal_position_res_country_state_rel("account_fiscal_position_id" int4 NOT NULL
	,"res_country_state_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_fiscal_position_res_country_state_rel');
drop foreign table if exists sm_account_fiscal_position_tax;	create FOREIGN TABLE sm_account_fiscal_position_tax("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"position_id" int4 NOT NULL
	,"tax_src_id" int4 NOT NULL
	,"write_uid" int4
	,"tax_dest_id" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_fiscal_position_tax');
drop foreign table if exists sm_account_fiscal_position_tax_template;	create FOREIGN TABLE sm_account_fiscal_position_tax_template("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"position_id" int4 NOT NULL
	,"tax_src_id" int4 NOT NULL
	,"write_uid" int4
	,"tax_dest_id" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_fiscal_position_tax_template');
drop foreign table if exists sm_account_fiscal_position_template;	create FOREIGN TABLE sm_account_fiscal_position_template("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"name" varchar NOT NULL
	,"chart_template_id" int4 NOT NULL
	,"write_uid" int4
	,"note" text
	,"write_date" timestamp
	,"sequence" int4
	,"auto_apply" bool
	,"vat_required" bool
	,"country_id" int4
	,"country_group_id" int4
	,"zip_from" int4
	,"zip_to" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_fiscal_position_template');
drop foreign table if exists sm_account_fiscal_position_template_res_country_state_rel;	create FOREIGN TABLE sm_account_fiscal_position_template_res_country_state_rel("account_fiscal_position_template_id" int4 NOT NULL
	,"res_country_state_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_fiscal_position_template_res_country_state_rel');
drop foreign table if exists sm_account_fiscal_year;	create FOREIGN TABLE sm_account_fiscal_year("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"date_from" date NOT NULL
	,"date_to" date NOT NULL
	,"company_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_fiscal_year');
drop foreign table if exists sm_account_full_reconcile;	create FOREIGN TABLE sm_account_full_reconcile("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"name" varchar NOT NULL
	,"write_uid" int4
	,"write_date" timestamp
	,"exchange_move_id" int4
	,"exchange_partial_rec_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_full_reconcile');
drop foreign table if exists sm_account_group;	create FOREIGN TABLE sm_account_group("id" int4 NOT NULL
	,"parent_left" int4
	,"parent_right" int4
	,"parent_id" int4
	,"name" varchar NOT NULL
	,"code_prefix" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"level" int4
	,"parent_path" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_group');
drop foreign table if exists sm_account_incoterms;	create FOREIGN TABLE sm_account_incoterms("id" int4 NOT NULL
	,"create_uid" int4
	,"code" varchar(3) NOT NULL
	,"name" varchar NOT NULL
	,"write_uid" int4
	,"write_date" timestamp
	,"active" bool
	,"create_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_incoterms');
drop foreign table if exists sm_account_invoice;	create FOREIGN TABLE sm_account_invoice("id" int4 NOT NULL
	,"comment" text
	,"date_due" date
	,"create_date" timestamp
	,"partner_bank_id" int4
	,"reference_type" varchar NOT NULL
	,"number" varchar
	,"journal_id" int4 NOT NULL
	,"currency_id" int4 NOT NULL
	,"amount_total_company_signed" numeric
	,"partner_id" int4
	,"create_uid" int4
	,"amount_untaxed" numeric
	,"reference" varchar
	,"residual_company_signed" numeric
	,"amount_total_signed" numeric
	,"message_last_post" timestamp
	,"company_id" int4 NOT NULL
	,"amount_tax" numeric
	,"state" varchar
	,"fiscal_position_id" int4
	,"type" varchar
	,"sent" bool
	,"account_id" int4
	,"reconciled" bool
	,"origin" varchar
	,"residual" numeric
	,"move_name" varchar
	,"date_invoice" date
	,"payment_term_id" int4
	,"write_date" timestamp
	,"residual_signed" numeric
	,"date" date
	,"user_id" int4
	,"write_uid" int4
	,"move_id" int4
	,"amount_total" numeric
	,"amount_untaxed_signed" numeric
	,"name" varchar
	,"commercial_partner_id" int4
	,"invoice_email_sent" bool
	,"team_id" int4
	,"amount_total_no_discount" numeric
	,"previous_pocketbook" numeric
	,"hide_payment_info" bool
	,"refund_invoice_id" int4
	,"partner_shipping_id" int4
	,"purchase_id" int4
	,"incoterms_id" int4
	,"openupgrade_legacy_11_0_state" varchar
	,"access_token" varchar
	,"cash_rounding_id" int4
	,"openupgrade_legacy_12_0_activity_date_deadline" date
	,"campaign_id" int4
	,"source_id" int4
	,"medium_id" int4
	,"payment_mode_id" int4
	,"mandate_id" int4
	,"invoice_number" varchar
	,"old_contract_id" int4
	,"eu_triangular_deal" bool
	,"invoice_report_id" int4
	,"invoice_template" varchar
	,"not_in_mod347" bool
	,"openupgrade_legacy_12_0_reference_type" varchar
	,"message_main_attachment_id" int4
	,"vendor_bill_id" int4
	,"incoterm_id" int4
	,"source_email" varchar
	,"vendor_display_name" varchar
	,"vendor_bill_purchase_id" int4
	,"subscription_request" int4
	,"release_capital_request" bool
	,"_api_external_id" int4
	,"external_id_sequence_id" int4
	,"agreement_id" int4
	,"returned_payment" bool)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_invoice');
drop foreign table if exists sm_account_invoice_account_invoice_send_rel;	create FOREIGN TABLE sm_account_invoice_account_invoice_send_rel("account_invoice_send_id" int4 NOT NULL
	,"account_invoice_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_invoice_account_invoice_send_rel');
drop foreign table if exists sm_account_invoice_account_move_line_rel;	create FOREIGN TABLE sm_account_invoice_account_move_line_rel("account_invoice_id" int4 NOT NULL
	,"account_move_line_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_invoice_account_move_line_rel');
drop foreign table if exists sm_account_invoice_account_register_payments_rel;	create FOREIGN TABLE sm_account_invoice_account_register_payments_rel("account_register_payments_id" int4 NOT NULL
	,"account_invoice_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_invoice_account_register_payments_rel');
drop foreign table if exists sm_account_invoice_cancel;	create FOREIGN TABLE sm_account_invoice_cancel("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_date" timestamp
	,"write_uid" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_invoice_cancel');
drop foreign table if exists sm_account_invoice_confirm;	create FOREIGN TABLE sm_account_invoice_confirm("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_date" timestamp
	,"write_uid" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_invoice_confirm');
drop foreign table if exists sm_account_invoice_import;	create FOREIGN TABLE sm_account_invoice_import("id" int4 NOT NULL
	,"invoice_file" bytea NOT NULL
	,"invoice_filename" varchar
	,"state" varchar
	,"partner_id" int4
	,"import_config_id" int4
	,"currency_id" int4
	,"invoice_type" varchar
	,"amount_untaxed" numeric
	,"amount_total" numeric
	,"invoice_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_invoice_import');
drop foreign table if exists sm_account_invoice_import_config;	create FOREIGN TABLE sm_account_invoice_import_config("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"partner_id" int4
	,"active" bool
	,"sequence" int4
	,"invoice_line_method" varchar NOT NULL
	,"company_id" int4 NOT NULL
	,"account_id" int4
	,"account_analytic_id" int4
	,"label" varchar
	,"static_product_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_invoice_import_config');
drop foreign table if exists sm_account_invoice_import_config_account_tax_rel;	create FOREIGN TABLE sm_account_invoice_import_config_account_tax_rel("account_invoice_import_config_id" int4 NOT NULL
	,"account_tax_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_invoice_import_config_account_tax_rel');
drop foreign table if exists sm_account_invoice_import_wizard;	create FOREIGN TABLE sm_account_invoice_import_wizard("id" int4 NOT NULL
	,"journal_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_invoice_import_wizard');
drop foreign table if exists sm_account_invoice_import_wizard_ir_attachment_rel;	create FOREIGN TABLE sm_account_invoice_import_wizard_ir_attachment_rel("account_invoice_import_wizard_id" int4 NOT NULL
	,"ir_attachment_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_invoice_import_wizard_ir_attachment_rel');
drop foreign table if exists sm_account_invoice_line;	create FOREIGN TABLE sm_account_invoice_line("id" int4 NOT NULL
	,"origin" varchar
	,"create_date" timestamp
	,"sequence" int4
	,"price_unit" numeric NOT NULL
	,"price_subtotal" numeric
	,"write_uid" int4
	,"currency_id" int4
	,"uom_id" int4
	,"partner_id" int4
	,"create_uid" int4
	,"company_id" int4
	,"account_analytic_id" int4
	,"account_id" int4
	,"discount" numeric
	,"write_date" timestamp
	,"price_subtotal_signed" numeric
	,"product_id" int4
	,"name" text NOT NULL
	,"invoice_id" int4
	,"quantity" numeric NOT NULL
	,"related_tariff_id" int4
	,"related_reservation_compute_id" int4
	,"line_type" varchar
	,"invoice_report_id" int4
	,"line_tariff_type" varchar
	,"layout_category_sequence" int4
	,"layout_category_id" int4
	,"purchase_line_id" int4
	,"price_total" numeric
	,"is_rounding_line" bool
	,"asset_profile_id" int4
	,"asset_start_date" date
	,"asset_end_date" date
	,"asset_mrr" numeric
	,"initial_price" float8
	,"display_type" varchar
	,"asset_id" int4
	,"contract_line_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_invoice_line');
drop foreign table if exists sm_account_invoice_line_tax;	create FOREIGN TABLE sm_account_invoice_line_tax("invoice_line_id" int4 NOT NULL
	,"tax_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_invoice_line_tax');
drop foreign table if exists sm_account_invoice_payment_line_multi;	create FOREIGN TABLE sm_account_invoice_payment_line_multi("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_invoice_payment_line_multi');
drop foreign table if exists sm_account_invoice_payment_rel;	create FOREIGN TABLE sm_account_invoice_payment_rel("payment_id" int4 NOT NULL
	,"invoice_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_invoice_payment_rel');
drop foreign table if exists sm_account_invoice_purchase_order_rel;	create FOREIGN TABLE sm_account_invoice_purchase_order_rel("purchase_order_id" int4 NOT NULL
	,"account_invoice_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_invoice_purchase_order_rel');
drop foreign table if exists sm_account_invoice_refund;	create FOREIGN TABLE sm_account_invoice_refund("id" int4 NOT NULL
	,"create_uid" int4
	,"filter_refund" varchar NOT NULL
	,"create_date" timestamp
	,"description" varchar NOT NULL
	,"write_uid" int4
	,"date_invoice" date NOT NULL
	,"write_date" timestamp
	,"date" date)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_invoice_refund');
drop foreign table if exists sm_account_invoice_report;	create FOREIGN TABLE sm_account_invoice_report("id" int4
	,"number" varchar
	,"date" date
	,"product_id" int4
	,"partner_id" int4
	,"country_id" int4
	,"account_analytic_id" int4
	,"payment_term_id" int4
	,"uom_name" varchar
	,"currency_id" int4
	,"journal_id" int4
	,"fiscal_position_id" int4
	,"user_id" int4
	,"company_id" int4
	,"nbr" int4
	,"invoice_id" int4
	,"type" varchar
	,"state" varchar
	,"categ_id" int4
	,"date_due" date
	,"account_id" int4
	,"account_line_id" int4
	,"partner_bank_id" int4
	,"product_qty" numeric
	,"price_total" numeric
	,"price_average" numeric
	,"amount_total" numeric
	,"currency_rate" numeric
	,"residual" numeric
	,"commercial_partner_id" int4
	,"team_id" int4
	,"release_capital_request" bool)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_invoice_report');
drop foreign table if exists sm_account_invoice_send;	create FOREIGN TABLE sm_account_invoice_send("id" int4 NOT NULL
	,"is_email" bool
	,"is_print" bool
	,"printed" bool
	,"composer_id" int4 NOT NULL
	,"template_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_invoice_send');
drop foreign table if exists sm_account_invoice_tax;	create FOREIGN TABLE sm_account_invoice_tax("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"account_id" int4 NOT NULL
	,"sequence" int4
	,"invoice_id" int4
	,"manual" bool
	,"company_id" int4
	,"write_uid" int4
	,"currency_id" int4
	,"amount" numeric
	,"write_date" timestamp
	,"account_analytic_id" int4
	,"tax_id" int4
	,"name" varchar NOT NULL
	,"amount_rounding" numeric
	,"base" numeric)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_invoice_tax');
drop foreign table if exists sm_account_invoice_transaction_rel;	create FOREIGN TABLE sm_account_invoice_transaction_rel("transaction_id" int4 NOT NULL
	,"invoice_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_invoice_transaction_rel');
drop foreign table if exists sm_account_journal;	create FOREIGN TABLE sm_account_journal("id" int4 NOT NULL
	,"code" varchar(5) NOT NULL
	,"create_date" timestamp
	,"sequence" int4
	,"write_uid" int4
	,"currency_id" int4
	,"at_least_one_inbound" bool
	,"bank_statements_source" varchar
	,"bank_account_id" int4
	,"create_uid" int4
	,"group_invoice_lines" bool
	,"company_id" int4 NOT NULL
	,"profit_account_id" int4
	,"display_on_footer" bool
	,"type" varchar NOT NULL
	,"default_debit_account_id" int4
	,"show_on_dashboard" bool
	,"default_credit_account_id" int4
	,"sequence_id" int4 NOT NULL
	,"write_date" timestamp
	,"refund_sequence_id" int4
	,"loss_account_id" int4
	,"update_posted" bool
	,"name" varchar NOT NULL
	,"at_least_one_outbound" bool
	,"refund_sequence" bool
	,"active" bool
	,"color" int4
	,"n43_date_type" varchar
	,"invoice_sequence_id" int4
	,"refund_inv_sequence_id" int4
	,"openupgrade_legacy_12_0_bank_statements_source" varchar
	,"post_at_bank_rec" bool
	,"alias_id" int4
	,"inbound_payment_order_only" bool
	,"outbound_payment_order_only" bool
	,"get_cooperator_payment" bool
	,"get_general_payment" bool
	,"_api_external_id" int4
	,"external_id_sequence_id" int4
	,"default_expense_account_id" int4
	,"default_expense_partner_id" int4
	,"return_auto_reconcile" bool)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_journal');
drop foreign table if exists sm_account_journal_account_payment_line_create_rel;	create FOREIGN TABLE sm_account_journal_account_payment_line_create_rel("account_payment_line_create_id" int4 NOT NULL
	,"account_journal_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_journal_account_payment_line_create_rel');
drop foreign table if exists sm_account_journal_account_payment_mode_rel;	create FOREIGN TABLE sm_account_journal_account_payment_mode_rel("account_payment_mode_id" int4 NOT NULL
	,"account_journal_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_journal_account_payment_mode_rel');
drop foreign table if exists sm_account_journal_account_print_journal_rel;	create FOREIGN TABLE sm_account_journal_account_print_journal_rel("account_print_journal_id" int4 NOT NULL
	,"account_journal_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_journal_account_print_journal_rel');
drop foreign table if exists sm_account_journal_account_reconcile_model_rel;	create FOREIGN TABLE sm_account_journal_account_reconcile_model_rel("account_reconcile_model_id" int4 NOT NULL
	,"account_journal_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_journal_account_reconcile_model_rel');
drop foreign table if exists sm_account_journal_account_reconcile_model_template_rel;	create FOREIGN TABLE sm_account_journal_account_reconcile_model_template_rel("account_reconcile_model_template_id" int4 NOT NULL
	,"account_journal_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_journal_account_reconcile_model_template_rel');
drop foreign table if exists sm_account_journal_account_report_partner_ledger_rel;	create FOREIGN TABLE sm_account_journal_account_report_partner_ledger_rel("account_report_partner_ledger_id" int4 NOT NULL
	,"account_journal_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_journal_account_report_partner_ledger_rel');
drop foreign table if exists sm_account_journal_account_tax_report_rel;	create FOREIGN TABLE sm_account_journal_account_tax_report_rel("account_tax_report_id" int4 NOT NULL
	,"account_journal_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_journal_account_tax_report_rel');
drop foreign table if exists sm_account_journal_accounting_report_rel;	create FOREIGN TABLE sm_account_journal_accounting_report_rel("accounting_report_id" int4 NOT NULL
	,"account_journal_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_journal_accounting_report_rel');
drop foreign table if exists sm_account_journal_general_ledger_report_wizard_rel;	create FOREIGN TABLE sm_account_journal_general_ledger_report_wizard_rel("general_ledger_report_wizard_id" int4 NOT NULL
	,"account_journal_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_journal_general_ledger_report_wizard_rel');
drop foreign table if exists sm_account_journal_inbound_payment_method_rel;	create FOREIGN TABLE sm_account_journal_inbound_payment_method_rel("journal_id" int4 NOT NULL
	,"inbound_payment_method" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_journal_inbound_payment_method_rel');
drop foreign table if exists sm_account_journal_journal_ledger_report_wizard_rel;	create FOREIGN TABLE sm_account_journal_journal_ledger_report_wizard_rel("journal_ledger_report_wizard_id" int4 NOT NULL
	,"account_journal_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_journal_journal_ledger_report_wizard_rel');
drop foreign table if exists sm_account_journal_outbound_payment_method_rel;	create FOREIGN TABLE sm_account_journal_outbound_payment_method_rel("journal_id" int4 NOT NULL
	,"outbound_payment_method" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_journal_outbound_payment_method_rel');
drop foreign table if exists sm_account_journal_report_general_ledger_rel;	create FOREIGN TABLE sm_account_journal_report_general_ledger_rel("report_general_ledger_id" int4 NOT NULL
	,"account_journal_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_journal_report_general_ledger_rel');
drop foreign table if exists sm_account_journal_report_journal_ledger_rel;	create FOREIGN TABLE sm_account_journal_report_journal_ledger_rel("report_journal_ledger_id" int4 NOT NULL
	,"account_journal_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_journal_report_journal_ledger_rel');
drop foreign table if exists sm_account_journal_report_trial_balance_rel;	create FOREIGN TABLE sm_account_journal_report_trial_balance_rel("report_trial_balance_id" int4 NOT NULL
	,"account_journal_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_journal_report_trial_balance_rel');
drop foreign table if exists sm_account_journal_trial_balance_report_wizard_rel;	create FOREIGN TABLE sm_account_journal_trial_balance_report_wizard_rel("trial_balance_report_wizard_id" int4 NOT NULL
	,"account_journal_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_journal_trial_balance_report_wizard_rel');
drop foreign table if exists sm_account_journal_type_rel;	create FOREIGN TABLE sm_account_journal_type_rel("journal_id" int4 NOT NULL
	,"type_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_journal_type_rel');
drop foreign table if exists sm_account_move;	create FOREIGN TABLE sm_account_move("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"statement_line_id" int4
	,"ref" varchar
	,"company_id" int4
	,"write_uid" int4
	,"currency_id" int4
	,"journal_id" int4 NOT NULL
	,"state" varchar NOT NULL
	,"rate_diff_partial_rec_id" int4
	,"matched_percentage" numeric
	,"write_date" timestamp
	,"narration" text
	,"date" date NOT NULL
	,"amount" numeric
	,"partner_id" int4
	,"name" varchar NOT NULL
	,"tax_cash_basis_rec_id" int4
	,"stock_move_id" int4
	,"payment_order_id" int4
	,"move_type" varchar
	,"not_in_mod347" bool
	,"auto_reverse" bool
	,"reverse_date" date
	,"reverse_entry_id" int4
	,"message_main_attachment_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_move');
drop foreign table if exists sm_account_move_line;	create FOREIGN TABLE sm_account_move_line("id" int4 NOT NULL
	,"create_date" timestamp
	,"statement_id" int4
	,"account_id" int4 NOT NULL
	,"company_id" int4
	,"currency_id" int4
	,"date_maturity" date NOT NULL
	,"user_type_id" int4
	,"partner_id" int4
	,"blocked" bool
	,"analytic_account_id" int4
	,"create_uid" int4
	,"credit" numeric
	,"journal_id" int4
	,"amount_residual_currency" numeric
	,"debit" numeric
	,"ref" varchar
	,"debit_cash_basis" numeric
	,"reconciled" bool
	,"balance_cash_basis" numeric
	,"write_date" timestamp
	,"date" date
	,"write_uid" int4
	,"move_id" int4 NOT NULL
	,"name" varchar
	,"payment_id" int4
	,"company_currency_id" int4
	,"balance" numeric
	,"product_id" int4
	,"invoice_id" int4
	,"tax_line_id" int4
	,"amount_residual" numeric
	,"product_uom_id" int4
	,"amount_currency" numeric
	,"credit_cash_basis" numeric
	,"quantity" numeric
	,"full_reconcile_id" int4
	,"tax_exigible" bool
	,"tax_base_amount" numeric
	,"statement_line_id" int4
	,"payment_mode_id" int4
	,"partner_bank_id" int4
	,"bank_payment_line_id" int4
	,"mandate_id" int4
	,"stored_invoice_id" int4
	,"invoice_user_id" int4
	,"l10n_es_aeat_349_operation_key" varchar
	,"expense_id" int4
	,"asset_profile_id" int4
	,"asset_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_move_line');
drop foreign table if exists sm_account_move_line_account_payment_line_create_rel;	create FOREIGN TABLE sm_account_move_line_account_payment_line_create_rel("account_payment_line_create_id" int4 NOT NULL
	,"account_move_line_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_move_line_account_payment_line_create_rel');
drop foreign table if exists sm_account_move_line_account_tax_rel;	create FOREIGN TABLE sm_account_move_line_account_tax_rel("account_move_line_id" int4 NOT NULL
	,"account_tax_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_move_line_account_tax_rel');
drop foreign table if exists sm_account_move_line_l10n_es_aeat_mod296_report_line_rel;	create FOREIGN TABLE sm_account_move_line_l10n_es_aeat_mod296_report_line_rel("l10n_es_aeat_mod296_report_line_id" int4 NOT NULL
	,"account_move_line_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_move_line_l10n_es_aeat_mod296_report_line_rel');
drop foreign table if exists sm_account_move_line_l10n_es_aeat_mod347_partner_record_rel;	create FOREIGN TABLE sm_account_move_line_l10n_es_aeat_mod347_partner_record_rel("l10n_es_aeat_mod347_partner_record_id" int4 NOT NULL
	,"account_move_line_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_move_line_l10n_es_aeat_mod347_partner_record_rel');
drop foreign table if exists sm_account_move_line_l10n_es_aeat_tax_line_rel;	create FOREIGN TABLE sm_account_move_line_l10n_es_aeat_tax_line_rel("l10n_es_aeat_tax_line_id" int4 NOT NULL
	,"account_move_line_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_move_line_l10n_es_aeat_tax_line_rel');
drop foreign table if exists sm_account_move_line_l10n_es_vat_book_line_tax_rel;	create FOREIGN TABLE sm_account_move_line_l10n_es_vat_book_line_tax_rel("l10n_es_vat_book_line_tax_id" int4 NOT NULL
	,"account_move_line_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_move_line_l10n_es_vat_book_line_tax_rel');
drop foreign table if exists sm_account_move_line_payment_return_line_rel;	create FOREIGN TABLE sm_account_move_line_payment_return_line_rel("payment_return_line_id" int4 NOT NULL
	,"account_move_line_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_move_line_payment_return_line_rel');
drop foreign table if exists sm_account_move_line_reconcile;	create FOREIGN TABLE sm_account_move_line_reconcile("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"writeoff" numeric
	,"company_id" int4 NOT NULL
	,"write_uid" int4
	,"credit" numeric
	,"write_date" timestamp
	,"debit" numeric
	,"trans_nbr" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_move_line_reconcile');
drop foreign table if exists sm_account_move_line_reconcile_writeoff;	create FOREIGN TABLE sm_account_move_line_reconcile_writeoff("id" int4 NOT NULL
	,"comment" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"writeoff_acc_id" int4 NOT NULL
	,"journal_id" int4 NOT NULL
	,"analytic_id" int4
	,"write_date" timestamp
	,"date_p" date
	,"write_uid" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_move_line_reconcile_writeoff');
drop foreign table if exists sm_account_move_reversal;	create FOREIGN TABLE sm_account_move_reversal("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"journal_id" int4
	,"write_uid" int4
	,"write_date" timestamp
	,"date" date NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_move_reversal');
drop foreign table if exists sm_account_opening;	create FOREIGN TABLE sm_account_opening("id" int4 NOT NULL
	,"company_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_opening');
drop foreign table if exists sm_account_partial_reconcile;	create FOREIGN TABLE sm_account_partial_reconcile("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"credit_move_id" int4 NOT NULL
	,"company_id" int4
	,"write_uid" int4
	,"currency_id" int4
	,"amount" numeric
	,"debit_move_id" int4 NOT NULL
	,"write_date" timestamp
	,"amount_currency" numeric
	,"full_reconcile_id" int4
	,"max_date" date)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_partial_reconcile');
drop foreign table if exists sm_account_partial_reconcile_account_move_line_rel;	create FOREIGN TABLE sm_account_partial_reconcile_account_move_line_rel("partial_reconcile_id" int4 NOT NULL
	,"move_line_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_partial_reconcile_account_move_line_rel');
drop foreign table if exists sm_account_payment;	create FOREIGN TABLE sm_account_payment("id" int4 NOT NULL
	,"create_date" timestamp
	,"communication" varchar
	,"write_uid" int4
	,"currency_id" int4 NOT NULL
	,"partner_id" int4
	,"payment_method_id" int4 NOT NULL
	,"create_uid" int4
	,"payment_difference_handling" varchar
	,"company_id" int4
	,"state" varchar
	,"writeoff_account_id" int4
	,"payment_date" date NOT NULL
	,"partner_type" varchar
	,"write_date" timestamp
	,"name" varchar
	,"destination_journal_id" int4
	,"journal_id" int4 NOT NULL
	,"amount" numeric NOT NULL
	,"payment_type" varchar NOT NULL
	,"payment_reference" varchar
	,"move_name" varchar
	,"payment_transaction_id" int4
	,"payment_token_id" int4
	,"writeoff_label" varchar
	,"message_last_post" timestamp
	,"message_main_attachment_id" int4
	,"multi" bool
	,"partner_bank_account_id" int4
	,"_api_external_id" int4
	,"external_id_sequence_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_payment');
drop foreign table if exists sm_account_payment_line;	create FOREIGN TABLE sm_account_payment_line("id" int4 NOT NULL
	,"name" varchar
	,"order_id" int4
	,"company_id" int4
	,"company_currency_id" int4
	,"payment_type" varchar
	,"state" varchar
	,"move_line_id" int4
	,"currency_id" int4 NOT NULL
	,"amount_currency" numeric
	,"partner_id" int4 NOT NULL
	,"partner_bank_id" int4
	,"date" date
	,"communication" varchar NOT NULL
	,"communication_type" varchar NOT NULL
	,"bank_line_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"mandate_id" int4
	,"priority" varchar
	,"local_instrument" varchar
	,"category_purpose" varchar
	,"purpose" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_payment_line');
drop foreign table if exists sm_account_payment_line_create;	create FOREIGN TABLE sm_account_payment_line_create("id" int4 NOT NULL
	,"order_id" int4
	,"target_move" varchar
	,"allow_blocked" bool
	,"invoice" bool
	,"date_type" varchar NOT NULL
	,"due_date" date
	,"move_date" date
	,"payment_mode" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_payment_line_create');
drop foreign table if exists sm_account_payment_line_create_res_partner_rel;	create FOREIGN TABLE sm_account_payment_line_create_res_partner_rel("account_payment_line_create_id" int4 NOT NULL
	,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_payment_line_create_res_partner_rel');
drop foreign table if exists sm_account_payment_method;	create FOREIGN TABLE sm_account_payment_method("id" int4 NOT NULL
	,"create_uid" int4
	,"code" varchar NOT NULL
	,"create_date" timestamp
	,"name" varchar NOT NULL
	,"write_uid" int4
	,"payment_type" varchar NOT NULL
	,"write_date" timestamp
	,"active" bool
	,"bank_account_required" bool
	,"mandate_required" bool
	,"pain_version" varchar
	,"convert_to_ascii" bool
	,"payment_order_only" bool)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_payment_method');
drop foreign table if exists sm_account_payment_mode;	create FOREIGN TABLE sm_account_payment_mode("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"company_id" int4 NOT NULL
	,"bank_account_link" varchar NOT NULL
	,"fixed_journal_id" int4
	,"payment_method_id" int4 NOT NULL
	,"payment_type" varchar
	,"payment_method_code" varchar
	,"active" bool
	,"note" text
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"show_bank_account" varchar
	,"show_bank_account_from_journal" bool
	,"show_bank_account_chars" int4
	,"payment_order_ok" bool
	,"no_debit_before_maturity" bool
	,"default_payment_mode" varchar
	,"default_invoice" bool
	,"default_target_move" varchar
	,"default_date_type" varchar
	,"default_date_prefered" varchar
	,"group_lines" bool
	,"generate_move" bool
	,"offsetting_account" varchar
	,"transfer_account_id" int4
	,"transfer_journal_id" int4
	,"move_option" varchar
	,"post_move" bool
	,"initiating_party_issuer" varchar(35)
	,"initiating_party_identifier" varchar(35)
	,"initiating_party_scheme" varchar(35)
	,"sepa_creditor_identifier" varchar(35))SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_payment_mode');
drop foreign table if exists sm_account_payment_mode_variable_journal_rel;	create FOREIGN TABLE sm_account_payment_mode_variable_journal_rel("payment_mode_id" int4 NOT NULL
	,"journal_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_payment_mode_variable_journal_rel');
drop foreign table if exists sm_account_payment_order;	create FOREIGN TABLE sm_account_payment_order("id" int4 NOT NULL
	,"name" varchar
	,"payment_mode_id" int4 NOT NULL
	,"payment_type" varchar NOT NULL
	,"payment_method_id" int4
	,"company_id" int4
	,"company_currency_id" int4
	,"journal_id" int4
	,"state" varchar
	,"date_prefered" varchar NOT NULL
	,"date_scheduled" date
	,"date_generated" date
	,"date_uploaded" date
	,"date_done" date
	,"generated_user_id" int4
	,"total_company_currency" numeric
	,"description" varchar
	,"message_last_post" timestamp
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"charge_bearer" varchar
	,"batch_booking" bool
	,"message_main_attachment_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_payment_order');
drop foreign table if exists sm_account_payment_term;	create FOREIGN TABLE sm_account_payment_term("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"name" varchar NOT NULL
	,"company_id" int4 NOT NULL
	,"write_uid" int4
	,"note" text
	,"write_date" timestamp
	,"active" bool
	,"sequence" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_payment_term');
drop foreign table if exists sm_account_payment_term_line;	create FOREIGN TABLE sm_account_payment_term_line("id" int4 NOT NULL
	,"payment_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"option" varchar NOT NULL
	,"sequence" int4
	,"days" int4 NOT NULL
	,"value" varchar NOT NULL
	,"write_uid" int4
	,"write_date" timestamp
	,"value_amount" numeric
	,"openupgrade_legacy_12_0_option" varchar
	,"day_of_the_month" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_payment_term_line');
drop foreign table if exists sm_account_print_journal;	create FOREIGN TABLE sm_account_print_journal("id" int4 NOT NULL
	,"create_uid" int4
	,"sort_selection" varchar NOT NULL
	,"date_from" date
	,"company_id" int4 NOT NULL
	,"create_date" timestamp
	,"write_date" timestamp
	,"date_to" date
	,"amount_currency" bool
	,"write_uid" int4
	,"target_move" varchar NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_print_journal');
drop foreign table if exists sm_account_reconcile_model;	create FOREIGN TABLE sm_account_reconcile_model("id" int4 NOT NULL
	,"second_analytic_account_id" int4
	,"create_date" timestamp
	,"sequence" int4 NOT NULL
	,"second_amount_type" varchar NOT NULL
	,"second_journal_id" int4
	,"write_uid" int4
	,"analytic_account_id" int4
	,"create_uid" int4
	,"second_tax_id" int4
	,"has_second_line" bool
	,"journal_id" int4
	,"label" varchar
	,"second_label" varchar
	,"second_account_id" int4
	,"account_id" int4
	,"company_id" int4 NOT NULL
	,"write_date" timestamp
	,"tax_id" int4
	,"amount_type" varchar NOT NULL
	,"name" varchar NOT NULL
	,"amount" numeric NOT NULL
	,"second_amount" numeric NOT NULL
	,"rule_type" varchar NOT NULL
	,"auto_reconcile" bool
	,"match_nature" varchar NOT NULL
	,"match_amount" varchar
	,"match_amount_min" float8
	,"match_amount_max" float8
	,"match_label" varchar
	,"match_label_param" varchar
	,"match_same_currency" bool
	,"match_total_amount" bool
	,"match_total_amount_param" float8
	,"match_partner" bool
	,"force_tax_included" bool
	,"force_second_tax_included" bool)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_reconcile_model');
drop foreign table if exists sm_account_reconcile_model_res_partner_category_rel;	create FOREIGN TABLE sm_account_reconcile_model_res_partner_category_rel("account_reconcile_model_id" int4 NOT NULL
	,"res_partner_category_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_reconcile_model_res_partner_category_rel');
drop foreign table if exists sm_account_reconcile_model_res_partner_rel;	create FOREIGN TABLE sm_account_reconcile_model_res_partner_rel("account_reconcile_model_id" int4 NOT NULL
	,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_reconcile_model_res_partner_rel');
drop foreign table if exists sm_account_reconcile_model_template;	create FOREIGN TABLE sm_account_reconcile_model_template("id" int4 NOT NULL
	,"amount_type" varchar NOT NULL
	,"second_account_id" int4
	,"create_uid" int4
	,"account_id" int4
	,"second_tax_id" int4
	,"second_amount_type" varchar NOT NULL
	,"has_second_line" bool
	,"sequence" int4 NOT NULL
	,"label" varchar
	,"write_uid" int4
	,"amount" numeric NOT NULL
	,"write_date" timestamp
	,"second_label" varchar
	,"create_date" timestamp
	,"second_amount" numeric NOT NULL
	,"tax_id" int4
	,"name" varchar NOT NULL
	,"chart_template_id" int4 NOT NULL
	,"rule_type" varchar NOT NULL
	,"auto_reconcile" bool
	,"match_nature" varchar NOT NULL
	,"match_amount" varchar
	,"match_amount_min" float8
	,"match_amount_max" float8
	,"match_label" varchar
	,"match_label_param" varchar
	,"match_same_currency" bool
	,"match_total_amount" bool
	,"match_total_amount_param" float8
	,"match_partner" bool
	,"force_tax_included" bool
	,"force_second_tax_included" bool)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_reconcile_model_template');
drop foreign table if exists sm_account_reconcile_model_template_res_partner_category_rel;	create FOREIGN TABLE sm_account_reconcile_model_template_res_partner_category_rel("account_reconcile_model_template_id" int4 NOT NULL
	,"res_partner_category_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_reconcile_model_template_res_partner_category_rel');
drop foreign table if exists sm_account_reconcile_model_template_res_partner_rel;	create FOREIGN TABLE sm_account_reconcile_model_template_res_partner_rel("account_reconcile_model_template_id" int4 NOT NULL
	,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_reconcile_model_template_res_partner_rel');
drop foreign table if exists sm_account_register_payments;	create FOREIGN TABLE sm_account_register_payments("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"payment_date" date NOT NULL
	,"communication" varchar
	,"journal_id" int4 NOT NULL
	,"write_uid" int4
	,"currency_id" int4 NOT NULL
	,"partner_type" varchar
	,"amount" numeric NOT NULL
	,"payment_type" varchar NOT NULL
	,"write_date" timestamp
	,"partner_id" int4
	,"payment_method_id" int4 NOT NULL
	,"multi" bool
	,"group_invoices" bool
	,"payment_difference_handling" varchar
	,"writeoff_account_id" int4
	,"writeoff_label" varchar
	,"partner_bank_account_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_register_payments');
drop foreign table if exists sm_account_report_general_ledger;	create FOREIGN TABLE sm_account_report_general_ledger("id" int4 NOT NULL
	,"create_uid" int4
	,"initial_balance" bool
	,"create_date" timestamp
	,"display_account" varchar NOT NULL
	,"date_from" date
	,"company_id" int4
	,"sortby" varchar NOT NULL
	,"write_date" timestamp
	,"date_to" date
	,"write_uid" int4
	,"target_move" varchar NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_report_general_ledger');
drop foreign table if exists sm_account_report_general_ledger_journal_rel;	create FOREIGN TABLE sm_account_report_general_ledger_journal_rel("account_id" int4 NOT NULL
	,"journal_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_report_general_ledger_journal_rel');
drop foreign table if exists sm_account_report_partner_ledger;	create FOREIGN TABLE sm_account_report_partner_ledger("id" int4 NOT NULL
	,"create_uid" int4
	,"reconciled" bool
	,"result_selection" varchar NOT NULL
	,"company_id" int4
	,"create_date" timestamp
	,"write_date" timestamp
	,"date_to" date
	,"amount_currency" bool
	,"write_uid" int4
	,"date_from" date
	,"target_move" varchar NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_report_partner_ledger');
drop foreign table if exists sm_account_setup_bank_manual_config;	create FOREIGN TABLE sm_account_setup_bank_manual_config("id" int4 NOT NULL
	,"res_partner_bank_id" int4 NOT NULL
	,"create_or_link_option" varchar
	,"new_journal_code" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_setup_bank_manual_config');
drop foreign table if exists sm_account_tax;	create FOREIGN TABLE sm_account_tax("id" int4 NOT NULL
	,"amount_type" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"account_id" int4
	,"name" varchar NOT NULL
	,"sequence" int4 NOT NULL
	,"tax_group_id" int4 NOT NULL
	,"company_id" int4 NOT NULL
	,"type_tax_use" varchar NOT NULL
	,"write_uid" int4
	,"analytic" bool
	,"amount" numeric NOT NULL
	,"include_base_amount" bool
	,"write_date" timestamp
	,"price_include" bool
	,"active" bool
	,"refund_account_id" int4
	,"description" varchar
	,"openupgrade_legacy_12_0_tax_adjustment" bool
	,"tax_exigibility" varchar
	,"cash_basis_account_id" int4
	,"cash_basis_base_account_id" int4
	,"unece_type_id" int4
	,"unece_type_code" varchar
	,"unece_categ_id" int4
	,"unece_categ_code" varchar
	,"unece_due_date_id" int4
	,"unece_due_date_code" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_tax');
drop foreign table if exists sm_account_tax_account_tag;	create FOREIGN TABLE sm_account_tax_account_tag("account_tax_id" int4 NOT NULL
	,"account_account_tag_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_tax_account_tag');
drop foreign table if exists sm_account_tax_filiation_rel;	create FOREIGN TABLE sm_account_tax_filiation_rel("parent_tax" int4 NOT NULL
	,"child_tax" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_tax_filiation_rel');
drop foreign table if exists sm_account_tax_group;	create FOREIGN TABLE sm_account_tax_group("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"name" varchar NOT NULL
	,"sequence" int4
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_tax_group');
drop foreign table if exists sm_account_tax_purchase_order_line_rel;	create FOREIGN TABLE sm_account_tax_purchase_order_line_rel("purchase_order_line_id" int4 NOT NULL
	,"account_tax_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_tax_purchase_order_line_rel');
drop foreign table if exists sm_account_tax_report;	create FOREIGN TABLE sm_account_tax_report("id" int4 NOT NULL
	,"company_id" int4
	,"date_from" date
	,"date_to" date
	,"target_move" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_tax_report');
drop foreign table if exists sm_account_tax_sale_advance_payment_inv_rel;	create FOREIGN TABLE sm_account_tax_sale_advance_payment_inv_rel("sale_advance_payment_inv_id" int4 NOT NULL
	,"account_tax_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_tax_sale_advance_payment_inv_rel');
drop foreign table if exists sm_account_tax_sale_order_line_rel;	create FOREIGN TABLE sm_account_tax_sale_order_line_rel("sale_order_line_id" int4 NOT NULL
	,"account_tax_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_tax_sale_order_line_rel');
drop foreign table if exists sm_account_tax_template;	create FOREIGN TABLE sm_account_tax_template("id" int4 NOT NULL
	,"amount_type" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"account_id" int4
	,"name" varchar NOT NULL
	,"sequence" int4 NOT NULL
	,"price_include" bool
	,"chart_template_id" int4 NOT NULL
	,"company_id" int4
	,"type_tax_use" varchar NOT NULL
	,"write_uid" int4
	,"analytic" bool
	,"amount" numeric NOT NULL
	,"include_base_amount" bool
	,"write_date" timestamp
	,"active" bool
	,"refund_account_id" int4
	,"description" varchar
	,"openupgrade_legacy_12_0_tax_adjustment" bool
	,"tax_group_id" int4
	,"tax_exigibility" varchar
	,"cash_basis_account_id" int4
	,"aeat_349_map_line" int4
	,"unece_type_id" int4
	,"unece_categ_id" int4
	,"cash_basis_base_account_id" int4
	,"unece_due_date_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_tax_template');
drop foreign table if exists sm_account_tax_template_aeat_vat_book_map_line_rel;	create FOREIGN TABLE sm_account_tax_template_aeat_vat_book_map_line_rel("aeat_vat_book_map_line_id" int4 NOT NULL
	,"account_tax_template_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_tax_template_aeat_vat_book_map_line_rel');
drop foreign table if exists sm_account_tax_template_filiation_rel;	create FOREIGN TABLE sm_account_tax_template_filiation_rel("parent_tax" int4 NOT NULL
	,"child_tax" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_tax_template_filiation_rel');
drop foreign table if exists sm_account_tax_template_l10n_es_aeat_map_tax_line_rel;	create FOREIGN TABLE sm_account_tax_template_l10n_es_aeat_map_tax_line_rel("l10n_es_aeat_map_tax_line_id" int4 NOT NULL
	,"account_tax_template_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_tax_template_l10n_es_aeat_map_tax_line_rel');
drop foreign table if exists sm_account_unreconcile;	create FOREIGN TABLE sm_account_unreconcile("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_date" timestamp
	,"write_uid" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'account_unreconcile');
drop foreign table if exists sm_accounting_report;	create FOREIGN TABLE sm_accounting_report("id" int4 NOT NULL
	,"create_uid" int4
	,"date_to" date
	,"create_date" timestamp
	,"filter_cmp" varchar NOT NULL
	,"date_from" date
	,"enable_filter" bool
	,"company_id" int4
	,"date_to_cmp" date
	,"label_filter" varchar
	,"write_date" timestamp
	,"date_from_cmp" date
	,"account_report_id" int4 NOT NULL
	,"write_uid" int4
	,"debit_credit" bool
	,"target_move" varchar NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'accounting_report');
drop foreign table if exists sm_add_mis_report_instance_dashboard_wizard;	create FOREIGN TABLE sm_add_mis_report_instance_dashboard_wizard("id" int4 NOT NULL
	,"name" varchar(32) NOT NULL
	,"dashboard_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'add_mis_report_instance_dashboard_wizard');
drop foreign table if exists sm_aeat_349_map_line;	create FOREIGN TABLE sm_aeat_349_map_line("id" int4 NOT NULL
	,"physical_product" bool
	,"operation_key" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'aeat_349_map_line');
drop foreign table if exists sm_aeat_model_export_config;	create FOREIGN TABLE sm_aeat_model_export_config("id" int4 NOT NULL
	,"name" varchar
	,"model_number" varchar(3)
	,"model_id" int4
	,"active" bool
	,"date_start" date
	,"date_end" date
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'aeat_model_export_config');
drop foreign table if exists sm_aeat_model_export_config_line;	create FOREIGN TABLE sm_aeat_model_export_config_line("id" int4 NOT NULL
	,"sequence" int4
	,"export_config_id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"repeat_expression" varchar
	,"repeat" bool
	,"conditional_expression" varchar
	,"conditional" bool
	,"subconfig_id" int4
	,"export_type" varchar NOT NULL
	,"apply_sign" bool
	,"positive_sign" varchar(1)
	,"negative_sign" varchar(1)
	,"size" int4
	,"alignment" varchar
	,"bool_no" varchar(1)
	,"bool_yes" varchar(1)
	,"decimal_size" int4
	,"expression" varchar
	,"fixed_value" varchar
	,"value" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'aeat_model_export_config_line');
drop foreign table if exists sm_aeat_tax_agency;	create FOREIGN TABLE sm_aeat_tax_agency("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'aeat_tax_agency');
drop foreign table if exists sm_aeat_vat_book_map_line;	create FOREIGN TABLE sm_aeat_vat_book_map_line("id" int4 NOT NULL
	,"name" varchar
	,"book_type" varchar
	,"special_tax_group" varchar
	,"fee_type_xlsx_column" varchar
	,"fee_amount_xlsx_column" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'aeat_vat_book_map_line');
drop foreign table if exists sm_aged_partner_balance_wizard;	create FOREIGN TABLE sm_aged_partner_balance_wizard("id" int4 NOT NULL
	,"company_id" int4
	,"date_at" date NOT NULL
	,"target_move" varchar NOT NULL
	,"receivable_accounts_only" bool
	,"payable_accounts_only" bool
	,"show_move_line_details" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'aged_partner_balance_wizard');
drop foreign table if exists sm_aged_partner_balance_wizard_res_partner_rel;	create FOREIGN TABLE sm_aged_partner_balance_wizard_res_partner_rel("aged_partner_balance_wizard_id" int4 NOT NULL
	,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'aged_partner_balance_wizard_res_partner_rel');
drop foreign table if exists sm_agreement;	create FOREIGN TABLE sm_agreement("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"is_template" bool
	,"version" int4
	,"revision" int4
	,"description" text
	,"start_date" date
	,"end_date" date
	,"color" int4
	,"active" bool
	,"company_signed_date" date
	,"partner_signed_date" date
	,"term" int4
	,"expiration_notice" int4
	,"change_notice" int4
	,"special_terms" text
	,"contract_value" numeric
	,"reference" varchar
	,"total_company_mrc" numeric
	,"total_customer_mrc" numeric
	,"total_company_nrc" numeric
	,"total_customer_nrc" numeric
	,"increase_type_id" int4
	,"termination_requested" date
	,"termination_date" date
	,"reviewed_date" date
	,"reviewed_user_id" int4
	,"approved_date" date
	,"approved_user_id" int4
	,"currency_id" int4
	,"partner_id" int4
	,"company_partner_id" int4
	,"partner_contact_id" int4
	,"company_contact_id" int4
	,"agreement_type_id" int4
	,"agreement_subtype_id" int4
	,"sale_order_id" int4
	,"payment_term_id" int4
	,"assigned_user_id" int4
	,"company_signed_user_id" int4
	,"partner_signed_user_id" int4
	,"parent_agreement_id" int4
	,"renewal_type_id" int4
	,"analytic_id" int4
	,"state" varchar
	,"notification_address_id" int4
	,"signed_contract_filename" varchar
	,"signed_contract" bytea
	,"field_id" int4
	,"sub_object_id" int4
	,"sub_model_object_field_id" int4
	,"default_value" varchar
	,"copyvalue" varchar
	,"stage_id" int4
	,"message_last_post" timestamp
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"message_main_attachment_id" int4
	,"code" varchar NOT NULL
	,"company_id" int4
	,"domain" varchar
	,"signature_date" date
	,"use_parties_content" bool
	,"parties" text
	,"field_domain" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'agreement');
drop foreign table if exists sm_agreement_appendix;	create FOREIGN TABLE sm_agreement_appendix("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"title" varchar NOT NULL
	,"sequence" int4
	,"content" text
	,"agreement_id" int4
	,"active" bool
	,"field_id" int4
	,"sub_object_id" int4
	,"sub_model_object_field_id" int4
	,"default_value" varchar
	,"copyvalue" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"field_domain" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'agreement_appendix');
drop foreign table if exists sm_agreement_clause;	create FOREIGN TABLE sm_agreement_clause("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"title" varchar
	,"sequence" int4
	,"agreement_id" int4
	,"section_id" int4
	,"content" text
	,"active" bool
	,"field_id" int4
	,"sub_object_id" int4
	,"sub_model_object_field_id" int4
	,"default_value" varchar
	,"copyvalue" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"field_domain" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'agreement_clause');
drop foreign table if exists sm_agreement_increasetype;	create FOREIGN TABLE sm_agreement_increasetype("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"description" text NOT NULL
	,"increase_percent" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'agreement_increasetype');
drop foreign table if exists sm_agreement_line;	create FOREIGN TABLE sm_agreement_line("id" int4 NOT NULL
	,"product_id" int4
	,"name" varchar NOT NULL
	,"agreement_id" int4
	,"qty" float8
	,"uom_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'agreement_line');
drop foreign table if exists sm_agreement_product_template_rel;	create FOREIGN TABLE sm_agreement_product_template_rel("agreement_id" int4 NOT NULL
	,"product_template_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'agreement_product_template_rel');
drop foreign table if exists sm_agreement_recital;	create FOREIGN TABLE sm_agreement_recital("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"title" varchar
	,"sequence" int4
	,"content" text
	,"agreement_id" int4
	,"active" bool
	,"field_id" int4
	,"sub_object_id" int4
	,"sub_model_object_field_id" int4
	,"default_value" varchar
	,"copyvalue" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"field_domain" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'agreement_recital');
drop foreign table if exists sm_agreement_renewaltype;	create FOREIGN TABLE sm_agreement_renewaltype("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"description" text NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'agreement_renewaltype');
drop foreign table if exists sm_agreement_section;	create FOREIGN TABLE sm_agreement_section("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"title" varchar
	,"sequence" int4
	,"agreement_id" int4
	,"content" text
	,"active" bool
	,"field_id" int4
	,"sub_object_id" int4
	,"sub_model_object_field_id" int4
	,"default_value" varchar
	,"copyvalue" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"field_domain" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'agreement_section');
drop foreign table if exists sm_agreement_serviceprofile;	create FOREIGN TABLE sm_agreement_serviceprofile("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"agreement_id" int4
	,"active" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'agreement_serviceprofile');
drop foreign table if exists sm_agreement_sm_carsharing_structure_cs_community_rel;	create FOREIGN TABLE sm_agreement_sm_carsharing_structure_cs_community_rel("sm_carsharing_structure_cs_community_id" int4 NOT NULL
	,"agreement_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'agreement_sm_carsharing_structure_cs_community_rel');
drop foreign table if exists sm_agreement_stage;	create FOREIGN TABLE sm_agreement_stage("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"description" text
	,"sequence" int4
	,"fold" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"stage_type" varchar NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'agreement_stage');
drop foreign table if exists sm_agreement_subtype;	create FOREIGN TABLE sm_agreement_subtype("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"agreement_type_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'agreement_subtype');
drop foreign table if exists sm_agreement_type;	create FOREIGN TABLE sm_agreement_type("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"active" bool
	,"domain" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'agreement_type');
drop foreign table if exists sm_asset_depreciation_confirmation_wizard;	create FOREIGN TABLE sm_asset_depreciation_confirmation_wizard("id" int4 NOT NULL
	,"date" date NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'asset_depreciation_confirmation_wizard');
drop foreign table if exists sm_asset_modify;	create FOREIGN TABLE sm_asset_modify("id" int4 NOT NULL
	,"name" text NOT NULL
	,"method_number" int4 NOT NULL
	,"method_period" int4
	,"method_end" date
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'asset_modify');
drop foreign table if exists sm_auth_api_key;	create FOREIGN TABLE sm_auth_api_key("id" int4 NOT NULL
	,"server_env_defaults" text
	,"name" varchar
	,"user_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'auth_api_key');
drop foreign table if exists sm_auto_reload_banks_module;	create FOREIGN TABLE sm_auto_reload_banks_module("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'auto_reload_banks_module');
drop foreign table if exists sm_bank_payment_line;	create FOREIGN TABLE sm_bank_payment_line("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"order_id" int4
	,"payment_type" varchar
	,"state" varchar
	,"partner_id" int4
	,"amount_currency" numeric
	,"amount_company_currency" numeric
	,"communication" varchar NOT NULL
	,"company_id" int4
	,"company_currency_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'bank_payment_line');
drop foreign table if exists sm_barcode_nomenclature;	create FOREIGN TABLE sm_barcode_nomenclature("id" int4 NOT NULL
	,"create_uid" int4
	,"name" varchar(32) NOT NULL
	,"write_uid" int4
	,"create_date" timestamp
	,"write_date" timestamp
	,"upc_ean_conv" varchar NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'barcode_nomenclature');
drop foreign table if exists sm_barcode_rule;	create FOREIGN TABLE sm_barcode_rule("id" int4 NOT NULL
	,"create_uid" int4
	,"name" varchar(32) NOT NULL
	,"encoding" varchar NOT NULL
	,"pattern" varchar(32) NOT NULL
	,"sequence" int4
	,"write_uid" int4
	,"alias" varchar(32) NOT NULL
	,"write_date" timestamp
	,"barcode_nomenclature_id" int4
	,"create_date" timestamp
	,"type" varchar NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'barcode_rule');
drop foreign table if exists sm_base_automation;	create FOREIGN TABLE sm_base_automation("id" int4 NOT NULL
	,"action_server_id" int4 NOT NULL
	,"active" bool
	,"trigger" varchar NOT NULL
	,"trg_date_id" int4
	,"trg_date_range" int4
	,"trg_date_range_type" varchar
	,"trg_date_calendar_id" int4
	,"filter_pre_domain" varchar
	,"filter_domain" varchar
	,"last_run" timestamp
	,"on_change_fields" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'base_automation');
drop foreign table if exists sm_base_automation_lead_test;	create FOREIGN TABLE sm_base_automation_lead_test("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"user_id" int4
	,"state" varchar
	,"active" bool
	,"partner_id" int4
	,"date_action_last" timestamp
	,"customer" bool
	,"priority" bool
	,"deadline" bool
	,"is_assigned_to_admin" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'base_automation_lead_test');
drop foreign table if exists sm_base_automation_line_test;	create FOREIGN TABLE sm_base_automation_line_test("id" int4 NOT NULL
	,"name" varchar
	,"lead_id" int4
	,"user_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'base_automation_line_test');
drop foreign table if exists sm_base_config_settings;	create FOREIGN TABLE sm_base_config_settings("id" int4 NOT NULL
	,"create_uid" int4
	,"group_light_multi_company" bool
	,"create_date" timestamp
	,"module_google_drive" bool
	,"module_inter_company_rules" bool
	,"module_base_import" bool
	,"write_uid" int4
	,"module_share" bool
	,"module_google_calendar" bool
	,"write_date" timestamp
	,"module_portal" bool
	,"module_auth_oauth" bool
	,"company_share_partner" bool
	,"fail_counter" int4
	,"alias_domain" varchar
	,"auth_signup_uninvited" bool
	,"auth_signup_template_user_id" int4
	,"auth_signup_reset_password" bool
	,"company_share_product" bool
	,"group_product_variant_moved0" bool
	,"company_id" int4 NOT NULL
	,"group_multi_currency" bool
	,"group_multi_company" bool
	,"group_product_variant" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'base_config_settings');
drop foreign table if exists sm_base_import_import;	create FOREIGN TABLE sm_base_import_import("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_date" timestamp
	,"file_type" varchar
	,"file_name" varchar
	,"write_uid" int4
	,"file" bytea
	,"res_model" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'base_import_import');
drop foreign table if exists sm_base_import_mapping;	create FOREIGN TABLE sm_base_import_mapping("id" int4 NOT NULL
	,"res_model" varchar
	,"column_name" varchar
	,"field_name" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'base_import_mapping');
drop foreign table if exists sm_base_import_module;	create FOREIGN TABLE sm_base_import_module("id" int4 NOT NULL
	,"module_file" bytea NOT NULL
	,"state" varchar
	,"import_message" varchar
	,"force" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'base_import_module');
drop foreign table if exists sm_base_import_tests_models_char;	create FOREIGN TABLE sm_base_import_tests_models_char("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"value" varchar
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'base_import_tests_models_char');
drop foreign table if exists sm_base_import_tests_models_char_noreadonly;	create FOREIGN TABLE sm_base_import_tests_models_char_noreadonly("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"value" varchar
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'base_import_tests_models_char_noreadonly');
drop foreign table if exists sm_base_import_tests_models_char_readonly;	create FOREIGN TABLE sm_base_import_tests_models_char_readonly("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"value" varchar
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'base_import_tests_models_char_readonly');
drop foreign table if exists sm_base_import_tests_models_char_required;	create FOREIGN TABLE sm_base_import_tests_models_char_required("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"value" varchar NOT NULL
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'base_import_tests_models_char_required');
drop foreign table if exists sm_base_import_tests_models_char_states;	create FOREIGN TABLE sm_base_import_tests_models_char_states("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"value" varchar
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'base_import_tests_models_char_states');
drop foreign table if exists sm_base_import_tests_models_char_stillreadonly;	create FOREIGN TABLE sm_base_import_tests_models_char_stillreadonly("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"value" varchar
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'base_import_tests_models_char_stillreadonly');
drop foreign table if exists sm_base_import_tests_models_complex;	create FOREIGN TABLE sm_base_import_tests_models_complex("id" int4 NOT NULL
	,"f" float8
	,"m" numeric
	,"c" varchar
	,"currency_id" int4
	,"d" date
	,"dt" timestamp
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'base_import_tests_models_complex');
drop foreign table if exists sm_base_import_tests_models_float;	create FOREIGN TABLE sm_base_import_tests_models_float("id" int4 NOT NULL
	,"value" float8
	,"value2" numeric
	,"currency_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'base_import_tests_models_float');
drop foreign table if exists sm_base_import_tests_models_m2o;	create FOREIGN TABLE sm_base_import_tests_models_m2o("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"value" int4
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'base_import_tests_models_m2o');
drop foreign table if exists sm_base_import_tests_models_m2o_related;	create FOREIGN TABLE sm_base_import_tests_models_m2o_related("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"value" int4
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'base_import_tests_models_m2o_related');
drop foreign table if exists sm_base_import_tests_models_m2o_required;	create FOREIGN TABLE sm_base_import_tests_models_m2o_required("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"value" int4 NOT NULL
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'base_import_tests_models_m2o_required');
drop foreign table if exists sm_base_import_tests_models_m2o_required_related;	create FOREIGN TABLE sm_base_import_tests_models_m2o_required_related("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"value" int4
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'base_import_tests_models_m2o_required_related');
drop foreign table if exists sm_base_import_tests_models_o2m;	create FOREIGN TABLE sm_base_import_tests_models_o2m("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'base_import_tests_models_o2m');
drop foreign table if exists sm_base_import_tests_models_o2m_child;	create FOREIGN TABLE sm_base_import_tests_models_o2m_child("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"value" int4
	,"write_uid" int4
	,"parent_id" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'base_import_tests_models_o2m_child');
drop foreign table if exists sm_base_import_tests_models_preview;	create FOREIGN TABLE sm_base_import_tests_models_preview("id" int4 NOT NULL
	,"create_uid" int4
	,"othervalue" int4
	,"create_date" timestamp
	,"name" varchar
	,"write_uid" int4
	,"write_date" timestamp
	,"somevalue" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'base_import_tests_models_preview');
drop foreign table if exists sm_base_language_export;	create FOREIGN TABLE sm_base_language_export("id" int4 NOT NULL
	,"lang" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"name" varchar
	,"format" varchar NOT NULL
	,"write_uid" int4
	,"state" varchar
	,"write_date" timestamp
	,"data" bytea)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'base_language_export');
drop foreign table if exists sm_base_language_import;	create FOREIGN TABLE sm_base_language_import("id" int4 NOT NULL
	,"create_uid" int4
	,"code" varchar(6) NOT NULL
	,"create_date" timestamp
	,"name" varchar NOT NULL
	,"filename" varchar NOT NULL
	,"write_date" timestamp
	,"write_uid" int4
	,"data" bytea NOT NULL
	,"overwrite" bool)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'base_language_import');
drop foreign table if exists sm_base_language_install;	create FOREIGN TABLE sm_base_language_install("id" int4 NOT NULL
	,"lang" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"state" varchar
	,"write_date" timestamp
	,"overwrite" bool)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'base_language_install');
drop foreign table if exists sm_base_module_configuration;	create FOREIGN TABLE sm_base_module_configuration("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_date" timestamp
	,"write_uid" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'base_module_configuration');
drop foreign table if exists sm_base_module_uninstall;	create FOREIGN TABLE sm_base_module_uninstall("id" int4 NOT NULL
	,"show_all" bool
	,"module_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'base_module_uninstall');
drop foreign table if exists sm_base_module_update;	create FOREIGN TABLE sm_base_module_update("id" int4 NOT NULL
	,"create_uid" int4
	,"updated" int4
	,"added" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"state" varchar
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'base_module_update');
drop foreign table if exists sm_base_module_upgrade;	create FOREIGN TABLE sm_base_module_upgrade("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"module_info" text)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'base_module_upgrade');
drop foreign table if exists sm_base_partner_merge_automatic_wizard;	create FOREIGN TABLE sm_base_partner_merge_automatic_wizard("id" int4 NOT NULL
	,"group_by_email" bool
	,"group_by_name" bool
	,"group_by_is_company" bool
	,"group_by_vat" bool
	,"group_by_parent_id" bool
	,"state" varchar NOT NULL
	,"number_group" int4
	,"current_line_id" int4
	,"dst_partner_id" int4
	,"exclude_contact" bool
	,"exclude_journal_item" bool
	,"maximum_group" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'base_partner_merge_automatic_wizard');
drop foreign table if exists sm_base_partner_merge_automatic_wizard_res_partner_rel;	create FOREIGN TABLE sm_base_partner_merge_automatic_wizard_res_partner_rel("base_partner_merge_automatic_wizard_id" int4 NOT NULL
	,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'base_partner_merge_automatic_wizard_res_partner_rel');
drop foreign table if exists sm_base_partner_merge_line;	create FOREIGN TABLE sm_base_partner_merge_line("id" int4 NOT NULL
	,"wizard_id" int4
	,"min_id" int4
	,"aggr_ids" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'base_partner_merge_line');
drop foreign table if exists sm_base_setup_terminology;	create FOREIGN TABLE sm_base_setup_terminology("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"partner" varchar NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'base_setup_terminology');
drop foreign table if exists sm_base_update_translations;	create FOREIGN TABLE sm_base_update_translations("id" int4 NOT NULL
	,"lang" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'base_update_translations');
drop foreign table if exists sm_bus_bus;	create FOREIGN TABLE sm_bus_bus("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"message" varchar
	,"channel" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'bus_bus');
drop foreign table if exists sm_bus_presence;	create FOREIGN TABLE sm_bus_presence("id" int4 NOT NULL
	,"status" varchar
	,"last_presence" timestamp
	,"user_id" int4 NOT NULL
	,"last_poll" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'bus_presence');
drop foreign table if exists sm_calendar_alarm;	create FOREIGN TABLE sm_calendar_alarm("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"type" varchar NOT NULL
	,"duration" int4 NOT NULL
	,"interval" varchar NOT NULL
	,"duration_minutes" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'calendar_alarm');
drop foreign table if exists sm_calendar_alarm_calendar_event_rel;	create FOREIGN TABLE sm_calendar_alarm_calendar_event_rel("calendar_event_id" int4 NOT NULL
	,"calendar_alarm_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'calendar_alarm_calendar_event_rel');
drop foreign table if exists sm_calendar_attendee;	create FOREIGN TABLE sm_calendar_attendee("id" int4 NOT NULL
	,"state" varchar
	,"common_name" varchar
	,"partner_id" int4
	,"email" varchar
	,"availability" varchar
	,"access_token" varchar
	,"event_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"google_internal_event_id" varchar
	,"oe_synchro_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'calendar_attendee');
drop foreign table if exists sm_calendar_contacts;	create FOREIGN TABLE sm_calendar_contacts("id" int4 NOT NULL
	,"user_id" int4 NOT NULL
	,"partner_id" int4 NOT NULL
	,"active" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'calendar_contacts');
drop foreign table if exists sm_calendar_event;	create FOREIGN TABLE sm_calendar_event("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"state" varchar
	,"display_start" varchar
	,"start" timestamp NOT NULL
	,"stop" timestamp NOT NULL
	,"allday" bool
	,"start_date" date
	,"start_datetime" timestamp
	,"stop_date" date
	,"stop_datetime" timestamp
	,"duration" float8
	,"description" text
	,"privacy" varchar
	,"location" varchar
	,"show_as" varchar
	,"res_id" int4
	,"res_model_id" int4
	,"res_model" varchar
	,"rrule" varchar
	,"rrule_type" varchar
	,"recurrency" bool
	,"recurrent_id" int4
	,"recurrent_id_date" timestamp
	,"end_type" varchar
	,"interval" int4
	,"count" int4
	,"mo" bool
	,"tu" bool
	,"we" bool
	,"th" bool
	,"fr" bool
	,"sa" bool
	,"su" bool
	,"month_by" varchar
	,"day" int4
	,"week_list" varchar
	,"byday" varchar
	,"final_date" date
	,"user_id" int4
	,"active" bool
	,"message_last_post" timestamp
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"opportunity_id" int4
	,"phonecall_id" int4
	,"oe_update_date" timestamp
	,"message_main_attachment_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'calendar_event');
drop foreign table if exists sm_calendar_event_res_partner_rel;	create FOREIGN TABLE sm_calendar_event_res_partner_rel("calendar_event_id" int4 NOT NULL
	,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'calendar_event_res_partner_rel');
drop foreign table if exists sm_calendar_event_type;	create FOREIGN TABLE sm_calendar_event_type("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'calendar_event_type');
drop foreign table if exists sm_cash_box_in;	create FOREIGN TABLE sm_cash_box_in("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"name" varchar NOT NULL
	,"write_uid" int4
	,"amount" numeric NOT NULL
	,"write_date" timestamp
	,"ref" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'cash_box_in');
drop foreign table if exists sm_cash_box_out;	create FOREIGN TABLE sm_cash_box_out("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"name" varchar NOT NULL
	,"write_uid" int4
	,"amount" numeric NOT NULL
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'cash_box_out');
drop foreign table if exists sm_change_password_user;	create FOREIGN TABLE sm_change_password_user("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"user_login" varchar
	,"new_passwd" varchar
	,"wizard_id" int4 NOT NULL
	,"write_uid" int4
	,"write_date" timestamp
	,"user_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'change_password_user');
drop foreign table if exists sm_change_password_wizard;	create FOREIGN TABLE sm_change_password_wizard("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'change_password_wizard');
drop foreign table if exists sm_city_zip_geonames_import;	create FOREIGN TABLE sm_city_zip_geonames_import("id" int4 NOT NULL
	,"country_id" int4 NOT NULL
	,"letter_case" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'city_zip_geonames_import');
drop foreign table if exists sm_config_es_toponyms;	create FOREIGN TABLE sm_config_es_toponyms("id" int4 NOT NULL
	,"name" varchar(64)
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'config_es_toponyms');
drop foreign table if exists sm_connector_checkpoint;	create FOREIGN TABLE sm_connector_checkpoint("id" int4 NOT NULL
	,"record_id" int4 NOT NULL
	,"model_id" int4 NOT NULL
	,"backend_id" varchar NOT NULL
	,"state" varchar NOT NULL
	,"message_last_post" timestamp
	,"openupgrade_legacy_12_0_activity_date_deadline" date
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"company_id" int4
	,"message_main_attachment_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'connector_checkpoint');
drop foreign table if exists sm_connector_checkpoint_review;	create FOREIGN TABLE sm_connector_checkpoint_review("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'connector_checkpoint_review');
drop foreign table if exists sm_connector_checkpoint_review_rel;	create FOREIGN TABLE sm_connector_checkpoint_review_rel("review_id" int4 NOT NULL
	,"checkpoint_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'connector_checkpoint_review_rel');
drop foreign table if exists sm_connector_config_settings;	create FOREIGN TABLE sm_connector_config_settings("id" int4 NOT NULL
	,"fail_counter" int4
	,"alias_domain" varchar
	,"chart_template_id" int4
	,"module_account_accountant" bool
	,"group_analytic_accounting" bool
	,"group_warning_account" bool
	,"group_cash_rounding" bool
	,"module_account_asset" bool
	,"module_account_deferred_revenue" bool
	,"module_account_budget" bool
	,"module_account_payment" bool
	,"module_account_reports" bool
	,"module_account_reports_followup" bool
	,"module_l10n_us_check_printing" bool
	,"module_account_batch_deposit" bool
	,"module_account_sepa" bool
	,"module_account_sepa_direct_debit" bool
	,"module_account_plaid" bool
	,"module_account_yodlee" bool
	,"module_account_bank_statement_import_qif" bool
	,"module_account_bank_statement_import_ofx" bool
	,"module_account_bank_statement_import_csv" bool
	,"module_account_bank_statement_import_camt" bool
	,"module_currency_rate_live" bool
	,"module_print_docsaway" bool
	,"module_product_margin" bool
	,"module_l10n_eu_service" bool
	,"module_account_taxcloud" bool
	,"auth_signup_reset_password" bool
	,"auth_signup_template_user_id" int4
	,"company_share_product" bool
	,"group_uom" bool
	,"group_product_variant" bool
	,"group_stock_packaging" bool
	,"group_sale_pricelist" bool
	,"group_product_pricelist" bool
	,"group_pricelist_item" bool
	,"module_hr_timesheet" bool
	,"module_rating_project" bool
	,"module_project_forecast" bool
	,"group_subtask_project" bool
	,"lock_confirmed_po" bool
	,"po_order_approval" bool
	,"default_purchase_method" varchar
	,"module_purchase_requisition" bool
	,"group_warning_purchase" bool
	,"module_stock_dropshipping" bool
	,"group_manage_vendor_price" bool
	,"module_account_3way_match" bool
	,"is_installed_sale" bool
	,"group_analytic_account_for_purchases" bool
	,"use_po_lead" bool
	,"use_sale_note" bool
	,"group_discount_per_so_line" bool
	,"module_sale_margin" bool
	,"group_sale_layout" bool
	,"group_warning_sale" bool
	,"portal_confirmation" bool
	,"portal_confirmation_options" varchar
	,"module_sale_payment" bool
	,"module_website_quote" bool
	,"group_sale_delivery_address" bool
	,"multi_sales_price" bool
	,"multi_sales_price_method" varchar
	,"sale_pricelist_setting" varchar
	,"group_show_price_subtotal" bool
	,"group_show_price_total" bool
	,"group_proforma_sales" bool
	,"sale_show_tax" varchar NOT NULL
	,"default_invoice_policy" varchar
	,"default_deposit_product_id" int4
	,"auto_done_setting" bool
	,"module_website_sale_digital" bool
	,"auth_signup_uninvited" varchar
	,"module_delivery" bool
	,"module_delivery_dhl" bool
	,"module_delivery_fedex" bool
	,"module_delivery_ups" bool
	,"module_delivery_usps" bool
	,"module_delivery_bpost" bool
	,"module_product_email_template" bool
	,"module_sale_coupon" bool
	,"group_route_so_lines" bool
	,"module_sale_order_dates" bool
	,"group_display_incoterm" bool
	,"use_security_lead" bool
	,"default_picking_policy" varchar NOT NULL
	,"module_procurement_jit" int4
	,"module_product_expiry" bool
	,"group_stock_production_lot" bool
	,"group_stock_tracking_lot" bool
	,"group_stock_tracking_owner" bool
	,"group_stock_adv_location" bool
	,"group_warning_stock" bool
	,"use_propagation_minimum_delta" bool
	,"module_stock_picking_batch" bool
	,"module_stock_barcode" bool
	,"group_stock_multi_locations" bool
	,"group_stock_multi_warehouses" bool
	,"group_multi_company" bool
	,"company_id" int4 NOT NULL
	,"default_user_rights" bool
	,"default_external_email_server" bool
	,"module_base_import" bool
	,"module_google_calendar" bool
	,"module_google_drive" bool
	,"module_google_spreadsheet" bool
	,"module_auth_oauth" bool
	,"module_auth_ldap" bool
	,"module_base_gengo" bool
	,"module_inter_company_rules" bool
	,"module_pad" bool
	,"module_voip" bool
	,"company_share_partner" bool
	,"default_custom_report_footer" bool
	,"group_multi_currency" bool
	,"module_stock_landed_costs" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"crm_alias_prefix" varchar
	,"generate_lead_from_alias" bool
	,"group_use_lead" bool
	,"module_crm_phone_validation" bool
	,"module_web_clearbit" bool
	,"cal_client_id" varchar
	,"cal_client_secret" varchar
	,"server_uri" varchar
	,"group_pain_multiple_identifier" bool
	,"module_document" bool
	,"group_ir_attachment_user" bool
	,"module_document_page" bool
	,"module_document_page_approval" bool
	,"module_cmis_read" bool
	,"module_cmis_write" bool
	,"module_hr_org_chart" bool
	,"module_l10n_fr_hr_payroll" bool
	,"module_l10n_be_hr_payroll" bool
	,"module_l10n_in_hr_payroll" bool
	,"module_agreement_maintenance" bool
	,"module_agreement_mrp" bool
	,"module_agreement_project" bool
	,"module_agreement_repair" bool
	,"module_agreement_rma" bool
	,"module_agreement_sale" bool
	,"module_agreement_sale_subscription" bool
	,"module_agreement_stock" bool
	,"module_fieldservice_agreement" bool
	,"module_helpdesk_agreement" bool
	,"expense_alias_prefix" varchar
	,"use_mailgateway" bool
	,"module_sale_management" bool)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'connector_config_settings');
drop foreign table if exists sm_contract_contract;	create FOREIGN TABLE sm_contract_contract("id" int4 NOT NULL
	,"code" varchar
	,"create_date" timestamp
	,"name" varchar NOT NULL
	,"create_uid" int4
	,"message_last_post" timestamp
	,"company_id" int4 NOT NULL
	,"write_uid" int4
	,"write_date" timestamp
	,"account_type" varchar
	,"partner_id" int4 NOT NULL
	,"use_tasks" bool
	,"use_issues" bool
	,"openupgrade_legacy_10_0_account_type" varchar
	,"active" bool
	,"contract_type" varchar
	,"pricelist_id" int4
	,"recurring_rule_type" varchar
	,"recurring_invoicing_type" varchar
	,"recurring_interval" int4
	,"journal_id" int4
	,"contract_template_id" int4
	,"date_start" date
	,"date_end" date
	,"recurring_invoices" bool
	,"recurring_next_date" date
	,"user_id" int4
	,"payment_mode_id" int4
	,"invoicing_sales" bool
	,"skip_zero_qty" bool
	,"department_id" int4
	,"parent_left" int4
	,"parent_right" int4
	,"parent_id" int4
	,"message_main_attachment_id" int4
	,"group_id" int4
	,"parent_path" varchar
	,"complete_name" varchar
	,"access_token" varchar
	,"manual_currency_id" int4
	,"payment_term_id" int4
	,"fiscal_position_id" int4
	,"invoice_partner_id" int4
	,"commercial_partner_id" int4
	,"note" text
	,"is_terminated" bool
	,"terminate_reason_id" int4
	,"terminate_comment" text
	,"terminate_date" date)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'contract_contract');
drop foreign table if exists sm_contract_contract_contract_tag_rel;	create FOREIGN TABLE sm_contract_contract_contract_tag_rel("contract_contract_id" int4 NOT NULL
	,"contract_tag_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'contract_contract_contract_tag_rel');
drop foreign table if exists sm_contract_contract_terminate;	create FOREIGN TABLE sm_contract_contract_terminate("id" int4 NOT NULL
	,"contract_id" int4 NOT NULL
	,"terminate_reason_id" int4 NOT NULL
	,"terminate_comment" text
	,"terminate_date" date NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'contract_contract_terminate');
drop foreign table if exists sm_contract_line;	create FOREIGN TABLE sm_contract_line("id" int4 NOT NULL
	,"analytic_account_id" int4
	,"product_id" int4
	,"name" text NOT NULL
	,"quantity" float8 NOT NULL
	,"uom_id" int4
	,"automatic_price" bool
	,"specific_price" float8
	,"discount" numeric
	,"sequence" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"qty_type" varchar NOT NULL
	,"qty_formula_id" int4
	,"recurring_rule_type" varchar(255) NOT NULL
	,"recurring_invoicing_type" varchar(255) NOT NULL
	,"recurring_interval" int4 NOT NULL
	,"recurring_next_date" date
	,"date_start" date NOT NULL
	,"date_end" date
	,"contract_id" int4 NOT NULL
	,"last_date_invoiced" date
	,"termination_notice_date" date
	,"successor_contract_line_id" int4
	,"predecessor_contract_line_id" int4
	,"manual_renew_needed" bool
	,"active" bool
	,"is_canceled" bool
	,"is_auto_renew" bool
	,"auto_renew_interval" int4
	,"auto_renew_rule_type" varchar
	,"termination_notice_interval" int4
	,"termination_notice_rule_type" varchar
	,"display_type" varchar
	,"note_invoicing_mode" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'contract_line');
drop foreign table if exists sm_contract_line_qty_formula;	create FOREIGN TABLE sm_contract_line_qty_formula("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"code" text NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'contract_line_qty_formula');
drop foreign table if exists sm_contract_line_wizard;	create FOREIGN TABLE sm_contract_line_wizard("id" int4 NOT NULL
	,"date_start" date
	,"date_end" date
	,"recurring_next_date" date
	,"is_auto_renew" bool
	,"manual_renew_needed" bool
	,"contract_line_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'contract_line_wizard');
drop foreign table if exists sm_contract_manually_create_invoice;	create FOREIGN TABLE sm_contract_manually_create_invoice("id" int4 NOT NULL
	,"invoice_date" date NOT NULL
	,"contract_type" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'contract_manually_create_invoice');
drop foreign table if exists sm_contract_modification;	create FOREIGN TABLE sm_contract_modification("id" int4 NOT NULL
	,"date" date NOT NULL
	,"description" text NOT NULL
	,"contract_id" int4 NOT NULL
	,"sent" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'contract_modification');
drop foreign table if exists sm_contract_tag;	create FOREIGN TABLE sm_contract_tag("id" int4 NOT NULL
	,"name" varchar
	,"company_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'contract_tag');
drop foreign table if exists sm_contract_template;	create FOREIGN TABLE sm_contract_template("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"partner_id" int4
	,"contract_type" varchar
	,"pricelist_id" int4
	,"recurring_rule_type" varchar
	,"recurring_invoicing_type" varchar
	,"recurring_interval" int4
	,"journal_id" int4
	,"company_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'contract_template');
drop foreign table if exists sm_contract_template_line;	create FOREIGN TABLE sm_contract_template_line("id" int4 NOT NULL
	,"product_id" int4
	,"contract_id" int4 NOT NULL
	,"name" text NOT NULL
	,"quantity" float8 NOT NULL
	,"uom_id" int4
	,"automatic_price" bool
	,"specific_price" float8
	,"discount" numeric
	,"sequence" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"qty_type" varchar NOT NULL
	,"qty_formula_id" int4
	,"recurring_rule_type" varchar(255) NOT NULL
	,"recurring_invoicing_type" varchar(255) NOT NULL
	,"recurring_interval" int4 NOT NULL
	,"date_start" date
	,"recurring_next_date" date
	,"last_date_invoiced" date
	,"is_canceled" bool
	,"is_auto_renew" bool
	,"auto_renew_interval" int4
	,"auto_renew_rule_type" varchar
	,"termination_notice_interval" int4
	,"termination_notice_rule_type" varchar
	,"display_type" varchar
	,"note_invoicing_mode" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'contract_template_line');
drop foreign table if exists sm_contract_terminate_reason;	create FOREIGN TABLE sm_contract_terminate_reason("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"terminate_comment_required" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'contract_terminate_reason');
drop foreign table if exists sm_crm_activity_report;	create FOREIGN TABLE sm_crm_activity_report("id" int4
	,"subtype_id" int4
	,"mail_activity_type_id" int4
	,"author_id" int4
	,"date" timestamp
	,"subject" varchar
	,"lead_id" int4
	,"user_id" int4
	,"team_id" int4
	,"country_id" int4
	,"company_id" int4
	,"stage_id" int4
	,"partner_id" int4
	,"lead_type" varchar
	,"active" bool
	,"probability" float8)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'crm_activity_report');
drop foreign table if exists sm_crm_claim;	create FOREIGN TABLE sm_crm_claim("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"active" bool
	,"description" text
	,"resolution" text
	,"create_date" timestamp
	,"write_date" timestamp
	,"date_deadline" date
	,"date_closed" timestamp
	,"date" timestamp
	,"model_ref_id" varchar
	,"categ_id" int4
	,"priority" varchar
	,"type_action" varchar
	,"user_id" int4
	,"user_fault" varchar
	,"team_id" int4
	,"company_id" int4
	,"partner_id" int4
	,"email_cc" text
	,"email_from" varchar
	,"partner_phone" varchar
	,"stage_id" int4
	,"cause" text
	,"message_last_post" timestamp
	,"openupgrade_legacy_12_0_activity_date_deadline" date
	,"create_uid" int4
	,"write_uid" int4
	,"code" varchar NOT NULL
	,"message_main_attachment_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'crm_claim');
drop foreign table if exists sm_crm_claim_category;	create FOREIGN TABLE sm_crm_claim_category("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"team_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'crm_claim_category');
drop foreign table if exists sm_crm_claim_report;	create FOREIGN TABLE sm_crm_claim_report("id" int4
	,"claim_date" timestamp
	,"date_closed" timestamp
	,"date_deadline" date
	,"user_id" int4
	,"stage_id" int4
	,"team_id" int4
	,"partner_id" int4
	,"company_id" int4
	,"categ_id" int4
	,"subject" varchar
	,"nbr_claims" int8
	,"priority" varchar
	,"type_action" varchar
	,"create_date" timestamp
	,"delay_close" float8
	,"email" int8
	,"delay_expected" float8)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'crm_claim_report');
drop foreign table if exists sm_crm_claim_stage;	create FOREIGN TABLE sm_crm_claim_stage("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"sequence" int4
	,"case_default" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'crm_claim_stage');
drop foreign table if exists sm_crm_lead;	create FOREIGN TABLE sm_crm_lead("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"partner_id" int4
	,"active" bool
	,"date_action_last" timestamp
	,"email_from" varchar
	,"website" varchar
	,"team_id" int4
	,"email_cc" text
	,"description" text
	,"create_date" timestamp
	,"write_date" timestamp
	,"contact_name" varchar
	,"partner_name" varchar
	,"openupgrade_legacy_12_0_opt_out" bool
	,"type" varchar NOT NULL
	,"priority" varchar
	,"date_closed" timestamp
	,"stage_id" int4
	,"user_id" int4
	,"referred" varchar
	,"date_open" timestamp
	,"day_open" float8
	,"day_close" float8
	,"date_last_stage_update" timestamp
	,"date_conversion" timestamp
	,"message_bounce" int4
	,"probability" float8
	,"date_deadline" date
	,"color" int4
	,"street" varchar
	,"street2" varchar
	,"zip" varchar
	,"city" varchar
	,"state_id" int4
	,"country_id" int4
	,"phone" varchar
	,"mobile" varchar
	,"function" varchar
	,"title" int4
	,"company_id" int4
	,"lost_reason" int4
	,"message_last_post" timestamp
	,"campaign_id" int4
	,"source_id" int4
	,"medium_id" int4
	,"openupgrade_legacy_12_0_activity_date_deadline" date
	,"create_uid" int4
	,"write_uid" int4
	,"message_main_attachment_id" int4
	,"planned_revenue" numeric
	,"expected_revenue" numeric)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'crm_lead');
drop foreign table if exists sm_crm_lead2opportunity_partner;	create FOREIGN TABLE sm_crm_lead2opportunity_partner("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"user_id" int4
	,"team_id" int4
	,"action" varchar NOT NULL
	,"partner_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'crm_lead2opportunity_partner');
drop foreign table if exists sm_crm_lead2opportunity_partner_mass;	create FOREIGN TABLE sm_crm_lead2opportunity_partner_mass("id" int4 NOT NULL
	,"team_id" int4
	,"deduplicate" bool
	,"action" varchar NOT NULL
	,"force_assignation" bool
	,"name" varchar NOT NULL
	,"user_id" int4
	,"partner_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'crm_lead2opportunity_partner_mass');
drop foreign table if exists sm_crm_lead2opportunity_partner_mass_res_users_rel;	create FOREIGN TABLE sm_crm_lead2opportunity_partner_mass_res_users_rel("crm_lead2opportunity_partner_mass_id" int4 NOT NULL
	,"res_users_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'crm_lead2opportunity_partner_mass_res_users_rel');
drop foreign table if exists sm_crm_lead_convert2task;	create FOREIGN TABLE sm_crm_lead_convert2task("id" int4 NOT NULL
	,"action" varchar NOT NULL
	,"partner_id" int4
	,"lead_id" int4
	,"project_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'crm_lead_convert2task');
drop foreign table if exists sm_crm_lead_crm_lead2opportunity_partner_mass_rel;	create FOREIGN TABLE sm_crm_lead_crm_lead2opportunity_partner_mass_rel("crm_lead2opportunity_partner_mass_id" int4 NOT NULL
	,"crm_lead_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'crm_lead_crm_lead2opportunity_partner_mass_rel');
drop foreign table if exists sm_crm_lead_crm_lead2opportunity_partner_rel;	create FOREIGN TABLE sm_crm_lead_crm_lead2opportunity_partner_rel("crm_lead2opportunity_partner_id" int4 NOT NULL
	,"crm_lead_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'crm_lead_crm_lead2opportunity_partner_rel');
drop foreign table if exists sm_crm_lead_line;	create FOREIGN TABLE sm_crm_lead_line("id" int4 NOT NULL
	,"lead_id" int4
	,"name" varchar NOT NULL
	,"product_id" int4
	,"category_id" int4
	,"product_tmpl_id" int4
	,"product_qty" int4 NOT NULL
	,"uom_id" int4
	,"price_unit" float8
	,"planned_revenue" float8
	,"expected_revenue" float8
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'crm_lead_line');
drop foreign table if exists sm_crm_lead_lost;	create FOREIGN TABLE sm_crm_lead_lost("id" int4 NOT NULL
	,"lost_reason_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'crm_lead_lost');
drop foreign table if exists sm_crm_lead_tag;	create FOREIGN TABLE sm_crm_lead_tag("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"color" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'crm_lead_tag');
drop foreign table if exists sm_crm_lead_tag_crm_phonecall_planner_rel;	create FOREIGN TABLE sm_crm_lead_tag_crm_phonecall_planner_rel("crm_phonecall_planner_id" int4 NOT NULL
	,"crm_lead_tag_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'crm_lead_tag_crm_phonecall_planner_rel');
drop foreign table if exists sm_crm_lead_tag_rel;	create FOREIGN TABLE sm_crm_lead_tag_rel("lead_id" int4 NOT NULL
	,"tag_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'crm_lead_tag_rel');
drop foreign table if exists sm_crm_lost_reason;	create FOREIGN TABLE sm_crm_lost_reason("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"active" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'crm_lost_reason');
drop foreign table if exists sm_crm_merge_opportunity;	create FOREIGN TABLE sm_crm_merge_opportunity("id" int4 NOT NULL
	,"user_id" int4
	,"team_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'crm_merge_opportunity');
drop foreign table if exists sm_crm_partner_binding;	create FOREIGN TABLE sm_crm_partner_binding("id" int4 NOT NULL
	,"action" varchar NOT NULL
	,"partner_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'crm_partner_binding');
drop foreign table if exists sm_crm_phonecall;	create FOREIGN TABLE sm_crm_phonecall("id" int4 NOT NULL
	,"date_action_last" timestamp
	,"date_action_next" timestamp
	,"create_date" timestamp
	,"team_id" int4
	,"user_id" int4
	,"partner_id" int4
	,"company_id" int4
	,"description" text
	,"state" varchar
	,"email_from" varchar
	,"date_open" timestamp
	,"name" varchar
	,"active" bool
	,"duration" float8
	,"partner_phone" varchar
	,"partner_mobile" varchar
	,"priority" varchar
	,"date_closed" timestamp
	,"date" timestamp
	,"opportunity_id" int4
	,"message_last_post" timestamp
	,"campaign_id" int4
	,"source_id" int4
	,"medium_id" int4
	,"create_uid" int4
	,"write_uid" int4
	,"write_date" timestamp
	,"summary_id" int4 NOT NULL
	,"message_main_attachment_id" int4
	,"direction" varchar NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'crm_phonecall');
drop foreign table if exists sm_crm_phonecall2phonecall;	create FOREIGN TABLE sm_crm_phonecall2phonecall("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"user_id" int4
	,"contact_name" varchar
	,"phone" varchar
	,"date" timestamp
	,"team_id" int4
	,"action" varchar NOT NULL
	,"partner_id" int4
	,"note" text
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'crm_phonecall2phonecall');
drop foreign table if exists sm_crm_phonecall_crm_phonecall_planner_rel;	create FOREIGN TABLE sm_crm_phonecall_crm_phonecall_planner_rel("crm_phonecall_planner_id" int4 NOT NULL
	,"crm_phonecall_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'crm_phonecall_crm_phonecall_planner_rel');
drop foreign table if exists sm_crm_phonecall_planner;	create FOREIGN TABLE sm_crm_phonecall_planner("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"user_id" int4
	,"team_id" int4
	,"res_partner_domain" varchar
	,"duration" float8 NOT NULL
	,"start" timestamp NOT NULL
	,"end" timestamp NOT NULL
	,"repeat_calls" bool
	,"days_gap" int4 NOT NULL
	,"campaign_id" int4
	,"source_id" int4
	,"medium_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'crm_phonecall_planner');
drop foreign table if exists sm_crm_phonecall_report;	create FOREIGN TABLE sm_crm_phonecall_report("id" int4
	,"opening_date" timestamp
	,"date_closed" timestamp
	,"state" varchar
	,"user_id" int4
	,"team_id" int4
	,"partner_id" int4
	,"duration" float8
	,"company_id" int4
	,"priority" varchar
	,"nbr_cases" int4
	,"create_date" timestamp
	,"delay_close" float8
	,"delay_open" float8)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'crm_phonecall_report');
drop foreign table if exists sm_crm_phonecall_summary;	create FOREIGN TABLE sm_crm_phonecall_summary("id" int4 NOT NULL
	,"name" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'crm_phonecall_summary');
drop foreign table if exists sm_crm_phonecall_tag_rel;	create FOREIGN TABLE sm_crm_phonecall_tag_rel("phone_id" int4 NOT NULL
	,"tag_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'crm_phonecall_tag_rel');
drop foreign table if exists sm_crm_product_report;	create FOREIGN TABLE sm_crm_product_report("id" int4
	,"active" bool
	,"lead_id" int4
	,"campaign_id" int4
	,"country_id" int4
	,"company_id" int4
	,"create_date" timestamp
	,"date_closed" timestamp
	,"date_conversion" timestamp
	,"date_deadline" date
	,"date_open" timestamp
	,"lost_reason" int4
	,"name" varchar
	,"partner_id" int4
	,"partner_name" varchar
	,"probability" float8
	,"type" varchar
	,"stage_id" int4
	,"team_id" int4
	,"user_id" int4
	,"category_id" int4
	,"expected_revenue" float8
	,"planned_revenue" float8
	,"product_id" int4
	,"product_qty" int4
	,"product_tmpl_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'crm_product_report');
drop foreign table if exists sm_crm_stage;	create FOREIGN TABLE sm_crm_stage("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"sequence" int4
	,"probability" float8 NOT NULL
	,"on_change" bool
	,"requirements" text
	,"team_id" int4
	,"legend_priority" text
	,"fold" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'crm_stage');
drop foreign table if exists sm_crm_team;	create FOREIGN TABLE sm_crm_team("id" int4 NOT NULL
	,"openupgrade_legacy_10_0_code" varchar(8)
	,"openupgrade_legacy_10_0_working_hours" numeric
	,"name" varchar NOT NULL
	,"create_uid" int4
	,"color" int4
	,"message_last_post" timestamp
	,"user_id" int4
	,"company_id" int4
	,"write_uid" int4
	,"active" bool
	,"reply_to" varchar
	,"write_date" timestamp
	,"create_date" timestamp
	,"use_quotations" bool
	,"invoiced_target" int4
	,"use_invoices" bool
	,"team_type" varchar NOT NULL
	,"dashboard_graph_model" varchar
	,"dashboard_graph_group" varchar
	,"dashboard_graph_period" varchar
	,"use_leads" bool
	,"use_opportunities" bool
	,"alias_id" int4 NOT NULL
	,"dashboard_graph_group_pipeline" varchar
	,"message_main_attachment_id" int4
	,"openupgrade_legacy_12_0_dashboard_graph_model" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'crm_team');
drop foreign table if exists sm_crm_team_claim_stage_rel;	create FOREIGN TABLE sm_crm_team_claim_stage_rel("stage_id" int4 NOT NULL
	,"team_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'crm_team_claim_stage_rel');
drop foreign table if exists sm_date_range;	create FOREIGN TABLE sm_date_range("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"date_start" date NOT NULL
	,"date_end" date NOT NULL
	,"type_id" int4 NOT NULL
	,"type_name" varchar
	,"company_id" int4
	,"active" bool
	,"parent_type_id" int4
	,"parent_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'date_range');
drop foreign table if exists sm_date_range_generator;	create FOREIGN TABLE sm_date_range_generator("id" int4 NOT NULL
	,"name_prefix" varchar NOT NULL
	,"date_start" date NOT NULL
	,"type_id" int4 NOT NULL
	,"company_id" int4
	,"unit_of_time" int4 NOT NULL
	,"duration_count" int4 NOT NULL
	,"count" int4 NOT NULL
	,"parent_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'date_range_generator');
drop foreign table if exists sm_date_range_type;	create FOREIGN TABLE sm_date_range_type("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"allow_overlap" bool
	,"active" bool
	,"company_id" int4
	,"parent_type_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'date_range_type');
drop foreign table if exists sm_decimal_precision;	create FOREIGN TABLE sm_decimal_precision("id" int4 NOT NULL
	,"digits" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"name" varchar NOT NULL
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'decimal_precision');
drop foreign table if exists sm_decimal_precision_test;	create FOREIGN TABLE sm_decimal_precision_test("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"float_2" numeric
	,"float" float8
	,"float_4" numeric
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'decimal_precision_test');
drop foreign table if exists sm_digest_digest;	create FOREIGN TABLE sm_digest_digest("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"periodicity" varchar NOT NULL
	,"next_run_date" date
	,"template_id" int4 NOT NULL
	,"company_id" int4
	,"state" varchar
	,"kpi_res_users_connected" bool
	,"kpi_mail_message_total" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"kpi_account_total_revenue" bool
	,"kpi_crm_lead_created" bool
	,"kpi_crm_opportunities_won" bool
	,"kpi_project_task_opened" bool
	,"kpi_all_sale_total" bool)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'digest_digest');
drop foreign table if exists sm_digest_digest_res_users_rel;	create FOREIGN TABLE sm_digest_digest_res_users_rel("digest_digest_id" int4 NOT NULL
	,"res_users_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'digest_digest_res_users_rel');
drop foreign table if exists sm_digest_tip;	create FOREIGN TABLE sm_digest_tip("id" int4 NOT NULL
	,"sequence" int4
	,"tip_description" text
	,"group_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'digest_tip');
drop foreign table if exists sm_digest_tip_res_users_rel;	create FOREIGN TABLE sm_digest_tip_res_users_rel("digest_tip_id" int4 NOT NULL
	,"res_users_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'digest_tip_res_users_rel');
drop foreign table if exists sm_document_page;	create FOREIGN TABLE sm_document_page("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"type" varchar
	,"active" bool
	,"parent_id" int4
	,"template" text
	,"history_head" int4
	,"menu_id" int4
	,"content_date" timestamp
	,"content_uid" int4
	,"company_id" int4
	,"message_last_post" timestamp
	,"openupgrade_legacy_12_0_activity_date_deadline" date
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"project_id" int4
	,"message_main_attachment_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'document_page');
drop foreign table if exists sm_document_page_create_menu;	create FOREIGN TABLE sm_document_page_create_menu("id" int4 NOT NULL
	,"menu_name" varchar NOT NULL
	,"menu_parent_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'document_page_create_menu');
drop foreign table if exists sm_document_page_history;	create FOREIGN TABLE sm_document_page_history("id" int4 NOT NULL
	,"page_id" int4
	,"name" varchar
	,"summary" varchar
	,"content" text
	,"company_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'document_page_history');
drop foreign table if exists sm_email_template_attachment_rel;	create FOREIGN TABLE sm_email_template_attachment_rel("email_template_id" int4 NOT NULL
	,"attachment_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'email_template_attachment_rel');
drop foreign table if exists sm_email_template_preview;	create FOREIGN TABLE sm_email_template_preview("id" int4 NOT NULL
	,"create_date" timestamp
	,"sub_object" int4
	,"auto_delete" bool
	,"mail_server_id" int4
	,"write_uid" int4
	,"partner_to" varchar
	,"ref_ir_act_window" int4
	,"subject" varchar
	,"create_uid" int4
	,"report_template" int4
	,"ref_ir_value" int4
	,"user_signature" bool
	,"null_value" varchar
	,"email_cc" varchar
	,"res_id" varchar
	,"model_id" int4
	,"sub_model_object_field" int4
	,"body_html" text
	,"email_to" varchar
	,"write_date" timestamp
	,"copyvalue" varchar
	,"lang" varchar
	,"name" varchar
	,"model_object_field" int4
	,"report_name" varchar
	,"use_default_to" bool
	,"reply_to" varchar
	,"model" varchar
	,"email_from" varchar
	,"scheduled_date" varchar
	,"force_email_send" bool
	,"easy_my_coop" bool)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'email_template_preview');
drop foreign table if exists sm_email_template_preview_res_partner_rel;	create FOREIGN TABLE sm_email_template_preview_res_partner_rel("email_template_preview_id" int4 NOT NULL
	,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'email_template_preview_res_partner_rel');
drop foreign table if exists sm_employee_category_rel;	create FOREIGN TABLE sm_employee_category_rel("category_id" int4 NOT NULL
	,"emp_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'employee_category_rel');
drop foreign table if exists sm_expense_tax;	create FOREIGN TABLE sm_expense_tax("expense_id" int4 NOT NULL
	,"tax_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'expense_tax');
drop foreign table if exists sm_export_boolean;	create FOREIGN TABLE sm_export_boolean("id" int4 NOT NULL
	,"value" bool
	,"const" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'export_boolean');
drop foreign table if exists sm_export_date;	create FOREIGN TABLE sm_export_date("id" int4 NOT NULL
	,"value" date
	,"const" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'export_date');
drop foreign table if exists sm_export_datetime;	create FOREIGN TABLE sm_export_datetime("id" int4 NOT NULL
	,"value" timestamp
	,"const" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'export_datetime');
drop foreign table if exists sm_export_decimal;	create FOREIGN TABLE sm_export_decimal("id" int4 NOT NULL
	,"value" numeric
	,"const" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'export_decimal');
drop foreign table if exists sm_export_float;	create FOREIGN TABLE sm_export_float("id" int4 NOT NULL
	,"value" float8
	,"const" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'export_float');
drop foreign table if exists sm_export_function;	create FOREIGN TABLE sm_export_function("id" int4 NOT NULL
	,"const" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'export_function');
drop foreign table if exists sm_export_integer;	create FOREIGN TABLE sm_export_integer("id" int4 NOT NULL
	,"value" int4
	,"const" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'export_integer');
drop foreign table if exists sm_export_many2many;	create FOREIGN TABLE sm_export_many2many("id" int4 NOT NULL
	,"const" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'export_many2many');
drop foreign table if exists sm_export_many2many_export_many2many_other_rel;	create FOREIGN TABLE sm_export_many2many_export_many2many_other_rel("export_many2many_id" int4 NOT NULL
	,"export_many2many_other_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'export_many2many_export_many2many_other_rel');
drop foreign table if exists sm_export_many2many_other;	create FOREIGN TABLE sm_export_many2many_other("id" int4 NOT NULL
	,"str" varchar
	,"value" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'export_many2many_other');
drop foreign table if exists sm_export_many2one;	create FOREIGN TABLE sm_export_many2one("id" int4 NOT NULL
	,"value" int4
	,"const" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'export_many2one');
drop foreign table if exists sm_export_one2many;	create FOREIGN TABLE sm_export_one2many("id" int4 NOT NULL
	,"const" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'export_one2many');
drop foreign table if exists sm_export_one2many_child;	create FOREIGN TABLE sm_export_one2many_child("id" int4 NOT NULL
	,"parent_id" int4
	,"str" varchar
	,"value" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'export_one2many_child');
drop foreign table if exists sm_export_one2many_child_1;	create FOREIGN TABLE sm_export_one2many_child_1("id" int4 NOT NULL
	,"parent_id" int4
	,"str" varchar
	,"value" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'export_one2many_child_1');
drop foreign table if exists sm_export_one2many_child_2;	create FOREIGN TABLE sm_export_one2many_child_2("id" int4 NOT NULL
	,"parent_id" int4
	,"str" varchar
	,"value" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'export_one2many_child_2');
drop foreign table if exists sm_export_one2many_multiple;	create FOREIGN TABLE sm_export_one2many_multiple("id" int4 NOT NULL
	,"parent_id" int4
	,"const" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'export_one2many_multiple');
drop foreign table if exists sm_export_one2many_multiple_child;	create FOREIGN TABLE sm_export_one2many_multiple_child("id" int4 NOT NULL
	,"parent_id" int4
	,"str" varchar
	,"value" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'export_one2many_multiple_child');
drop foreign table if exists sm_export_one2many_recursive;	create FOREIGN TABLE sm_export_one2many_recursive("id" int4 NOT NULL
	,"value" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'export_one2many_recursive');
drop foreign table if exists sm_export_selection;	create FOREIGN TABLE sm_export_selection("id" int4 NOT NULL
	,"value" int4
	,"const" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'export_selection');
drop foreign table if exists sm_export_selection_function;	create FOREIGN TABLE sm_export_selection_function("id" int4 NOT NULL
	,"value" varchar
	,"const" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'export_selection_function');
drop foreign table if exists sm_export_selection_withdefault;	create FOREIGN TABLE sm_export_selection_withdefault("id" int4 NOT NULL
	,"const" int4
	,"value" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'export_selection_withdefault');
drop foreign table if exists sm_export_string;	create FOREIGN TABLE sm_export_string("id" int4 NOT NULL
	,"value" varchar
	,"const" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'export_string');
drop foreign table if exists sm_export_string_bounded;	create FOREIGN TABLE sm_export_string_bounded("id" int4 NOT NULL
	,"value" varchar(16)
	,"const" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'export_string_bounded');
drop foreign table if exists sm_export_string_required;	create FOREIGN TABLE sm_export_string_required("id" int4 NOT NULL
	,"value" varchar NOT NULL
	,"const" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'export_string_required');
drop foreign table if exists sm_export_text;	create FOREIGN TABLE sm_export_text("id" int4 NOT NULL
	,"value" text
	,"const" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'export_text');
drop foreign table if exists sm_export_unique;	create FOREIGN TABLE sm_export_unique("id" int4 NOT NULL
	,"value" int4
	,"value2" int4
	,"value3" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'export_unique');
drop foreign table if exists sm_fetchmail_config_settings;	create FOREIGN TABLE sm_fetchmail_config_settings("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_date" timestamp
	,"write_uid" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'fetchmail_config_settings');
drop foreign table if exists sm_fetchmail_server;	create FOREIGN TABLE sm_fetchmail_server("id" int4 NOT NULL
	,"create_date" timestamp
	,"write_uid" int4
	,"date" timestamp
	,"create_uid" int4
	,"configuration" text
	,"script" varchar
	,"object_id" int4
	,"port" int4
	,"priority" int4
	,"attach" bool
	,"state" varchar
	,"type" varchar NOT NULL
	,"action_id" int4
	,"user" varchar
	,"write_date" timestamp
	,"active" bool
	,"password" varchar
	,"name" varchar NOT NULL
	,"is_ssl" bool
	,"server" varchar
	,"original" bool)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'fetchmail_server');
drop foreign table if exists sm_fleet_service_type;	create FOREIGN TABLE sm_fleet_service_type("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"category" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'fleet_service_type');
drop foreign table if exists sm_fleet_vehicle;	create FOREIGN TABLE sm_fleet_vehicle("id" int4 NOT NULL
	,"name" varchar
	,"active" bool
	,"company_id" int4
	,"license_plate" varchar
	,"vin_sn" varchar
	,"driver_id" int4
	,"model_id" int4 NOT NULL
	,"acquisition_date" date
	,"color" varchar
	,"state_id" int4
	,"location" varchar
	,"seats" int4
	,"model_year" varchar
	,"doors" int4
	,"odometer_unit" varchar NOT NULL
	,"transmission" varchar
	,"fuel_type" varchar
	,"horsepower" int4
	,"horsepower_tax" float8
	,"power" int4
	,"co2" float8
	,"car_value" float8
	,"residual_value" float8
	,"message_last_post" timestamp
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"project_id" int4
	,"db_car_id" int4
	,"battery_fee" varchar
	,"car_type" varchar
	,"bougth_km" int4
	,"bougth_date" date
	,"r_link_update" bool
	,"key_number" varchar
	,"insurance_company" varchar
	,"insurance_age" varchar
	,"insurance_policy" varchar
	,"insurance_expiricy" date
	,"battery_size" int4
	,"next_tech_revision" date
	,"next_revision" date
	,"viat_applies" bool
	,"viat_pan" varchar
	,"viat_expiricy" date
	,"viat_onplace" bool
	,"viat_eco_accepted" bool
	,"viat_eco_approved_date" date
	,"ivtm_status" varchar
	,"live_card" varchar
	,"live_card_status" varchar
	,"electromaps_code" varchar
	,"garagekey_code" varchar
	,"secondary_key_location" varchar
	,"battery_rental" float8
	,"contact_person_txt" varchar
	,"vinyl" varchar
	,"insurance_extras" text
	,"live_smou" varchar
	,"origin" varchar
	,"drive_docs" varchar
	,"db_car_owner_group_index" varchar
	,"vehicle_type" varchar NOT NULL
	,"has_gps" bool
	,"message_main_attachment_id" int4
	,"brand_id" int4
	,"first_contract_date" date
	,"analytic_account_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'fleet_vehicle');
drop foreign table if exists sm_fleet_vehicle_assignation_log;	create FOREIGN TABLE sm_fleet_vehicle_assignation_log("id" int4 NOT NULL
	,"vehicle_id" int4 NOT NULL
	,"driver_id" int4 NOT NULL
	,"date_start" date
	,"date_end" date
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'fleet_vehicle_assignation_log');
drop foreign table if exists sm_fleet_vehicle_cost;	create FOREIGN TABLE sm_fleet_vehicle_cost("id" int4 NOT NULL
	,"name" varchar
	,"vehicle_id" int4 NOT NULL
	,"cost_subtype_id" int4
	,"amount" float8
	,"cost_type" varchar NOT NULL
	,"parent_id" int4
	,"odometer_id" int4
	,"date" date
	,"contract_id" int4
	,"auto_generated" bool
	,"description" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'fleet_vehicle_cost');
drop foreign table if exists sm_fleet_vehicle_log_contract;	create FOREIGN TABLE sm_fleet_vehicle_log_contract("id" int4 NOT NULL
	,"name" text
	,"active" bool
	,"start_date" date
	,"expiration_date" date
	,"insurer_id" int4
	,"purchaser_id" int4
	,"ins_ref" varchar(64)
	,"state" varchar
	,"notes" text
	,"cost_generated" float8
	,"cost_frequency" varchar NOT NULL
	,"cost_id" int4 NOT NULL
	,"cost_amount" float8
	,"odometer" float8
	,"message_last_post" timestamp
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"message_main_attachment_id" int4
	,"user_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'fleet_vehicle_log_contract');
drop foreign table if exists sm_fleet_vehicle_log_fuel;	create FOREIGN TABLE sm_fleet_vehicle_log_fuel("id" int4 NOT NULL
	,"liter" float8
	,"price_per_liter" float8
	,"purchaser_id" int4
	,"inv_ref" varchar(64)
	,"vendor_id" int4
	,"notes" text
	,"cost_id" int4 NOT NULL
	,"cost_amount" float8
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'fleet_vehicle_log_fuel');
drop foreign table if exists sm_fleet_vehicle_log_services;	create FOREIGN TABLE sm_fleet_vehicle_log_services("id" int4 NOT NULL
	,"purchaser_id" int4
	,"inv_ref" varchar
	,"vendor_id" int4
	,"cost_amount" float8
	,"notes" text
	,"cost_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"related_member_id" int4
	,"related_invoice_id" int4
	,"related_reward_id" int4
	,"related_task_id" int4
	,"related_ticket_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'fleet_vehicle_log_services');
drop foreign table if exists sm_fleet_vehicle_model;	create FOREIGN TABLE sm_fleet_vehicle_model("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"brand_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'fleet_vehicle_model');
drop foreign table if exists sm_fleet_vehicle_model_brand;	create FOREIGN TABLE sm_fleet_vehicle_model_brand("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'fleet_vehicle_model_brand');
drop foreign table if exists sm_fleet_vehicle_model_vendors;	create FOREIGN TABLE sm_fleet_vehicle_model_vendors("model_id" int4 NOT NULL
	,"partner_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'fleet_vehicle_model_vendors');
drop foreign table if exists sm_fleet_vehicle_odometer;	create FOREIGN TABLE sm_fleet_vehicle_odometer("id" int4 NOT NULL
	,"name" varchar
	,"date" date
	,"value" float8
	,"vehicle_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'fleet_vehicle_odometer');
drop foreign table if exists sm_fleet_vehicle_state;	create FOREIGN TABLE sm_fleet_vehicle_state("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"sequence" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'fleet_vehicle_state');
drop foreign table if exists sm_fleet_vehicle_tag;	create FOREIGN TABLE sm_fleet_vehicle_tag("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"color" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'fleet_vehicle_tag');
drop foreign table if exists sm_fleet_vehicle_vehicle_tag_rel;	create FOREIGN TABLE sm_fleet_vehicle_vehicle_tag_rel("vehicle_tag_id" int4 NOT NULL
	,"tag_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'fleet_vehicle_vehicle_tag_rel');
drop foreign table if exists sm_general_ledger_report_wizard;	create FOREIGN TABLE sm_general_ledger_report_wizard("id" int4 NOT NULL
	,"company_id" int4
	,"date_range_id" int4
	,"date_from" date NOT NULL
	,"date_to" date NOT NULL
	,"target_move" varchar NOT NULL
	,"centralize" bool
	,"hide_account_at_0" bool
	,"show_analytic_tags" bool
	,"receivable_accounts_only" bool
	,"payable_accounts_only" bool
	,"not_only_one_unaffected_earnings_account" bool
	,"foreign_currency" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"partner_ungrouped" bool)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'general_ledger_report_wizard');
drop foreign table if exists sm_general_ledger_report_wizard_res_partner_rel;	create FOREIGN TABLE sm_general_ledger_report_wizard_res_partner_rel("general_ledger_report_wizard_id" int4 NOT NULL
	,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'general_ledger_report_wizard_res_partner_rel');
drop foreign table if exists sm_google_service;	create FOREIGN TABLE sm_google_service("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'google_service');
drop foreign table if exists sm_helpdesk_ticket;	create FOREIGN TABLE sm_helpdesk_ticket("id" int4 NOT NULL
	,"access_token" varchar
	,"message_main_attachment_id" int4
	,"active" bool
	,"number" varchar
	,"name" varchar NOT NULL
	,"description" text NOT NULL
	,"user_id" int4
	,"stage_id" int4
	,"partner_id" int4
	,"partner_name" varchar
	,"partner_email" varchar
	,"last_stage_update" timestamp
	,"assigned_date" timestamp
	,"closed_date" timestamp
	,"unattended" bool
	,"company_id" int4
	,"channel_id" int4
	,"category_id" int4
	,"team_id" int4
	,"priority" varchar
	,"color" int4
	,"kanban_state" varchar
	,"sequence" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"motive_id" int4
	,"type_id" int4
	,"project_id" int4
	,"task_id" int4
	,"planned_hours" float8
	,"progress" float8
	,"remaining_hours" float8
	,"total_hours" float8
	,"last_timesheet_activity" date
	,"cs_ticket_type" varchar
	,"cs_car_id" int4
	,"cs_carconfig_id" int4
	,"cs_pu_id" int4
	,"cs_community_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'helpdesk_ticket');
drop foreign table if exists sm_helpdesk_ticket_category;	create FOREIGN TABLE sm_helpdesk_ticket_category("id" int4 NOT NULL
	,"active" bool
	,"name" varchar NOT NULL
	,"company_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'helpdesk_ticket_category');
drop foreign table if exists sm_helpdesk_ticket_category_helpdesk_ticket_team_rel;	create FOREIGN TABLE sm_helpdesk_ticket_category_helpdesk_ticket_team_rel("helpdesk_ticket_team_id" int4 NOT NULL
	,"helpdesk_ticket_category_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'helpdesk_ticket_category_helpdesk_ticket_team_rel');
drop foreign table if exists sm_helpdesk_ticket_channel;	create FOREIGN TABLE sm_helpdesk_ticket_channel("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"active" bool
	,"company_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'helpdesk_ticket_channel');
drop foreign table if exists sm_helpdesk_ticket_helpdesk_ticket_tag_rel;	create FOREIGN TABLE sm_helpdesk_ticket_helpdesk_ticket_tag_rel("helpdesk_ticket_id" int4 NOT NULL
	,"helpdesk_ticket_tag_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'helpdesk_ticket_helpdesk_ticket_tag_rel');
drop foreign table if exists sm_helpdesk_ticket_motive;	create FOREIGN TABLE sm_helpdesk_ticket_motive("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"team_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'helpdesk_ticket_motive');
drop foreign table if exists sm_helpdesk_ticket_stage;	create FOREIGN TABLE sm_helpdesk_ticket_stage("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"description" text
	,"sequence" int4
	,"active" bool
	,"unattended" bool
	,"closed" bool
	,"portal_user_can_close" bool
	,"mail_template_id" int4
	,"fold" bool
	,"company_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'helpdesk_ticket_stage');
drop foreign table if exists sm_helpdesk_ticket_tag;	create FOREIGN TABLE sm_helpdesk_ticket_tag("id" int4 NOT NULL
	,"name" varchar
	,"color" int4
	,"active" bool
	,"company_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'helpdesk_ticket_tag');
drop foreign table if exists sm_helpdesk_ticket_team;	create FOREIGN TABLE sm_helpdesk_ticket_team("id" int4 NOT NULL
	,"message_main_attachment_id" int4
	,"name" varchar NOT NULL
	,"active" bool
	,"company_id" int4
	,"alias_id" int4 NOT NULL
	,"color" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"allow_timesheet" bool
	,"default_project_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'helpdesk_ticket_team');
drop foreign table if exists sm_helpdesk_ticket_team_helpdesk_ticket_type_rel;	create FOREIGN TABLE sm_helpdesk_ticket_team_helpdesk_ticket_type_rel("helpdesk_ticket_type_id" int4 NOT NULL
	,"helpdesk_ticket_team_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'helpdesk_ticket_team_helpdesk_ticket_type_rel');
drop foreign table if exists sm_helpdesk_ticket_team_res_users_rel;	create FOREIGN TABLE sm_helpdesk_ticket_team_res_users_rel("helpdesk_ticket_team_id" int4 NOT NULL
	,"res_users_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'helpdesk_ticket_team_res_users_rel');
drop foreign table if exists sm_helpdesk_ticket_type;	create FOREIGN TABLE sm_helpdesk_ticket_type("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'helpdesk_ticket_type');
drop foreign table if exists sm_hr_contract;	create FOREIGN TABLE sm_hr_contract("id" int4 NOT NULL
	,"message_last_post" timestamp
	,"name" varchar NOT NULL
	,"employee_id" int4
	,"department_id" int4
	,"type_id" int4 NOT NULL
	,"job_id" int4
	,"date_start" date NOT NULL
	,"date_end" date
	,"trial_date_end" date
	,"resource_calendar_id" int4 NOT NULL
	,"wage" numeric NOT NULL
	,"advantages" text
	,"notes" text
	,"state" varchar
	,"company_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"struct_id" int4
	,"schedule_pay" varchar
	,"message_main_attachment_id" int4
	,"active" bool
	,"reported_to_secretariat" bool)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'hr_contract');
drop foreign table if exists sm_hr_contract_advantage_template;	create FOREIGN TABLE sm_hr_contract_advantage_template("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"code" varchar NOT NULL
	,"lower_bound" float8
	,"upper_bound" float8
	,"default_value" float8
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'hr_contract_advantage_template');
drop foreign table if exists sm_hr_contract_type;	create FOREIGN TABLE sm_hr_contract_type("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"sequence" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'hr_contract_type');
drop foreign table if exists sm_hr_contribution_register;	create FOREIGN TABLE sm_hr_contribution_register("id" int4 NOT NULL
	,"company_id" int4
	,"partner_id" int4
	,"name" varchar NOT NULL
	,"note" text
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'hr_contribution_register');
drop foreign table if exists sm_hr_department;	create FOREIGN TABLE sm_hr_department("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"complete_name" varchar
	,"active" bool
	,"company_id" int4
	,"parent_id" int4
	,"manager_id" int4
	,"note" text
	,"color" int4
	,"message_last_post" timestamp
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"message_main_attachment_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'hr_department');
drop foreign table if exists sm_hr_department_mail_channel_rel;	create FOREIGN TABLE sm_hr_department_mail_channel_rel("mail_channel_id" int4 NOT NULL
	,"hr_department_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'hr_department_mail_channel_rel');
drop foreign table if exists sm_hr_employee;	create FOREIGN TABLE sm_hr_employee("id" int4 NOT NULL
	,"name" varchar
	,"active" bool
	,"address_home_id" int4
	,"country_id" int4
	,"gender" varchar
	,"marital" varchar
	,"birthday" date
	,"ssnid" varchar
	,"sinid" varchar
	,"identification_id" varchar
	,"passport_id" varchar
	,"bank_account_id" int4
	,"permit_no" varchar
	,"visa_no" varchar
	,"visa_expire" date
	,"address_id" int4
	,"work_phone" varchar
	,"mobile_phone" varchar
	,"work_email" varchar
	,"work_location" varchar
	,"job_id" int4
	,"department_id" int4
	,"parent_id" int4
	,"coach_id" int4
	,"notes" text
	,"color" int4
	,"resource_id" int4 NOT NULL
	,"message_last_post" timestamp
	,"company_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"manager" bool
	,"medic_exam" date
	,"place_of_birth" varchar
	,"children" int4
	,"vehicle" varchar
	,"km_home_work" int4
	,"message_main_attachment_id" int4
	,"user_id" int4
	,"spouse_complete_name" varchar
	,"spouse_birthdate" date
	,"country_of_birth" int4
	,"additional_note" text
	,"certificate" varchar
	,"study_field" varchar
	,"study_school" varchar
	,"emergency_contact" varchar
	,"emergency_phone" varchar
	,"google_drive_link" varchar
	,"job_title" varchar
	,"resource_calendar_id" int4
	,"expense_manager_id" int4
	,"timesheet_cost" numeric)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'hr_employee');
drop foreign table if exists sm_hr_employee_category;	create FOREIGN TABLE sm_hr_employee_category("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"color" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'hr_employee_category');
drop foreign table if exists sm_hr_employee_group_rel;	create FOREIGN TABLE sm_hr_employee_group_rel("payslip_id" int4 NOT NULL
	,"employee_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'hr_employee_group_rel');
drop foreign table if exists sm_hr_expense;	create FOREIGN TABLE sm_hr_expense("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"date" date
	,"employee_id" int4 NOT NULL
	,"product_id" int4 NOT NULL
	,"product_uom_id" int4 NOT NULL
	,"unit_amount" numeric NOT NULL
	,"quantity" numeric NOT NULL
	,"untaxed_amount" numeric
	,"total_amount" numeric
	,"company_id" int4
	,"currency_id" int4
	,"analytic_account_id" int4
	,"account_id" int4
	,"description" text
	,"payment_mode" varchar
	,"state" varchar
	,"sheet_id" int4
	,"reference" varchar
	,"is_refused" bool
	,"message_last_post" timestamp
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"sale_order_id" int4
	,"message_main_attachment_id" int4
	,"company_currency_id" int4
	,"total_amount_company" numeric)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'hr_expense');
drop foreign table if exists sm_hr_expense_hr_expense_refuse_wizard_rel;	create FOREIGN TABLE sm_hr_expense_hr_expense_refuse_wizard_rel("hr_expense_refuse_wizard_id" int4 NOT NULL
	,"hr_expense_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'hr_expense_hr_expense_refuse_wizard_rel');
drop foreign table if exists sm_hr_expense_refuse_wizard;	create FOREIGN TABLE sm_hr_expense_refuse_wizard("id" int4 NOT NULL
	,"reason" varchar NOT NULL
	,"hr_expense_sheet_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'hr_expense_refuse_wizard');
drop foreign table if exists sm_hr_expense_sheet;	create FOREIGN TABLE sm_hr_expense_sheet("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"state" varchar NOT NULL
	,"employee_id" int4 NOT NULL
	,"address_id" int4
	,"user_id" int4
	,"total_amount" numeric
	,"company_id" int4
	,"currency_id" int4
	,"journal_id" int4
	,"bank_journal_id" int4
	,"accounting_date" date
	,"account_move_id" int4
	,"department_id" int4
	,"message_last_post" timestamp
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"message_main_attachment_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'hr_expense_sheet');
drop foreign table if exists sm_hr_expense_sheet_register_payment_wizard;	create FOREIGN TABLE sm_hr_expense_sheet_register_payment_wizard("id" int4 NOT NULL
	,"partner_id" int4 NOT NULL
	,"journal_id" int4 NOT NULL
	,"payment_method_id" int4 NOT NULL
	,"amount" numeric NOT NULL
	,"currency_id" int4 NOT NULL
	,"payment_date" date NOT NULL
	,"communication" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"partner_bank_account_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'hr_expense_sheet_register_payment_wizard');
drop foreign table if exists sm_hr_holidays;	create FOREIGN TABLE sm_hr_holidays("id" int4 NOT NULL
	,"name" varchar
	,"state" varchar
	,"payslip_status" bool
	,"report_note" text
	,"user_id" int4
	,"date_from" timestamp
	,"date_to" timestamp
	,"holiday_status_id" int4 NOT NULL
	,"employee_id" int4
	,"manager_id" int4
	,"notes" text
	,"number_of_days" float8
	,"openupgrade_legacy_12_0_number_of_days" float8
	,"meeting_id" int4
	,"openupgrade_legacy_12_0_type" varchar NOT NULL
	,"parent_id" int4
	,"department_id" int4
	,"category_id" int4
	,"holiday_type" varchar NOT NULL
	,"first_approver_id" int4
	,"second_approver_id" int4
	,"message_last_post" timestamp
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'hr_holidays');
drop foreign table if exists sm_hr_holidays_remaining_leaves_user;	create FOREIGN TABLE sm_hr_holidays_remaining_leaves_user("id" int4
	,"name" varchar
	,"no_of_leaves" float8
	,"user_id" int4
	,"leave_type" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'hr_holidays_remaining_leaves_user');
drop foreign table if exists sm_hr_holidays_summary_dept;	create FOREIGN TABLE sm_hr_holidays_summary_dept("id" int4 NOT NULL
	,"date_from" date NOT NULL
	,"holiday_type" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'hr_holidays_summary_dept');
drop foreign table if exists sm_hr_holidays_summary_employee;	create FOREIGN TABLE sm_hr_holidays_summary_employee("id" int4 NOT NULL
	,"date_from" date NOT NULL
	,"holiday_type" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'hr_holidays_summary_employee');
drop foreign table if exists sm_hr_job;	create FOREIGN TABLE sm_hr_job("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"expected_employees" int4
	,"no_of_employee" int4
	,"no_of_recruitment" int4
	,"no_of_hired_employee" int4
	,"description" text
	,"requirements" text
	,"department_id" int4
	,"company_id" int4
	,"state" varchar NOT NULL
	,"message_last_post" timestamp
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"message_main_attachment_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'hr_job');
drop foreign table if exists sm_hr_leave;	create FOREIGN TABLE sm_hr_leave("id" int4 NOT NULL
	,"message_main_attachment_id" int4
	,"name" varchar
	,"state" varchar
	,"payslip_status" bool
	,"report_note" text
	,"user_id" int4
	,"holiday_status_id" int4 NOT NULL
	,"employee_id" int4
	,"manager_id" int4
	,"department_id" int4
	,"notes" text
	,"date_from" timestamp NOT NULL
	,"date_to" timestamp NOT NULL
	,"number_of_days" float8
	,"meeting_id" int4
	,"parent_id" int4
	,"holiday_type" varchar NOT NULL
	,"category_id" int4
	,"mode_company_id" int4
	,"first_approver_id" int4
	,"second_approver_id" int4
	,"request_date_from" date
	,"request_date_to" date
	,"request_hour_from" int4
	,"request_hour_to" int4
	,"request_date_from_period" varchar
	,"request_unit_half" bool
	,"request_unit_hours" bool
	,"request_unit_custom" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'hr_leave');
drop foreign table if exists sm_hr_leave_allocation;	create FOREIGN TABLE sm_hr_leave_allocation("id" int4 NOT NULL
	,"message_main_attachment_id" int4
	,"name" varchar
	,"state" varchar
	,"date_from" timestamp
	,"date_to" timestamp
	,"holiday_status_id" int4 NOT NULL
	,"employee_id" int4
	,"notes" text
	,"number_of_days" float8
	,"parent_id" int4
	,"first_approver_id" int4
	,"second_approver_id" int4
	,"holiday_type" varchar NOT NULL
	,"mode_company_id" int4
	,"department_id" int4
	,"category_id" int4
	,"accrual" bool
	,"accrual_limit" int4
	,"number_per_interval" float8
	,"interval_number" int4
	,"unit_per_interval" varchar
	,"interval_unit" varchar
	,"nextcall" date
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'hr_leave_allocation');
drop foreign table if exists sm_hr_leave_report;	create FOREIGN TABLE sm_hr_leave_report("id" int8
	,"employee_id" int4
	,"name" varchar
	,"number_of_days" float8
	,"type" text
	,"category_id" int4
	,"department_id" int4
	,"holiday_status_id" int4
	,"state" varchar
	,"holiday_type" varchar
	,"date_from" timestamp
	,"date_to" timestamp
	,"payslip_status" bool)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'hr_leave_report');
drop foreign table if exists sm_hr_leave_type;	create FOREIGN TABLE sm_hr_leave_type("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"categ_id" int4
	,"color_name" varchar NOT NULL
	,"openupgrade_legacy_12_0_limit" bool
	,"active" bool
	,"openupgrade_legacy_12_0_double_validation" bool
	,"company_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"validity_start" date
	,"sequence" int4
	,"validation_type" varchar
	,"allocation_type" varchar
	,"validity_stop" date
	,"time_type" varchar
	,"request_unit" varchar NOT NULL
	,"unpaid" bool
	,"timesheet_generate" bool
	,"timesheet_project_id" int4
	,"timesheet_task_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'hr_leave_type');
drop foreign table if exists sm_hr_payroll_structure;	create FOREIGN TABLE sm_hr_payroll_structure("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"code" varchar NOT NULL
	,"company_id" int4 NOT NULL
	,"note" text
	,"parent_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'hr_payroll_structure');
drop foreign table if exists sm_hr_payslip;	create FOREIGN TABLE sm_hr_payslip("id" int4 NOT NULL
	,"struct_id" int4
	,"name" varchar
	,"number" varchar
	,"employee_id" int4 NOT NULL
	,"date_from" date NOT NULL
	,"date_to" date NOT NULL
	,"state" varchar
	,"company_id" int4
	,"paid" bool
	,"note" text
	,"contract_id" int4
	,"credit_note" bool
	,"payslip_run_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'hr_payslip');
drop foreign table if exists sm_hr_payslip_employees;	create FOREIGN TABLE sm_hr_payslip_employees("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'hr_payslip_employees');
drop foreign table if exists sm_hr_payslip_input;	create FOREIGN TABLE sm_hr_payslip_input("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"payslip_id" int4 NOT NULL
	,"sequence" int4 NOT NULL
	,"code" varchar NOT NULL
	,"amount" float8
	,"contract_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'hr_payslip_input');
drop foreign table if exists sm_hr_payslip_line;	create FOREIGN TABLE sm_hr_payslip_line("id" int4 NOT NULL
	,"slip_id" int4 NOT NULL
	,"salary_rule_id" int4 NOT NULL
	,"employee_id" int4 NOT NULL
	,"contract_id" int4 NOT NULL
	,"rate" numeric
	,"amount" numeric
	,"quantity" numeric
	,"total" numeric
	,"name" varchar NOT NULL
	,"code" varchar NOT NULL
	,"sequence" int4 NOT NULL
	,"category_id" int4 NOT NULL
	,"active" bool
	,"appears_on_payslip" bool
	,"parent_rule_id" int4
	,"company_id" int4
	,"condition_select" varchar NOT NULL
	,"condition_range" varchar
	,"condition_python" text NOT NULL
	,"condition_range_min" float8
	,"condition_range_max" float8
	,"amount_select" varchar NOT NULL
	,"amount_fix" numeric
	,"amount_percentage" numeric
	,"amount_python_compute" text
	,"amount_percentage_base" varchar
	,"register_id" int4
	,"note" text
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'hr_payslip_line');
drop foreign table if exists sm_hr_payslip_run;	create FOREIGN TABLE sm_hr_payslip_run("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"state" varchar
	,"date_start" date NOT NULL
	,"date_end" date NOT NULL
	,"credit_note" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'hr_payslip_run');
drop foreign table if exists sm_hr_payslip_worked_days;	create FOREIGN TABLE sm_hr_payslip_worked_days("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"payslip_id" int4 NOT NULL
	,"sequence" int4 NOT NULL
	,"code" varchar NOT NULL
	,"number_of_days" float8
	,"number_of_hours" float8
	,"contract_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'hr_payslip_worked_days');
drop foreign table if exists sm_hr_rule_input;	create FOREIGN TABLE sm_hr_rule_input("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"code" varchar NOT NULL
	,"input_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'hr_rule_input');
drop foreign table if exists sm_hr_salary_rule;	create FOREIGN TABLE sm_hr_salary_rule("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"code" varchar NOT NULL
	,"sequence" int4 NOT NULL
	,"quantity" varchar
	,"category_id" int4 NOT NULL
	,"active" bool
	,"appears_on_payslip" bool
	,"parent_rule_id" int4
	,"company_id" int4
	,"condition_select" varchar NOT NULL
	,"condition_range" varchar
	,"condition_python" text NOT NULL
	,"condition_range_min" float8
	,"condition_range_max" float8
	,"amount_select" varchar NOT NULL
	,"amount_fix" numeric
	,"amount_percentage" numeric
	,"amount_python_compute" text
	,"amount_percentage_base" varchar
	,"register_id" int4
	,"note" text
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'hr_salary_rule');
drop foreign table if exists sm_hr_salary_rule_category;	create FOREIGN TABLE sm_hr_salary_rule_category("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"code" varchar NOT NULL
	,"parent_id" int4
	,"note" text
	,"company_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'hr_salary_rule_category');
drop foreign table if exists sm_hr_structure_salary_rule_rel;	create FOREIGN TABLE sm_hr_structure_salary_rule_rel("struct_id" int4 NOT NULL
	,"rule_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'hr_structure_salary_rule_rel');
drop foreign table if exists sm_hr_timesheet_switch;	create FOREIGN TABLE sm_hr_timesheet_switch("id" int4 NOT NULL
	,"running_timer_id" int4
	,"name" varchar NOT NULL
	,"date" date NOT NULL
	,"amount" numeric NOT NULL
	,"unit_amount" float8
	,"product_uom_id" int4
	,"account_id" int4 NOT NULL
	,"partner_id" int4
	,"user_id" int4
	,"company_id" int4 NOT NULL
	,"currency_id" int4
	,"group_id" int4
	,"product_id" int4
	,"general_account_id" int4
	,"move_id" int4
	,"code" varchar(8)
	,"ref" varchar
	,"account_department_id" int4
	,"so_line" int4
	,"task_id" int4
	,"project_id" int4
	,"employee_id" int4
	,"department_id" int4
	,"ticket_id" int4
	,"ticket_partner_id" int4
	,"holiday_id" int4
	,"date_time" timestamp
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"timesheet_invoice_type" varchar
	,"timesheet_invoice_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'hr_timesheet_switch');
drop foreign table if exists sm_iap_account;	create FOREIGN TABLE sm_iap_account("id" int4 NOT NULL
	,"service_name" varchar
	,"account_token" varchar
	,"company_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'iap_account');
drop foreign table if exists sm_ir_act_client;	create FOREIGN TABLE sm_ir_act_client("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"help" text
	,"write_uid" int4
	,"write_date" timestamp
	,"openupgrade_legacy_11_0_usage" varchar
	,"type" varchar NOT NULL
	,"name" varchar NOT NULL
	,"binding_model_id" int4
	,"binding_type" varchar NOT NULL
	,"res_model" varchar
	,"params_store" bytea
	,"tag" varchar NOT NULL
	,"context" varchar NOT NULL
	,"target" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'ir_act_client');
drop foreign table if exists sm_ir_act_report_xml;	create FOREIGN TABLE sm_ir_act_report_xml("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"help" text
	,"write_uid" int4
	,"write_date" timestamp
	,"openupgrade_legacy_11_0_usage" varchar
	,"type" varchar NOT NULL
	,"name" varchar NOT NULL
	,"binding_model_id" int4
	,"binding_type" varchar NOT NULL
	,"parser" varchar
	,"header" bool
	,"report_type" varchar NOT NULL
	,"ir_values_id" int4
	,"attachment" varchar
	,"report_sxw_content_data" bytea
	,"report_xml" varchar
	,"report_rml_content_data" bytea
	,"auto" bool
	,"report_file" varchar
	,"multi" bool
	,"report_xsl" varchar
	,"report_rml" varchar
	,"report_name" varchar NOT NULL
	,"attachment_use" bool
	,"model" varchar NOT NULL
	,"paperformat_id" int4
	,"print_report_name" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'ir_act_report_xml');
drop foreign table if exists sm_ir_act_server;	create FOREIGN TABLE sm_ir_act_server("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"help" text
	,"write_uid" int4
	,"write_date" timestamp
	,"openupgrade_legacy_11_0_usage" varchar
	,"type" varchar NOT NULL
	,"name" varchar NOT NULL
	,"binding_model_id" int4
	,"binding_type" varchar NOT NULL
	,"code" text
	,"sequence" int4
	,"crud_model_id" int4
	,"ref_object" varchar(128)
	,"id_object" varchar(128)
	,"crud_model_name" varchar
	,"use_relational_model" varchar
	,"use_create" varchar
	,"wkf_field_id" int4
	,"wkf_model_id" int4
	,"state" varchar NOT NULL
	,"id_value" varchar
	,"action_id" int4
	,"model_id" int4 NOT NULL
	,"sub_model_object_field" int4
	,"link_new_record" bool
	,"wkf_transition_id" int4
	,"sub_object" int4
	,"use_write" varchar
	,"condition" varchar
	,"copyvalue" varchar
	,"write_expression" varchar
	,"wkf_model_name" varchar
	,"menu_ir_values_id" int4
	,"model_object_field" int4
	,"link_field_id" int4
	,"template_id" int4
	,"usage" varchar NOT NULL
	,"model_name" varchar
	,"activity_type_id" int4
	,"activity_summary" varchar
	,"activity_note" text
	,"activity_date_deadline_range" int4
	,"activity_date_deadline_range_type" varchar
	,"activity_user_type" varchar NOT NULL
	,"activity_user_id" int4
	,"activity_user_field_name" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'ir_act_server');
drop foreign table if exists sm_ir_act_server_mail_channel_rel;	create FOREIGN TABLE sm_ir_act_server_mail_channel_rel("ir_act_server_id" int4 NOT NULL
	,"mail_channel_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'ir_act_server_mail_channel_rel');
drop foreign table if exists sm_ir_act_server_res_partner_rel;	create FOREIGN TABLE sm_ir_act_server_res_partner_rel("ir_act_server_id" int4 NOT NULL
	,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'ir_act_server_res_partner_rel');
drop foreign table if exists sm_ir_act_url;	create FOREIGN TABLE sm_ir_act_url("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"help" text
	,"write_uid" int4
	,"write_date" timestamp
	,"openupgrade_legacy_11_0_usage" varchar
	,"type" varchar NOT NULL
	,"name" varchar NOT NULL
	,"binding_model_id" int4
	,"binding_type" varchar NOT NULL
	,"target" varchar NOT NULL
	,"url" text NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'ir_act_url');
drop foreign table if exists sm_ir_act_window;	create FOREIGN TABLE sm_ir_act_window("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"help" text
	,"write_uid" int4
	,"write_date" timestamp
	,"openupgrade_legacy_11_0_usage" varchar
	,"type" varchar NOT NULL
	,"name" varchar NOT NULL
	,"binding_model_id" int4
	,"binding_type" varchar NOT NULL
	,"domain" varchar
	,"res_model" varchar NOT NULL
	,"search_view_id" int4
	,"view_type" varchar NOT NULL
	,"src_model" varchar
	,"res_id" int4
	,"view_id" int4
	,"auto_refresh" int4
	,"view_mode" varchar NOT NULL
	,"multi" bool
	,"target" varchar
	,"auto_search" bool
	,"filter" bool
	,"limit" int4
	,"context" varchar NOT NULL
	,"openupgrade_legacy_10_0_target" varchar
	,"usage" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'ir_act_window');
drop foreign table if exists sm_ir_act_window_group_rel;	create FOREIGN TABLE sm_ir_act_window_group_rel("act_id" int4 NOT NULL
	,"gid" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'ir_act_window_group_rel');
drop foreign table if exists sm_ir_act_window_view;	create FOREIGN TABLE sm_ir_act_window_view("id" int4 NOT NULL
	,"create_uid" int4
	,"multi" bool
	,"create_date" timestamp
	,"sequence" int4
	,"view_id" int4
	,"write_uid" int4
	,"view_mode" varchar NOT NULL
	,"write_date" timestamp
	,"act_window_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'ir_act_window_view');
drop foreign table if exists sm_ir_actions;	create FOREIGN TABLE sm_ir_actions("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"help" text
	,"write_uid" int4
	,"write_date" timestamp
	,"openupgrade_legacy_11_0_usage" varchar
	,"type" varchar NOT NULL
	,"name" varchar NOT NULL
	,"binding_model_id" int4
	,"binding_type" varchar NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'ir_actions');
drop foreign table if exists sm_ir_actions_todo;	create FOREIGN TABLE sm_ir_actions_todo("id" int4 NOT NULL
	,"create_date" timestamp
	,"name" varchar
	,"sequence" int4
	,"write_uid" int4
	,"note" text
	,"state" varchar NOT NULL
	,"write_date" timestamp
	,"type" varchar
	,"create_uid" int4
	,"action_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'ir_actions_todo');
drop foreign table if exists sm_ir_attachment;	create FOREIGN TABLE sm_ir_attachment("id" int4 NOT NULL
	,"create_date" timestamp
	,"write_date" timestamp
	,"res_model" varchar
	,"write_uid" int4
	,"res_name" varchar
	,"db_datas" bytea
	,"file_size" int4
	,"create_uid" int4
	,"company_id" int4
	,"index_content" text
	,"type" varchar NOT NULL
	,"public" bool
	,"store_fname" varchar
	,"description" text
	,"res_field" varchar
	,"mimetype" varchar
	,"name" varchar NOT NULL
	,"url" varchar(1024)
	,"res_id" int4
	,"checksum" varchar(40)
	,"datas_fname" varchar
	,"access_token" varchar
	,"res_model_name" varchar
	,"active" bool)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'ir_attachment');
drop foreign table if exists sm_ir_autovacuum;	create FOREIGN TABLE sm_ir_autovacuum("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_date" timestamp
	,"write_uid" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'ir_autovacuum');
drop foreign table if exists sm_ir_config_parameter;	create FOREIGN TABLE sm_ir_config_parameter("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_date" timestamp
	,"value" text NOT NULL
	,"write_uid" int4
	,"key" varchar NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'ir_config_parameter');
drop foreign table if exists sm_ir_config_parameter_groups_rel;	create FOREIGN TABLE sm_ir_config_parameter_groups_rel("icp_id" int4 NOT NULL
	,"group_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'ir_config_parameter_groups_rel');
drop foreign table if exists sm_ir_cron;	create FOREIGN TABLE sm_ir_cron("id" int4 NOT NULL
	,"function" varchar
	,"create_uid" int4
	,"args" text
	,"create_date" timestamp
	,"name" varchar
	,"interval_type" varchar
	,"numbercall" int4
	,"nextcall" timestamp NOT NULL
	,"priority" int4
	,"doall" bool
	,"write_date" timestamp
	,"active" bool
	,"user_id" int4 NOT NULL
	,"model" varchar
	,"write_uid" int4
	,"interval_number" int4
	,"ir_actions_server_id" int4 NOT NULL
	,"cron_name" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'ir_cron');
drop foreign table if exists sm_ir_default;	create FOREIGN TABLE sm_ir_default("id" int4 NOT NULL
	,"field_id" int4 NOT NULL
	,"user_id" int4
	,"company_id" int4
	,"condition" varchar
	,"json_value" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'ir_default');
drop foreign table if exists sm_ir_demo;	create FOREIGN TABLE sm_ir_demo("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'ir_demo');
drop foreign table if exists sm_ir_demo_failure;	create FOREIGN TABLE sm_ir_demo_failure("id" int4 NOT NULL
	,"module_id" int4 NOT NULL
	,"error" varchar
	,"wizard_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'ir_demo_failure');
drop foreign table if exists sm_ir_demo_failure_wizard;	create FOREIGN TABLE sm_ir_demo_failure_wizard("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'ir_demo_failure_wizard');
drop foreign table if exists sm_ir_exports;	create FOREIGN TABLE sm_ir_exports("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"name" varchar
	,"write_uid" int4
	,"write_date" timestamp
	,"resource" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'ir_exports');
drop foreign table if exists sm_ir_exports_line;	create FOREIGN TABLE sm_ir_exports_line("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"name" varchar
	,"write_uid" int4
	,"write_date" timestamp
	,"export_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'ir_exports_line');
drop foreign table if exists sm_ir_filters;	create FOREIGN TABLE sm_ir_filters("id" int4 NOT NULL
	,"sort" text NOT NULL
	,"model_id" varchar NOT NULL
	,"domain" text NOT NULL
	,"create_date" timestamp
	,"name" varchar NOT NULL
	,"create_uid" int4
	,"write_uid" int4
	,"is_default" bool
	,"active" bool
	,"context" text NOT NULL
	,"write_date" timestamp
	,"user_id" int4
	,"action_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'ir_filters');
drop foreign table if exists sm_ir_logging;	create FOREIGN TABLE sm_ir_logging("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"name" varchar NOT NULL
	,"level" varchar
	,"line" varchar NOT NULL
	,"dbname" varchar
	,"write_uid" int4
	,"func" varchar NOT NULL
	,"write_date" timestamp
	,"path" varchar NOT NULL
	,"message" text NOT NULL
	,"type" varchar NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'ir_logging');
drop foreign table if exists sm_ir_mail_server;	create FOREIGN TABLE sm_ir_mail_server("id" int4 NOT NULL
	,"create_uid" int4
	,"smtp_encryption" varchar NOT NULL
	,"create_date" timestamp
	,"name" varchar NOT NULL
	,"sequence" int4
	,"smtp_port" int4 NOT NULL
	,"smtp_host" varchar NOT NULL
	,"write_uid" int4
	,"smtp_pass" varchar
	,"smtp_debug" bool
	,"write_date" timestamp
	,"active" bool
	,"smtp_user" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'ir_mail_server');
drop foreign table if exists sm_ir_model;	create FOREIGN TABLE sm_ir_model("id" int4 NOT NULL
	,"model" varchar NOT NULL
	,"name" varchar NOT NULL
	,"state" varchar
	,"info" text
	,"transient" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"is_mail_thread" bool)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'ir_model');
drop foreign table if exists sm_ir_model_access;	create FOREIGN TABLE sm_ir_model_access("id" int4 NOT NULL
	,"model_id" int4 NOT NULL
	,"perm_read" bool
	,"name" varchar NOT NULL
	,"create_uid" int4
	,"write_uid" int4
	,"active" bool
	,"write_date" timestamp
	,"perm_unlink" bool
	,"perm_write" bool
	,"create_date" timestamp
	,"perm_create" bool
	,"group_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'ir_model_access');
drop foreign table if exists sm_ir_model_constraint;	create FOREIGN TABLE sm_ir_model_constraint("id" int4 NOT NULL
	,"date_init" timestamp
	,"date_update" timestamp
	,"module" int4 NOT NULL
	,"model" int4 NOT NULL
	,"type" varchar(1) NOT NULL
	,"definition" varchar
	,"name" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'ir_model_constraint');
drop foreign table if exists sm_ir_model_data;	create FOREIGN TABLE sm_ir_model_data("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_date" timestamp
	,"write_uid" int4
	,"noupdate" bool
	,"name" varchar NOT NULL
	,"date_init" timestamp
	,"date_update" timestamp
	,"module" varchar NOT NULL
	,"model" varchar NOT NULL
	,"res_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'ir_model_data');
drop foreign table if exists sm_ir_model_fields;	create FOREIGN TABLE sm_ir_model_fields("id" int4 NOT NULL
	,"model" varchar NOT NULL
	,"model_id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"state" varchar NOT NULL
	,"field_description" varchar NOT NULL
	,"help" text
	,"ttype" varchar NOT NULL
	,"relation" varchar
	,"relation_field" varchar
	,"index" bool
	,"copied" bool
	,"related" varchar
	,"readonly" bool
	,"required" bool
	,"selectable" bool
	,"translate" bool
	,"serialization_field_id" int4
	,"relation_table" varchar
	,"column1" varchar
	,"column2" varchar
	,"domain" varchar
	,"selection" varchar
	,"create_date" timestamp
	,"on_delete" varchar
	,"write_uid" int4
	,"depends" varchar
	,"size" int4
	,"create_uid" int4
	,"complete_name" varchar
	,"compute" text
	,"write_date" timestamp
	,"store" bool
	,"track_visibility" varchar
	,"relation_field_id" int4
	,"related_field_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'ir_model_fields');
drop foreign table if exists sm_ir_model_fields_group_rel;	create FOREIGN TABLE sm_ir_model_fields_group_rel("field_id" int4 NOT NULL
	,"group_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'ir_model_fields_group_rel');
drop foreign table if exists sm_ir_model_fields_mis_report_query_rel;	create FOREIGN TABLE sm_ir_model_fields_mis_report_query_rel("mis_report_query_id" int4 NOT NULL
	,"ir_model_fields_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'ir_model_fields_mis_report_query_rel');
drop foreign table if exists sm_ir_model_relation;	create FOREIGN TABLE sm_ir_model_relation("id" int4 NOT NULL
	,"date_init" timestamp
	,"date_update" timestamp
	,"module" int4 NOT NULL
	,"model" int4 NOT NULL
	,"name" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'ir_model_relation');
drop foreign table if exists sm_ir_module_category;	create FOREIGN TABLE sm_ir_module_category("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_date" timestamp
	,"write_uid" int4
	,"parent_id" int4
	,"name" varchar NOT NULL
	,"description" text
	,"sequence" int4
	,"visible" bool
	,"exclusive" bool)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'ir_module_category');
drop foreign table if exists sm_ir_module_module;	create FOREIGN TABLE sm_ir_module_module("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_date" timestamp
	,"write_uid" int4
	,"website" varchar
	,"summary" varchar
	,"name" varchar NOT NULL
	,"author" varchar
	,"icon" varchar
	,"state" varchar
	,"latest_version" varchar
	,"shortdesc" varchar
	,"category_id" int4
	,"description" text
	,"application" bool
	,"demo" bool
	,"web" bool
	,"license" varchar
	,"sequence" int4
	,"auto_install" bool
	,"menus_by_module" text
	,"maintainer" varchar
	,"contributors" text
	,"views_by_module" text
	,"published_version" varchar
	,"reports_by_module" text
	,"url" varchar
	,"imported" bool
	,"to_buy" bool)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'ir_module_module');
drop foreign table if exists sm_ir_module_module_dependency;	create FOREIGN TABLE sm_ir_module_module_dependency("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_date" timestamp
	,"write_uid" int4
	,"name" varchar
	,"module_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'ir_module_module_dependency');
drop foreign table if exists sm_ir_module_module_exclusion;	create FOREIGN TABLE sm_ir_module_module_exclusion("id" int4 NOT NULL
	,"name" varchar
	,"module_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'ir_module_module_exclusion');
drop foreign table if exists sm_ir_property;	create FOREIGN TABLE sm_ir_property("id" int4 NOT NULL
	,"value_text" text
	,"value_float" float8
	,"create_date" timestamp
	,"name" varchar
	,"create_uid" int4
	,"type" varchar NOT NULL
	,"company_id" int4
	,"write_uid" int4
	,"fields_id" int4 NOT NULL
	,"value_datetime" timestamp
	,"value_binary" bytea
	,"write_date" timestamp
	,"value_reference" varchar
	,"value_integer" int4
	,"res_id" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'ir_property');
drop foreign table if exists sm_ir_rule;	create FOREIGN TABLE sm_ir_rule("id" int4 NOT NULL
	,"model_id" int4 NOT NULL
	,"domain_force" text
	,"name" varchar
	,"create_uid" int4
	,"global" bool
	,"write_uid" int4
	,"active" bool
	,"perm_read" bool
	,"perm_unlink" bool
	,"perm_write" bool
	,"create_date" timestamp
	,"perm_create" bool
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'ir_rule');
drop foreign table if exists sm_ir_sequence;	create FOREIGN TABLE sm_ir_sequence("id" int4 NOT NULL
	,"padding" int4 NOT NULL
	,"create_uid" int4
	,"code" varchar
	,"create_date" timestamp
	,"suffix" varchar
	,"number_next" int4 NOT NULL
	,"implementation" varchar NOT NULL
	,"company_id" int4
	,"write_uid" int4
	,"use_date_range" bool
	,"number_increment" int4 NOT NULL
	,"prefix" varchar
	,"write_date" timestamp
	,"active" bool
	,"name" varchar NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'ir_sequence');
drop foreign table if exists sm_ir_sequence_date_range;	create FOREIGN TABLE sm_ir_sequence_date_range("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"number_next" int4 NOT NULL
	,"date_from" date NOT NULL
	,"write_uid" int4
	,"sequence_id" int4 NOT NULL
	,"write_date" timestamp
	,"date_to" date NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'ir_sequence_date_range');
drop foreign table if exists sm_ir_server_object_lines;	create FOREIGN TABLE sm_ir_server_object_lines("id" int4 NOT NULL
	,"create_uid" int4
	,"server_id" int4
	,"create_date" timestamp
	,"value" text NOT NULL
	,"col1" int4 NOT NULL
	,"write_date" timestamp
	,"write_uid" int4
	,"type" varchar NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'ir_server_object_lines');
drop foreign table if exists sm_ir_translation;	create FOREIGN TABLE sm_ir_translation("id" int4 NOT NULL
	,"lang" varchar
	,"src" text
	,"name" varchar NOT NULL
	,"res_id" int4
	,"module" varchar
	,"state" varchar
	,"comments" text
	,"value" text
	,"type" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'ir_translation');
drop foreign table if exists sm_ir_ui_menu;	create FOREIGN TABLE sm_ir_ui_menu("id" int4 NOT NULL
	,"parent_left" int4
	,"parent_right" int4
	,"create_date" timestamp
	,"name" varchar NOT NULL
	,"web_icon" varchar
	,"sequence" int4
	,"write_uid" int4
	,"parent_id" int4
	,"write_date" timestamp
	,"action" varchar
	,"create_uid" int4
	,"active" bool
	,"load_xmlid" bool
	,"parent_path" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'ir_ui_menu');
drop foreign table if exists sm_ir_ui_menu_group_rel;	create FOREIGN TABLE sm_ir_ui_menu_group_rel("menu_id" int4 NOT NULL
	,"gid" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'ir_ui_menu_group_rel');
drop foreign table if exists sm_ir_ui_view;	create FOREIGN TABLE sm_ir_ui_view("id" int4 NOT NULL
	,"create_date" timestamp
	,"key" varchar
	,"write_uid" int4
	,"field_parent" varchar
	,"create_uid" int4
	,"model_data_id" int4
	,"priority" int4 NOT NULL
	,"type" varchar
	,"arch_db" text
	,"inherit_id" int4
	,"write_date" timestamp
	,"active" bool
	,"arch_fs" varchar
	,"name" varchar NOT NULL
	,"mode" varchar NOT NULL
	,"model" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'ir_ui_view');
drop foreign table if exists sm_ir_ui_view_custom;	create FOREIGN TABLE sm_ir_ui_view_custom("id" int4 NOT NULL
	,"create_uid" int4
	,"user_id" int4 NOT NULL
	,"write_uid" int4
	,"write_date" timestamp
	,"create_date" timestamp
	,"ref_id" int4 NOT NULL
	,"arch" text NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'ir_ui_view_custom');
drop foreign table if exists sm_ir_ui_view_group_rel;	create FOREIGN TABLE sm_ir_ui_view_group_rel("view_id" int4 NOT NULL
	,"group_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'ir_ui_view_group_rel');
drop foreign table if exists sm_ir_values;	create FOREIGN TABLE sm_ir_values("id" int4 NOT NULL
	,"model_id" int4
	,"create_date" timestamp
	,"key" varchar NOT NULL
	,"name" varchar NOT NULL
	,"create_uid" int4
	,"key2" varchar
	,"company_id" int4
	,"value" text
	,"write_uid" int4
	,"write_date" timestamp
	,"user_id" int4
	,"model" varchar NOT NULL
	,"res_id" int4
	,"action_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'ir_values');
drop foreign table if exists sm_journal_ledger_report_wizard;	create FOREIGN TABLE sm_journal_ledger_report_wizard("id" int4 NOT NULL
	,"company_id" int4
	,"date_range_id" int4
	,"date_from" date NOT NULL
	,"date_to" date NOT NULL
	,"move_target" varchar NOT NULL
	,"foreign_currency" bool
	,"sort_option" varchar NOT NULL
	,"group_option" varchar NOT NULL
	,"with_account_name" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'journal_ledger_report_wizard');
drop foreign table if exists sm_l10n_es_aeat_certificate;	create FOREIGN TABLE sm_l10n_es_aeat_certificate("id" int4 NOT NULL
	,"name" varchar
	,"state" varchar
	,"file" bytea NOT NULL
	,"folder" varchar NOT NULL
	,"date_start" date
	,"date_end" date
	,"public_key" varchar
	,"private_key" varchar
	,"company_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_certificate');
drop foreign table if exists sm_l10n_es_aeat_certificate_password;	create FOREIGN TABLE sm_l10n_es_aeat_certificate_password("id" int4 NOT NULL
	,"password" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_certificate_password');
drop foreign table if exists sm_l10n_es_aeat_map_tax;	create FOREIGN TABLE sm_l10n_es_aeat_map_tax("id" int4 NOT NULL
	,"date_from" date
	,"date_to" date
	,"model" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_map_tax');
drop foreign table if exists sm_l10n_es_aeat_map_tax_line;	create FOREIGN TABLE sm_l10n_es_aeat_map_tax_line("id" int4 NOT NULL
	,"field_number" int4 NOT NULL
	,"name" varchar NOT NULL
	,"map_parent_id" int4 NOT NULL
	,"move_type" varchar NOT NULL
	,"field_type" varchar NOT NULL
	,"sum_type" varchar NOT NULL
	,"exigible_type" varchar NOT NULL
	,"inverse" bool
	,"to_regularize" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_map_tax_line');
drop foreign table if exists sm_l10n_es_aeat_mod111_report;	create FOREIGN TABLE sm_l10n_es_aeat_mod111_report("id" int4 NOT NULL
	,"casilla_01" int4
	,"casilla_04" int4
	,"casilla_07" int4
	,"casilla_10" int4
	,"casilla_11" float8
	,"casilla_12" float8
	,"casilla_13" int4
	,"casilla_14" float8
	,"casilla_15" float8
	,"casilla_16" int4
	,"casilla_17" float8
	,"casilla_18" float8
	,"casilla_19" int4
	,"casilla_20" float8
	,"casilla_21" float8
	,"casilla_22" int4
	,"casilla_23" float8
	,"casilla_24" float8
	,"casilla_25" int4
	,"casilla_26" float8
	,"casilla_27" float8
	,"casilla_29" float8
	,"tipo_declaracion" varchar NOT NULL
	,"colegio_concertado" bool
	,"company_id" int4 NOT NULL
	,"company_vat" varchar(9) NOT NULL
	,"number" varchar(3) NOT NULL
	,"previous_number" varchar(13)
	,"contact_name" varchar(40) NOT NULL
	,"contact_phone" varchar(9) NOT NULL
	,"contact_email" varchar(50)
	,"representative_vat" varchar(9)
	,"year" int4 NOT NULL
	,"type" varchar NOT NULL
	,"support_type" varchar NOT NULL
	,"calculation_date" timestamp
	,"state" varchar
	,"name" varchar(13)
	,"export_config_id" int4
	,"period_type" varchar NOT NULL
	,"date_start" date NOT NULL
	,"date_end" date NOT NULL
	,"counterpart_account_id" int4
	,"journal_id" int4
	,"move_id" int4
	,"partner_bank_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"message_main_attachment_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_mod111_report');
drop foreign table if exists sm_l10n_es_aeat_mod115_report;	create FOREIGN TABLE sm_l10n_es_aeat_mod115_report("id" int4 NOT NULL
	,"casilla_04" float8
	,"tipo_declaracion" varchar NOT NULL
	,"company_id" int4 NOT NULL
	,"company_vat" varchar(9) NOT NULL
	,"number" varchar(3) NOT NULL
	,"previous_number" varchar(13)
	,"contact_name" varchar(40) NOT NULL
	,"contact_phone" varchar(9) NOT NULL
	,"contact_email" varchar(50)
	,"representative_vat" varchar(9)
	,"year" int4 NOT NULL
	,"type" varchar NOT NULL
	,"support_type" varchar NOT NULL
	,"calculation_date" timestamp
	,"state" varchar
	,"name" varchar(13)
	,"export_config_id" int4
	,"period_type" varchar NOT NULL
	,"date_start" date NOT NULL
	,"date_end" date NOT NULL
	,"counterpart_account_id" int4
	,"journal_id" int4
	,"move_id" int4
	,"partner_bank_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"message_main_attachment_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_mod115_report');
drop foreign table if exists sm_l10n_es_aeat_mod123_report;	create FOREIGN TABLE sm_l10n_es_aeat_mod123_report("id" int4 NOT NULL
	,"number" varchar(3) NOT NULL
	,"casilla_01" int4
	,"casilla_02" float8
	,"casilla_03" float8
	,"casilla_04" float8
	,"casilla_05" float8
	,"casilla_07" float8
	,"currency_id" int4
	,"tipo_declaracion" varchar NOT NULL
	,"company_id" int4 NOT NULL
	,"company_vat" varchar(9) NOT NULL
	,"previous_number" varchar(13)
	,"contact_name" varchar(40) NOT NULL
	,"contact_phone" varchar(9) NOT NULL
	,"contact_email" varchar(50)
	,"representative_vat" varchar(9)
	,"year" int4 NOT NULL
	,"type" varchar NOT NULL
	,"support_type" varchar NOT NULL
	,"calculation_date" timestamp
	,"state" varchar
	,"name" varchar(13)
	,"export_config_id" int4
	,"period_type" varchar NOT NULL
	,"date_start" date NOT NULL
	,"date_end" date NOT NULL
	,"counterpart_account_id" int4
	,"journal_id" int4
	,"move_id" int4
	,"partner_bank_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"message_main_attachment_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_mod123_report');
drop foreign table if exists sm_l10n_es_aeat_mod130_report;	create FOREIGN TABLE sm_l10n_es_aeat_mod130_report("id" int4 NOT NULL
	,"company_partner_id" int4
	,"currency_id" int4
	,"activity_type" varchar NOT NULL
	,"has_deduccion_80" bool
	,"has_prestamo" bool
	,"comments" varchar(350)
	,"casilla_01" numeric
	,"real_expenses" numeric
	,"non_justified_expenses" numeric
	,"casilla_02" numeric
	,"casilla_03" numeric
	,"casilla_04" numeric
	,"casilla_05" numeric
	,"casilla_06" numeric
	,"casilla_07" numeric
	,"casilla_08" numeric
	,"casilla_09" numeric
	,"casilla_10" numeric
	,"casilla_11" numeric
	,"casilla_12" numeric
	,"casilla_13" numeric
	,"casilla_14" numeric
	,"casilla_15" numeric
	,"casilla_16" numeric
	,"casilla_17" numeric
	,"casilla_18" numeric
	,"result" numeric
	,"tipo_declaracion" varchar
	,"company_id" int4 NOT NULL
	,"company_vat" varchar(9) NOT NULL
	,"number" varchar(3) NOT NULL
	,"previous_number" varchar(13)
	,"contact_name" varchar(40) NOT NULL
	,"contact_phone" varchar(9) NOT NULL
	,"contact_email" varchar(50)
	,"representative_vat" varchar(9)
	,"year" int4 NOT NULL
	,"type" varchar NOT NULL
	,"support_type" varchar NOT NULL
	,"calculation_date" timestamp
	,"state" varchar
	,"name" varchar(13)
	,"export_config_id" int4
	,"period_type" varchar NOT NULL
	,"date_start" date NOT NULL
	,"date_end" date NOT NULL
	,"counterpart_account_id" int4
	,"journal_id" int4
	,"move_id" int4
	,"partner_bank_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"message_main_attachment_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_mod130_report');
drop foreign table if exists sm_l10n_es_aeat_mod216_report;	create FOREIGN TABLE sm_l10n_es_aeat_mod216_report("id" int4 NOT NULL
	,"casilla_04" int4
	,"casilla_05" numeric
	,"casilla_06" numeric
	,"tipo_declaracion" varchar NOT NULL
	,"company_id" int4 NOT NULL
	,"company_vat" varchar(9) NOT NULL
	,"number" varchar(3) NOT NULL
	,"previous_number" varchar(13)
	,"contact_name" varchar(40) NOT NULL
	,"contact_phone" varchar(9) NOT NULL
	,"contact_email" varchar(50)
	,"representative_vat" varchar(9)
	,"year" int4 NOT NULL
	,"type" varchar NOT NULL
	,"support_type" varchar NOT NULL
	,"calculation_date" timestamp
	,"state" varchar
	,"name" varchar(13)
	,"export_config_id" int4
	,"period_type" varchar NOT NULL
	,"date_start" date NOT NULL
	,"date_end" date NOT NULL
	,"counterpart_account_id" int4
	,"journal_id" int4
	,"move_id" int4
	,"partner_bank_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"message_main_attachment_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_mod216_report');
drop foreign table if exists sm_l10n_es_aeat_mod296_report;	create FOREIGN TABLE sm_l10n_es_aeat_mod296_report("id" int4 NOT NULL
	,"casilla_01" int4
	,"casilla_02" float8
	,"casilla_03" float8
	,"casilla_04" float8
	,"company_id" int4 NOT NULL
	,"company_vat" varchar(9) NOT NULL
	,"number" varchar(3) NOT NULL
	,"previous_number" varchar(13)
	,"contact_name" varchar(40) NOT NULL
	,"contact_phone" varchar(9) NOT NULL
	,"contact_email" varchar(50)
	,"representative_vat" varchar(9)
	,"year" int4 NOT NULL
	,"type" varchar NOT NULL
	,"support_type" varchar NOT NULL
	,"calculation_date" timestamp
	,"state" varchar
	,"name" varchar(13)
	,"export_config_id" int4
	,"period_type" varchar NOT NULL
	,"date_start" date NOT NULL
	,"date_end" date NOT NULL
	,"counterpart_account_id" int4
	,"journal_id" int4
	,"move_id" int4
	,"partner_bank_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"message_main_attachment_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_mod296_report');
drop foreign table if exists sm_l10n_es_aeat_mod296_report_line;	create FOREIGN TABLE sm_l10n_es_aeat_mod296_report_line("id" int4 NOT NULL
	,"mod296_id" int4
	,"partner_id" int4
	,"base_retenciones_ingresos" float8
	,"porcentaje_retencion" float8
	,"retenciones_ingresos" float8
	,"fisica_juridica" varchar
	,"naturaleza" varchar
	,"fecha_devengo" date
	,"clave" varchar
	,"subclave" varchar
	,"mediador" bool
	,"codigo" varchar
	,"codigo_emisor" varchar(12)
	,"pago" varchar
	,"tipo_codigo" varchar
	,"cuenta_valores" int4
	,"pendiente" bool
	,"domicilio" varchar(50)
	,"complemento_domicilio" varchar(40)
	,"poblacion" varchar(30)
	,"provincia" int4
	,"zip" varchar(10)
	,"pais" int4
	,"nif_pais_residencia" varchar(20)
	,"fecha_nacimiento" date
	,"ciudad_nacimiento" varchar(35)
	,"pais_nacimiento" int4
	,"pais_residencia_fiscal" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_mod296_report_line');
drop foreign table if exists sm_l10n_es_aeat_mod303_report;	create FOREIGN TABLE sm_l10n_es_aeat_mod303_report("id" int4 NOT NULL
	,"devolucion_mensual" bool
	,"total_devengado" float8
	,"total_deducir" float8
	,"casilla_46" float8
	,"porcentaje_atribuible_estado" float8
	,"atribuible_estado" float8
	,"cuota_compensar" float8
	,"regularizacion_anual" float8
	,"casilla_69" float8
	,"casilla_77" float8
	,"previous_result" float8
	,"resultado_liquidacion" float8
	,"counterpart_account_id" int4
	,"exonerated_390" varchar NOT NULL
	,"has_operation_volume" bool
	,"has_347" bool
	,"is_voluntary_sii" bool
	,"main_activity_code" int4
	,"main_activity_iae" varchar(4)
	,"other_first_activity_code" int4
	,"other_first_activity_iae" varchar(4)
	,"other_second_activity_code" int4
	,"other_second_activity_iae" varchar(4)
	,"other_third_activity_code" int4
	,"other_third_activity_iae" varchar(4)
	,"other_fourth_activity_code" int4
	,"other_fourth_activity_iae" varchar(4)
	,"other_fifth_activity_code" int4
	,"other_fifth_activity_iae" varchar(4)
	,"casilla_88" float8
	,"company_id" int4 NOT NULL
	,"company_vat" varchar(9) NOT NULL
	,"number" varchar(3) NOT NULL
	,"previous_number" varchar(13)
	,"contact_name" varchar(40) NOT NULL
	,"contact_phone" varchar(9) NOT NULL
	,"contact_email" varchar(50)
	,"representative_vat" varchar(9)
	,"year" int4 NOT NULL
	,"type" varchar NOT NULL
	,"support_type" varchar NOT NULL
	,"calculation_date" timestamp
	,"state" varchar
	,"name" varchar(13)
	,"export_config_id" int4
	,"period_type" varchar NOT NULL
	,"date_start" date NOT NULL
	,"date_end" date NOT NULL
	,"journal_id" int4
	,"move_id" int4
	,"partner_bank_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"message_main_attachment_id" int4
	,"potential_cuota_compensar" float8)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_mod303_report');
drop foreign table if exists sm_l10n_es_aeat_mod303_report_activity_code;	create FOREIGN TABLE sm_l10n_es_aeat_mod303_report_activity_code("id" int4 NOT NULL
	,"period_type" varchar NOT NULL
	,"code" int4 NOT NULL
	,"name" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_mod303_report_activity_code');
drop foreign table if exists sm_l10n_es_aeat_mod347_move_record;	create FOREIGN TABLE sm_l10n_es_aeat_mod347_move_record("id" int4 NOT NULL
	,"partner_record_id" int4 NOT NULL
	,"move_id" int4
	,"move_type" varchar
	,"invoice_id" int4
	,"date" date
	,"amount" float8
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_mod347_move_record');
drop foreign table if exists sm_l10n_es_aeat_mod347_partner_record;	create FOREIGN TABLE sm_l10n_es_aeat_mod347_partner_record("id" int4 NOT NULL
	,"report_id" int4
	,"user_id" int4
	,"state" varchar
	,"operation_key" varchar
	,"partner_id" int4 NOT NULL
	,"partner_vat" varchar(9)
	,"representative_vat" varchar(9)
	,"community_vat" varchar(17)
	,"partner_country_code" varchar(2)
	,"partner_state_code" varchar(2)
	,"first_quarter" numeric
	,"first_quarter_real_estate_transmission" numeric
	,"second_quarter" numeric
	,"second_quarter_real_estate_transmission" numeric
	,"third_quarter" numeric
	,"third_quarter_real_estate_transmission" numeric
	,"fourth_quarter" numeric
	,"fourth_quarter_real_estate_transmission" numeric
	,"amount" numeric
	,"cash_amount" numeric
	,"real_estate_transmissions_amount" numeric
	,"insurance_operation" bool
	,"cash_basis_operation" bool
	,"tax_person_operation" bool
	,"related_goods_operation" bool
	,"bussiness_real_estate_rent" bool
	,"origin_year" int4
	,"check_ok" bool
	,"openupgrade_legacy_12_0_activity_date_deadline" date
	,"message_last_post" timestamp
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"access_token" varchar
	,"message_main_attachment_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_mod347_partner_record');
drop foreign table if exists sm_l10n_es_aeat_mod347_real_estate_record;	create FOREIGN TABLE sm_l10n_es_aeat_mod347_real_estate_record("id" int4 NOT NULL
	,"report_id" int4
	,"partner_id" int4 NOT NULL
	,"partner_vat" varchar(32)
	,"representative_vat" varchar(32)
	,"amount" numeric
	,"situation" varchar NOT NULL
	,"reference" varchar(25)
	,"address_type" varchar(5)
	,"address" varchar(50)
	,"number_type" varchar
	,"number" int4
	,"number_calification" varchar
	,"block" varchar(3)
	,"portal" varchar(3)
	,"stairway" varchar(3)
	,"floor" varchar(3)
	,"door" varchar(3)
	,"complement" varchar(40)
	,"city" varchar(30)
	,"township" varchar(30)
	,"township_code" varchar(5)
	,"partner_state_code" varchar(2)
	,"postal_code" varchar(5)
	,"check_ok" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"state_code" varchar(2))SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_mod347_real_estate_record');
drop foreign table if exists sm_l10n_es_aeat_mod347_report;	create FOREIGN TABLE sm_l10n_es_aeat_mod347_report("id" int4 NOT NULL
	,"number" varchar(3) NOT NULL
	,"operations_limit" numeric
	,"received_cash_limit" numeric
	,"total_partner_records" int4
	,"total_amount" float8
	,"total_cash_amount" float8
	,"total_real_estate_transmissions_amount" float8
	,"total_real_estate_records" int4
	,"total_real_estate_amount" float8
	,"company_id" int4 NOT NULL
	,"company_vat" varchar(9) NOT NULL
	,"previous_number" varchar(13)
	,"contact_name" varchar(40) NOT NULL
	,"contact_phone" varchar(9) NOT NULL
	,"contact_email" varchar(50)
	,"representative_vat" varchar(9)
	,"year" int4 NOT NULL
	,"type" varchar NOT NULL
	,"support_type" varchar NOT NULL
	,"calculation_date" timestamp
	,"state" varchar
	,"name" varchar(13)
	,"export_config_id" int4
	,"period_type" varchar NOT NULL
	,"date_start" date NOT NULL
	,"date_end" date NOT NULL
	,"counterpart_account_id" int4
	,"journal_id" int4
	,"move_id" int4
	,"partner_bank_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"message_main_attachment_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_mod347_report');
drop foreign table if exists sm_l10n_es_aeat_mod349_partner_record;	create FOREIGN TABLE sm_l10n_es_aeat_mod349_partner_record("id" int4 NOT NULL
	,"report_id" int4
	,"partner_id" int4 NOT NULL
	,"partner_vat" varchar(15)
	,"country_id" int4
	,"operation_key" varchar
	,"total_operation_amount" float8
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_mod349_partner_record');
drop foreign table if exists sm_l10n_es_aeat_mod349_partner_record_detail;	create FOREIGN TABLE sm_l10n_es_aeat_mod349_partner_record_detail("id" int4 NOT NULL
	,"report_id" int4 NOT NULL
	,"report_type" varchar
	,"partner_record_id" int4
	,"move_line_id" int4 NOT NULL
	,"amount_untaxed" float8
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_mod349_partner_record_detail');
drop foreign table if exists sm_l10n_es_aeat_mod349_partner_refund;	create FOREIGN TABLE sm_l10n_es_aeat_mod349_partner_refund("id" int4 NOT NULL
	,"report_id" int4
	,"partner_id" int4 NOT NULL
	,"partner_vat" varchar(15)
	,"operation_key" varchar
	,"country_id" int4
	,"total_operation_amount" float8
	,"total_origin_amount" float8
	,"period_type" varchar
	,"year" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_mod349_partner_refund');
drop foreign table if exists sm_l10n_es_aeat_mod349_partner_refund_detail;	create FOREIGN TABLE sm_l10n_es_aeat_mod349_partner_refund_detail("id" int4 NOT NULL
	,"report_id" int4 NOT NULL
	,"report_type" varchar
	,"refund_id" int4
	,"refund_line_id" int4 NOT NULL
	,"amount_untaxed" float8
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_mod349_partner_refund_detail');
drop foreign table if exists sm_l10n_es_aeat_mod349_report;	create FOREIGN TABLE sm_l10n_es_aeat_mod349_report("id" int4 NOT NULL
	,"frequency_change" bool
	,"total_partner_records" int4
	,"total_partner_records_amount" float8
	,"total_partner_refunds" int4
	,"total_partner_refunds_amount" float8
	,"number" varchar(3) NOT NULL
	,"company_id" int4 NOT NULL
	,"company_vat" varchar(9) NOT NULL
	,"previous_number" varchar(13)
	,"contact_name" varchar(40) NOT NULL
	,"contact_phone" varchar(9) NOT NULL
	,"contact_email" varchar(50)
	,"representative_vat" varchar(9)
	,"year" int4 NOT NULL
	,"type" varchar NOT NULL
	,"support_type" varchar NOT NULL
	,"calculation_date" timestamp
	,"state" varchar
	,"name" varchar(13)
	,"export_config_id" int4
	,"period_type" varchar NOT NULL
	,"date_start" date NOT NULL
	,"date_end" date NOT NULL
	,"counterpart_account_id" int4
	,"journal_id" int4
	,"move_id" int4
	,"partner_bank_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"message_main_attachment_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_mod349_report');
drop foreign table if exists sm_l10n_es_aeat_mod390_report;	create FOREIGN TABLE sm_l10n_es_aeat_mod390_report("id" int4 NOT NULL
	,"has_347" bool
	,"main_activity" varchar(40)
	,"main_activity_code" varchar
	,"main_activity_iae" varchar(4)
	,"other_first_activity" varchar(40)
	,"other_first_activity_code" varchar
	,"other_first_activity_iae" varchar(4)
	,"other_second_activity" varchar(40)
	,"other_second_activity_code" varchar
	,"other_second_activity_iae" varchar(4)
	,"other_third_activity" varchar(40)
	,"other_third_activity_code" varchar
	,"other_third_activity_iae" varchar(4)
	,"other_fourth_activity" varchar(40)
	,"other_fourth_activity_code" varchar
	,"other_fourth_activity_iae" varchar(4)
	,"other_fifth_activity" varchar(40)
	,"other_fifth_activity_code" varchar
	,"other_fifth_activity_iae" varchar(4)
	,"first_representative_name" varchar(80)
	,"first_representative_vat" varchar(9)
	,"first_representative_date" date
	,"first_representative_notary" varchar(12)
	,"second_representative_name" varchar(80)
	,"second_representative_vat" varchar(9)
	,"second_representative_date" date
	,"second_representative_notary" varchar(12)
	,"third_representative_name" varchar(80)
	,"third_representative_vat" varchar(9)
	,"third_representative_date" date
	,"third_representative_notary" varchar(12)
	,"casilla_33" float8
	,"casilla_34" float8
	,"casilla_47" float8
	,"casilla_48" float8
	,"casilla_49" float8
	,"casilla_50" float8
	,"casilla_51" float8
	,"casilla_52" float8
	,"casilla_53" float8
	,"casilla_54" float8
	,"casilla_55" float8
	,"casilla_56" float8
	,"casilla_57" float8
	,"casilla_58" float8
	,"casilla_59" float8
	,"casilla_597" float8
	,"casilla_598" float8
	,"casilla_64" float8
	,"casilla_65" float8
	,"casilla_85" float8
	,"casilla_86" float8
	,"casilla_95" float8
	,"casilla_97" float8
	,"casilla_98" float8
	,"casilla_108" float8
	,"company_id" int4 NOT NULL
	,"company_vat" varchar(9) NOT NULL
	,"number" varchar(3) NOT NULL
	,"previous_number" varchar(13)
	,"contact_name" varchar(40) NOT NULL
	,"contact_phone" varchar(9) NOT NULL
	,"contact_email" varchar(50)
	,"representative_vat" varchar(9)
	,"year" int4 NOT NULL
	,"type" varchar NOT NULL
	,"support_type" varchar NOT NULL
	,"calculation_date" timestamp
	,"state" varchar
	,"name" varchar(13)
	,"export_config_id" int4
	,"period_type" varchar NOT NULL
	,"date_start" date NOT NULL
	,"date_end" date NOT NULL
	,"counterpart_account_id" int4
	,"journal_id" int4
	,"move_id" int4
	,"partner_bank_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"message_main_attachment_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_mod390_report');
drop foreign table if exists sm_l10n_es_aeat_report_compare_boe_file;	create FOREIGN TABLE sm_l10n_es_aeat_report_compare_boe_file("id" int4 NOT NULL
	,"data" bytea NOT NULL
	,"state" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_report_compare_boe_file');
drop foreign table if exists sm_l10n_es_aeat_report_compare_boe_file_line;	create FOREIGN TABLE sm_l10n_es_aeat_report_compare_boe_file_line("id" int4 NOT NULL
	,"wizard_id" int4 NOT NULL
	,"export_line_id" int4 NOT NULL
	,"content" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_report_compare_boe_file_line');
drop foreign table if exists sm_l10n_es_aeat_report_export_to_boe;	create FOREIGN TABLE sm_l10n_es_aeat_report_export_to_boe("id" int4 NOT NULL
	,"name" varchar
	,"data" bytea
	,"state" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_report_export_to_boe');
drop foreign table if exists sm_l10n_es_aeat_soap;	create FOREIGN TABLE sm_l10n_es_aeat_soap("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_soap');
drop foreign table if exists sm_l10n_es_aeat_tax_line;	create FOREIGN TABLE sm_l10n_es_aeat_tax_line("id" int4 NOT NULL
	,"res_id" int4 NOT NULL
	,"field_number" int4
	,"name" varchar
	,"amount" numeric
	,"map_line_id" int4 NOT NULL
	,"model" varchar NOT NULL
	,"model_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'l10n_es_aeat_tax_line');
drop foreign table if exists sm_l10n_es_partner_import_wizard;	create FOREIGN TABLE sm_l10n_es_partner_import_wizard("id" int4 NOT NULL
	,"import_fail" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'l10n_es_partner_import_wizard');
drop foreign table if exists sm_l10n_es_vat_book;	create FOREIGN TABLE sm_l10n_es_vat_book("id" int4 NOT NULL
	,"number" varchar(3) NOT NULL
	,"calculation_date" date
	,"auto_renumber" bool
	,"company_id" int4 NOT NULL
	,"company_vat" varchar(9) NOT NULL
	,"previous_number" varchar(13)
	,"contact_name" varchar(40) NOT NULL
	,"contact_phone" varchar(9) NOT NULL
	,"contact_email" varchar(50)
	,"representative_vat" varchar(9)
	,"year" int4 NOT NULL
	,"type" varchar NOT NULL
	,"support_type" varchar NOT NULL
	,"state" varchar
	,"name" varchar(13)
	,"export_config_id" int4
	,"period_type" varchar NOT NULL
	,"date_start" date NOT NULL
	,"date_end" date NOT NULL
	,"counterpart_account_id" int4
	,"journal_id" int4
	,"move_id" int4
	,"partner_bank_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"message_main_attachment_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'l10n_es_vat_book');
drop foreign table if exists sm_l10n_es_vat_book_line;	create FOREIGN TABLE sm_l10n_es_vat_book_line("id" int4 NOT NULL
	,"ref" varchar
	,"entry_number" int4
	,"external_ref" varchar
	,"line_type" varchar
	,"invoice_date" date
	,"partner_id" int4
	,"vat_number" varchar
	,"vat_book_id" int4
	,"invoice_id" int4
	,"move_id" int4
	,"exception_text" varchar
	,"base_amount" float8
	,"total_amount" float8
	,"special_tax_group" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'l10n_es_vat_book_line');
drop foreign table if exists sm_l10n_es_vat_book_line_tax;	create FOREIGN TABLE sm_l10n_es_vat_book_line_tax("id" int4 NOT NULL
	,"vat_book_line_id" int4 NOT NULL
	,"base_amount" float8
	,"tax_id" int4
	,"tax_amount" float8
	,"total_amount" float8
	,"special_tax_group" varchar
	,"special_tax_id" int4
	,"special_tax_amount" float8
	,"total_amount_special_include" float8
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'l10n_es_vat_book_line_tax');
drop foreign table if exists sm_l10n_es_vat_book_summary;	create FOREIGN TABLE sm_l10n_es_vat_book_summary("id" int4 NOT NULL
	,"vat_book_id" int4
	,"book_type" varchar
	,"base_amount" float8
	,"tax_amount" float8
	,"total_amount" float8
	,"special_tax_group" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'l10n_es_vat_book_summary');
drop foreign table if exists sm_l10n_es_vat_book_tax_summary;	create FOREIGN TABLE sm_l10n_es_vat_book_tax_summary("id" int4 NOT NULL
	,"tax_id" int4 NOT NULL
	,"vat_book_id" int4
	,"book_type" varchar
	,"base_amount" float8
	,"tax_amount" float8
	,"total_amount" float8
	,"special_tax_group" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'l10n_es_vat_book_tax_summary');
drop foreign table if exists sm_link_tracker;	create FOREIGN TABLE sm_link_tracker("id" int4 NOT NULL
	,"campaign_id" int4
	,"source_id" int4
	,"medium_id" int4
	,"url" varchar NOT NULL
	,"count" int4
	,"title" varchar
	,"favicon" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"mass_mailing_id" int4
	,"mass_mailing_campaign_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'link_tracker');
drop foreign table if exists sm_link_tracker_click;	create FOREIGN TABLE sm_link_tracker_click("id" int4 NOT NULL
	,"click_date" date
	,"link_id" int4 NOT NULL
	,"ip" varchar
	,"country_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"mail_stat_id" int4
	,"mass_mailing_id" int4
	,"mass_mailing_campaign_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'link_tracker_click');
drop foreign table if exists sm_link_tracker_code;	create FOREIGN TABLE sm_link_tracker_code("id" int4 NOT NULL
	,"code" varchar
	,"link_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'link_tracker_code');
drop foreign table if exists sm_mail_activity;	create FOREIGN TABLE sm_mail_activity("id" int4 NOT NULL
	,"res_id" int4 NOT NULL
	,"res_model_id" int4 NOT NULL
	,"res_model" varchar
	,"res_name" varchar
	,"activity_type_id" int4
	,"summary" varchar
	,"note" text
	,"feedback" text
	,"date_deadline" date NOT NULL
	,"user_id" int4 NOT NULL
	,"recommended_activity_type_id" int4
	,"previous_activity_type_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"calendar_event_id" int4
	,"automated" bool
	,"create_user_id" int4
	,"done" bool
	,"date_done" date)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mail_activity');
drop foreign table if exists sm_mail_activity_rel;	create FOREIGN TABLE sm_mail_activity_rel("activity_id" int4 NOT NULL
	,"recommended_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mail_activity_rel');
drop foreign table if exists sm_mail_activity_type;	create FOREIGN TABLE sm_mail_activity_type("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"summary" varchar
	,"sequence" int4
	,"delay_count" int4
	,"icon" varchar
	,"res_model_id" int4
	,"category" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"active" bool
	,"delay_unit" varchar NOT NULL
	,"delay_from" varchar NOT NULL
	,"decoration_type" varchar
	,"default_next_type_id" int4
	,"force_next" bool)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mail_activity_type');
drop foreign table if exists sm_mail_activity_type_mail_template_rel;	create FOREIGN TABLE sm_mail_activity_type_mail_template_rel("mail_activity_type_id" int4 NOT NULL
	,"mail_template_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mail_activity_type_mail_template_rel');
drop foreign table if exists sm_mail_alias;	create FOREIGN TABLE sm_mail_alias("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"alias_defaults" text NOT NULL
	,"alias_contact" varchar NOT NULL
	,"alias_parent_model_id" int4
	,"write_uid" int4
	,"alias_force_thread_id" int4
	,"alias_model_id" int4 NOT NULL
	,"write_date" timestamp
	,"alias_parent_thread_id" int4
	,"alias_user_id" int4
	,"alias_name" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mail_alias');
drop foreign table if exists sm_mail_blacklist;	create FOREIGN TABLE sm_mail_blacklist("id" int4 NOT NULL
	,"email" varchar NOT NULL
	,"active" bool
	,"message_main_attachment_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mail_blacklist');
drop foreign table if exists sm_mail_channel;	create FOREIGN TABLE sm_mail_channel("id" int4 NOT NULL
	,"create_date" timestamp
	,"alias_id" int4 NOT NULL
	,"write_uid" int4
	,"create_uid" int4
	,"uuid" varchar(50)
	,"message_last_post" timestamp
	,"public" varchar NOT NULL
	,"description" text
	,"group_public_id" int4
	,"write_date" timestamp
	,"name" varchar NOT NULL
	,"channel_type" varchar
	,"email_send" bool
	,"moderation" bool
	,"moderation_notify" bool
	,"moderation_notify_msg" text
	,"moderation_guidelines" bool
	,"moderation_guidelines_msg" text
	,"message_main_attachment_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mail_channel');
drop foreign table if exists sm_mail_channel_mail_wizard_invite_rel;	create FOREIGN TABLE sm_mail_channel_mail_wizard_invite_rel("mail_wizard_invite_id" int4 NOT NULL
	,"mail_channel_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mail_channel_mail_wizard_invite_rel');
drop foreign table if exists sm_mail_channel_moderator_rel;	create FOREIGN TABLE sm_mail_channel_moderator_rel("mail_channel_id" int4 NOT NULL
	,"res_users_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mail_channel_moderator_rel');
drop foreign table if exists sm_mail_channel_partner;	create FOREIGN TABLE sm_mail_channel_partner("id" int4 NOT NULL
	,"seen_message_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"is_minimized" bool
	,"is_pinned" bool
	,"write_uid" int4
	,"channel_id" int4
	,"fold_state" varchar
	,"write_date" timestamp
	,"partner_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mail_channel_partner');
drop foreign table if exists sm_mail_channel_res_group_rel;	create FOREIGN TABLE sm_mail_channel_res_group_rel("mail_channel_id" int4 NOT NULL
	,"groups_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mail_channel_res_group_rel');
drop foreign table if exists sm_mail_channel_res_groups_rel;	create FOREIGN TABLE sm_mail_channel_res_groups_rel("mail_channel_id" int4 NOT NULL
	,"res_groups_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mail_channel_res_groups_rel');
drop foreign table if exists sm_mail_compose_message;	create FOREIGN TABLE sm_mail_compose_message("id" int4 NOT NULL
	,"create_date" timestamp
	,"no_auto_thread" bool
	,"mail_server_id" int4
	,"write_uid" int4
	,"notify" bool
	,"active_domain" text
	,"subject" varchar
	,"composition_mode" varchar
	,"create_uid" int4
	,"is_log" bool
	,"parent_id" int4
	,"subtype_id" int4
	,"res_id" int4
	,"message_id" varchar
	,"body" text
	,"record_name" varchar
	,"write_date" timestamp
	,"date" timestamp
	,"model" varchar
	,"use_active_domain" bool
	,"email_from" varchar
	,"reply_to" varchar
	,"author_id" int4
	,"message_type" varchar NOT NULL
	,"template_id" int4
	,"auto_delete_message" bool
	,"auto_delete" bool
	,"mail_activity_type_id" int4
	,"moderation_status" varchar
	,"moderator_id" int4
	,"layout" varchar
	,"add_sign" bool
	,"mass_mailing_campaign_id" int4
	,"mass_mailing_id" int4
	,"mass_mailing_name" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mail_compose_message');
drop foreign table if exists sm_mail_compose_message_ir_attachments_rel;	create FOREIGN TABLE sm_mail_compose_message_ir_attachments_rel("wizard_id" int4 NOT NULL
	,"attachment_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mail_compose_message_ir_attachments_rel');
drop foreign table if exists sm_mail_compose_message_mail_mass_mailing_list_rel;	create FOREIGN TABLE sm_mail_compose_message_mail_mass_mailing_list_rel("mail_compose_message_id" int4 NOT NULL
	,"mail_mass_mailing_list_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mail_compose_message_mail_mass_mailing_list_rel');
drop foreign table if exists sm_mail_compose_message_res_partner_rel;	create FOREIGN TABLE sm_mail_compose_message_res_partner_rel("wizard_id" int4 NOT NULL
	,"partner_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mail_compose_message_res_partner_rel');
drop foreign table if exists sm_mail_followers;	create FOREIGN TABLE sm_mail_followers("id" int4 NOT NULL
	,"res_model" varchar NOT NULL
	,"partner_id" int4
	,"channel_id" int4
	,"res_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mail_followers');
drop foreign table if exists sm_mail_followers_mail_message_subtype_rel;	create FOREIGN TABLE sm_mail_followers_mail_message_subtype_rel("mail_followers_id" int4 NOT NULL
	,"mail_message_subtype_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mail_followers_mail_message_subtype_rel');
drop foreign table if exists sm_mail_mail;	create FOREIGN TABLE sm_mail_mail("id" int4 NOT NULL
	,"mail_message_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"notification" bool
	,"auto_delete" bool
	,"body_html" text
	,"write_uid" int4
	,"email_to" text
	,"headers" text
	,"state" varchar
	,"references" text
	,"write_date" timestamp
	,"email_cc" varchar
	,"failure_reason" text
	,"fetchmail_server_id" int4
	,"scheduled_date" varchar
	,"mailing_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mail_mail');
drop foreign table if exists sm_mail_mail_res_partner_rel;	create FOREIGN TABLE sm_mail_mail_res_partner_rel("mail_mail_id" int4 NOT NULL
	,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mail_mail_res_partner_rel');
drop foreign table if exists sm_mail_mail_statistics;	create FOREIGN TABLE sm_mail_mail_statistics("id" int4 NOT NULL
	,"mail_mail_id" int4
	,"mail_mail_id_int" int4
	,"message_id" varchar
	,"model" varchar
	,"res_id" int4
	,"mass_mailing_id" int4
	,"mass_mailing_campaign_id" int4
	,"ignored" timestamp
	,"scheduled" timestamp
	,"sent" timestamp
	,"exception" timestamp
	,"opened" timestamp
	,"replied" timestamp
	,"bounced" timestamp
	,"clicked" timestamp
	,"state" varchar
	,"state_update" timestamp
	,"email" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mail_mail_statistics');
drop foreign table if exists sm_mail_mass_mailing;	create FOREIGN TABLE sm_mail_mass_mailing("id" int4 NOT NULL
	,"active" bool
	,"email_from" varchar NOT NULL
	,"sent_date" timestamp
	,"schedule_date" timestamp
	,"body_html" text
	,"keep_archives" bool
	,"mass_mailing_campaign_id" int4
	,"campaign_id" int4
	,"source_id" int4 NOT NULL
	,"medium_id" int4
	,"state" varchar NOT NULL
	,"color" int4
	,"user_id" int4
	,"reply_to_mode" varchar NOT NULL
	,"reply_to" varchar
	,"mailing_model_id" int4
	,"mailing_domain" varchar
	,"mail_server_id" int4
	,"contact_ab_pc" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mail_mass_mailing');
drop foreign table if exists sm_mail_mass_mailing_campaign;	create FOREIGN TABLE sm_mail_mass_mailing_campaign("id" int4 NOT NULL
	,"stage_id" int4 NOT NULL
	,"user_id" int4 NOT NULL
	,"campaign_id" int4 NOT NULL
	,"source_id" int4
	,"medium_id" int4
	,"unique_ab_testing" bool
	,"color" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mail_mass_mailing_campaign');
drop foreign table if exists sm_mail_mass_mailing_contact;	create FOREIGN TABLE sm_mail_mass_mailing_contact("id" int4 NOT NULL
	,"name" varchar
	,"company_name" varchar
	,"title_id" int4
	,"email" varchar NOT NULL
	,"is_email_valid" bool
	,"message_bounce" int4
	,"country_id" int4
	,"message_main_attachment_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mail_mass_mailing_contact');
drop foreign table if exists sm_mail_mass_mailing_contact_list_rel;	create FOREIGN TABLE sm_mail_mass_mailing_contact_list_rel("id" int4 NOT NULL
	,"contact_id" int4 NOT NULL
	,"list_id" int4 NOT NULL
	,"opt_out" bool
	,"unsubscription_date" timestamp
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mail_mass_mailing_contact_list_rel');
drop foreign table if exists sm_mail_mass_mailing_contact_res_partner_category_rel;	create FOREIGN TABLE sm_mail_mass_mailing_contact_res_partner_category_rel("mail_mass_mailing_contact_id" int4 NOT NULL
	,"res_partner_category_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mail_mass_mailing_contact_res_partner_category_rel');
drop foreign table if exists sm_mail_mass_mailing_list;	create FOREIGN TABLE sm_mail_mass_mailing_list("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"active" bool
	,"is_public" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mail_mass_mailing_list');
drop foreign table if exists sm_mail_mass_mailing_list_mass_mailing_list_merge_rel;	create FOREIGN TABLE sm_mail_mass_mailing_list_mass_mailing_list_merge_rel("mass_mailing_list_merge_id" int4 NOT NULL
	,"mail_mass_mailing_list_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mail_mass_mailing_list_mass_mailing_list_merge_rel');
drop foreign table if exists sm_mail_mass_mailing_list_rel;	create FOREIGN TABLE sm_mail_mass_mailing_list_rel("mail_mass_mailing_id" int4 NOT NULL
	,"mail_mass_mailing_list_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mail_mass_mailing_list_rel');
drop foreign table if exists sm_mail_mass_mailing_stage;	create FOREIGN TABLE sm_mail_mass_mailing_stage("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"sequence" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mail_mass_mailing_stage');
drop foreign table if exists sm_mail_mass_mailing_tag;	create FOREIGN TABLE sm_mail_mass_mailing_tag("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"color" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mail_mass_mailing_tag');
drop foreign table if exists sm_mail_mass_mailing_tag_rel;	create FOREIGN TABLE sm_mail_mass_mailing_tag_rel("tag_id" int4 NOT NULL
	,"campaign_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mail_mass_mailing_tag_rel');
drop foreign table if exists sm_mail_mass_mailing_test;	create FOREIGN TABLE sm_mail_mass_mailing_test("id" int4 NOT NULL
	,"email_to" varchar NOT NULL
	,"mass_mailing_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mail_mass_mailing_test');
drop foreign table if exists sm_mail_message;	create FOREIGN TABLE sm_mail_message("id" int4 NOT NULL
	,"create_date" timestamp
	,"write_date" timestamp
	,"mail_server_id" int4
	,"write_uid" int4
	,"subject" varchar
	,"create_uid" int4
	,"parent_id" int4
	,"subtype_id" int4
	,"res_id" int4
	,"message_id" varchar
	,"body" text
	,"record_name" varchar
	,"no_auto_thread" bool
	,"date" timestamp
	,"model" varchar
	,"reply_to" varchar
	,"author_id" int4
	,"message_type" varchar NOT NULL
	,"email_from" varchar
	,"mail_activity_type_id" int4
	,"add_sign" bool
	,"moderation_status" varchar
	,"moderator_id" int4
	,"layout" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mail_message');
drop foreign table if exists sm_mail_message_mail_channel_rel;	create FOREIGN TABLE sm_mail_message_mail_channel_rel("mail_message_id" int4 NOT NULL
	,"mail_channel_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mail_message_mail_channel_rel');
drop foreign table if exists sm_mail_message_res_partner_needaction_rel;	create FOREIGN TABLE sm_mail_message_res_partner_needaction_rel("id" int4 NOT NULL
	,"mail_message_id" int4 NOT NULL
	,"res_partner_id" int4 NOT NULL
	,"is_read" bool
	,"is_email" bool
	,"email_status" varchar
	,"mail_id" int4
	,"failure_type" varchar
	,"failure_reason" text)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mail_message_res_partner_needaction_rel');
drop foreign table if exists sm_mail_message_res_partner_needaction_rel_mail_resend_message_rel;	create FOREIGN TABLE sm_mail_message_res_partner_needaction_rel_mail_resend_message_rel("mail_resend_message_id" int4 NOT NULL
	,"mail_message_res_partner_needaction_rel_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mail_message_res_partner_needaction_rel_mail_resend_message_rel');
drop foreign table if exists sm_mail_message_res_partner_rel;	create FOREIGN TABLE sm_mail_message_res_partner_rel("mail_message_id" int4 NOT NULL
	,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mail_message_res_partner_rel');
drop foreign table if exists sm_mail_message_res_partner_starred_rel;	create FOREIGN TABLE sm_mail_message_res_partner_starred_rel("mail_message_id" int4 NOT NULL
	,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mail_message_res_partner_starred_rel');
drop foreign table if exists sm_mail_message_subtype;	create FOREIGN TABLE sm_mail_message_subtype("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"description" text
	,"sequence" int4
	,"default" bool
	,"res_model" varchar
	,"write_uid" int4
	,"parent_id" int4
	,"internal" bool
	,"write_date" timestamp
	,"relation_field" varchar
	,"hidden" bool
	,"name" varchar NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mail_message_subtype');
drop foreign table if exists sm_mail_moderation;	create FOREIGN TABLE sm_mail_moderation("id" int4 NOT NULL
	,"email" varchar NOT NULL
	,"status" varchar NOT NULL
	,"channel_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mail_moderation');
drop foreign table if exists sm_mail_resend_cancel;	create FOREIGN TABLE sm_mail_resend_cancel("id" int4 NOT NULL
	,"model" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mail_resend_cancel');
drop foreign table if exists sm_mail_resend_message;	create FOREIGN TABLE sm_mail_resend_message("id" int4 NOT NULL
	,"mail_message_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mail_resend_message');
drop foreign table if exists sm_mail_resend_partner;	create FOREIGN TABLE sm_mail_resend_partner("id" int4 NOT NULL
	,"partner_id" int4 NOT NULL
	,"resend" bool
	,"resend_wizard_id" int4
	,"message" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mail_resend_partner');
drop foreign table if exists sm_mail_shortcode;	create FOREIGN TABLE sm_mail_shortcode("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"description" varchar
	,"write_uid" int4
	,"source" varchar NOT NULL
	,"shortcode_type" varchar
	,"substitution" text NOT NULL
	,"write_date" timestamp
	,"unicode_source" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mail_shortcode');
drop foreign table if exists sm_mail_statistics_report;	create FOREIGN TABLE sm_mail_statistics_report("id" int4
	,"scheduled_date" timestamp
	,"name" varchar
	,"campaign" varchar
	,"bounced" int8
	,"sent" int8
	,"delivered" int8
	,"opened" int8
	,"replied" int8
	,"clicked" int8
	,"state" varchar
	,"email_from" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mail_statistics_report');
drop foreign table if exists sm_mail_template;	create FOREIGN TABLE sm_mail_template("id" int4 NOT NULL
	,"create_date" timestamp
	,"write_date" timestamp
	,"auto_delete" bool
	,"mail_server_id" int4
	,"write_uid" int4
	,"partner_to" varchar
	,"ref_ir_act_window" int4
	,"subject" varchar
	,"create_uid" int4
	,"report_template" int4
	,"ref_ir_value" int4
	,"user_signature" bool
	,"null_value" varchar
	,"email_cc" varchar
	,"model_id" int4
	,"sub_model_object_field" int4
	,"body_html" text
	,"email_to" varchar
	,"sub_object" int4
	,"copyvalue" varchar
	,"lang" varchar
	,"name" varchar
	,"model_object_field" int4
	,"report_name" varchar
	,"use_default_to" bool
	,"reply_to" varchar
	,"model" varchar
	,"email_from" varchar
	,"scheduled_date" varchar
	,"force_email_send" bool
	,"easy_my_coop" bool)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mail_template');
drop foreign table if exists sm_mail_test;	create FOREIGN TABLE sm_mail_test("id" int4 NOT NULL
	,"name" varchar
	,"create_uid" int4
	,"message_last_post" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"create_date" timestamp
	,"alias_id" int4 NOT NULL
	,"description" text)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mail_test');
drop foreign table if exists sm_mail_test_simple;	create FOREIGN TABLE sm_mail_test_simple("id" int4 NOT NULL
	,"name" varchar
	,"email_from" varchar
	,"description" text
	,"message_last_post" timestamp
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mail_test_simple');
drop foreign table if exists sm_mail_tracking_value;	create FOREIGN TABLE sm_mail_tracking_value("id" int4 NOT NULL
	,"create_uid" int4
	,"field_type" varchar
	,"create_date" timestamp
	,"old_value_datetime" timestamp
	,"old_value_monetary" float8
	,"new_value_monetary" float8
	,"mail_message_id" int4 NOT NULL
	,"old_value_char" varchar
	,"old_value_float" float8
	,"new_value_text" text
	,"write_uid" int4
	,"new_value_float" float8
	,"field" varchar NOT NULL
	,"old_value_text" text
	,"field_desc" varchar NOT NULL
	,"new_value_char" varchar
	,"write_date" timestamp
	,"new_value_datetime" timestamp
	,"new_value_integer" int4
	,"old_value_integer" int4
	,"track_sequence" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mail_tracking_value');
drop foreign table if exists sm_mail_wizard_invite;	create FOREIGN TABLE sm_mail_wizard_invite("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"res_model" varchar NOT NULL
	,"send_mail" bool
	,"write_uid" int4
	,"write_date" timestamp
	,"message" text
	,"res_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mail_wizard_invite');
drop foreign table if exists sm_mail_wizard_invite_res_partner_rel;	create FOREIGN TABLE sm_mail_wizard_invite_res_partner_rel("mail_wizard_invite_id" int4 NOT NULL
	,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mail_wizard_invite_res_partner_rel');
drop foreign table if exists sm_mailchimp_list;	create FOREIGN TABLE sm_mailchimp_list("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"name" varchar
	,"write_uid" int4
	,"write_date" timestamp
	,"list_id" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mailchimp_list');
drop foreign table if exists sm_maintenance_equipment;	create FOREIGN TABLE sm_maintenance_equipment("id" int4 NOT NULL
	,"warranty_date" date
	,"create_date" timestamp
	,"write_date" timestamp
	,"color" int4
	,"serial_no" varchar
	,"period" int4
	,"maintenance_team_id" int4
	,"write_uid" int4
	,"technician_user_id" int4
	,"cost" float8
	,"partner_id" int4
	,"create_uid" int4
	,"message_last_post" timestamp
	,"note" text
	,"location" varchar
	,"owner_user_id" int4
	,"active" bool
	,"partner_ref" varchar
	,"maintenance_duration" float8
	,"maintenance_count" int4
	,"category_id" int4
	,"name" varchar NOT NULL
	,"assign_date" date
	,"maintenance_open_count" int4
	,"next_action_date" date
	,"model" varchar
	,"scrap_date" date
	,"openupgrade_legacy_12_0_activity_date_deadline" date
	,"employee_id" int4
	,"department_id" int4
	,"equipment_assign_to" varchar NOT NULL
	,"message_main_attachment_id" int4
	,"company_id" int4
	,"effective_date" date NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'maintenance_equipment');
drop foreign table if exists sm_maintenance_equipment_category;	create FOREIGN TABLE sm_maintenance_equipment_category("id" int4 NOT NULL
	,"create_date" timestamp
	,"color" int4
	,"alias_id" int4 NOT NULL
	,"write_uid" int4
	,"fold" bool
	,"technician_user_id" int4
	,"create_uid" int4
	,"message_last_post" timestamp
	,"note" text
	,"write_date" timestamp
	,"name" varchar NOT NULL
	,"message_main_attachment_id" int4
	,"company_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'maintenance_equipment_category');
drop foreign table if exists sm_maintenance_request;	create FOREIGN TABLE sm_maintenance_request("id" int4 NOT NULL
	,"create_date" timestamp
	,"write_date" timestamp
	,"request_date" date
	,"color" int4
	,"equipment_id" int4
	,"maintenance_team_id" int4 NOT NULL
	,"write_uid" int4
	,"user_id" int4
	,"close_date" date
	,"duration" float8
	,"create_uid" int4
	,"message_last_post" timestamp
	,"archive" bool
	,"priority" varchar
	,"maintenance_type" varchar
	,"description" text
	,"kanban_state" varchar NOT NULL
	,"owner_user_id" int4
	,"schedule_date" timestamp
	,"stage_id" int4
	,"category_id" int4
	,"name" varchar NOT NULL
	,"openupgrade_legacy_12_0_activity_date_deadline" date
	,"employee_id" int4
	,"department_id" int4
	,"message_main_attachment_id" int4
	,"company_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'maintenance_request');
drop foreign table if exists sm_maintenance_smp_user_scheduler;	create FOREIGN TABLE sm_maintenance_smp_user_scheduler("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"name" varchar NOT NULL
	,"write_uid" int4
	,"last_modified" date
	,"write_date" timestamp
	,"num_updates" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'maintenance_smp_user_scheduler');
drop foreign table if exists sm_maintenance_stage;	create FOREIGN TABLE sm_maintenance_stage("id" int4 NOT NULL
	,"create_uid" int4
	,"name" varchar NOT NULL
	,"sequence" int4
	,"write_uid" int4
	,"fold" bool
	,"done" bool
	,"write_date" timestamp
	,"create_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'maintenance_stage');
drop foreign table if exists sm_maintenance_team;	create FOREIGN TABLE sm_maintenance_team("id" int4 NOT NULL
	,"create_uid" int4
	,"name" varchar NOT NULL
	,"color" int4
	,"create_date" timestamp
	,"write_date" timestamp
	,"write_uid" int4
	,"partner_id" int4
	,"active" bool
	,"company_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'maintenance_team');
drop foreign table if exists sm_maintenance_team_users_rel;	create FOREIGN TABLE sm_maintenance_team_users_rel("maintenance_team_id" int4 NOT NULL
	,"res_users_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'maintenance_team_users_rel');
drop foreign table if exists sm_make_procurement;	create FOREIGN TABLE sm_make_procurement("id" int4 NOT NULL
	,"uom_id" int4 NOT NULL
	,"create_uid" int4
	,"product_id" int4 NOT NULL
	,"date_planned" date NOT NULL
	,"write_uid" int4
	,"res_model" varchar
	,"qty" numeric NOT NULL
	,"product_tmpl_id" int4 NOT NULL
	,"write_date" timestamp
	,"create_date" timestamp
	,"warehouse_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'make_procurement');
drop foreign table if exists sm_make_procurement_stock_location_route_rel;	create FOREIGN TABLE sm_make_procurement_stock_location_route_rel("make_procurement_id" int4 NOT NULL
	,"stock_location_route_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'make_procurement_stock_location_route_rel');
drop foreign table if exists sm_mass_mailing_ir_attachments_rel;	create FOREIGN TABLE sm_mass_mailing_ir_attachments_rel("mass_mailing_id" int4 NOT NULL
	,"attachment_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mass_mailing_ir_attachments_rel');
drop foreign table if exists sm_mass_mailing_list_merge;	create FOREIGN TABLE sm_mass_mailing_list_merge("id" int4 NOT NULL
	,"dest_list_id" int4
	,"merge_options" varchar NOT NULL
	,"new_list_name" varchar
	,"archive_src_lists" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mass_mailing_list_merge');
drop foreign table if exists sm_mass_mailing_schedule_date;	create FOREIGN TABLE sm_mass_mailing_schedule_date("id" int4 NOT NULL
	,"schedule_date" timestamp
	,"mass_mailing_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mass_mailing_schedule_date');
drop foreign table if exists sm_meeting_category_rel;	create FOREIGN TABLE sm_meeting_category_rel("event_id" int4 NOT NULL
	,"type_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'meeting_category_rel');
drop foreign table if exists sm_merge_opportunity_rel;	create FOREIGN TABLE sm_merge_opportunity_rel("merge_id" int4 NOT NULL
	,"opportunity_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'merge_opportunity_rel');
drop foreign table if exists sm_message_attachment_rel;	create FOREIGN TABLE sm_message_attachment_rel("message_id" int4 NOT NULL
	,"attachment_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'message_attachment_rel');
drop foreign table if exists sm_mis_budget;	create FOREIGN TABLE sm_mis_budget("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"description" varchar
	,"report_id" int4 NOT NULL
	,"date_range_id" int4
	,"date_from" date NOT NULL
	,"date_to" date NOT NULL
	,"state" varchar NOT NULL
	,"company_id" int4
	,"message_last_post" timestamp
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"message_main_attachment_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mis_budget');
drop foreign table if exists sm_mis_budget_by_account;	create FOREIGN TABLE sm_mis_budget_by_account("id" int4 NOT NULL
	,"company_id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"description" varchar
	,"date_range_id" int4
	,"date_from" date NOT NULL
	,"date_to" date NOT NULL
	,"state" varchar NOT NULL
	,"message_last_post" timestamp
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"message_main_attachment_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mis_budget_by_account');
drop foreign table if exists sm_mis_budget_by_account_item;	create FOREIGN TABLE sm_mis_budget_by_account_item("id" int4 NOT NULL
	,"budget_id" int4 NOT NULL
	,"debit" numeric
	,"credit" numeric
	,"balance" numeric
	,"company_id" int4
	,"company_currency_id" int4
	,"account_id" int4 NOT NULL
	,"date_range_id" int4
	,"date_from" date NOT NULL
	,"date_to" date NOT NULL
	,"analytic_account_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mis_budget_by_account_item');
drop foreign table if exists sm_mis_budget_item;	create FOREIGN TABLE sm_mis_budget_item("id" int4 NOT NULL
	,"budget_id" int4 NOT NULL
	,"kpi_expression_id" int4 NOT NULL
	,"date_range_id" int4
	,"analytic_account_id" int4
	,"date_from" date NOT NULL
	,"date_to" date NOT NULL
	,"amount" float8
	,"seq1" int4
	,"seq2" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mis_budget_item');
drop foreign table if exists sm_mis_report;	create FOREIGN TABLE sm_mis_report("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"description" varchar
	,"style_id" int4
	,"move_lines_source" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mis_report');
drop foreign table if exists sm_mis_report_instance;	create FOREIGN TABLE sm_mis_report_instance("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"date" date
	,"report_id" int4 NOT NULL
	,"target_move" varchar NOT NULL
	,"company_id" int4 NOT NULL
	,"multi_company" bool
	,"currency_id" int4
	,"landscape_pdf" bool
	,"no_auto_expand_accounts" bool
	,"display_columns_description" bool
	,"date_range_id" int4
	,"date_from" date
	,"date_to" date
	,"temporary" bool
	,"analytic_account_id" int4
	,"hide_analytic_filters" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"analytic_group_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mis_report_instance');
drop foreign table if exists sm_mis_report_instance_period;	create FOREIGN TABLE sm_mis_report_instance_period("id" int4 NOT NULL
	,"name" varchar(32) NOT NULL
	,"mode" varchar NOT NULL
	,"type" varchar
	,"is_ytd" bool
	,"date_range_type_id" int4
	,"offset" int4
	,"duration" int4
	,"manual_date_from" date
	,"manual_date_to" date
	,"date_range_id" int4
	,"sequence" int4
	,"report_instance_id" int4 NOT NULL
	,"normalize_factor" int4
	,"source" varchar NOT NULL
	,"source_aml_model_id" int4
	,"source_sumcol_accdet" bool
	,"source_cmpcol_from_id" int4
	,"source_cmpcol_to_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"source_mis_budget_id" int4
	,"analytic_account_id" int4
	,"source_mis_budget_by_account_id" int4
	,"analytic_group_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mis_report_instance_period');
drop foreign table if exists sm_mis_report_instance_period_mis_report_subkpi_rel;	create FOREIGN TABLE sm_mis_report_instance_period_mis_report_subkpi_rel("mis_report_instance_period_id" int4 NOT NULL
	,"mis_report_subkpi_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mis_report_instance_period_mis_report_subkpi_rel');
drop foreign table if exists sm_mis_report_instance_period_sum;	create FOREIGN TABLE sm_mis_report_instance_period_sum("id" int4 NOT NULL
	,"period_id" int4 NOT NULL
	,"period_to_sum_id" int4 NOT NULL
	,"sign" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mis_report_instance_period_sum');
drop foreign table if exists sm_mis_report_instance_res_company_rel;	create FOREIGN TABLE sm_mis_report_instance_res_company_rel("mis_report_instance_id" int4 NOT NULL
	,"res_company_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mis_report_instance_res_company_rel');
drop foreign table if exists sm_mis_report_kpi;	create FOREIGN TABLE sm_mis_report_kpi("id" int4 NOT NULL
	,"name" varchar(32) NOT NULL
	,"description" varchar NOT NULL
	,"multi" bool
	,"auto_expand_accounts" bool
	,"auto_expand_accounts_style_id" int4
	,"style_id" int4
	,"style_expression" varchar
	,"type" varchar NOT NULL
	,"compare_method" varchar NOT NULL
	,"accumulation_method" varchar NOT NULL
	,"sequence" int4
	,"report_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"budgetable" bool)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mis_report_kpi');
drop foreign table if exists sm_mis_report_kpi_expression;	create FOREIGN TABLE sm_mis_report_kpi_expression("id" int4 NOT NULL
	,"sequence" int4
	,"name" varchar
	,"kpi_id" int4 NOT NULL
	,"subkpi_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mis_report_kpi_expression');
drop foreign table if exists sm_mis_report_query;	create FOREIGN TABLE sm_mis_report_query("id" int4 NOT NULL
	,"name" varchar(32) NOT NULL
	,"model_id" int4 NOT NULL
	,"aggregate" varchar
	,"date_field" int4 NOT NULL
	,"domain" varchar
	,"report_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mis_report_query');
drop foreign table if exists sm_mis_report_style;	create FOREIGN TABLE sm_mis_report_style("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"color_inherit" bool
	,"color" varchar
	,"background_color_inherit" bool
	,"background_color" varchar
	,"font_style_inherit" bool
	,"font_style" varchar
	,"font_weight_inherit" bool
	,"font_weight" varchar
	,"font_size_inherit" bool
	,"font_size" varchar
	,"indent_level_inherit" bool
	,"indent_level" int4
	,"prefix_inherit" bool
	,"prefix" varchar(16)
	,"suffix_inherit" bool
	,"suffix" varchar(16)
	,"dp_inherit" bool
	,"dp" int4
	,"divider_inherit" bool
	,"divider" varchar
	,"hide_empty_inherit" bool
	,"hide_empty" bool
	,"hide_always_inherit" bool
	,"hide_always" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mis_report_style');
drop foreign table if exists sm_mis_report_subkpi;	create FOREIGN TABLE sm_mis_report_subkpi("id" int4 NOT NULL
	,"sequence" int4
	,"report_id" int4 NOT NULL
	,"name" varchar(32) NOT NULL
	,"description" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mis_report_subkpi');
drop foreign table if exists sm_mis_report_subreport;	create FOREIGN TABLE sm_mis_report_subreport("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"report_id" int4 NOT NULL
	,"subreport_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'mis_report_subreport');
drop foreign table if exists sm_open_items_report_wizard;	create FOREIGN TABLE sm_open_items_report_wizard("id" int4 NOT NULL
	,"company_id" int4
	,"date_at" date NOT NULL
	,"target_move" varchar NOT NULL
	,"hide_account_at_0" bool
	,"receivable_accounts_only" bool
	,"payable_accounts_only" bool
	,"foreign_currency" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'open_items_report_wizard');
drop foreign table if exists sm_open_items_report_wizard_res_partner_rel;	create FOREIGN TABLE sm_open_items_report_wizard_res_partner_rel("open_items_report_wizard_id" int4 NOT NULL
	,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'open_items_report_wizard_res_partner_rel');
drop foreign table if exists sm_openupgrade_legacy_11_0_procurement_rule;	create FOREIGN TABLE sm_openupgrade_legacy_11_0_procurement_rule("id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"name" varchar
	,"sequence" int4
	,"company_id" int4
	,"write_uid" int4
	,"action" varchar
	,"write_date" timestamp
	,"active" bool
	,"group_id" int4
	,"group_propagation_option" varchar
	,"partner_address_id" int4
	,"location_id" int4
	,"location_src_id" int4
	,"picking_type_id" int4
	,"delay" int4
	,"warehouse_id" int4
	,"propagate" bool
	,"procure_method" varchar
	,"route_sequence" int4
	,"route_id" int4
	,"propagate_warehouse_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'openupgrade_legacy_11_0_procurement_rule');
drop foreign table if exists sm_openupgrade_legacy_11_0_stock_location_path;	create FOREIGN TABLE sm_openupgrade_legacy_11_0_stock_location_path("id" int4
	,"location_from_id" int4
	,"create_uid" int4
	,"route_sequence" int4
	,"name" varchar
	,"picking_type_id" int4
	,"auto" varchar
	,"sequence" int4
	,"company_id" int4
	,"warehouse_id" int4
	,"delay" int4
	,"route_id" int4
	,"create_date" timestamp
	,"location_dest_id" int4
	,"write_date" timestamp
	,"active" bool
	,"propagate" bool
	,"write_uid" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'openupgrade_legacy_11_0_stock_location_path');
drop foreign table if exists sm_operation_request;	create FOREIGN TABLE sm_operation_request("id" int4 NOT NULL
	,"request_date" date
	,"effective_date" date
	,"partner_id" int4 NOT NULL
	,"partner_id_to" int4
	,"operation_type" varchar NOT NULL
	,"share_product_id" int4 NOT NULL
	,"share_to_product_id" int4
	,"quantity" int4 NOT NULL
	,"state" varchar NOT NULL
	,"user_id" int4
	,"receiver_not_member" bool
	,"company_id" int4 NOT NULL
	,"invoice" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'operation_request');
drop foreign table if exists sm_partago_user_sm_carsharing_registration_wizard;	create FOREIGN TABLE sm_partago_user_sm_carsharing_registration_wizard("id" int4 NOT NULL
	,"cs_mins_product_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"send_registration_email" bool
	,"write_uid" int4
	,"cs_mileage_product_id" int4
	,"write_date" timestamp
	,"cs_days_product_id" int4
	,"registration_billing_account" bool
	,"billing_account_minutes" int4
	,"group_config_moved0" varchar
	,"group_config" int4
	,"current_member" int4
	,"force_registration" bool
	,"group" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'partago_user_sm_carsharing_registration_wizard');
drop foreign table if exists sm_partner_create_subscription;	create FOREIGN TABLE sm_partner_create_subscription("id" int4 NOT NULL
	,"is_company" bool
	,"cooperator" int4
	,"register_number" varchar
	,"email" varchar NOT NULL
	,"bank_account" varchar NOT NULL
	,"share_product" int4 NOT NULL
	,"share_qty" int4 NOT NULL
	,"representative_name" varchar
	,"representative_email" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'partner_create_subscription');
drop foreign table if exists sm_partner_update_info;	create FOREIGN TABLE sm_partner_update_info("id" int4 NOT NULL
	,"is_company" bool
	,"register_number" varchar
	,"cooperator" int4
	,"from_sub_req" bool
	,"all" bool
	,"birthdate" bool
	,"legal_form" bool
	,"representative_function" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'partner_update_info');
drop foreign table if exists sm_payment_acquirer;	create FOREIGN TABLE sm_payment_acquirer("id" int4 NOT NULL
	,"create_date" timestamp
	,"done_msg" text
	,"fees_active" bool
	,"write_uid" int4
	,"cancel_msg" text
	,"registration_view_template_id" int4
	,"fees_dom_fixed" float8
	,"fees_int_fixed" float8
	,"create_uid" int4
	,"sequence" int4
	,"company_id" int4 NOT NULL
	,"environment" varchar NOT NULL
	,"provider" varchar NOT NULL
	,"website_published" bool
	,"auto_confirm" varchar
	,"pending_msg" text
	,"post_msg" text
	,"fees_int_var" float8
	,"write_date" timestamp
	,"pre_msg" text
	,"error_msg" text
	,"fees_dom_var" float8
	,"name" varchar NOT NULL
	,"view_template_id" int4 NOT NULL
	,"openupgrade_legacy_10_0_auto_confirm" varchar
	,"journal_id" int4
	,"module_id" int4
	,"save_token" varchar
	,"description" text
	,"capture_manually" bool
	,"specific_countries" bool
	,"payment_flow" varchar NOT NULL
	,"qr_code" bool
	,"so_reference_type" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'payment_acquirer');
drop foreign table if exists sm_payment_acquirer_onboarding_wizard;	create FOREIGN TABLE sm_payment_acquirer_onboarding_wizard("id" int4 NOT NULL
	,"payment_method" varchar
	,"paypal_email_account" varchar
	,"paypal_seller_account" varchar
	,"paypal_pdt_token" varchar
	,"stripe_secret_key" varchar
	,"stripe_publishable_key" varchar
	,"manual_name" varchar
	,"journal_name" varchar
	,"acc_number" varchar
	,"manual_post_msg" text
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'payment_acquirer_onboarding_wizard');
drop foreign table if exists sm_payment_acquirer_payment_icon_rel;	create FOREIGN TABLE sm_payment_acquirer_payment_icon_rel("payment_acquirer_id" int4 NOT NULL
	,"payment_icon_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'payment_acquirer_payment_icon_rel');
drop foreign table if exists sm_payment_country_rel;	create FOREIGN TABLE sm_payment_country_rel("payment_id" int4 NOT NULL
	,"country_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'payment_country_rel');
drop foreign table if exists sm_payment_icon;	create FOREIGN TABLE sm_payment_icon("id" int4 NOT NULL
	,"name" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'payment_icon');
drop foreign table if exists sm_payment_method;	create FOREIGN TABLE sm_payment_method("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"name" varchar
	,"write_uid" int4
	,"acquirer_ref" varchar NOT NULL
	,"acquirer_id" int4 NOT NULL
	,"write_date" timestamp
	,"active" bool
	,"partner_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'payment_method');
drop foreign table if exists sm_payment_return;	create FOREIGN TABLE sm_payment_return("id" int4 NOT NULL
	,"message_main_attachment_id" int4
	,"company_id" int4 NOT NULL
	,"date" date
	,"name" varchar NOT NULL
	,"journal_id" int4 NOT NULL
	,"move_id" int4
	,"state" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"imported_bank_account_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'payment_return');
drop foreign table if exists sm_payment_return_import;	create FOREIGN TABLE sm_payment_return_import("id" int4 NOT NULL
	,"journal_id" int4
	,"data_file" bytea NOT NULL
	,"match_after_import" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'payment_return_import');
drop foreign table if exists sm_payment_return_line;	create FOREIGN TABLE sm_payment_return_line("id" int4 NOT NULL
	,"return_id" int4 NOT NULL
	,"concept" varchar
	,"reason_id" int4
	,"reason_additional_information" varchar
	,"reference" varchar
	,"date" date
	,"partner_name" varchar
	,"partner_id" int4
	,"amount" numeric
	,"expense_account" int4
	,"expense_amount" float8
	,"expense_partner_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"unique_import_id" varchar
	,"raw_import_data" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'payment_return_line');
drop foreign table if exists sm_payment_return_reason;	create FOREIGN TABLE sm_payment_return_reason("id" int4 NOT NULL
	,"code" varchar
	,"name" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'payment_return_reason');
drop foreign table if exists sm_payment_token;	create FOREIGN TABLE sm_payment_token("id" int4 NOT NULL
	,"create_uid" int4
	,"name" varchar
	,"write_uid" int4
	,"acquirer_ref" varchar NOT NULL
	,"acquirer_id" int4 NOT NULL
	,"write_date" timestamp
	,"active" bool
	,"create_date" timestamp
	,"partner_id" int4 NOT NULL
	,"verified" bool)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'payment_token');
drop foreign table if exists sm_payment_transaction;	create FOREIGN TABLE sm_payment_transaction("id" int4 NOT NULL
	,"state_message" text
	,"callback_eval" varchar
	,"create_date" timestamp
	,"reference" varchar NOT NULL
	,"write_uid" int4
	,"currency_id" int4 NOT NULL
	,"acquirer_id" int4 NOT NULL
	,"fees" numeric
	,"partner_id" int4
	,"payment_method_id" int4
	,"create_uid" int4
	,"partner_name" varchar
	,"partner_phone" varchar
	,"state" varchar NOT NULL
	,"type" varchar NOT NULL
	,"partner_country_id" int4 NOT NULL
	,"acquirer_reference" varchar
	,"partner_address" varchar
	,"partner_email" varchar
	,"partner_lang" varchar
	,"write_date" timestamp
	,"partner_zip" varchar
	,"html_3ds" varchar
	,"date" timestamp
	,"partner_city" varchar
	,"amount" numeric NOT NULL
	,"payment_token_id" int4
	,"callback_model_id" int4
	,"callback_res_id" int4
	,"callback_method" varchar
	,"callback_hash" varchar
	,"openupgrade_legacy_12_0_state" varchar
	,"return_url" varchar
	,"is_processed" bool
	,"payment_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'payment_transaction');
drop foreign table if exists sm_payslip_lines_contribution_register;	create FOREIGN TABLE sm_payslip_lines_contribution_register("id" int4 NOT NULL
	,"date_from" date NOT NULL
	,"date_to" date NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'payslip_lines_contribution_register');
drop foreign table if exists sm_pocketbook_pocketbook_history;	create FOREIGN TABLE sm_pocketbook_pocketbook_history("id" int4 NOT NULL
	,"create_uid" int4
	,"obs" varchar
	,"create_date" timestamp
	,"name" varchar NOT NULL
	,"related_tariff_id" int4
	,"related_report_id" int4
	,"write_uid" int4
	,"amount" float8
	,"htype" varchar
	,"write_date" timestamp
	,"date" date
	,"related_reward_id" int4
	,"related_member_id" int4
	,"related_invoice_id" int4
	,"related_invoice_report_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'pocketbook_pocketbook_history');
drop foreign table if exists sm_pocketbook_pocketbook_history_wizard;	create FOREIGN TABLE sm_pocketbook_pocketbook_history_wizard("id" int4 NOT NULL
	,"create_uid" int4
	,"obs" varchar
	,"create_date" timestamp
	,"related_invoice_id" int4
	,"write_uid" int4
	,"amount" float8
	,"htype" varchar
	,"write_date" timestamp
	,"date" date
	,"name" varchar
	,"current_member" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'pocketbook_pocketbook_history_wizard');
drop foreign table if exists sm_pocketbook_pocketbook_record;	create FOREIGN TABLE sm_pocketbook_pocketbook_record("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"date" date
	,"amount" float8
	,"obs" varchar
	,"related_member_id" int4
	,"related_account_id" int4
	,"related_analytic_account_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"related_reward_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'pocketbook_pocketbook_record');
drop foreign table if exists sm_pocketbook_pocketbook_record_history;	create FOREIGN TABLE sm_pocketbook_pocketbook_record_history("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"date" date
	,"amount" float8
	,"related_invoice_line_id" int4
	,"obs" varchar
	,"related_pb_record_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'pocketbook_pocketbook_record_history');
drop foreign table if exists sm_portal_share;	create FOREIGN TABLE sm_portal_share("id" int4 NOT NULL
	,"res_model" varchar NOT NULL
	,"res_id" int4 NOT NULL
	,"note" text
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'portal_share');
drop foreign table if exists sm_portal_share_res_partner_rel;	create FOREIGN TABLE sm_portal_share_res_partner_rel("portal_share_id" int4 NOT NULL
	,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'portal_share_res_partner_rel');
drop foreign table if exists sm_portal_wizard;	create FOREIGN TABLE sm_portal_wizard("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"welcome_message" text
	,"write_uid" int4
	,"write_date" timestamp
	,"portal_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'portal_wizard');
drop foreign table if exists sm_portal_wizard_user;	create FOREIGN TABLE sm_portal_wizard_user("id" int4 NOT NULL
	,"create_uid" int4
	,"user_id" int4
	,"wizard_id" int4 NOT NULL
	,"write_uid" int4
	,"email" varchar
	,"write_date" timestamp
	,"create_date" timestamp
	,"in_portal" bool
	,"partner_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'portal_wizard_user');
drop foreign table if exists sm_procurement_group;	create FOREIGN TABLE sm_procurement_group("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"name" varchar NOT NULL
	,"move_type" varchar NOT NULL
	,"write_uid" int4
	,"write_date" timestamp
	,"partner_id" int4
	,"sale_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'procurement_group');
drop foreign table if exists sm_procurement_order;	create FOREIGN TABLE sm_procurement_order("id" int4 NOT NULL
	,"origin" varchar
	,"create_date" timestamp
	,"product_id" int4 NOT NULL
	,"product_uom" int4 NOT NULL
	,"create_uid" int4
	,"date_planned" timestamp NOT NULL
	,"message_last_post" timestamp
	,"company_id" int4 NOT NULL
	,"write_uid" int4
	,"priority" varchar NOT NULL
	,"state" varchar NOT NULL
	,"write_date" timestamp
	,"product_qty" numeric NOT NULL
	,"rule_id" int4
	,"group_id" int4
	,"name" text NOT NULL
	,"sale_line_id" int4
	,"location_id" int4
	,"partner_dest_id" int4
	,"orderpoint_id" int4
	,"warehouse_id" int4
	,"move_dest_id" int4
	,"purchase_line_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'procurement_order');
drop foreign table if exists sm_procurement_order_compute_all;	create FOREIGN TABLE sm_procurement_order_compute_all("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_date" timestamp
	,"write_uid" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'procurement_order_compute_all');
drop foreign table if exists sm_procurement_orderpoint_compute;	create FOREIGN TABLE sm_procurement_orderpoint_compute("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'procurement_orderpoint_compute');
drop foreign table if exists sm_product_attr_exclusion_value_ids_rel;	create FOREIGN TABLE sm_product_attr_exclusion_value_ids_rel("product_template_attribute_exclusion_id" int4 NOT NULL
	,"product_template_attribute_value_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'product_attr_exclusion_value_ids_rel');
drop foreign table if exists sm_product_attribute;	create FOREIGN TABLE sm_product_attribute("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"name" varchar NOT NULL
	,"sequence" int4
	,"write_uid" int4
	,"write_date" timestamp
	,"openupgrade_legacy_12_0_create_variant" bool
	,"create_variant" varchar NOT NULL
	,"type" varchar NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'product_attribute');
drop foreign table if exists sm_product_attribute_custom_value;	create FOREIGN TABLE sm_product_attribute_custom_value("id" int4 NOT NULL
	,"attribute_value_id" int4
	,"sale_order_line_id" int4
	,"custom_value" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'product_attribute_custom_value');
drop foreign table if exists sm_product_attribute_value;	create FOREIGN TABLE sm_product_attribute_value("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"name" varchar NOT NULL
	,"sequence" int4
	,"attribute_id" int4 NOT NULL
	,"write_date" timestamp
	,"write_uid" int4
	,"is_custom" bool
	,"html_color" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'product_attribute_value');
drop foreign table if exists sm_product_attribute_value_product_product_rel;	create FOREIGN TABLE sm_product_attribute_value_product_product_rel("product_attribute_value_id" int4 NOT NULL
	,"product_product_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'product_attribute_value_product_product_rel');
drop foreign table if exists sm_product_attribute_value_product_template_attribute_line_rel;	create FOREIGN TABLE sm_product_attribute_value_product_template_attribute_line_rel("product_template_attribute_line_id" int4 NOT NULL
	,"product_attribute_value_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'product_attribute_value_product_template_attribute_line_rel');
drop foreign table if exists sm_product_category;	create FOREIGN TABLE sm_product_category("id" int4 NOT NULL
	,"parent_left" int4
	,"parent_right" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"name" varchar NOT NULL
	,"openupgrade_legacy_10_0_sequence" int4
	,"write_uid" int4
	,"parent_id" int4
	,"write_date" timestamp
	,"type" varchar
	,"removal_strategy_id" int4
	,"complete_name" varchar
	,"parent_path" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'product_category');
drop foreign table if exists sm_product_optional_rel;	create FOREIGN TABLE sm_product_optional_rel("src_id" int4 NOT NULL
	,"dest_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'product_optional_rel');
drop foreign table if exists sm_product_packaging;	create FOREIGN TABLE sm_product_packaging("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"name" varchar NOT NULL
	,"sequence" int4
	,"qty" float8
	,"product_tmpl_id" int4
	,"write_date" timestamp
	,"write_uid" int4
	,"product_id" int4
	,"barcode" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'product_packaging');
drop foreign table if exists sm_product_price_history;	create FOREIGN TABLE sm_product_price_history("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"product_id" int4 NOT NULL
	,"company_id" int4 NOT NULL
	,"datetime" timestamp
	,"cost" numeric
	,"write_date" timestamp
	,"write_uid" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'product_price_history');
drop foreign table if exists sm_product_price_list;	create FOREIGN TABLE sm_product_price_list("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"price_list" int4 NOT NULL
	,"write_uid" int4
	,"qty1" int4
	,"qty2" int4
	,"qty3" int4
	,"qty4" int4
	,"qty5" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'product_price_list');
drop foreign table if exists sm_product_pricelist;	create FOREIGN TABLE sm_product_pricelist("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"name" varchar NOT NULL
	,"company_id" int4
	,"write_uid" int4
	,"currency_id" int4 NOT NULL
	,"write_date" timestamp
	,"active" bool
	,"sequence" int4
	,"discount_policy" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'product_pricelist');
drop foreign table if exists sm_product_pricelist_item;	create FOREIGN TABLE sm_product_pricelist_item("id" int4 NOT NULL
	,"fixed_price" numeric
	,"create_date" timestamp
	,"price_discount" numeric
	,"sequence" int4
	,"price_max_margin" numeric
	,"date_end" date
	,"write_uid" int4
	,"currency_id" int4
	,"applied_on" varchar NOT NULL
	,"min_quantity" int4
	,"create_uid" int4
	,"percent_price" float8
	,"date_start" date
	,"company_id" int4
	,"product_tmpl_id" int4
	,"pricelist_id" int4
	,"price_min_margin" numeric
	,"compute_price" varchar
	,"base" varchar NOT NULL
	,"write_date" timestamp
	,"categ_id" int4
	,"price_surcharge" numeric
	,"price_round" numeric
	,"product_id" int4
	,"base_pricelist_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'product_pricelist_item');
drop foreign table if exists sm_product_product;	create FOREIGN TABLE sm_product_product("id" int4 NOT NULL
	,"create_date" timestamp
	,"weight" numeric
	,"default_code" varchar
	,"name_template" varchar
	,"create_uid" int4
	,"message_last_post" timestamp
	,"product_tmpl_id" int4 NOT NULL
	,"barcode" varchar
	,"volume" float8
	,"write_date" timestamp
	,"active" bool
	,"write_uid" int4
	,"is_carsharing_product" bool
	,"carsharing_product_index" varchar
	,"is_pocketbook_product" bool
	,"openupgrade_legacy_12_0_activity_date_deadline" date
	,"message_main_attachment_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'product_product');
drop foreign table if exists sm_product_putaway;	create FOREIGN TABLE sm_product_putaway("id" int4 NOT NULL
	,"create_uid" int4
	,"name" varchar NOT NULL
	,"method" varchar
	,"write_date" timestamp
	,"create_date" timestamp
	,"write_uid" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'product_putaway');
drop foreign table if exists sm_product_removal;	create FOREIGN TABLE sm_product_removal("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"name" varchar NOT NULL
	,"method" varchar NOT NULL
	,"write_date" timestamp
	,"write_uid" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'product_removal');
drop foreign table if exists sm_product_replenish;	create FOREIGN TABLE sm_product_replenish("id" int4 NOT NULL
	,"product_id" int4 NOT NULL
	,"product_tmpl_id" int4 NOT NULL
	,"product_has_variants" bool NOT NULL
	,"product_uom_id" int4 NOT NULL
	,"quantity" float8 NOT NULL
	,"date_planned" timestamp
	,"warehouse_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'product_replenish');
drop foreign table if exists sm_product_replenish_stock_location_route_rel;	create FOREIGN TABLE sm_product_replenish_stock_location_route_rel("product_replenish_id" int4 NOT NULL
	,"stock_location_route_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'product_replenish_stock_location_route_rel');
drop foreign table if exists sm_product_supplier_taxes_rel;	create FOREIGN TABLE sm_product_supplier_taxes_rel("prod_id" int4 NOT NULL
	,"tax_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'product_supplier_taxes_rel');
drop foreign table if exists sm_product_supplierinfo;	create FOREIGN TABLE sm_product_supplierinfo("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"product_id" int4
	,"sequence" int4
	,"write_uid" int4
	,"product_name" varchar
	,"date_end" date
	,"date_start" date
	,"company_id" int4
	,"qty" numeric
	,"currency_id" int4 NOT NULL
	,"delay" int4 NOT NULL
	,"write_date" timestamp
	,"price" numeric NOT NULL
	,"min_qty" float8 NOT NULL
	,"product_code" varchar
	,"product_tmpl_id" int4
	,"name" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'product_supplierinfo');
drop foreign table if exists sm_product_taxes_rel;	create FOREIGN TABLE sm_product_taxes_rel("prod_id" int4 NOT NULL
	,"tax_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'product_taxes_rel');
drop foreign table if exists sm_product_template;	create FOREIGN TABLE sm_product_template("id" int4 NOT NULL
	,"warranty" float8
	,"list_price" numeric
	,"weight" numeric
	,"sequence" int4
	,"color" int4
	,"write_uid" int4
	,"uom_id" int4 NOT NULL
	,"description_purchase" text
	,"create_date" timestamp
	,"create_uid" int4
	,"sale_ok" bool
	,"categ_id" int4 NOT NULL
	,"openupgrade_legacy_10_0_product_manager" int4
	,"message_last_post" timestamp
	,"company_id" int4
	,"openupgrade_legacy_10_0_state" varchar
	,"uom_po_id" int4 NOT NULL
	,"description_sale" text
	,"description" text
	,"volume" float8
	,"write_date" timestamp
	,"active" bool
	,"rental" bool
	,"name" varchar NOT NULL
	,"type" varchar NOT NULL
	,"service_type" varchar
	,"invoice_policy" varchar
	,"default_code" varchar
	,"purchase_ok" bool
	,"openupgrade_legacy_10_0_invoice_policy" varchar
	,"sale_line_warn" varchar NOT NULL
	,"sale_line_warn_msg" text
	,"expense_policy" varchar
	,"tracking" varchar NOT NULL
	,"location_id" int4
	,"description_picking" text
	,"warehouse_id" int4
	,"sale_delay" float8
	,"purchase_line_warn_msg" text
	,"purchase_method" varchar
	,"purchase_line_warn" varchar NOT NULL
	,"openupgrade_legacy_12_0_activity_date_deadline" date
	,"responsible_id" int4 NOT NULL
	,"description_pickingout" text
	,"description_pickingin" text
	,"can_be_expensed" bool
	,"message_main_attachment_id" int4
	,"service_to_purchase" bool
	,"is_share" bool
	,"short_name" varchar
	,"display_on_website" bool
	,"default_share_product" bool
	,"minimum_quantity" int4
	,"force_min_qty" bool
	,"by_company" bool
	,"by_individual" bool
	,"customer" bool
	,"mail_template" int4
	,"_api_external_id" int4
	,"external_id_sequence_id" int4
	,"service_tracking" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'product_template');
drop foreign table if exists sm_product_template_attribute_exclusion;	create FOREIGN TABLE sm_product_template_attribute_exclusion("id" int4 NOT NULL
	,"product_template_attribute_value_id" int4
	,"product_tmpl_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'product_template_attribute_exclusion');
drop foreign table if exists sm_product_template_attribute_line;	create FOREIGN TABLE sm_product_template_attribute_line("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"product_tmpl_id" int4 NOT NULL
	,"attribute_id" int4 NOT NULL
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'product_template_attribute_line');
drop foreign table if exists sm_product_template_attribute_value;	create FOREIGN TABLE sm_product_template_attribute_value("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"price_extra" numeric
	,"product_tmpl_id" int4 NOT NULL
	,"product_attribute_value_id" int4 NOT NULL
	,"write_date" timestamp
	,"write_uid" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'product_template_attribute_value');
drop foreign table if exists sm_product_template_attribute_value_sale_order_line_rel;	create FOREIGN TABLE sm_product_template_attribute_value_sale_order_line_rel("sale_order_line_id" int4 NOT NULL
	,"product_template_attribute_value_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'product_template_attribute_value_sale_order_line_rel');
drop foreign table if exists sm_project_config_settings;	create FOREIGN TABLE sm_project_config_settings("id" int4 NOT NULL
	,"module_sale_service" int4
	,"module_pad" int4
	,"create_uid" int4
	,"module_project_timesheet_synchro" bool
	,"module_project_forecast" bool
	,"module_project_issue_sheet" int4
	,"write_uid" int4
	,"write_date" timestamp
	,"module_rating_project" int4
	,"generate_project_alias" int4
	,"create_date" timestamp
	,"group_time_work_estimation_tasks" int4
	,"module_project_timesheet" int4
	,"company_id" int4 NOT NULL
	,"module_rating_project_issue" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'project_config_settings');
drop foreign table if exists sm_project_create_invoice;	create FOREIGN TABLE sm_project_create_invoice("id" int4 NOT NULL
	,"project_id" int4 NOT NULL
	,"sale_order_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'project_create_invoice');
drop foreign table if exists sm_project_create_sale_order;	create FOREIGN TABLE sm_project_create_sale_order("id" int4 NOT NULL
	,"project_id" int4 NOT NULL
	,"partner_id" int4 NOT NULL
	,"product_id" int4
	,"price_unit" float8
	,"billable_type" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'project_create_sale_order');
drop foreign table if exists sm_project_create_sale_order_line;	create FOREIGN TABLE sm_project_create_sale_order_line("id" int4 NOT NULL
	,"wizard_id" int4 NOT NULL
	,"product_id" int4 NOT NULL
	,"price_unit" float8
	,"employee_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'project_create_sale_order_line');
drop foreign table if exists sm_project_favorite_user_rel;	create FOREIGN TABLE sm_project_favorite_user_rel("project_id" int4 NOT NULL
	,"user_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'project_favorite_user_rel');
drop foreign table if exists sm_project_issue;	create FOREIGN TABLE sm_project_issue("id" int4 NOT NULL
	,"date_closed" timestamp
	,"create_date" timestamp
	,"date_deadline" date
	,"color" int4
	,"date_last_stage_update" timestamp
	,"date_action_last" timestamp
	,"day_close" float8
	,"write_uid" int4
	,"date" timestamp
	,"team_id" int4
	,"duration" float8
	,"day_open" float8
	,"partner_id" int4
	,"date_action_next" timestamp
	,"create_uid" int4
	,"user_id" int4
	,"date_open" timestamp
	,"message_last_post" timestamp
	,"company_id" int4
	,"priority" varchar
	,"working_hours_open" float8
	,"email_cc" varchar
	,"project_id" int4
	,"channel" varchar
	,"description" text
	,"kanban_state" varchar NOT NULL
	,"write_date" timestamp
	,"active" bool
	,"stage_id" int4
	,"name" varchar NOT NULL
	,"task_id" int4
	,"email_from" varchar
	,"closed_description" text
	,"closed_user_id" int4
	,"open_description" text
	,"type" varchar
	,"rel_invoice_id" int4
	,"working_hours_close" float8)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'project_issue');
drop foreign table if exists sm_project_issue_report;	create FOREIGN TABLE sm_project_issue_report("id" int4
	,"opening_date" timestamp
	,"create_date" timestamp
	,"date_last_stage_update" timestamp
	,"user_id" int4
	,"working_hours_open" float8
	,"working_hours_close" float8
	,"stage_id" int4
	,"date_closed" timestamp
	,"company_id" int4
	,"priority" varchar
	,"project_id" int4
	,"nbr_issues" int4
	,"partner_id" int4
	,"delay_open" float8
	,"delay_close" float8
	,"email" int8)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'project_issue_report');
drop foreign table if exists sm_project_profitability_report;	create FOREIGN TABLE sm_project_profitability_report("id" int8
	,"project_id" int4
	,"user_id" int4
	,"sale_line_id" int4
	,"analytic_account_id" int4
	,"partner_id" int4
	,"company_id" int4
	,"currency_id" int4
	,"sale_order_id" int4
	,"order_confirmation_date" timestamp
	,"product_id" int4
	,"sale_qty_delivered_method" varchar
	,"expense_amount_untaxed_to_invoice" numeric
	,"expense_amount_untaxed_invoiced" numeric
	,"amount_untaxed_to_invoice" numeric
	,"amount_untaxed_invoiced" numeric
	,"timesheet_unit_amount" float8
	,"timesheet_cost" numeric
	,"expense_cost" numeric)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'project_profitability_report');
drop foreign table if exists sm_project_project;	create FOREIGN TABLE sm_project_project("id" int4 NOT NULL
	,"alias_model" varchar
	,"create_date" timestamp
	,"color" int4
	,"alias_id" int4 NOT NULL
	,"write_uid" int4
	,"privacy_visibility" varchar NOT NULL
	,"label_tasks" varchar
	,"analytic_account_id" int4
	,"create_uid" int4
	,"user_id" int4
	,"date_start" date
	,"message_last_post" timestamp
	,"state" varchar
	,"sequence" int4
	,"write_date" timestamp
	,"date" date
	,"active" bool
	,"resource_calendar_id" int4
	,"label_issues" varchar
	,"subtask_project_id" int4
	,"openupgrade_legacy_11_0_issue_project_id" int4
	,"type_id" int4
	,"description" text
	,"name" varchar NOT NULL
	,"partner_id" int4
	,"company_id" int4 NOT NULL
	,"openupgrade_legacy_12_0_analytic_account_id" int4
	,"access_token" varchar
	,"message_main_attachment_id" int4
	,"percentage_satisfaction_task" int4
	,"percentage_satisfaction_project" int4
	,"rating_request_deadline" timestamp
	,"rating_status" varchar NOT NULL
	,"rating_status_period" varchar
	,"portal_show_rating" bool
	,"ticket_count" int4
	,"label_tickets" varchar
	,"todo_ticket_count" int4
	,"allow_timesheets" bool
	,"is_template" bool
	,"sale_line_id" int4
	,"sale_order_id" int4
	,"billable_type" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'project_project');
drop foreign table if exists sm_project_sale_line_employee_map;	create FOREIGN TABLE sm_project_sale_line_employee_map("id" int4 NOT NULL
	,"project_id" int4 NOT NULL
	,"employee_id" int4 NOT NULL
	,"sale_line_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'project_sale_line_employee_map');
drop foreign table if exists sm_project_tags;	create FOREIGN TABLE sm_project_tags("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"name" varchar NOT NULL
	,"color" int4
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'project_tags');
drop foreign table if exists sm_project_tags_project_task_rel;	create FOREIGN TABLE sm_project_tags_project_task_rel("project_task_id" int4 NOT NULL
	,"project_tags_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'project_tags_project_task_rel');
drop foreign table if exists sm_project_task;	create FOREIGN TABLE sm_project_task("id" int4 NOT NULL
	,"create_date" timestamp
	,"sequence" int4
	,"color" int4
	,"date_end" timestamp
	,"write_uid" int4
	,"planned_hours" float8
	,"partner_id" int4
	,"create_uid" int4
	,"displayed_image_id" int4
	,"user_id" int4
	,"date_start" timestamp
	,"message_last_post" timestamp
	,"company_id" int4
	,"priority" varchar
	,"project_id" int4
	,"date_last_stage_update" timestamp
	,"date_assign" timestamp
	,"description" text
	,"kanban_state" varchar NOT NULL
	,"write_date" timestamp
	,"active" bool
	,"stage_id" int4
	,"date_deadline" date
	,"notes" text
	,"remaining_hours" float8
	,"name" varchar NOT NULL
	,"parent_id" int4
	,"email_from" varchar
	,"email_cc" varchar
	,"working_hours_open" float8
	,"working_hours_close" float8
	,"working_days_open" float8
	,"working_days_close" float8
	,"openupgrade_legacy_12_0_activity_date_deadline" date
	,"openupgrade_legacy_11_0_origin_issue_id" int4
	,"type_id" int4
	,"cs_task_type" varchar
	,"cs_task_car_id" int4
	,"cs_task_carconfig_id" int4
	,"cs_task_pu_id" int4
	,"cs_task_community_id" int4
	,"rating_last_value" float8
	,"access_token" varchar
	,"message_main_attachment_id" int4
	,"ticket_count" int4
	,"label_tickets" varchar
	,"todo_ticket_count" int4
	,"effective_hours" float8
	,"total_hours_spent" float8
	,"progress" float8
	,"subtask_effective_hours" float8
	,"sale_line_id" int4
	,"sale_order_id" int4
	,"billable_type" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'project_task');
drop foreign table if exists sm_project_task_assign_sale;	create FOREIGN TABLE sm_project_task_assign_sale("id" int4 NOT NULL
	,"sale_line_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'project_task_assign_sale');
drop foreign table if exists sm_project_task_assign_so_line_rel;	create FOREIGN TABLE sm_project_task_assign_so_line_rel("task_id" int4 NOT NULL
	,"wizard_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'project_task_assign_so_line_rel');
drop foreign table if exists sm_project_task_copy_map;	create FOREIGN TABLE sm_project_task_copy_map("id" int4 NOT NULL
	,"old_task_id" int4
	,"new_task_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'project_task_copy_map');
drop foreign table if exists sm_project_task_dependency_task_rel;	create FOREIGN TABLE sm_project_task_dependency_task_rel("task_id" int4 NOT NULL
	,"dependency_task_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'project_task_dependency_task_rel');
drop foreign table if exists sm_project_task_history;	create FOREIGN TABLE sm_project_task_history("id" int4 NOT NULL
	,"user_id" int4
	,"end_date" date
	,"task_id" int4 NOT NULL
	,"type_id" int4
	,"kanban_state" varchar
	,"date" date
	,"planned_hours" numeric
	,"remaining_hours" numeric)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'project_task_history');
drop foreign table if exists sm_project_task_history_cumulative;	create FOREIGN TABLE sm_project_task_history_cumulative("id" text
	,"end_date" date
	,"history_id" int4
	,"date" date
	,"task_id" int4
	,"type_id" int4
	,"user_id" int4
	,"kanban_state" varchar
	,"nbr_tasks" int8
	,"remaining_hours" numeric
	,"planned_hours" numeric
	,"project_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'project_task_history_cumulative');
drop foreign table if exists sm_project_task_material;	create FOREIGN TABLE sm_project_task_material("id" int4 NOT NULL
	,"task_id" int4 NOT NULL
	,"product_id" int4 NOT NULL
	,"quantity" float8
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'project_task_material');
drop foreign table if exists sm_project_task_merge_wizard;	create FOREIGN TABLE sm_project_task_merge_wizard("id" int4 NOT NULL
	,"user_id" int4
	,"create_new_task" bool
	,"target_task_name" varchar
	,"target_project_id" int4
	,"target_task_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'project_task_merge_wizard');
drop foreign table if exists sm_project_task_parent_rel;	create FOREIGN TABLE sm_project_task_parent_rel("parent_id" int4 NOT NULL
	,"task_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'project_task_parent_rel');
drop foreign table if exists sm_project_task_project_task_merge_wizard_rel;	create FOREIGN TABLE sm_project_task_project_task_merge_wizard_rel("project_task_merge_wizard_id" int4 NOT NULL
	,"project_task_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'project_task_project_task_merge_wizard_rel');
drop foreign table if exists sm_project_task_type;	create FOREIGN TABLE sm_project_task_type("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"description" text
	,"sequence" int4
	,"legend_done" varchar NOT NULL
	,"write_uid" int4
	,"fold" bool
	,"legend_blocked" varchar NOT NULL
	,"legend_priority" varchar
	,"write_date" timestamp
	,"legend_normal" varchar NOT NULL
	,"name" varchar NOT NULL
	,"mail_template_id" int4
	,"rating_template_id" int4
	,"auto_validation_kanban_state" bool
	,"closed" bool)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'project_task_type');
drop foreign table if exists sm_project_task_type_rel;	create FOREIGN TABLE sm_project_task_type_rel("type_id" int4 NOT NULL
	,"project_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'project_task_type_rel');
drop foreign table if exists sm_project_type;	create FOREIGN TABLE sm_project_type("id" int4 NOT NULL
	,"parent_id" int4
	,"name" varchar NOT NULL
	,"complete_name" varchar
	,"description" text
	,"project_ok" bool
	,"task_ok" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"code" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'project_type');
drop foreign table if exists sm_purchase_bill_union;	create FOREIGN TABLE sm_purchase_bill_union("id" int4
	,"name" varchar
	,"reference" varchar
	,"partner_id" int4
	,"date" date
	,"amount" numeric
	,"currency_id" int4
	,"company_id" int4
	,"vendor_bill_id" int4
	,"purchase_order_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'purchase_bill_union');
drop foreign table if exists sm_purchase_config_settings;	create FOREIGN TABLE sm_purchase_config_settings("id" int4 NOT NULL
	,"group_uom" int4
	,"group_warning_purchase" int4
	,"module_stock_dropshipping" int4
	,"group_costing_method" int4
	,"company_id" int4 NOT NULL
	,"write_uid" int4
	,"group_manage_vendor_price" int4
	,"write_date" timestamp
	,"create_uid" int4
	,"create_date" timestamp
	,"group_product_variant" int4
	,"module_purchase_requisition" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'purchase_config_settings');
drop foreign table if exists sm_purchase_order;	create FOREIGN TABLE sm_purchase_order("id" int4 NOT NULL
	,"origin" varchar
	,"create_date" timestamp
	,"write_uid" int4
	,"currency_id" int4 NOT NULL
	,"date_order" timestamp NOT NULL
	,"partner_id" int4 NOT NULL
	,"dest_address_id" int4
	,"create_uid" int4
	,"amount_untaxed" numeric
	,"picking_type_id" int4 NOT NULL
	,"message_last_post" timestamp
	,"company_id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"amount_tax" numeric
	,"state" varchar
	,"date_approve" date
	,"incoterm_id" int4
	,"payment_term_id" int4
	,"write_date" timestamp
	,"partner_ref" varchar
	,"fiscal_position_id" int4
	,"amount_total" numeric
	,"invoice_status" varchar
	,"date_planned" timestamp
	,"notes" text
	,"group_id" int4
	,"invoice_count" int4
	,"picking_count" int4
	,"openupgrade_legacy_12_0_activity_date_deadline" date
	,"supplier_partner_bank_id" int4
	,"payment_mode_id" int4
	,"user_id" int4
	,"access_token" varchar
	,"message_main_attachment_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'purchase_order');
drop foreign table if exists sm_purchase_order_line;	create FOREIGN TABLE sm_purchase_order_line("id" int4 NOT NULL
	,"create_date" timestamp
	,"product_uom" int4 NOT NULL
	,"price_unit" numeric NOT NULL
	,"qty_invoiced" numeric
	,"write_uid" int4
	,"currency_id" int4
	,"product_qty" numeric NOT NULL
	,"partner_id" int4
	,"qty_received" numeric
	,"create_uid" int4
	,"price_tax" float8
	,"sequence" int4
	,"company_id" int4
	,"state" varchar
	,"account_analytic_id" int4
	,"order_id" int4 NOT NULL
	,"price_subtotal" numeric
	,"write_date" timestamp
	,"product_id" int4 NOT NULL
	,"price_total" numeric
	,"name" text NOT NULL
	,"date_planned" timestamp NOT NULL
	,"orderpoint_id" int4
	,"product_uom_qty" float8
	,"sale_order_id" int4
	,"sale_line_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'purchase_order_line');
drop foreign table if exists sm_purchase_order_stock_picking_rel;	create FOREIGN TABLE sm_purchase_order_stock_picking_rel("purchase_order_id" int4 NOT NULL
	,"stock_picking_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'purchase_order_stock_picking_rel');
drop foreign table if exists sm_purchase_report;	create FOREIGN TABLE sm_purchase_report("id" int4
	,"date_order" timestamp
	,"state" varchar
	,"date_approve" date
	,"dest_address_id" int4
	,"partner_id" int4
	,"user_id" int4
	,"company_id" int4
	,"fiscal_position_id" int4
	,"product_id" int4
	,"product_tmpl_id" int4
	,"category_id" int4
	,"currency_id" int4
	,"product_uom" int4
	,"unit_quantity" numeric
	,"delay" float8
	,"delay_pass" float8
	,"nbr_lines" int8
	,"price_total" numeric(16,2)
	,"negociation" numeric(16,2)
	,"price_standard" numeric(16,2)
	,"price_average" numeric(16,2)
	,"country_id" int4
	,"commercial_partner_id" int4
	,"account_analytic_id" int4
	,"weight" numeric
	,"volume" float8
	,"picking_type_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'purchase_report');
drop foreign table if exists sm_queue_job;	create FOREIGN TABLE sm_queue_job("id" int4 NOT NULL
	,"uuid" varchar NOT NULL
	,"user_id" int4 NOT NULL
	,"company_id" int4
	,"name" varchar
	,"model_name" varchar
	,"method_name" varchar
	,"record_ids" text
	,"args" text
	,"kwargs" text
	,"func_string" varchar
	,"state" varchar NOT NULL
	,"priority" int4
	,"exc_info" text
	,"result" text
	,"date_created" timestamp
	,"date_started" timestamp
	,"date_enqueued" timestamp
	,"date_done" timestamp
	,"eta" timestamp
	,"retry" int4
	,"max_retries" int4
	,"channel_method_name" varchar
	,"job_function_id" int4
	,"channel" varchar
	,"identity_key" varchar
	,"openupgrade_legacy_12_0_activity_date_deadline" date
	,"message_last_post" timestamp
	,"override_channel" varchar
	,"message_main_attachment_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'queue_job');
drop foreign table if exists sm_queue_job_channel;	create FOREIGN TABLE sm_queue_job_channel("id" int4 NOT NULL
	,"name" varchar
	,"complete_name" varchar
	,"parent_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"removal_interval" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'queue_job_channel');
drop foreign table if exists sm_queue_job_function;	create FOREIGN TABLE sm_queue_job_function("id" int4 NOT NULL
	,"name" varchar
	,"channel_id" int4 NOT NULL
	,"channel" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'queue_job_function');
drop foreign table if exists sm_queue_job_queue_jobs_to_done_rel;	create FOREIGN TABLE sm_queue_job_queue_jobs_to_done_rel("queue_jobs_to_done_id" int4 NOT NULL
	,"queue_job_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'queue_job_queue_jobs_to_done_rel');
drop foreign table if exists sm_queue_job_queue_requeue_job_rel;	create FOREIGN TABLE sm_queue_job_queue_requeue_job_rel("queue_requeue_job_id" int4 NOT NULL
	,"queue_job_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'queue_job_queue_requeue_job_rel');
drop foreign table if exists sm_queue_jobs_to_done;	create FOREIGN TABLE sm_queue_jobs_to_done("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'queue_jobs_to_done');
drop foreign table if exists sm_queue_requeue_job;	create FOREIGN TABLE sm_queue_requeue_job("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'queue_requeue_job');
drop foreign table if exists sm_rating_rating;	create FOREIGN TABLE sm_rating_rating("id" int4 NOT NULL
	,"res_name" varchar
	,"res_model_id" int4
	,"res_model" varchar
	,"res_id" int4 NOT NULL
	,"parent_res_name" varchar
	,"parent_res_model_id" int4
	,"parent_res_model" varchar
	,"parent_res_id" int4
	,"rated_partner_id" int4
	,"partner_id" int4
	,"rating" float8
	,"rating_text" varchar
	,"feedback" text
	,"message_id" int4
	,"access_token" varchar
	,"consumed" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'rating_rating');
drop foreign table if exists sm_rel_modules_langexport;	create FOREIGN TABLE sm_rel_modules_langexport("wiz_id" int4 NOT NULL
	,"module_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'rel_modules_langexport');
drop foreign table if exists sm_rel_server_actions;	create FOREIGN TABLE sm_rel_server_actions("server_id" int4 NOT NULL
	,"action_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'rel_server_actions');
drop foreign table if exists sm_report;	create FOREIGN TABLE sm_report("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_date" timestamp
	,"write_uid" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'report');
drop foreign table if exists sm_report_aged_partner_balance;	create FOREIGN TABLE sm_report_aged_partner_balance("id" int4 NOT NULL
	,"date_at" date
	,"only_posted_moves" bool
	,"company_id" int4
	,"show_move_line_details" bool
	,"open_items_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'report_aged_partner_balance');
drop foreign table if exists sm_report_aged_partner_balance_account;	create FOREIGN TABLE sm_report_aged_partner_balance_account("id" int4 NOT NULL
	,"report_id" int4
	,"account_id" int4
	,"code" varchar
	,"name" varchar
	,"cumul_amount_residual" numeric
	,"cumul_current" numeric
	,"cumul_age_30_days" numeric
	,"cumul_age_60_days" numeric
	,"cumul_age_90_days" numeric
	,"cumul_age_120_days" numeric
	,"cumul_older" numeric
	,"percent_current" numeric
	,"percent_age_30_days" numeric
	,"percent_age_60_days" numeric
	,"percent_age_90_days" numeric
	,"percent_age_120_days" numeric
	,"percent_older" numeric
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'report_aged_partner_balance_account');
drop foreign table if exists sm_report_aged_partner_balance_line;	create FOREIGN TABLE sm_report_aged_partner_balance_line("id" int4 NOT NULL
	,"report_partner_id" int4
	,"partner" varchar
	,"amount_residual" numeric
	,"current" numeric
	,"age_30_days" numeric
	,"age_60_days" numeric
	,"age_90_days" numeric
	,"age_120_days" numeric
	,"older" numeric
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'report_aged_partner_balance_line');
drop foreign table if exists sm_report_aged_partner_balance_move_line;	create FOREIGN TABLE sm_report_aged_partner_balance_move_line("id" int4 NOT NULL
	,"report_partner_id" int4
	,"move_line_id" int4
	,"date" date
	,"date_due" date
	,"entry" varchar
	,"journal" varchar
	,"account" varchar
	,"partner" varchar
	,"label" varchar
	,"amount_residual" numeric
	,"current" numeric
	,"age_30_days" numeric
	,"age_60_days" numeric
	,"age_90_days" numeric
	,"age_120_days" numeric
	,"older" numeric
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'report_aged_partner_balance_move_line');
drop foreign table if exists sm_report_aged_partner_balance_partner;	create FOREIGN TABLE sm_report_aged_partner_balance_partner("id" int4 NOT NULL
	,"report_account_id" int4
	,"partner_id" int4
	,"name" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'report_aged_partner_balance_partner');
drop foreign table if exists sm_report_aged_partner_balance_res_partner_rel;	create FOREIGN TABLE sm_report_aged_partner_balance_res_partner_rel("report_aged_partner_balance_id" int4 NOT NULL
	,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'report_aged_partner_balance_res_partner_rel');
drop foreign table if exists sm_report_all_channels_sales;	create FOREIGN TABLE sm_report_all_channels_sales("id" int4
	,"name" varchar
	,"partner_id" int4
	,"product_id" int4
	,"product_tmpl_id" int4
	,"date_order" timestamp
	,"user_id" int4
	,"categ_id" int4
	,"company_id" int4
	,"price_total" numeric
	,"pricelist_id" int4
	,"analytic_account_id" int4
	,"country_id" int4
	,"team_id" int4
	,"price_subtotal" numeric
	,"product_qty" numeric)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'report_all_channels_sales');
drop foreign table if exists sm_report_general_ledger;	create FOREIGN TABLE sm_report_general_ledger("id" int4 NOT NULL
	,"date_from" date
	,"date_to" date
	,"fy_start_date" date
	,"only_posted_moves" bool
	,"hide_account_at_0" bool
	,"foreign_currency" bool
	,"show_analytic_tags" bool
	,"company_id" int4
	,"centralize" bool
	,"show_cost_center" bool
	,"unaffected_earnings_account" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"partner_ungrouped" bool)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'report_general_ledger');
drop foreign table if exists sm_report_general_ledger_account;	create FOREIGN TABLE sm_report_general_ledger_account("id" int4 NOT NULL
	,"report_id" int4
	,"account_id" int4
	,"code" varchar
	,"name" varchar
	,"initial_debit" numeric
	,"initial_credit" numeric
	,"initial_balance" numeric
	,"currency_id" int4
	,"initial_balance_foreign_currency" numeric
	,"final_debit" numeric
	,"final_credit" numeric
	,"final_balance" numeric
	,"final_balance_foreign_currency" numeric
	,"is_partner_account" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'report_general_ledger_account');
drop foreign table if exists sm_report_general_ledger_move_line;	create FOREIGN TABLE sm_report_general_ledger_move_line("id" int4 NOT NULL
	,"report_account_id" int4
	,"report_partner_id" int4
	,"move_line_id" int4
	,"date" date
	,"entry" varchar
	,"journal" varchar
	,"account" varchar
	,"taxes_description" varchar
	,"partner" varchar
	,"label" varchar
	,"cost_center" varchar
	,"tags" varchar
	,"matching_number" varchar
	,"debit" numeric
	,"credit" numeric
	,"cumul_balance" numeric
	,"currency_id" int4
	,"amount_currency" numeric
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'report_general_ledger_move_line');
drop foreign table if exists sm_report_general_ledger_partner;	create FOREIGN TABLE sm_report_general_ledger_partner("id" int4 NOT NULL
	,"report_account_id" int4
	,"partner_id" int4
	,"name" varchar
	,"initial_debit" numeric
	,"initial_credit" numeric
	,"initial_balance" numeric
	,"currency_id" int4
	,"initial_balance_foreign_currency" numeric
	,"final_debit" numeric
	,"final_credit" numeric
	,"final_balance" numeric
	,"final_balance_foreign_currency" numeric
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'report_general_ledger_partner');
drop foreign table if exists sm_report_general_ledger_res_partner_rel;	create FOREIGN TABLE sm_report_general_ledger_res_partner_rel("report_general_ledger_id" int4 NOT NULL
	,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'report_general_ledger_res_partner_rel');
drop foreign table if exists sm_report_journal_ledger;	create FOREIGN TABLE sm_report_journal_ledger("id" int4 NOT NULL
	,"date_from" date NOT NULL
	,"date_to" date NOT NULL
	,"company_id" int4 NOT NULL
	,"move_target" varchar NOT NULL
	,"sort_option" varchar NOT NULL
	,"group_option" varchar NOT NULL
	,"foreign_currency" bool
	,"with_account_name" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'report_journal_ledger');
drop foreign table if exists sm_report_journal_ledger_journal;	create FOREIGN TABLE sm_report_journal_ledger_journal("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"code" varchar
	,"report_id" int4 NOT NULL
	,"journal_id" int4 NOT NULL
	,"debit" numeric
	,"credit" numeric
	,"company_id" int4 NOT NULL
	,"currency_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'report_journal_ledger_journal');
drop foreign table if exists sm_report_journal_ledger_journal_tax_line;	create FOREIGN TABLE sm_report_journal_ledger_journal_tax_line("id" int4 NOT NULL
	,"report_journal_ledger_id" int4 NOT NULL
	,"report_id" int4 NOT NULL
	,"tax_id" int4
	,"tax_name" varchar
	,"tax_code" varchar
	,"base_debit" numeric
	,"base_credit" numeric
	,"tax_debit" numeric
	,"tax_credit" numeric
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'report_journal_ledger_journal_tax_line');
drop foreign table if exists sm_report_journal_ledger_move;	create FOREIGN TABLE sm_report_journal_ledger_move("id" int4 NOT NULL
	,"report_id" int4 NOT NULL
	,"report_journal_ledger_id" int4 NOT NULL
	,"move_id" int4 NOT NULL
	,"name" varchar
	,"company_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'report_journal_ledger_move');
drop foreign table if exists sm_report_journal_ledger_move_line;	create FOREIGN TABLE sm_report_journal_ledger_move_line("id" int4 NOT NULL
	,"report_id" int4 NOT NULL
	,"report_journal_ledger_id" int4 NOT NULL
	,"report_move_id" int4 NOT NULL
	,"move_line_id" int4 NOT NULL
	,"account_id" int4
	,"account" varchar
	,"account_code" varchar
	,"account_type" varchar
	,"partner" varchar
	,"partner_id" int4
	,"date" date
	,"entry" varchar
	,"label" varchar
	,"debit" numeric
	,"credit" numeric
	,"company_currency_id" int4
	,"amount_currency" numeric
	,"currency_id" int4
	,"currency_name" varchar
	,"taxes_description" varchar
	,"tax_id" int4
	,"company_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'report_journal_ledger_move_line');
drop foreign table if exists sm_report_journal_ledger_report_tax_line;	create FOREIGN TABLE sm_report_journal_ledger_report_tax_line("id" int4 NOT NULL
	,"report_id" int4 NOT NULL
	,"tax_id" int4
	,"tax_name" varchar
	,"tax_code" varchar
	,"base_debit" numeric
	,"base_credit" numeric
	,"tax_debit" numeric
	,"tax_credit" numeric
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'report_journal_ledger_report_tax_line');
drop foreign table if exists sm_report_layout;	create FOREIGN TABLE sm_report_layout("id" int4 NOT NULL
	,"view_id" int4 NOT NULL
	,"image" varchar
	,"pdf" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'report_layout');
drop foreign table if exists sm_report_open_items;	create FOREIGN TABLE sm_report_open_items("id" int4 NOT NULL
	,"date_at" date
	,"only_posted_moves" bool
	,"hide_account_at_0" bool
	,"foreign_currency" bool
	,"company_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'report_open_items');
drop foreign table if exists sm_report_open_items_account;	create FOREIGN TABLE sm_report_open_items_account("id" int4 NOT NULL
	,"report_id" int4
	,"account_id" int4
	,"code" varchar
	,"name" varchar
	,"currency_id" int4
	,"final_amount_residual" numeric
	,"final_amount_total_due" numeric
	,"final_amount_residual_currency" numeric
	,"final_amount_total_due_currency" numeric
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'report_open_items_account');
drop foreign table if exists sm_report_open_items_move_line;	create FOREIGN TABLE sm_report_open_items_move_line("id" int4 NOT NULL
	,"report_partner_id" int4
	,"move_line_id" int4
	,"date" date
	,"date_due" date
	,"entry" varchar
	,"journal" varchar
	,"account" varchar
	,"partner" varchar
	,"label" varchar
	,"amount_total_due" numeric
	,"amount_residual" numeric
	,"currency_id" int4
	,"amount_total_due_currency" numeric
	,"amount_residual_currency" numeric
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'report_open_items_move_line');
drop foreign table if exists sm_report_open_items_partner;	create FOREIGN TABLE sm_report_open_items_partner("id" int4 NOT NULL
	,"report_account_id" int4
	,"partner_id" int4
	,"name" varchar
	,"currency_id" int4
	,"final_amount_residual" numeric
	,"final_amount_total_due" numeric
	,"final_amount_residual_currency" numeric
	,"final_amount_total_due_currency" numeric
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'report_open_items_partner');
drop foreign table if exists sm_report_open_items_res_partner_rel;	create FOREIGN TABLE sm_report_open_items_res_partner_rel("report_open_items_id" int4 NOT NULL
	,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'report_open_items_res_partner_rel');
drop foreign table if exists sm_report_paperformat;	create FOREIGN TABLE sm_report_paperformat("id" int4 NOT NULL
	,"create_uid" int4
	,"page_width" int4
	,"create_date" timestamp
	,"orientation" varchar
	,"format" varchar
	,"default" bool
	,"header_line" bool
	,"header_spacing" int4
	,"write_uid" int4
	,"margin_right" float8
	,"margin_top" float8
	,"margin_left" float8
	,"write_date" timestamp
	,"page_height" int4
	,"margin_bottom" float8
	,"dpi" int4 NOT NULL
	,"name" varchar NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'report_paperformat');
drop foreign table if exists sm_report_project_task_user;	create FOREIGN TABLE sm_report_project_task_user("nbr" int4
	,"id" int4
	,"date_start" timestamp
	,"date_end" timestamp
	,"date_last_stage_update" timestamp
	,"date_deadline" date
	,"user_id" int4
	,"project_id" int4
	,"priority" varchar
	,"name" varchar
	,"company_id" int4
	,"partner_id" int4
	,"stage_id" int4
	,"state" varchar
	,"working_days_close" float8
	,"working_days_open" float8
	,"delay_endings_days" float8
	,"progress" float8
	,"hours_effective" float8
	,"remaining_hours" float8
	,"hours_planned" float8)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'report_project_task_user');
drop foreign table if exists sm_report_stock_forecast;	create FOREIGN TABLE sm_report_stock_forecast("id" int4
	,"product_id" int4
	,"date" text
	,"quantity" float8
	,"cumulative_quantity" float8
	,"company_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'report_stock_forecast');
drop foreign table if exists sm_report_stock_lines_date;	create FOREIGN TABLE sm_report_stock_lines_date("id" int4
	,"product_id" int4
	,"date" timestamp
	,"move_date" timestamp
	,"active" bool)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'report_stock_lines_date');
drop foreign table if exists sm_report_trial_balance;	create FOREIGN TABLE sm_report_trial_balance("id" int4 NOT NULL
	,"date_from" date
	,"date_to" date
	,"fy_start_date" date
	,"only_posted_moves" bool
	,"hide_account_at_0" bool
	,"foreign_currency" bool
	,"company_id" int4
	,"show_partner_details" bool
	,"hierarchy_on" varchar NOT NULL
	,"limit_hierarchy_level" bool
	,"show_hierarchy_level" int4
	,"hide_parent_hierarchy_level" bool
	,"general_ledger_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'report_trial_balance');
drop foreign table if exists sm_report_trial_balance_account;	create FOREIGN TABLE sm_report_trial_balance_account("id" int4 NOT NULL
	,"report_id" int4
	,"sequence_moved0" int4
	,"level" int4
	,"account_id" int4
	,"account_group_id" int4
	,"parent_id" int4
	,"child_account_ids" varchar
	,"code" varchar
	,"name" varchar
	,"currency_id" int4
	,"initial_balance" numeric
	,"initial_balance_foreign_currency" numeric
	,"debit" numeric
	,"credit" numeric
	,"period_balance" numeric
	,"final_balance" numeric
	,"final_balance_foreign_currency" numeric
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"sequence" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'report_trial_balance_account');
drop foreign table if exists sm_report_trial_balance_partner;	create FOREIGN TABLE sm_report_trial_balance_partner("id" int4 NOT NULL
	,"report_account_id" int4
	,"partner_id" int4
	,"name" varchar
	,"currency_id" int4
	,"initial_balance" numeric
	,"initial_balance_foreign_currency" numeric
	,"debit" numeric
	,"credit" numeric
	,"period_balance" numeric
	,"final_balance" numeric
	,"final_balance_foreign_currency" numeric
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'report_trial_balance_partner');
drop foreign table if exists sm_report_trial_balance_res_partner_rel;	create FOREIGN TABLE sm_report_trial_balance_res_partner_rel("report_trial_balance_id" int4 NOT NULL
	,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'report_trial_balance_res_partner_rel');
drop foreign table if exists sm_report_vat_report;	create FOREIGN TABLE sm_report_vat_report("id" int4 NOT NULL
	,"company_id" int4
	,"date_from" date
	,"date_to" date
	,"based_on" varchar NOT NULL
	,"tax_detail" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'report_vat_report');
drop foreign table if exists sm_report_vat_report_tax;	create FOREIGN TABLE sm_report_vat_report_tax("id" int4 NOT NULL
	,"report_tax_id" int4
	,"tax_id" int4
	,"code" varchar
	,"name" varchar
	,"net" numeric
	,"tax" numeric
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'report_vat_report_tax');
drop foreign table if exists sm_report_vat_report_taxtag;	create FOREIGN TABLE sm_report_vat_report_taxtag("id" int4 NOT NULL
	,"report_id" int4
	,"taxtag_id" int4
	,"taxgroup_id" int4
	,"code" varchar
	,"name" varchar
	,"net" numeric
	,"tax" numeric
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'report_vat_report_taxtag');
drop foreign table if exists sm_res_bank;	create FOREIGN TABLE sm_res_bank("id" int4 NOT NULL
	,"create_uid" int4
	,"fax" varchar
	,"create_date" timestamp
	,"name" varchar NOT NULL
	,"zip" varchar
	,"city" varchar
	,"country" int4
	,"street2" varchar
	,"bic" varchar
	,"write_uid" int4
	,"phone" varchar
	,"state" int4
	,"street" varchar
	,"write_date" timestamp
	,"active" bool
	,"email" varchar
	,"code" varchar
	,"lname" varchar(128)
	,"vat" varchar(32)
	,"website" varchar(64))SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'res_bank');
drop foreign table if exists sm_res_city;	create FOREIGN TABLE sm_res_city("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"zipcode" varchar
	,"country_id" int4 NOT NULL
	,"state_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'res_city');
drop foreign table if exists sm_res_city_zip;	create FOREIGN TABLE sm_res_city_zip("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"city_id" int4 NOT NULL
	,"display_name" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'res_city_zip');
drop foreign table if exists sm_res_company;	create FOREIGN TABLE sm_res_company("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"partner_id" int4 NOT NULL
	,"currency_id" int4 NOT NULL
	,"rml_footer" text
	,"create_date" timestamp
	,"rml_header" text
	,"rml_paper_format" varchar
	,"write_uid" int4
	,"logo_web" bytea
	,"font" int4
	,"account_no" varchar
	,"parent_id" int4
	,"email" varchar
	,"create_uid" int4
	,"custom_footer" bool
	,"phone" varchar
	,"rml_header2" text
	,"rml_header3" text
	,"write_date" timestamp
	,"rml_header1" varchar
	,"company_registry" varchar
	,"paperformat_id" int4
	,"fiscalyear_lock_date" date
	,"bank_account_code_prefix" varchar
	,"cash_account_code_prefix" varchar
	,"anglo_saxon_accounting" bool
	,"fiscalyear_last_day" int4 NOT NULL
	,"expects_chart_of_accounts" bool
	,"property_stock_valuation_account_id" int4
	,"transfer_account_id" int4
	,"property_stock_account_output_categ_id" int4
	,"property_stock_account_input_categ_id" int4
	,"period_lock_date" date
	,"paypal_account" varchar(128)
	,"accounts_code_digits" int4
	,"chart_template_id" int4
	,"overdue_msg" text
	,"currency_exchange_journal_id" int4
	,"fiscalyear_last_month" int4 NOT NULL
	,"tax_calculation_rounding_method" varchar
	,"vat_check_vies" bool
	,"project_time_mode_id" int4
	,"sale_note" text
	,"cs_teletac_product_id" int4
	,"invoice_report_mail_template_id" int4
	,"abouttoexpire_mail_template_id" int4
	,"invoice_mail_template_id" int4
	,"default_tariff_model_id" int4
	,"tariffs_pdf_id" int4
	,"welcome_tariff_model_id" int4
	,"cs_carsharing_product_id" int4
	,"lopd_mail_template_id" int4
	,"contribution_year_report_email_template_id" int4
	,"sequence" int4
	,"propagation_minimum_delta" int4
	,"internal_transit_location_id" int4
	,"po_lead" float8 NOT NULL
	,"po_double_validation_amount" numeric
	,"po_double_validation" varchar
	,"po_lock" varchar
	,"security_lead" float8 NOT NULL
	,"report_header" text
	,"report_footer" text
	,"openupgrade_legacy_12_0_external_report_layout" varchar
	,"resource_calendar_id" int4
	,"tax_cash_basis_journal_id" int4
	,"tax_exigibility" bool
	,"account_opening_move_id" int4
	,"account_setup_company_data_done" bool
	,"account_setup_bank_data_done" bool
	,"account_setup_fy_data_done" bool
	,"account_setup_coa_done" bool
	,"account_setup_bar_closed" bool
	,"cs_missing_data_email_template_id" int4
	,"cs_reward_completed_email_template_id" int4
	,"cs_reward_soci_not_found_email_template_id" int4
	,"cs_complete_data_soci_not_found_email_template_id" int4
	,"cs_complete_data_successful_email_template_id" int4
	,"cs_already_active" int4
	,"cs_already_requested_access" int4
	,"percentage_extra_minutes_cost" int4
	,"percentage_not_used" int4
	,"maxim_kms_per_hour" int4
	,"maxim_kms_per_day" int4
	,"pocketbook_threshold" float8
	,"default_welcome" text
	,"invoice_report_teletac_mail_template_id" int4
	,"initiating_party_issuer" varchar(35)
	,"initiating_party_identifier" varchar(35)
	,"initiating_party_scheme" varchar(35)
	,"sepa_creditor_identifier" varchar(35)
	,"favicon_backend" bytea
	,"favicon_backend_mimetype" varchar
	,"adjustment_credit_account_id" int4
	,"adjustment_debit_account_id" int4
	,"invoice_import_email" varchar
	,"sm_contribution_account_id" int4
	,"sm_contribution_tax_account_id" int4
	,"lopd_company_mail_template_id" int4
	,"cs_discount_product_id" int4
	,"reward_account_id" int4
	,"reward_analytic_account_id" int4
	,"notfound_car_analytic_account_id" int4
	,"notfound_teletac_analytic_account_id" int4
	,"cs_invoice_payment_mode_id" int4
	,"cs_company_access_already_requested" int4
	,"notification_tariff_creation_id" int4
	,"external_report_layout_id" int4
	,"base_onboarding_company_state" varchar
	,"nomenclature_id" int4
	,"snailmail_color" bool
	,"snailmail_duplex" bool
	,"incoterm_id" int4
	,"transfer_account_code_prefix" varchar
	,"account_sale_tax_id" int4
	,"account_purchase_tax_id" int4
	,"account_bank_reconciliation_start" date
	,"invoice_reference_type" varchar
	,"qr_code" bool
	,"invoice_is_email" bool
	,"invoice_is_print" bool
	,"account_setup_bank_data_state" varchar
	,"account_setup_fy_data_state" varchar
	,"account_setup_coa_state" varchar
	,"account_onboarding_invoice_layout_state" varchar
	,"account_onboarding_sample_invoice_state" varchar
	,"account_onboarding_sale_tax_state" varchar
	,"account_invoice_onboarding_state" varchar
	,"account_dashboard_onboarding_state" varchar
	,"create_new_line_at_contract_line_renew" bool
	,"payment_acquirer_onboarding_state" varchar
	,"payment_onboarding_payment_method" varchar
	,"portal_confirmation_sign" bool
	,"portal_confirmation_pay" bool
	,"quotation_validity_days" int4
	,"sale_quotation_onboarding_state" varchar
	,"sale_onboarding_order_confirmation_state" varchar
	,"sale_onboarding_sample_quotation_state" varchar
	,"sale_onboarding_payment_method" varchar
	,"tax_agency_id" int4
	,"invoice_import_create_bank_account" bool
	,"social_twitter" varchar
	,"social_facebook" varchar
	,"social_github" varchar
	,"social_linkedin" varchar
	,"social_youtube" varchar
	,"social_googleplus" varchar
	,"social_instagram" varchar
	,"coop_email_contact" varchar
	,"subscription_maximum_amount" float8
	,"default_country_id" int4
	,"default_lang_id" int4
	,"allow_id_card_upload" bool
	,"create_user" bool
	,"board_representative" varchar
	,"signature_scan" bytea
	,"unmix_share_type" bool
	,"display_logo1" bool
	,"display_logo2" bool
	,"bottom_logo1" bytea
	,"bottom_logo2" bytea
	,"display_data_policy_approval" bool
	,"data_policy_approval_required" bool
	,"data_policy_approval_text" text
	,"display_internal_rules_approval" bool
	,"internal_rules_approval_required" bool
	,"internal_rules_approval_text" text
	,"display_financial_risk_approval" bool
	,"financial_risk_approval_required" bool
	,"financial_risk_approval_text" text
	,"send_certificate_email" bool
	,"send_confirmation_email" bool
	,"send_capital_release_email" bool
	,"subscription_invoice_payment_mode_id" int4
	,"timesheet_encode_uom_id" int4 NOT NULL
	,"leave_timesheet_project_id" int4
	,"leave_timesheet_task_id" int4
	,"cs_invoice_journal_id" int4
	,"sm_carsharing_api_credentials_api_key" varchar
	,"sm_carsharing_api_credentials_cs_url" varchar
	,"sm_carsharing_api_credentials_admin_group" varchar
	,"sm_wordpress_db_credentials_admin_host" varchar
	,"sm_wordpress_db_credentials_admin_username" varchar
	,"sm_wordpress_db_credentials_admin_password" varchar
	,"sm_wordpress_db_credentials_db_host" varchar
	,"sm_wordpress_db_credentials_db_username" varchar
	,"sm_wordpress_db_credentials_db_password" varchar
	,"sm_wordpress_db_credentials_db_database" varchar
	,"sm_firebase_auth_type" varchar
	,"sm_firebase_auth_project_id" varchar
	,"sm_firebase_auth_private_key_id" varchar
	,"sm_firebase_auth_private_key" varchar
	,"sm_firebase_auth_client_email" varchar
	,"sm_firebase_auth_client_id" varchar
	,"sm_firebase_auth_auth_uri" varchar
	,"sm_firebase_auth_token_uri" varchar
	,"sm_firebase_auth_provider_x509" varchar
	,"sm_firebase_auth_client_x509" varchar
	,"sm_firebase_auth_db_ref" varchar
	,"sm_user_person_group" varchar
	,"sm_user_person_group_prepayment" varchar
	,"sm_user_person_default_language" varchar
	,"sm_user_allowed_user_langs_es" varchar
	,"sm_user_allowed_user_langs_ca" varchar
	,"sm_tariff_month_duration_welcome" varchar
	,"sm_timezone" varchar
	,"sm_system_project_id_moved0" varchar
	,"sm_zip_api_key" varchar
	,"sm_system_project_id" int4
	,"sm_contribution_invoice_account_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'res_company');
drop foreign table if exists sm_res_company_users_rel;	create FOREIGN TABLE sm_res_company_users_rel("cid" int4 NOT NULL
	,"user_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'res_company_users_rel');
drop foreign table if exists sm_res_config;	create FOREIGN TABLE sm_res_config("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_date" timestamp
	,"write_uid" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'res_config');
drop foreign table if exists sm_res_config_installer;	create FOREIGN TABLE sm_res_config_installer("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_date" timestamp
	,"write_uid" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'res_config_installer');
drop foreign table if exists sm_res_config_settings;	create FOREIGN TABLE sm_res_config_settings("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_date" timestamp
	,"write_uid" int4
	,"group_multi_company" bool
	,"company_id" int4 NOT NULL
	,"user_default_rights" bool
	,"external_email_server_default" bool
	,"module_base_import" bool
	,"module_google_calendar" bool
	,"module_google_drive" bool
	,"module_google_spreadsheet" bool
	,"module_auth_oauth" bool
	,"module_auth_ldap" bool
	,"module_base_gengo" bool
	,"module_inter_company_rules" bool
	,"module_pad" bool
	,"module_voip" bool
	,"company_share_partner" bool
	,"default_custom_report_footer" bool
	,"group_multi_currency" bool
	,"fail_counter" int4
	,"alias_domain" varchar
	,"auth_signup_reset_password" bool
	,"auth_signup_uninvited" varchar
	,"auth_signup_template_user_id" int4
	,"company_share_product" bool
	,"group_uom" bool
	,"group_product_variant" bool
	,"group_stock_packaging" bool
	,"group_sale_pricelist" bool
	,"group_product_pricelist" bool
	,"group_pricelist_item" bool
	,"module_procurement_jit" int4
	,"module_product_expiry" bool
	,"group_stock_production_lot" bool
	,"group_stock_tracking_lot" bool
	,"group_stock_tracking_owner" bool
	,"group_stock_adv_location" bool
	,"group_warning_stock" bool
	,"use_propagation_minimum_delta" bool
	,"module_stock_picking_batch" bool
	,"module_stock_barcode" bool
	,"module_delivery_dhl" bool
	,"module_delivery_fedex" bool
	,"module_delivery_ups" bool
	,"module_delivery_usps" bool
	,"module_delivery_bpost" bool
	,"group_stock_multi_locations" bool
	,"group_stock_multi_warehouses" bool
	,"chart_template_id" int4
	,"module_account_accountant" bool
	,"group_analytic_accounting" bool
	,"group_warning_account" bool
	,"group_cash_rounding" bool
	,"module_account_asset" bool
	,"module_account_deferred_revenue" bool
	,"module_account_budget" bool
	,"module_account_payment" bool
	,"module_account_reports" bool
	,"module_account_reports_followup" bool
	,"module_l10n_us_check_printing" bool
	,"module_account_batch_deposit" bool
	,"module_account_sepa" bool
	,"module_account_sepa_direct_debit" bool
	,"module_account_plaid" bool
	,"module_account_yodlee" bool
	,"module_account_bank_statement_import_qif" bool
	,"module_account_bank_statement_import_ofx" bool
	,"module_account_bank_statement_import_csv" bool
	,"module_account_bank_statement_import_camt" bool
	,"module_currency_rate_live" bool
	,"module_print_docsaway" bool
	,"module_product_margin" bool
	,"module_l10n_eu_service" bool
	,"module_account_taxcloud" bool
	,"module_hr_timesheet" bool
	,"module_rating_project" bool
	,"module_project_forecast" bool
	,"group_subtask_project" bool
	,"use_sale_note" bool
	,"group_discount_per_so_line" bool
	,"module_sale_margin" bool
	,"group_sale_layout" bool
	,"group_warning_sale" bool
	,"portal_confirmation" bool
	,"portal_confirmation_options" varchar
	,"module_sale_payment" bool
	,"module_website_quote" bool
	,"group_sale_delivery_address" bool
	,"multi_sales_price" bool
	,"multi_sales_price_method" varchar
	,"sale_pricelist_setting" varchar
	,"group_show_line_subtotals_tax_excluded" bool
	,"group_show_line_subtotals_tax_included" bool
	,"group_proforma_sales" bool
	,"show_line_subtotals_tax_selection" varchar NOT NULL
	,"default_invoice_policy" varchar
	,"deposit_default_product_id" int4
	,"auto_done_setting" bool
	,"module_website_sale_digital" bool
	,"module_delivery" bool
	,"module_product_email_template" bool
	,"module_sale_coupon" bool
	,"module_stock_landed_costs" bool
	,"lock_confirmed_po" bool
	,"po_order_approval" bool
	,"default_purchase_method" varchar
	,"module_purchase_requisition" bool
	,"group_warning_purchase" bool
	,"module_stock_dropshipping" bool
	,"group_manage_vendor_price" bool
	,"module_account_3way_match" bool
	,"is_installed_sale" bool
	,"group_analytic_account_for_purchases" bool
	,"use_po_lead" bool
	,"group_route_so_lines" bool
	,"module_sale_order_dates" bool
	,"group_display_incoterm" bool
	,"use_security_lead" bool
	,"default_picking_policy" varchar NOT NULL
	,"crm_alias_prefix" varchar
	,"generate_lead_from_alias" bool
	,"group_use_lead" bool
	,"module_crm_phone_validation" bool
	,"module_web_clearbit" bool
	,"cal_client_id" varchar
	,"cal_client_secret" varchar
	,"server_uri" varchar
	,"group_pain_multiple_identifier" bool
	,"module_document" bool
	,"group_ir_attachment_user" bool
	,"module_document_page" bool
	,"module_document_page_approval" bool
	,"module_cmis_read" bool
	,"module_cmis_write" bool
	,"module_hr_org_chart" bool
	,"module_l10n_fr_hr_payroll" bool
	,"module_l10n_be_hr_payroll" bool
	,"module_l10n_in_hr_payroll" bool
	,"module_agreement_maintenance" bool
	,"module_agreement_mrp" bool
	,"module_agreement_project" bool
	,"module_agreement_repair" bool
	,"module_agreement_rma" bool
	,"module_agreement_sale" bool
	,"module_agreement_sale_subscription" bool
	,"module_agreement_stock" bool
	,"module_fieldservice_agreement" bool
	,"module_helpdesk_agreement" bool
	,"expense_alias_prefix" varchar
	,"use_mailgateway" bool
	,"module_sale_management" bool
	,"module_web_unsplash" bool
	,"module_partner_autocomplete" bool
	,"show_effect" bool
	,"unsplash_access_key" varchar
	,"product_weight_in_lbs" varchar
	,"digest_emails" bool
	,"digest_id" int4
	,"group_lot_on_delivery_slip" bool
	,"module_delivery_easypost" bool
	,"group_analytic_tags" bool
	,"group_fiscal_year" bool
	,"group_products_in_bills" bool
	,"module_account_check_printing" bool
	,"module_account_batch_payment" bool
	,"module_account_intrastat" bool
	,"module_account_invoice_extract" bool
	,"module_crm_reveal" bool
	,"group_project_rating" bool
	,"module_account_asset_management" bool
	,"group_show_form_view" bool
	,"use_quotation_validity_days" bool
	,"group_sale_order_dates" bool
	,"automatic_invoice" bool
	,"template_id" int4
	,"group_sale_order_template" bool
	,"default_sale_order_template_id" int4
	,"module_sale_quotation_builder" bool
	,"partner_names_order" varchar NOT NULL
	,"module_agreement_helpdesk" bool
	,"group_mass_mailing_campaign" bool
	,"mass_mailing_outgoing_mail_server" bool
	,"mass_mailing_mail_server_id" int4
	,"show_blacklist_buttons" bool
	,"group_use_agreement_type" bool
	,"group_use_agreement_template" bool
	,"module_project_timesheet_synchro" bool
	,"module_project_timesheet_holidays" bool)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'res_config_settings');
drop foreign table if exists sm_res_country;	create FOREIGN TABLE sm_res_country("id" int4 NOT NULL
	,"create_uid" int4
	,"code" varchar(2)
	,"create_date" timestamp
	,"name" varchar NOT NULL
	,"write_uid" int4
	,"currency_id" int4
	,"address_format" text
	,"phone_code" int4
	,"write_date" timestamp
	,"address_view_id" int4
	,"name_position" varchar
	,"vat_label" varchar
	,"enforce_cities" bool
	,"geonames_state_name_column" int4
	,"geonames_state_code_column" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'res_country');
drop foreign table if exists sm_res_country_group;	create FOREIGN TABLE sm_res_country_group("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"name" varchar NOT NULL
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'res_country_group');
drop foreign table if exists sm_res_country_group_pricelist_rel;	create FOREIGN TABLE sm_res_country_group_pricelist_rel("pricelist_id" int4 NOT NULL
	,"res_country_group_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'res_country_group_pricelist_rel');
drop foreign table if exists sm_res_country_res_country_group_rel;	create FOREIGN TABLE sm_res_country_res_country_group_rel("res_country_id" int4 NOT NULL
	,"res_country_group_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'res_country_res_country_group_rel');
drop foreign table if exists sm_res_country_state;	create FOREIGN TABLE sm_res_country_state("id" int4 NOT NULL
	,"create_uid" int4
	,"code" varchar NOT NULL
	,"create_date" timestamp
	,"name" varchar NOT NULL
	,"country_id" int4 NOT NULL
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'res_country_state');
drop foreign table if exists sm_res_currency;	create FOREIGN TABLE sm_res_currency("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"rounding" numeric
	,"symbol" varchar NOT NULL
	,"write_uid" int4
	,"write_date" timestamp
	,"active" bool
	,"position" varchar
	,"currency_unit_label" varchar
	,"currency_subunit_label" varchar
	,"decimal_places" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'res_currency');
drop foreign table if exists sm_res_currency_rate;	create FOREIGN TABLE sm_res_currency_rate("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"company_id" int4
	,"write_uid" int4
	,"currency_id" int4
	,"rate" numeric
	,"write_date" timestamp
	,"openupgrade_legacy_11_0_name" timestamp
	,"name" date NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'res_currency_rate');
drop foreign table if exists sm_res_font;	create FOREIGN TABLE sm_res_font("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"name" varchar NOT NULL
	,"family" varchar NOT NULL
	,"write_uid" int4
	,"mode" varchar NOT NULL
	,"write_date" timestamp
	,"path" varchar NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'res_font');
drop foreign table if exists sm_res_groups;	create FOREIGN TABLE sm_res_groups("id" int4 NOT NULL
	,"comment" text
	,"create_uid" int4
	,"create_date" timestamp
	,"name" varchar NOT NULL
	,"color" int4
	,"share" bool
	,"write_uid" int4
	,"write_date" timestamp
	,"category_id" int4
	,"is_portal" bool)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'res_groups');
drop foreign table if exists sm_res_groups_action_rel;	create FOREIGN TABLE sm_res_groups_action_rel("uid" int4 NOT NULL
	,"gid" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'res_groups_action_rel');
drop foreign table if exists sm_res_groups_implied_rel;	create FOREIGN TABLE sm_res_groups_implied_rel("gid" int4 NOT NULL
	,"hid" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'res_groups_implied_rel');
drop foreign table if exists sm_res_groups_report_rel;	create FOREIGN TABLE sm_res_groups_report_rel("uid" int4 NOT NULL
	,"gid" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'res_groups_report_rel');
drop foreign table if exists sm_res_groups_users_rel;	create FOREIGN TABLE sm_res_groups_users_rel("gid" int4 NOT NULL
	,"uid" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'res_groups_users_rel');
drop foreign table if exists sm_res_lang;	create FOREIGN TABLE sm_res_lang("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"code" varchar NOT NULL
	,"create_uid" int4
	,"direction" varchar NOT NULL
	,"create_date" timestamp
	,"date_format" varchar NOT NULL
	,"thousands_sep" varchar
	,"translatable" bool
	,"write_uid" int4
	,"time_format" varchar NOT NULL
	,"write_date" timestamp
	,"decimal_point" varchar NOT NULL
	,"active" bool
	,"iso_code" varchar
	,"grouping" varchar NOT NULL
	,"week_start" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'res_lang');
drop foreign table if exists sm_res_partner;	create FOREIGN TABLE sm_res_partner("id" int4 NOT NULL
	,"name" varchar
	,"company_id" int4
	,"comment" text
	,"function" varchar
	,"create_date" timestamp
	,"color" int4
	,"company_type" varchar
	,"date" date
	,"street" varchar
	,"city" varchar
	,"user_id" int4
	,"zip" varchar
	,"title" int4
	,"country_id" int4
	,"parent_id" int4
	,"supplier" bool
	,"ref" varchar
	,"email" varchar
	,"is_company" bool
	,"tz" varchar(64)
	,"website" varchar
	,"customer" bool
	,"fax" varchar
	,"street2" varchar
	,"barcode" varchar
	,"employee" bool
	,"credit_limit" float8
	,"write_date" timestamp
	,"active" bool
	,"display_name" varchar
	,"write_uid" int4
	,"lang" varchar
	,"create_uid" int4
	,"phone" varchar
	,"mobile" varchar
	,"type" varchar
	,"use_parent_address" bool
	,"openupgrade_legacy_10_0_birthdate" varchar
	,"vat" varchar
	,"state_id" int4
	,"commercial_partner_id" int4
	,"notify_email" varchar
	,"openupgrade_legacy_12_0_opt_out" bool
	,"message_last_post" timestamp
	,"signup_token" varchar
	,"signup_type" varchar
	,"signup_expiration" timestamp
	,"last_time_entries_checked" timestamp
	,"debit_limit" numeric
	,"firstname" varchar
	,"invoicing_email" varchar
	,"team_id" int4
	,"cs_person_index" varchar
	,"cs_pocketbook" float8
	,"invoicing_iban_moved0" varchar
	,"cs_registration_start_date" date
	,"cs_registration_sent_date" date
	,"cs_observations" varchar
	,"cs_registration_completed_date" date
	,"gender" varchar
	,"cs_abs_pocketbook" float8
	,"member_name" varchar
	,"cs_monthly_fee_type" varchar
	,"cs_monthly_fee" float8
	,"default_tariff_model_id" int4
	,"computed_state" varchar
	,"computed_city" varchar
	,"lopd_mail_sent" bool
	,"member_lng" numeric
	,"member_lat" numeric
	,"member_email_date" varchar
	,"computed_country" varchar
	,"registration_link" varchar
	,"commercial_company_name" varchar
	,"company_name" varchar
	,"partner_share" bool
	,"message_bounce" int4
	,"invoice_warn_msg" text
	,"invoice_warn" varchar
	,"sale_warn" varchar
	,"sale_warn_msg" text
	,"picking_warn" varchar
	,"picking_warn_msg" text
	,"purchase_warn" varchar
	,"purchase_warn_msg" text
	,"industry_id" int4
	,"commercial_partner_country_id" int4
	,"openupgrade_legacy_12_0_activity_date_deadline" date
	,"id_document_type" varchar
	,"member_group_config" varchar
	,"image_dni" varchar
	,"driving_license_expiration_date" varchar
	,"image_driving_license" varchar
	,"cs_force_registration" bool
	,"cs_firebase_uid" varchar
	,"cs_www_request" varchar
	,"cs_missing_data_email_counter" int4
	,"cs_missing_data_email_counter_date" date
	,"cs_state" varchar
	,"member_group" varchar
	,"missing_cs_data_email_sent" bool
	,"creation_coupon" varchar
	,"paymaster" int4
	,"partner_latitude" numeric
	,"partner_longitude" numeric
	,"date_localization" date
	,"calendar_last_notif_ack" timestamp
	,"comercial" varchar(128)
	,"aeat_anonymous_cash_customer" bool
	,"sanitized_vat" varchar
	,"cs_registration_coupon" varchar
	,"personal_billing_account_index" varchar
	,"cs_registration_info_ok" bool
	,"cs_registration_error_description" varchar
	,"cs_force_registration_without_data" bool
	,"not_in_mod347" bool
	,"reporting_related_member_id" int4
	,"related_representative_member_id" int4
	,"cs_user_type" varchar
	,"geolocation_computed" bool
	,"message_main_attachment_id" int4
	,"representative_moved0" varchar
	,"birthdate_date" date
	,"lastname" varchar
	,"cooperator" bool
	,"member" bool
	,"coop_candidate" bool
	,"old_member" bool
	,"cooperator_register_number" int4
	,"company_register_number" varchar
	,"cooperator_type" varchar
	,"effective_date" date
	,"representative" bool
	,"representative_of_member_company" bool
	,"legal_form" varchar
	,"data_policy_approved" bool
	,"internal_rules_approved" bool
	,"financial_risk_approved" bool
	,"_api_external_id" int4
	,"external_id_sequence_id" int4
	,"city_id" int4
	,"zip_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'res_partner');
drop foreign table if exists sm_res_partner_bank;	create FOREIGN TABLE sm_res_partner_bank("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"sequence" int4
	,"company_id" int4
	,"write_uid" int4
	,"currency_id" int4
	,"write_date" timestamp
	,"sanitized_acc_number" varchar
	,"acc_number" varchar NOT NULL
	,"partner_id" int4 NOT NULL
	,"bank_id" int4
	,"acc_type" varchar
	,"acc_holder_name" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'res_partner_bank');
drop foreign table if exists sm_res_partner_category;	create FOREIGN TABLE sm_res_partner_category("id" int4 NOT NULL
	,"parent_left" int4
	,"parent_right" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"name" varchar NOT NULL
	,"color" int4
	,"write_uid" int4
	,"parent_id" int4
	,"write_date" timestamp
	,"active" bool
	,"parent_path" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'res_partner_category');
drop foreign table if exists sm_res_partner_industry;	create FOREIGN TABLE sm_res_partner_industry("id" int4 NOT NULL
	,"name" varchar
	,"full_name" varchar
	,"active" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'res_partner_industry');
drop foreign table if exists sm_res_partner_res_partner_category_rel;	create FOREIGN TABLE sm_res_partner_res_partner_category_rel("category_id" int4 NOT NULL
	,"partner_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'res_partner_res_partner_category_rel');
drop foreign table if exists sm_res_partner_resource_calendar_rel;	create FOREIGN TABLE sm_res_partner_resource_calendar_rel("res_partner_id" int4 NOT NULL
	,"resource_calendar_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'res_partner_resource_calendar_rel');
drop foreign table if exists sm_res_partner_title;	create FOREIGN TABLE sm_res_partner_title("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"name" varchar NOT NULL
	,"shortcut" varchar
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'res_partner_title');
drop foreign table if exists sm_res_partner_trial_balance_report_wizard_rel;	create FOREIGN TABLE sm_res_partner_trial_balance_report_wizard_rel("trial_balance_report_wizard_id" int4 NOT NULL
	,"res_partner_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'res_partner_trial_balance_report_wizard_rel');
drop foreign table if exists sm_res_request_link;	create FOREIGN TABLE sm_res_request_link("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"name" varchar NOT NULL
	,"object" varchar NOT NULL
	,"write_uid" int4
	,"priority" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'res_request_link');
drop foreign table if exists sm_res_users;	create FOREIGN TABLE sm_res_users("id" int4 NOT NULL
	,"active" bool
	,"login" varchar NOT NULL
	,"password" varchar
	,"company_id" int4 NOT NULL
	,"partner_id" int4 NOT NULL
	,"create_date" timestamp
	,"create_uid" int4
	,"share" bool
	,"write_uid" int4
	,"write_date" timestamp
	,"signature" text
	,"action_id" int4
	,"password_crypt" varchar
	,"alias_id" int4
	,"chatter_needaction_auto" bool
	,"sale_team_id" int4
	,"notification_type" varchar NOT NULL
	,"target_sales_won" int4
	,"target_sales_done" int4
	,"target_sales_invoiced" int4
	,"google_calendar_rtoken" varchar
	,"google_calendar_token" varchar
	,"google_calendar_token_validity" timestamp
	,"google_calendar_last_sync_date" timestamp
	,"google_calendar_cal_id" varchar
	,"chatter_position" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'res_users');
drop foreign table if exists sm_res_users_log;	create FOREIGN TABLE sm_res_users_log("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_date" timestamp
	,"write_uid" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'res_users_log');
drop foreign table if exists sm_res_users_web_tip_rel;	create FOREIGN TABLE sm_res_users_web_tip_rel("web_tip_id" int4 NOT NULL
	,"res_users_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'res_users_web_tip_rel');
drop foreign table if exists sm_resource_calendar;	create FOREIGN TABLE sm_resource_calendar("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"name" varchar NOT NULL
	,"company_id" int4
	,"write_uid" int4
	,"manager" int4
	,"write_date" timestamp
	,"hours_per_day" float8
	,"tz" varchar NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'resource_calendar');
drop foreign table if exists sm_resource_calendar_attendance;	create FOREIGN TABLE sm_resource_calendar_attendance("id" int4 NOT NULL
	,"dayofweek" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"name" varchar NOT NULL
	,"date_from" date
	,"write_uid" int4
	,"hour_from" float8 NOT NULL
	,"hour_to" float8 NOT NULL
	,"write_date" timestamp
	,"date_to" date
	,"calendar_id" int4 NOT NULL
	,"day_period" varchar NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'resource_calendar_attendance');
drop foreign table if exists sm_resource_calendar_leaves;	create FOREIGN TABLE sm_resource_calendar_leaves("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"name" varchar
	,"resource_id" int4
	,"date_from" timestamp NOT NULL
	,"company_id" int4
	,"write_uid" int4
	,"write_date" timestamp
	,"date_to" timestamp NOT NULL
	,"calendar_id" int4
	,"openupgrade_legacy_12_0_tz" varchar
	,"openupgrade_legacy_12_0_holiday_id" int4
	,"time_type" varchar
	,"holiday_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'resource_calendar_leaves');
drop foreign table if exists sm_resource_resource;	create FOREIGN TABLE sm_resource_resource("id" int4 NOT NULL
	,"create_uid" int4
	,"time_efficiency" float8 NOT NULL
	,"code" varchar
	,"user_id" int4
	,"name" varchar NOT NULL
	,"company_id" int4
	,"write_uid" int4
	,"write_date" timestamp
	,"calendar_id" int4 NOT NULL
	,"active" bool
	,"create_date" timestamp
	,"resource_type" varchar NOT NULL
	,"tz" varchar NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'resource_resource');
drop foreign table if exists sm_resource_test;	create FOREIGN TABLE sm_resource_test("id" int4 NOT NULL
	,"name" varchar
	,"resource_id" int4 NOT NULL
	,"company_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"resource_calendar_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'resource_test');
drop foreign table if exists sm_rule_group_rel;	create FOREIGN TABLE sm_rule_group_rel("rule_group_id" int4 NOT NULL
	,"group_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'rule_group_rel');
drop foreign table if exists sm_sale_advance_payment_inv;	create FOREIGN TABLE sm_sale_advance_payment_inv("id" int4 NOT NULL
	,"count" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"product_id" int4
	,"advance_payment_method" varchar NOT NULL
	,"write_uid" int4
	,"amount" numeric
	,"write_date" timestamp
	,"deposit_account_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sale_advance_payment_inv');
drop foreign table if exists sm_sale_config_settings;	create FOREIGN TABLE sm_sale_config_settings("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_date" timestamp
	,"write_uid" int4
	,"deposit_product_id_setting" int4
	,"auto_done_setting" int4
	,"group_display_incoterm" int4
	,"group_pricelist_item" bool
	,"group_product_variant" int4
	,"group_sale_pricelist" bool
	,"default_invoice_policy" varchar
	,"group_product_pricelist" bool
	,"module_website_portal" bool
	,"module_website_quote" int4
	,"group_discount_per_so_line" int4
	,"module_sale_margin" int4
	,"sale_pricelist_setting" varchar NOT NULL
	,"module_website_sale_digital" bool
	,"group_uom" int4
	,"group_sale_delivery_address" int4
	,"module_sale_contract" bool
	,"sale_show_tax" varchar NOT NULL
	,"group_warning_sale" int4
	,"company_id" int4 NOT NULL
	,"group_sale_layout" int4
	,"group_show_price_total" bool
	,"group_show_price_subtotal" bool
	,"module_sale_order_dates" int4
	,"module_delivery" int4
	,"group_mrp_properties" int4
	,"group_route_so_lines" int4
	,"default_picking_policy" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sale_config_settings');
drop foreign table if exists sm_sale_layout_category;	create FOREIGN TABLE sm_sale_layout_category("id" int4 NOT NULL
	,"create_uid" int4
	,"name" varchar NOT NULL
	,"sequence" int4 NOT NULL
	,"write_uid" int4
	,"pagebreak" bool
	,"write_date" timestamp
	,"create_date" timestamp
	,"subtotal" bool)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sale_layout_category');
drop foreign table if exists sm_sale_order;	create FOREIGN TABLE sm_sale_order("id" int4 NOT NULL
	,"origin" varchar
	,"create_date" timestamp
	,"write_uid" int4
	,"team_id" int4
	,"client_order_ref" varchar
	,"date_order" timestamp NOT NULL
	,"partner_id" int4 NOT NULL
	,"create_uid" int4
	,"procurement_group_id" int4
	,"amount_untaxed" numeric
	,"message_last_post" timestamp
	,"company_id" int4
	,"note" text
	,"state" varchar
	,"pricelist_id" int4 NOT NULL
	,"analytic_account_id" int4
	,"amount_tax" numeric
	,"validity_date" date
	,"payment_term_id" int4
	,"write_date" timestamp
	,"partner_invoice_id" int4 NOT NULL
	,"user_id" int4
	,"fiscal_position_id" int4
	,"amount_total" numeric
	,"invoice_status" varchar
	,"name" varchar NOT NULL
	,"partner_shipping_id" int4 NOT NULL
	,"confirmation_date" timestamp
	,"picking_policy" varchar NOT NULL
	,"incoterm" int4
	,"warehouse_id" int4 NOT NULL
	,"access_token" varchar
	,"openupgrade_legacy_12_0_activity_date_deadline" date
	,"campaign_id" int4
	,"source_id" int4
	,"medium_id" int4
	,"opportunity_id" int4
	,"payment_mode_id" int4
	,"message_main_attachment_id" int4
	,"reference" varchar
	,"require_signature" bool
	,"require_payment" bool
	,"currency_rate" numeric
	,"signed_by" varchar
	,"commitment_date" timestamp
	,"sale_order_template_id" int4
	,"effective_date" date
	,"commercial_partner_id" int4
	,"agreement_id" int4
	,"agreement_type_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sale_order');
drop foreign table if exists sm_sale_order_line;	create FOREIGN TABLE sm_sale_order_line("id" int4 NOT NULL
	,"create_date" timestamp
	,"qty_to_invoice" numeric
	,"sequence" int4
	,"price_unit" numeric NOT NULL
	,"product_uom_qty" numeric NOT NULL
	,"qty_invoiced" numeric
	,"write_uid" int4
	,"currency_id" int4
	,"create_uid" int4
	,"price_tax" float8
	,"product_uom" int4
	,"customer_lead" float8 NOT NULL
	,"company_id" int4
	,"name" text NOT NULL
	,"state" varchar
	,"order_partner_id" int4
	,"order_id" int4 NOT NULL
	,"price_subtotal" numeric
	,"discount" numeric
	,"write_date" timestamp
	,"price_reduce" numeric
	,"qty_delivered" numeric
	,"price_total" numeric
	,"invoice_status" varchar
	,"product_id" int4
	,"salesman_id" int4
	,"price_reduce_taxexcl" numeric
	,"layout_category_sequence" int4
	,"layout_category_id" int4
	,"price_reduce_taxinc" numeric
	,"product_packaging" int4
	,"route_id" int4
	,"is_downpayment" bool
	,"amt_to_invoice" numeric
	,"amt_invoiced" numeric
	,"qty_delivered_manual" numeric
	,"display_type" varchar
	,"qty_delivered_method" varchar
	,"untaxed_amount_invoiced" numeric
	,"untaxed_amount_to_invoice" numeric
	,"is_expense" bool
	,"project_id" int4
	,"task_id" int4
	,"is_service" bool)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sale_order_line');
drop foreign table if exists sm_sale_order_line_invoice_rel;	create FOREIGN TABLE sm_sale_order_line_invoice_rel("order_line_id" int4 NOT NULL
	,"invoice_line_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sale_order_line_invoice_rel');
drop foreign table if exists sm_sale_order_option;	create FOREIGN TABLE sm_sale_order_option("id" int4 NOT NULL
	,"order_id" int4
	,"line_id" int4
	,"name" text NOT NULL
	,"product_id" int4 NOT NULL
	,"price_unit" numeric NOT NULL
	,"discount" numeric
	,"uom_id" int4 NOT NULL
	,"quantity" numeric NOT NULL
	,"sequence" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sale_order_option');
drop foreign table if exists sm_sale_order_tag_rel;	create FOREIGN TABLE sm_sale_order_tag_rel("order_id" int4 NOT NULL
	,"tag_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sale_order_tag_rel');
drop foreign table if exists sm_sale_order_template;	create FOREIGN TABLE sm_sale_order_template("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"note" text
	,"number_of_days" int4
	,"require_signature" bool
	,"require_payment" bool
	,"mail_template_id" int4
	,"active" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sale_order_template');
drop foreign table if exists sm_sale_order_template_line;	create FOREIGN TABLE sm_sale_order_template_line("id" int4 NOT NULL
	,"sequence" int4
	,"sale_order_template_id" int4 NOT NULL
	,"name" text NOT NULL
	,"product_id" int4
	,"price_unit" numeric NOT NULL
	,"discount" numeric
	,"product_uom_qty" numeric NOT NULL
	,"product_uom_id" int4
	,"display_type" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sale_order_template_line');
drop foreign table if exists sm_sale_order_template_option;	create FOREIGN TABLE sm_sale_order_template_option("id" int4 NOT NULL
	,"sale_order_template_id" int4 NOT NULL
	,"name" text NOT NULL
	,"product_id" int4 NOT NULL
	,"price_unit" numeric NOT NULL
	,"discount" numeric
	,"uom_id" int4 NOT NULL
	,"quantity" numeric NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sale_order_template_option');
drop foreign table if exists sm_sale_order_transaction_rel;	create FOREIGN TABLE sm_sale_order_transaction_rel("sale_order_id" int4 NOT NULL
	,"transaction_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sale_order_transaction_rel');
drop foreign table if exists sm_sale_payment_acquirer_onboarding_wizard;	create FOREIGN TABLE sm_sale_payment_acquirer_onboarding_wizard("id" int4 NOT NULL
	,"paypal_email_account" varchar
	,"paypal_seller_account" varchar
	,"paypal_pdt_token" varchar
	,"stripe_secret_key" varchar
	,"stripe_publishable_key" varchar
	,"manual_name" varchar
	,"journal_name" varchar
	,"acc_number" varchar
	,"manual_post_msg" text
	,"payment_method" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sale_payment_acquirer_onboarding_wizard');
drop foreign table if exists sm_sale_product_configurator;	create FOREIGN TABLE sm_sale_product_configurator("id" int4 NOT NULL
	,"product_template_id" int4 NOT NULL
	,"pricelist_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sale_product_configurator');
drop foreign table if exists sm_sale_report;	create FOREIGN TABLE sm_sale_report("id" int4
	,"product_id" int4
	,"product_uom" int4
	,"product_uom_qty" numeric
	,"qty_delivered" numeric
	,"qty_invoiced" numeric
	,"qty_to_invoice" numeric
	,"price_total" numeric
	,"price_subtotal" numeric
	,"untaxed_amount_to_invoice" numeric
	,"untaxed_amount_invoiced" numeric
	,"nbr" int8
	,"name" varchar
	,"date" timestamp
	,"confirmation_date" timestamp
	,"state" varchar
	,"partner_id" int4
	,"user_id" int4
	,"company_id" int4
	,"delay" float8
	,"categ_id" int4
	,"pricelist_id" int4
	,"analytic_account_id" int4
	,"team_id" int4
	,"product_tmpl_id" int4
	,"country_id" int4
	,"commercial_partner_id" int4
	,"weight" numeric
	,"volume" float8
	,"discount" numeric
	,"discount_amount" numeric
	,"order_id" int4
	,"warehouse_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sale_report');
drop foreign table if exists sm_server_config;	create FOREIGN TABLE sm_server_config("id" int4 NOT NULL
	,"config" text
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'server_config');
drop foreign table if exists sm_share_line;	create FOREIGN TABLE sm_share_line("id" int4 NOT NULL
	,"share_product_id" int4 NOT NULL
	,"share_number" int4 NOT NULL
	,"share_unit_price" numeric
	,"effective_date" date
	,"partner_id" int4 NOT NULL
	,"company_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'share_line');
drop foreign table if exists sm_share_line_update_info;	create FOREIGN TABLE sm_share_line_update_info("id" int4 NOT NULL
	,"effective_date" date NOT NULL
	,"share_line" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'share_line_update_info');
drop foreign table if exists sm_sm_carsharing_maintenance_cargroup_maintenance;	create FOREIGN TABLE sm_sm_carsharing_maintenance_cargroup_maintenance("id" int4 NOT NULL
	,"project_id" int4
	,"name" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sm_carsharing_maintenance_cargroup_maintenance');
drop foreign table if exists sm_sm_carsharing_send_email_tariff_wizard;	create FOREIGN TABLE sm_sm_carsharing_send_email_tariff_wizard("id" int4 NOT NULL
	,"name" varchar
	,"reason" varchar
	,"current_member_id" int4
	,"tariff_model_id" int4
	,"tariff_type" varchar
	,"date" date
	,"date_active" date
	,"pocketbook_initial" int4
	,"pocketbook_threshold" float8
	,"pocketbook" float8
	,"date_valid" date
	,"time_restricted" bool
	,"related_carconfig_id" int4
	,"carconfig_availability" varchar
	,"extra_tariff_model_id" int4
	,"description" text
	,"notify" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sm_carsharing_send_email_tariff_wizard');
drop foreign table if exists sm_sm_carsharing_structure_cs_carconfig;	create FOREIGN TABLE sm_sm_carsharing_structure_cs_carconfig("id" int4 NOT NULL
	,"name" varchar
	,"db_carconfig_id" int4
	,"analytic_account_id" int4
	,"teletac_analytic_account_id" int4
	,"production_unit_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"db_carconfig_group_index" varchar
	,"db_carconfig_owner_group_index" varchar
	,"db_carconfig_name" varchar
	,"related_price_group_id" int4
	,"initial_price" float8)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sm_carsharing_structure_cs_carconfig');
drop foreign table if exists sm_sm_carsharing_structure_cs_community;	create FOREIGN TABLE sm_sm_carsharing_structure_cs_community("id" int4 NOT NULL
	,"name" varchar
	,"analytic_account_id" int4
	,"project_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sm_carsharing_structure_cs_community');
drop foreign table if exists sm_sm_carsharing_structure_cs_production_unit;	create FOREIGN TABLE sm_sm_carsharing_structure_cs_production_unit("id" int4 NOT NULL
	,"name" varchar
	,"analytic_account_id" int4
	,"community_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sm_carsharing_structure_cs_production_unit');
drop foreign table if exists sm_sm_carsharing_structure_cs_task_service_wizard;	create FOREIGN TABLE sm_sm_carsharing_structure_cs_task_service_wizard("id" int4 NOT NULL
	,"service_car_id" int4
	,"service_type_id" int4
	,"amount" float8
	,"related_member_id" int4
	,"vendor_id" int4
	,"related_invoice_id" int4
	,"date" date
	,"related_task_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sm_carsharing_structure_cs_task_service_wizard');
drop foreign table if exists sm_sm_carsharing_structure_cs_ticket_service_wizard;	create FOREIGN TABLE sm_sm_carsharing_structure_cs_ticket_service_wizard("id" int4 NOT NULL
	,"service_car_id" int4
	,"service_type_id" int4
	,"amount" float8
	,"related_member_id" int4
	,"vendor_id" int4
	,"related_invoice_id" int4
	,"date" date
	,"related_ticket_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sm_carsharing_structure_cs_ticket_service_wizard');
drop foreign table if exists sm_sm_collaborators_sm_collaborator;	create FOREIGN TABLE sm_sm_collaborators_sm_collaborator("id" int4 NOT NULL
	,"create_uid" int4
	,"end" date
	,"name" varchar NOT NULL
	,"completed" date
	,"member_email" varchar
	,"write_uid" int4
	,"member_nr" varchar
	,"collaborator_type" varchar
	,"write_date" timestamp
	,"member_name" varchar
	,"date" date
	,"create_date" timestamp
	,"collaborator_info" varchar
	,"related_member_id" int4
	,"collaborator_iban" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sm_collaborators_sm_collaborator');
drop foreign table if exists sm_sm_collaborators_sm_collaborator_fetch_wizard;	create FOREIGN TABLE sm_sm_collaborators_sm_collaborator_fetch_wizard("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sm_collaborators_sm_collaborator_fetch_wizard');
drop foreign table if exists sm_sm_collaborators_sm_collaborator_registration_wizard;	create FOREIGN TABLE sm_sm_collaborators_sm_collaborator_registration_wizard("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"rel_batch_payment_order_id" int4 NOT NULL
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sm_collaborators_sm_collaborator_registration_wizard');
drop foreign table if exists sm_sm_configuration_successful_action_message;	create FOREIGN TABLE sm_sm_configuration_successful_action_message("id" int4 NOT NULL
	,"action" varchar
	,"model" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sm_configuration_successful_action_message');
drop foreign table if exists sm_sm_configuration_successful_action_message_wizard;	create FOREIGN TABLE sm_sm_configuration_successful_action_message_wizard("id" int4 NOT NULL
	,"action" varchar
	,"model" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sm_configuration_successful_action_message_wizard');
drop foreign table if exists sm_sm_contributions_contribution_line_selectable_wizard;	create FOREIGN TABLE sm_sm_contributions_contribution_line_selectable_wizard("id" int4 NOT NULL
	,"create_date" timestamp
	,"create_uid" int4
	,"parent_model" int4
	,"message_last_post" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"year" int4
	,"message_main_attachment_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sm_contributions_contribution_line_selectable_wizard');
drop foreign table if exists sm_sm_contributions_date_picker_wizard;	create FOREIGN TABLE sm_sm_contributions_date_picker_wizard("id" int4 NOT NULL
	,"create_date" timestamp
	,"date_chosen" date
	,"create_uid" int4
	,"parent_model" int4
	,"message_last_post" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"message_main_attachment_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sm_contributions_date_picker_wizard');
drop foreign table if exists sm_sm_contributions_sm_contribution;	create FOREIGN TABLE sm_sm_contributions_sm_contribution("id" int4 NOT NULL
	,"create_uid" int4
	,"returned" numeric
	,"create_date" timestamp
	,"initial_import" numeric NOT NULL
	,"initial_day" date NOT NULL
	,"file_name" varchar
	,"contract_number" int4
	,"write_uid" int4
	,"contribution_type" int4 NOT NULL
	,"write_date" timestamp
	,"contract_email_sent" bool
	,"closed_day" date
	,"associated_member" int4 NOT NULL
	,"upload_file" bytea
	,"initial_import_txt" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sm_contributions_sm_contribution');
drop foreign table if exists sm_sm_contributions_sm_contribution_interest;	create FOREIGN TABLE sm_sm_contributions_sm_contribution_interest("id" int4 NOT NULL
	,"create_uid" int4
	,"interest" float8
	,"create_date" timestamp
	,"iva" float8
	,"write_uid" int4
	,"write_date" timestamp
	,"year" int4
	,"type" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sm_contributions_sm_contribution_interest');
drop foreign table if exists sm_sm_contributions_sm_contribution_line;	create FOREIGN TABLE sm_sm_contributions_sm_contribution_line("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_date" timestamp
	,"initial_day" date
	,"write_uid" int4
	,"associated_contribution" int4
	,"year_report_sent" bool
	,"final_day" date
	,"year" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sm_contributions_sm_contribution_line');
drop foreign table if exists sm_sm_contributions_sm_contribution_type;	create FOREIGN TABLE sm_sm_contributions_sm_contribution_type("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"name" varchar
	,"write_uid" int4
	,"duration" int4
	,"write_date" timestamp
	,"template_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sm_contributions_sm_contribution_type');
drop foreign table if exists sm_sm_geolocation_partners_zone;	create FOREIGN TABLE sm_sm_geolocation_partners_zone("partner_id" int4 NOT NULL
	,"zone_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sm_geolocation_partners_zone');
drop foreign table if exists sm_sm_invoice_report;	create FOREIGN TABLE sm_sm_invoice_report("id" int4 NOT NULL
	,"fixed_amount_topay" float8
	,"create_date" timestamp
	,"afterfee_pocketbook" float8
	,"grouped_report" bool
	,"write_uid" int4
	,"date" date
	,"partner_id" int4
	,"total_amount_signed" float8
	,"create_uid" int4
	,"message_last_post" timestamp
	,"company_id" int4
	,"total_mileage" float8
	,"total_amount_lines_taxed" float8
	,"previous_pocketbook" float8
	,"name" varchar
	,"total_amount_lines_untaxed" float8
	,"timeframe_desc" varchar
	,"write_date" timestamp
	,"discount_amount" float8
	,"total_amount" float8
	,"invoice_id" int4
	,"email_sent" bool
	,"predetermined_tariff_description" text
	,"initial_price" float8
	,"total_amount_lines_taxed_no_initial" float8
	,"previous_pocketbook_taxed" float8
	,"final_pocketbook" float8
	,"final_pocketbook_taxed" float8
	,"message_main_attachment_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sm_invoice_report');
drop foreign table if exists sm_sm_invoices_reports_sm_invoice_report_wizard;	create FOREIGN TABLE sm_sm_invoices_reports_sm_invoice_report_wizard("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"timeframe_desc" varchar
	,"write_date" timestamp
	,"member_id" int4
	,"is_grouped" bool)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sm_invoices_reports_sm_invoice_report_wizard');
drop foreign table if exists sm_sm_lopd_sm_lopd_cron;	create FOREIGN TABLE sm_sm_lopd_sm_lopd_cron("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sm_lopd_sm_lopd_cron');
drop foreign table if exists sm_sm_mailchimp_fetch_wizard;	create FOREIGN TABLE sm_sm_mailchimp_fetch_wizard("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_date" timestamp
	,"write_uid" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sm_mailchimp_fetch_wizard');
drop foreign table if exists sm_sm_mailchimp_list_members_rel;	create FOREIGN TABLE sm_sm_mailchimp_list_members_rel("member_id" int4 NOT NULL
	,"name" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sm_mailchimp_list_members_rel');
drop foreign table if exists sm_sm_mailchimp_lists_user_wizard;	create FOREIGN TABLE sm_sm_mailchimp_lists_user_wizard("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"current_member" int4
	,"mailchimp_lists" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sm_mailchimp_lists_user_wizard');
drop foreign table if exists sm_sm_maintenance_maintenance_cron;	create FOREIGN TABLE sm_sm_maintenance_maintenance_cron("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sm_maintenance_maintenance_cron');
drop foreign table if exists sm_sm_maintenance_smp_user_scheduler;	create FOREIGN TABLE sm_sm_maintenance_smp_user_scheduler("id" int4 NOT NULL
	,"name" varchar
	,"num_updates" int4
	,"last_modified" date
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sm_maintenance_smp_user_scheduler');
drop foreign table if exists sm_sm_maintenance_successful_action_message;	create FOREIGN TABLE sm_sm_maintenance_successful_action_message("id" int4 NOT NULL
	,"action" varchar
	,"model" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sm_maintenance_successful_action_message');
drop foreign table if exists sm_sm_member_categorization;	create FOREIGN TABLE sm_sm_member_categorization("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"name" varchar
	,"associated_id" int4
	,"write_uid" int4
	,"write_date" timestamp
	,"is_reward_member_type" bool)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sm_member_categorization');
drop foreign table if exists sm_sm_member_geolocation_members_zone;	create FOREIGN TABLE sm_sm_member_geolocation_members_zone("id" int4 NOT NULL
	,"name" varchar
	,"lat" numeric
	,"lng" numeric
	,"rad" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sm_member_geolocation_members_zone');
drop foreign table if exists sm_sm_member_geolocation_sm_geolocation_cron;	create FOREIGN TABLE sm_sm_member_geolocation_sm_geolocation_cron("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sm_member_geolocation_sm_geolocation_cron');
drop foreign table if exists sm_sm_members_zone;	create FOREIGN TABLE sm_sm_members_zone("id" int4 NOT NULL
	,"create_uid" int4
	,"rad" int4
	,"create_date" timestamp
	,"name" varchar
	,"write_uid" int4
	,"mailchimp_related_list" int4
	,"write_date" timestamp
	,"lat" numeric
	,"lng" numeric)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sm_members_zone');
drop foreign table if exists sm_sm_partago_invoicing_sm_batch_reservation_compute_wizard;	create FOREIGN TABLE sm_sm_partago_invoicing_sm_batch_reservation_compute_wizard("id" int4 NOT NULL
	,"description" varchar
	,"compute_reservations" bool
	,"compute_teletacs" bool
	,"batch_type" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sm_partago_invoicing_sm_batch_reservation_compute_wizard');
drop foreign table if exists sm_sm_partago_invoicing_sm_invoice_report_wizard;	create FOREIGN TABLE sm_sm_partago_invoicing_sm_invoice_report_wizard("id" int4 NOT NULL
	,"is_grouped" bool
	,"member_id" int4
	,"timeframe_desc" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sm_partago_invoicing_sm_invoice_report_wizard');
drop foreign table if exists sm_sm_partago_sm_batch_reservation_compute_wizard;	create FOREIGN TABLE sm_sm_partago_sm_batch_reservation_compute_wizard("id" int4 NOT NULL
	,"description" varchar
	,"compute_reservations" bool
	,"compute_teletacs" bool
	,"batch_type" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sm_partago_sm_batch_reservation_compute_wizard');
drop foreign table if exists sm_sm_partago_tariffs_sm_config;	create FOREIGN TABLE sm_sm_partago_tariffs_sm_config("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"default_tariff_model_id" int4
	,"write_uid" int4
	,"welcome_tariff_model_id" int4
	,"write_date" timestamp
	,"tariffs_pdf_id" int4
	,"fail_counter" int4
	,"alias_domain" varchar
	,"chart_template_id" int4
	,"module_account_accountant" bool
	,"group_analytic_accounting" bool
	,"group_warning_account" bool
	,"group_cash_rounding" bool
	,"module_account_asset" bool
	,"module_account_deferred_revenue" bool
	,"module_account_budget" bool
	,"module_account_payment" bool
	,"module_account_reports" bool
	,"module_account_reports_followup" bool
	,"module_l10n_us_check_printing" bool
	,"module_account_batch_deposit" bool
	,"module_account_sepa" bool
	,"module_account_sepa_direct_debit" bool
	,"module_account_plaid" bool
	,"module_account_yodlee" bool
	,"module_account_bank_statement_import_qif" bool
	,"module_account_bank_statement_import_ofx" bool
	,"module_account_bank_statement_import_csv" bool
	,"module_account_bank_statement_import_camt" bool
	,"module_currency_rate_live" bool
	,"module_print_docsaway" bool
	,"module_product_margin" bool
	,"module_l10n_eu_service" bool
	,"module_account_taxcloud" bool
	,"auth_signup_reset_password" bool
	,"auth_signup_template_user_id" int4
	,"company_share_product" bool
	,"group_uom" bool
	,"group_product_variant" bool
	,"group_stock_packaging" bool
	,"group_sale_pricelist" bool
	,"group_product_pricelist" bool
	,"group_pricelist_item" bool
	,"module_hr_timesheet" bool
	,"module_rating_project" bool
	,"module_project_forecast" bool
	,"group_subtask_project" bool
	,"lock_confirmed_po" bool
	,"po_order_approval" bool
	,"default_purchase_method" varchar
	,"module_purchase_requisition" bool
	,"group_warning_purchase" bool
	,"module_stock_dropshipping" bool
	,"group_manage_vendor_price" bool
	,"module_account_3way_match" bool
	,"is_installed_sale" bool
	,"group_analytic_account_for_purchases" bool
	,"use_po_lead" bool
	,"use_sale_note" bool
	,"group_discount_per_so_line" bool
	,"module_sale_margin" bool
	,"group_sale_layout" bool
	,"group_warning_sale" bool
	,"portal_confirmation" bool
	,"portal_confirmation_options" varchar
	,"module_sale_payment" bool
	,"module_website_quote" bool
	,"group_sale_delivery_address" bool
	,"multi_sales_price" bool
	,"multi_sales_price_method" varchar
	,"sale_pricelist_setting" varchar
	,"group_show_price_subtotal" bool
	,"group_show_price_total" bool
	,"group_proforma_sales" bool
	,"sale_show_tax" varchar NOT NULL
	,"default_invoice_policy" varchar
	,"default_deposit_product_id" int4
	,"auto_done_setting" bool
	,"module_website_sale_digital" bool
	,"auth_signup_uninvited" varchar
	,"module_delivery" bool
	,"module_delivery_dhl" bool
	,"module_delivery_fedex" bool
	,"module_delivery_ups" bool
	,"module_delivery_usps" bool
	,"module_delivery_bpost" bool
	,"module_product_email_template" bool
	,"module_sale_coupon" bool
	,"group_route_so_lines" bool
	,"module_sale_order_dates" bool
	,"group_display_incoterm" bool
	,"use_security_lead" bool
	,"default_picking_policy" varchar NOT NULL
	,"module_procurement_jit" int4
	,"module_product_expiry" bool
	,"group_stock_production_lot" bool
	,"group_stock_tracking_lot" bool
	,"group_stock_tracking_owner" bool
	,"group_stock_adv_location" bool
	,"group_warning_stock" bool
	,"use_propagation_minimum_delta" bool
	,"module_stock_picking_batch" bool
	,"module_stock_barcode" bool
	,"group_stock_multi_locations" bool
	,"group_stock_multi_warehouses" bool
	,"group_multi_company" bool
	,"company_id" int4 NOT NULL
	,"default_user_rights" bool
	,"default_external_email_server" bool
	,"module_base_import" bool
	,"module_google_calendar" bool
	,"module_google_drive" bool
	,"module_google_spreadsheet" bool
	,"module_auth_oauth" bool
	,"module_auth_ldap" bool
	,"module_base_gengo" bool
	,"module_inter_company_rules" bool
	,"module_pad" bool
	,"module_voip" bool
	,"company_share_partner" bool
	,"default_custom_report_footer" bool
	,"group_multi_currency" bool
	,"module_stock_landed_costs" bool)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sm_partago_tariffs_sm_config');
drop foreign table if exists sm_sm_partago_tariffs_smp_tariffs_cron;	create FOREIGN TABLE sm_sm_partago_tariffs_smp_tariffs_cron("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sm_partago_tariffs_smp_tariffs_cron');
drop foreign table if exists sm_sm_partago_usage_smp_usage_cron;	create FOREIGN TABLE sm_sm_partago_usage_smp_usage_cron("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sm_partago_usage_smp_usage_cron');
drop foreign table if exists sm_sm_partago_user_carsharing_member_group;	create FOREIGN TABLE sm_sm_partago_user_carsharing_member_group("id" int4 NOT NULL
	,"related_member_id" int4
	,"related_group_id" int4
	,"related_billingaccount_id" int4
	,"role" varchar
	,"admin_role" varchar
	,"default_billingaccount_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sm_partago_user_carsharing_member_group');
drop foreign table if exists sm_sm_partago_user_carsharing_registration_request;	create FOREIGN TABLE sm_sm_partago_user_carsharing_registration_request("id" int4 NOT NULL
	,"related_member_id" int4
	,"force_registration" bool
	,"group_index" varchar
	,"ba_behaviour" varchar
	,"ba_credits" float8
	,"related_coupon_index" varchar
	,"related_cs_update_data_id" int4
	,"completed" bool
	,"completed_date" date
	,"completed_behaviour" varchar NOT NULL
	,"completed_behaviour_before_error" varchar NOT NULL
	,"completed_error_description" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"active" bool
	,"related_reward_id" int4
	,"_api_external_id" int4
	,"external_id_sequence_id" int4
	,"related_subscription_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sm_partago_user_carsharing_registration_request');
drop foreign table if exists sm_sm_partago_user_carsharing_update_data;	create FOREIGN TABLE sm_sm_partago_user_carsharing_update_data("id" int4 NOT NULL
	,"related_member_id" int4
	,"parent_id" int4
	,"form_id" varchar
	,"cs_update_type" varchar
	,"cs_update_name" varchar
	,"cs_update_first_surname" varchar
	,"cs_update_second_surname" varchar
	,"cs_update_dni" varchar
	,"cs_update_dni_image" varchar
	,"cs_update_email" varchar
	,"cs_update_mobile" varchar
	,"cs_update_birthday" date
	,"cs_update_driving_license_expiration_date" date
	,"cs_update_image_driving_license" varchar
	,"cs_update_comments" varchar
	,"cs_update_cif" varchar
	,"cs_update_group_config" varchar
	,"cs_update_group" varchar
	,"cs_fetch_date" date
	,"cs_complete_date" date
	,"state" varchar
	,"final_state" varchar
	,"status_completed" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"cs_update_group_secondary" varchar
	,"_api_external_id" int4
	,"external_id_sequence_id" int4
	,"cron_executed" bool)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sm_partago_user_carsharing_update_data');
drop foreign table if exists sm_sm_partago_user_carsharing_update_data_fetch_wizard;	create FOREIGN TABLE sm_sm_partago_user_carsharing_update_data_fetch_wizard("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sm_partago_user_carsharing_update_data_fetch_wizard');
drop foreign table if exists sm_sm_partago_user_partago_user_cron;	create FOREIGN TABLE sm_sm_partago_user_partago_user_cron("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sm_partago_user_partago_user_cron');
drop foreign table if exists sm_sm_partago_user_sm_change_email_wizard;	create FOREIGN TABLE sm_sm_partago_user_sm_change_email_wizard("id" int4 NOT NULL
	,"current_member" int4
	,"new_email" varchar
	,"change_in_odoo" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sm_partago_user_sm_change_email_wizard');
drop foreign table if exists sm_sm_partago_user_smp_user_cron;	create FOREIGN TABLE sm_sm_partago_user_smp_user_cron("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sm_partago_user_smp_user_cron');
drop foreign table if exists sm_sm_partners_zone;	create FOREIGN TABLE sm_sm_partners_zone("member_id" int4 NOT NULL
	,"zone_name" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sm_partners_zone');
drop foreign table if exists sm_sm_productbatch_members_rel;	create FOREIGN TABLE sm_sm_productbatch_members_rel("member_id" int4 NOT NULL
	,"product_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sm_productbatch_members_rel');
drop foreign table if exists sm_sm_productbatchorder_members_rel;	create FOREIGN TABLE sm_sm_productbatchorder_members_rel("member_id" int4 NOT NULL
	,"order_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sm_productbatchorder_members_rel');
drop foreign table if exists sm_sm_report_data_reports_cron;	create FOREIGN TABLE sm_sm_report_data_reports_cron("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sm_report_data_reports_cron');
drop foreign table if exists sm_sm_report_data_sm_report_data;	create FOREIGN TABLE sm_sm_report_data_sm_report_data("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"report_model" int4 NOT NULL
	,"recipient" int4 NOT NULL
	,"description" text
	,"last_execution" date
	,"next_execution" date
	,"final_date" date
	,"specific_search" bool
	,"general_range" varchar
	,"daily_range" int4
	,"email_template" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"active" bool
	,"email_subject" varchar
	,"email_body" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sm_report_data_sm_report_data');
drop foreign table if exists sm_sm_rewards_reward_cron;	create FOREIGN TABLE sm_sm_rewards_reward_cron("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sm_rewards_reward_cron');
drop foreign table if exists sm_sm_rewards_sm_db_rewards_wizard;	create FOREIGN TABLE sm_sm_rewards_sm_db_rewards_wizard("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_date" timestamp
	,"write_uid" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sm_rewards_sm_db_rewards_wizard');
drop foreign table if exists sm_sm_rewards_sm_member_related_coupon;	create FOREIGN TABLE sm_sm_rewards_sm_member_related_coupon("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"related_member_id" int4
	,"wp_member_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sm_rewards_sm_member_related_coupon');
drop foreign table if exists sm_sm_rewards_sm_reward;	create FOREIGN TABLE sm_sm_rewards_sm_reward("id" int4 NOT NULL
	,"data_partner_firstname" varchar
	,"create_uid" int4
	,"reward_type" varchar NOT NULL
	,"create_date" timestamp
	,"promo_code" varchar
	,"related_member_id" int4
	,"data_partner_email" varchar
	,"reward_date" date
	,"member_nr" varchar
	,"write_uid" int4
	,"write_date" timestamp
	,"reward_info" varchar
	,"name" varchar
	,"completed" date
	,"reward_register_cs" bool
	,"related_rewards" varchar
	,"group_config" varchar
	,"reward_addmoney" float8
	,"state" varchar
	,"status_completed" bool
	,"reward_addtime" int4
	,"data_partner_vat" varchar
	,"status_computed" bool
	,"wp_coupon_id" varchar
	,"wp_member_id" varchar
	,"coupon_group" varchar
	,"tariff_name" varchar
	,"tariff_related_model" varchar
	,"tariff_type" varchar
	,"tariff_quantity" varchar
	,"is_soci" bool
	,"final_state" varchar
	,"related_analytic_account_id" int4
	,"wp_member_coupon_id" varchar
	,"coupon_group_secondary" varchar
	,"force_register_cs" bool
	,"force_dedicated_ba" bool
	,"maintenance_reservation_type" varchar
	,"maintenance_forgive_reservation" bool
	,"maintenance_type" varchar
	,"maintenance_duration" varchar
	,"maintenance_observations" text
	,"maintenance_carconfig_index" varchar
	,"maintenance_carconfig_id" int4
	,"maintenance_carconfig_home" varchar
	,"maintenance_cs_person_index" varchar
	,"maintenance_reservation_start" varchar
	,"maintenance_reservation_id" int4
	,"maintenance_car_plate" varchar
	,"maintenance_car_id" int4
	,"maintenance_create_car_service" bool
	,"maintenance_car_service_id" int4
	,"maintenance_wp_entry_id" varchar
	,"message_last_post" timestamp
	,"maintenance_discount_reservation" bool
	,"message_main_attachment_id" int4
	,"data_partner_creation_type" varchar NOT NULL
	,"data_partner_cs_user_type" varchar
	,"data_partner_lastname" varchar
	,"data_partner_mobile" varchar
	,"data_partner_phone" varchar
	,"data_partner_gender" varchar
	,"data_partner_birthdate_date" date
	,"data_partner_street" varchar
	,"data_partner_zip" varchar
	,"data_partner_state_id" int4
	,"data_partner_city" varchar
	,"data_partner_iban" varchar
	,"data_partner_driving_license_expiration_date" varchar
	,"data_partner_image_dni" varchar
	,"data_partner_image_driving_license" varchar
	,"external_obj_id" int4
	,"external_promo_obj_id" int4
	,"_api_external_id" int4
	,"external_id_sequence_id" int4
	,"cron_executed" bool)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sm_rewards_sm_reward');
drop foreign table if exists sm_sm_rewards_sm_reward_fetch_wizard;	create FOREIGN TABLE sm_sm_rewards_sm_reward_fetch_wizard("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sm_rewards_sm_reward_fetch_wizard');
drop foreign table if exists sm_sm_sm_error;	create FOREIGN TABLE sm_sm_sm_error("id" int4 NOT NULL
	,"error_message" varchar
	,"observation" text
	,"status" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"related_member_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sm_sm_error');
drop foreign table if exists sm_sm_www_sm_carsharing_wizard;	create FOREIGN TABLE sm_sm_www_sm_carsharing_wizard("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sm_www_sm_carsharing_wizard');
drop foreign table if exists sm_sm_www_sm_db_collaborators_wizard;	create FOREIGN TABLE sm_sm_www_sm_db_collaborators_wizard("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_date" timestamp
	,"write_uid" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sm_www_sm_db_collaborators_wizard');
drop foreign table if exists sm_sm_www_sm_db_rewards_wizard;	create FOREIGN TABLE sm_sm_www_sm_db_rewards_wizard("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_date" timestamp
	,"write_uid" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sm_www_sm_db_rewards_wizard');
drop foreign table if exists sm_sm_www_sm_member_signup_wizard;	create FOREIGN TABLE sm_sm_www_sm_member_signup_wizard("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_date" timestamp
	,"write_uid" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sm_www_sm_member_signup_wizard');
drop foreign table if exists sm_smp_reservation_wizard_relation;	create FOREIGN TABLE sm_smp_reservation_wizard_relation("sm_partago_invoicing_sm_batch_reservation_compute_wizard_id" int4 NOT NULL
	,"smp_sm_reservation_compute_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'smp_reservation_wizard_relation');
drop foreign table if exists sm_smp_sm_batch_reservation_compute;	create FOREIGN TABLE sm_smp_sm_batch_reservation_compute("id" int4 NOT NULL
	,"create_uid" int4
	,"sepa_file" bytea
	,"create_date" timestamp
	,"name" varchar NOT NULL
	,"test_sepa_generated" bool
	,"write_uid" int4
	,"write_date" timestamp
	,"sepa_file_filename" varchar
	,"test_sepa_file_filename" varchar
	,"test_sepa_file" bytea
	,"sepa_generated" bool
	,"total_invoiced_amount_no_discount" float8
	,"total_invoiced_amount" float8
	,"invoice_id" int4
	,"invoice_report_id" int4
	,"description" varchar
	,"total_invoiced_amount_invoiced" float8
	,"state" varchar
	,"batch_type" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'smp_sm_batch_reservation_compute');
drop foreign table if exists sm_smp_sm_batch_reservation_compute_wizard;	create FOREIGN TABLE sm_smp_sm_batch_reservation_compute_wizard("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"description" varchar
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'smp_sm_batch_reservation_compute_wizard');
drop foreign table if exists sm_smp_sm_billing_account;	create FOREIGN TABLE sm_smp_sm_billing_account("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"name" varchar
	,"reservation_slots_left" float8
	,"reservation_cancelations" int4
	,"write_uid" int4
	,"minutes_left" float8
	,"write_date" timestamp
	,"group_id" int4
	,"minutesLeft" float8
	,"owner_group_id" int4
	,"group_index" varchar
	,"owner_group_index" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'smp_sm_billing_account');
drop foreign table if exists sm_smp_sm_billing_account_transaction;	create FOREIGN TABLE sm_smp_sm_billing_account_transaction("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"description" varchar
	,"billing_account_id" int4
	,"write_uid" int4
	,"amount" float8
	,"write_date" timestamp
	,"type" varchar
	,"name" varchar NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'smp_sm_billing_account_transaction');
drop foreign table if exists sm_smp_sm_car;	create FOREIGN TABLE sm_smp_sm_car("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"name" varchar NOT NULL
	,"car_status_id" int4
	,"stored_firebase_data" varchar
	,"license_plate" varchar
	,"write_uid" int4
	,"car_name" varchar
	,"invers_qnr" varchar
	,"write_date" timestamp
	,"swap_group" varchar
	,"invers_lomo_adapter" bool
	,"invers_type" varchar
	,"location" varchar
	,"car_brand" varchar
	,"car_model" varchar
	,"registration" date
	,"vin" varchar
	,"car_owner" int4
	,"kilometre_marker" float8
	,"car_color" varchar
	,"has_gps" bool
	,"vehicle_type" varchar
	,"home" varchar
	,"owner_group_id" int4
	,"owner_group_index" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'smp_sm_car');
drop foreign table if exists sm_smp_sm_car_config;	create FOREIGN TABLE sm_smp_sm_car_config("id" int4 NOT NULL
	,"picture" varchar
	,"create_date" timestamp
	,"description" varchar
	,"name" varchar NOT NULL
	,"create_uid" int4
	,"car_id" int4
	,"carconfig_name" varchar
	,"write_uid" int4
	,"home_id" int4
	,"write_date" timestamp
	,"type" varchar
	,"rel_tariff_model_carconfig_name" varchar
	,"rel_tariff_model_description" varchar
	,"rel_tariff_model_id" int4
	,"related_price_group_id" int4
	,"initial_price" float8
	,"home" varchar
	,"group_id" int4
	,"owner_group_id" int4
	,"rel_car_id" int4
	,"group_index" varchar
	,"owner_group_index" varchar
	,"rel_car_index" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'smp_sm_car_config');
drop foreign table if exists sm_smp_sm_car_config_availability;	create FOREIGN TABLE sm_smp_sm_car_config_availability("id" int4 NOT NULL
	,"create_uid" int4
	,"config_id" int4
	,"create_date" timestamp
	,"name" varchar
	,"daily" varchar
	,"timeBetweenReservations" varchar
	,"write_date" timestamp
	,"minDuration" varchar
	,"date" date NOT NULL
	,"write_uid" int4
	,"weekly" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'smp_sm_car_config_availability');
drop foreign table if exists sm_smp_sm_car_config_price;	create FOREIGN TABLE sm_smp_sm_car_config_price("id" int4 NOT NULL
	,"create_uid" int4
	,"config_id" int4
	,"create_date" timestamp
	,"name" varchar NOT NULL
	,"price" float8
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'smp_sm_car_config_price');
drop foreign table if exists sm_smp_sm_car_config_smp_smp_car_config_groups_locals_rel;	create FOREIGN TABLE sm_smp_sm_car_config_smp_smp_car_config_groups_locals_rel("smp_smp_car_config_groups_locals_id" int4 NOT NULL
	,"smp_sm_car_config_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'smp_sm_car_config_smp_smp_car_config_groups_locals_rel');
drop foreign table if exists sm_smp_sm_car_status;	create FOREIGN TABLE sm_smp_sm_car_status("id" int4 NOT NULL
	,"create_date" timestamp
	,"keyfob" varchar
	,"write_uid" int4
	,"mileage_since_immobilizer_unlock" int4
	,"fuel_level" int4
	,"speed" int4
	,"seconds_since_immobilizer_unlock" int4
	,"create_uid" int4
	,"bluetooth_connection" varchar
	,"immobilizer" varchar
	,"mileage" int4
	,"central_lock_last_command" varchar
	,"car_status_position_id" int4
	,"alarm_input" varchar
	,"write_date" timestamp
	,"board_voltage" float8
	,"central_lock" varchar
	,"name" varchar
	,"electric_vehicle_state_charge" varchar
	,"ignition" varchar
	,"handbrake" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'smp_sm_car_status');
drop foreign table if exists sm_smp_sm_car_status_position;	create FOREIGN TABLE sm_smp_sm_car_status_position("id" int4 NOT NULL
	,"create_uid" int4
	,"meters_driven_since_last_fix" float8
	,"create_date" timestamp
	,"hdop" float8
	,"name" varchar
	,"timestamp" varchar
	,"lon" numeric
	,"write_uid" int4
	,"write_date" timestamp
	,"lat" numeric
	,"satellites_in_use" int4
	,"quality" int4
	,"speed_over_ground" float8)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'smp_sm_car_status_position');
drop foreign table if exists sm_smp_sm_carconfig_price_group;	create FOREIGN TABLE sm_smp_sm_carconfig_price_group("id" int4 NOT NULL
	,"name" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'smp_sm_carconfig_price_group');
drop foreign table if exists sm_smp_sm_carsharing_registration_wizard;	create FOREIGN TABLE sm_smp_sm_carsharing_registration_wizard("id" int4 NOT NULL
	,"cs_mins_product_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"cs_mileage_product_id" int4 NOT NULL
	,"write_date" timestamp
	,"cs_days_product_id" int4 NOT NULL
	,"send_registration_email" bool)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'smp_sm_carsharing_registration_wizard');
drop foreign table if exists sm_smp_sm_carsharing_tariff;	create FOREIGN TABLE sm_smp_sm_carsharing_tariff("id" int4 NOT NULL
	,"cs_mins_product_id" int4
	,"related_carconfig_id" int4
	,"create_date" timestamp
	,"name" varchar
	,"pocketbook" float8
	,"date_active" date NOT NULL
	,"write_uid" int4
	,"cs_mileage_product_id" int4
	,"tariff_type" varchar NOT NULL
	,"date_valid" date
	,"write_date" timestamp
	,"date" date NOT NULL
	,"cs_days_product_id" int4
	,"related_member_id" int4
	,"create_uid" int4
	,"closed" bool
	,"description" text
	,"carconfig_availability" varchar
	,"abouttoexpire_member_notified" bool
	,"tariff_model_id" int4
	,"reason" varchar
	,"pocketbook_threshold" float8
	,"pocketbook_initial" int4
	,"extra_tariff_model_id" int4
	,"time_restricted" bool
	,"related_reward_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'smp_sm_carsharing_tariff');
drop foreign table if exists sm_smp_sm_carsharing_tariff_history;	create FOREIGN TABLE sm_smp_sm_carsharing_tariff_history("id" int4 NOT NULL
	,"create_uid" int4
	,"obs" varchar
	,"create_date" timestamp
	,"name" varchar NOT NULL
	,"related_invoice_line_id" int4
	,"related_tariff_id" int4
	,"amount" float8
	,"write_date" timestamp
	,"date" date
	,"write_uid" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'smp_sm_carsharing_tariff_history');
drop foreign table if exists sm_smp_sm_carsharing_tariff_model;	create FOREIGN TABLE sm_smp_sm_carsharing_tariff_model("id" int4 NOT NULL
	,"cs_mins_product_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"name" varchar
	,"write_uid" int4
	,"cs_mileage_product_id" int4
	,"write_date" timestamp
	,"cs_days_product_id" int4
	,"description" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'smp_sm_carsharing_tariff_model');
drop foreign table if exists sm_smp_sm_db_wizard;	create FOREIGN TABLE sm_smp_sm_db_wizard("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_date" timestamp
	,"write_uid" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'smp_sm_db_wizard');
drop foreign table if exists sm_smp_sm_edit_reservation_compute_wizard;	create FOREIGN TABLE sm_smp_sm_edit_reservation_compute_wizard("id" int4 NOT NULL
	,"startTime_moved0" timestamp
	,"effectiveStartTime_moved0" timestamp
	,"endTime_moved0" timestamp
	,"effectiveEndTime_moved0" timestamp
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"startTime" varchar
	,"effectiveStartTime" varchar
	,"endTime" varchar
	,"effectiveEndTime" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'smp_sm_edit_reservation_compute_wizard');
drop foreign table if exists sm_smp_sm_fetch_carsharing_db_cron;	create FOREIGN TABLE sm_smp_sm_fetch_carsharing_db_cron("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'smp_sm_fetch_carsharing_db_cron');
drop foreign table if exists sm_smp_sm_group;	create FOREIGN TABLE sm_smp_sm_group("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"name" varchar NOT NULL
	,"billing_account_id" int4
	,"car_group_id" int4
	,"group_name" varchar
	,"write_date" timestamp
	,"write_uid" int4
	,"icon" varchar
	,"related_billingaccount_id" int4
	,"related_config_id" int4
	,"owner_group_id" int4
	,"related_billingaccount_index" varchar
	,"related_config_index" varchar
	,"owner_group_index" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'smp_sm_group');
drop foreign table if exists sm_smp_sm_group_config;	create FOREIGN TABLE sm_smp_sm_group_config("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"name" varchar NOT NULL
	,"write_uid" int4
	,"write_date" timestamp
	,"city" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'smp_sm_group_config');
drop foreign table if exists sm_smp_sm_place;	create FOREIGN TABLE sm_smp_sm_place("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"name" varchar NOT NULL
	,"geometry" text
	,"write_uid" int4
	,"write_date" timestamp
	,"place_name" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'smp_sm_place');
drop foreign table if exists sm_smp_sm_report_reservation_compute;	create FOREIGN TABLE sm_smp_sm_report_reservation_compute("id" int4 NOT NULL
	,"create_uid" int4
	,"pocketbook_mins_update" float8
	,"create_date" timestamp
	,"name" varchar NOT NULL
	,"total_days_invoiced" float8
	,"pocketbook_mins" float8
	,"invoice_id" int4
	,"batch_reservation_compute_id" int4
	,"total_mins_invoiced" float8
	,"write_uid" int4
	,"total_mins" float8
	,"total_mileage_invoiced" float8
	,"total_days" float8
	,"write_date" timestamp
	,"member_id" int4
	,"mins_to_pocketbook" float8
	,"invoice_report_id" int4
	,"report_type" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'smp_sm_report_reservation_compute');
drop foreign table if exists sm_smp_sm_reservation_compute;	create FOREIGN TABLE sm_smp_sm_reservation_compute("id" int4 NOT NULL
	,"create_date" timestamp
	,"write_date" timestamp
	,"fuel_consume_invoiced" float8
	,"write_uid" int4
	,"duration" float8
	,"effectiveStartTime" timestamp
	,"create_uid" int4
	,"initial_fuel_level" float8
	,"extraTime" float8
	,"carconfig_id" int4
	,"member_id" int4
	,"final_fuel_level" float8
	,"fuel_consume" float8
	,"savedTimeInvoiced" float8
	,"startTime" timestamp
	,"used_mileage" float8
	,"effectiveEndTime" timestamp
	,"name" varchar NOT NULL
	,"extraTimeInvoiced" float8
	,"compute_invoiced" bool
	,"effectiveDuration" float8
	,"used_mileage_invoiced" float8
	,"endTime" timestamp
	,"savedTime" float8
	,"usage_mins_invoiced" float8
	,"extra_usage_mins_invoiced" float8
	,"total_usage_days_invoiced" float8
	,"total_usage_mins_invoiced" float8
	,"report_reservation_compute_id" int4
	,"non_usage_mins_invoiced" float8
	,"compute_forgiven" bool
	,"observations" text
	,"ignore_update" bool
	,"total_usage_days_tariff" float8
	,"total_usage_mins_tariff" float8
	,"non_usage_mins_nontariff" float8
	,"usage_mins_nontariff" float8
	,"related_company" varchar
	,"extra_usage_mins_nontariff" float8
	,"extra_usage_mins_tariff" float8
	,"non_usage_mins_tariff" float8
	,"usage_mins_tariff" float8
	,"applied_tariff_id" int4
	,"current_car" varchar
	,"compute_cancelled" bool
	,"compute_unused" bool
	,"credits" float8
	,"price" float8
	,"startTimechar" varchar
	,"fuel_consume_watts" float8
	,"cs_carconfig_id" int4
	,"cs_production_unit_id" int4
	,"reporting_rating_minutes" float8
	,"reporting_rating_hours" float8)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'smp_sm_reservation_compute');
drop foreign table if exists sm_smp_sm_reservation_compute_wizard;	create FOREIGN TABLE sm_smp_sm_reservation_compute_wizard("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_date" timestamp
	,"write_uid" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'smp_sm_reservation_compute_wizard');
drop foreign table if exists sm_smp_sm_reservation_incoming;	create FOREIGN TABLE sm_smp_sm_reservation_incoming("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"name" varchar NOT NULL
	,"carconfig_id" int4
	,"write_uid" int4
	,"startTime" timestamp
	,"member_id" int4
	,"write_date" timestamp
	,"endTime" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'smp_sm_reservation_incoming');
drop foreign table if exists sm_smp_sm_reservation_wizard;	create FOREIGN TABLE sm_smp_sm_reservation_wizard("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_date" timestamp
	,"write_uid" int4
	,"from_q_date" date
	,"till_q_date" date
	,"update_only_car" bool)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'smp_sm_reservation_wizard');
drop foreign table if exists sm_smp_sm_tariffmodel_price_group;	create FOREIGN TABLE sm_smp_sm_tariffmodel_price_group("id" int4 NOT NULL
	,"rel_tariff_model_id" int4
	,"applied_carconfig_price_group_id" int4
	,"cs_mins_product_id" int4
	,"cs_days_product_id" int4
	,"cs_mileage_product_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'smp_sm_tariffmodel_price_group');
drop foreign table if exists sm_smp_sm_teletac;	create FOREIGN TABLE sm_smp_sm_teletac("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"description" varchar
	,"hour" varchar
	,"license_plate" varchar
	,"write_uid" int4
	,"discount" float8
	,"amount" float8
	,"write_date" timestamp
	,"date" date
	,"ttype" varchar
	,"reservation_compute_id" int4
	,"related_invoice_id" int4
	,"reservation_compute_forgiven" bool
	,"related_member_id" int4
	,"reservation_compute_invoiced" bool
	,"invoice_report_id" int4
	,"report_reservation_compute_id" int4
	,"related_member_nr" int4
	,"cs_user_type" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'smp_sm_teletac');
drop foreign table if exists sm_smp_smp_car_config_groups_locals;	create FOREIGN TABLE sm_smp_smp_car_config_groups_locals("id" int4 NOT NULL
	,"name" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'smp_smp_car_config_groups_locals');
drop foreign table if exists sm_smp_teletacs_wizard_relation;	create FOREIGN TABLE sm_smp_teletacs_wizard_relation("sm_partago_invoicing_sm_batch_reservation_compute_wizard_id" int4 NOT NULL
	,"smp_sm_teletac_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'smp_teletacs_wizard_relation');
drop foreign table if exists sm_sms_send_sms;	create FOREIGN TABLE sm_sms_send_sms("id" int4 NOT NULL
	,"recipients" varchar NOT NULL
	,"message" text NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sms_send_sms');
drop foreign table if exists sm_snailmail_letter;	create FOREIGN TABLE sm_snailmail_letter("id" int4 NOT NULL
	,"user_id" int4
	,"model" varchar NOT NULL
	,"res_id" int4 NOT NULL
	,"partner_id" int4 NOT NULL
	,"company_id" int4 NOT NULL
	,"report_template" int4
	,"attachment_id" int4
	,"color" bool
	,"duplex" bool
	,"state" varchar
	,"info_msg" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'snailmail_letter');
drop foreign table if exists sm_sommobilitat_sm_batch_payment;	create FOREIGN TABLE sm_sommobilitat_sm_batch_payment("id" int4 NOT NULL
	,"create_uid" int4
	,"sepa_file" bytea
	,"create_date" timestamp
	,"product_id" int4
	,"order_id" int4
	,"email_sent" bool
	,"date" date
	,"write_date" timestamp
	,"sepa_file_filename" varchar
	,"write_uid" int4
	,"name" varchar
	,"test_sepa_generated" bool
	,"test_sepa_file_filename" varchar
	,"test_sepa_file" bytea
	,"sepa_generated" bool
	,"state" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sommobilitat_sm_batch_payment');
drop foreign table if exists sm_sommobilitat_sm_batch_payment_order;	create FOREIGN TABLE sm_sommobilitat_sm_batch_payment_order("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"product_id" int4
	,"periodicity" varchar
	,"salesperson_id" int4
	,"batch_generated" bool
	,"write_uid" int4
	,"batch_applies" varchar
	,"write_date" timestamp
	,"date" date
	,"name" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sommobilitat_sm_batch_payment_order');
drop foreign table if exists sm_sommobilitat_sm_config;	create FOREIGN TABLE sm_sommobilitat_sm_config("id" int4 NOT NULL
	,"cs_mins_product_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"cs_mileage_product_id" int4
	,"write_date" timestamp
	,"cs_days_product_id" int4
	,"cs_url" varchar
	,"cs_teletac_product_id" int4
	,"invoice_report_mail_template_id" int4
	,"abouttoexpire_mail_template_id" int4
	,"invoice_mail_template_id" int4
	,"cs_carsharing_product_id" int4
	,"lopd_mail_template_id" int4
	,"reward_confirmation_email_template_id" int4
	,"contribution_year_report_email_template_id" int4
	,"contributions_contract_email_template_id" int4
	,"partner_id" int4
	,"fail_counter" int4
	,"alias_domain" varchar
	,"chart_template_id" int4
	,"module_account_accountant" bool
	,"group_analytic_accounting" bool
	,"group_warning_account" bool
	,"group_cash_rounding" bool
	,"module_account_asset" bool
	,"module_account_deferred_revenue" bool
	,"module_account_budget" bool
	,"module_account_payment" bool
	,"module_account_reports" bool
	,"module_account_reports_followup" bool
	,"module_l10n_us_check_printing" bool
	,"module_account_batch_deposit" bool
	,"module_account_sepa" bool
	,"module_account_sepa_direct_debit" bool
	,"module_account_plaid" bool
	,"module_account_yodlee" bool
	,"module_account_bank_statement_import_qif" bool
	,"module_account_bank_statement_import_ofx" bool
	,"module_account_bank_statement_import_csv" bool
	,"module_account_bank_statement_import_camt" bool
	,"module_currency_rate_live" bool
	,"module_print_docsaway" bool
	,"module_product_margin" bool
	,"module_l10n_eu_service" bool
	,"module_account_taxcloud" bool
	,"auth_signup_reset_password" bool
	,"auth_signup_template_user_id" int4
	,"company_share_product" bool
	,"group_uom" bool
	,"group_product_variant" bool
	,"group_stock_packaging" bool
	,"group_sale_pricelist" bool
	,"group_product_pricelist" bool
	,"group_pricelist_item" bool
	,"module_hr_timesheet" bool
	,"module_rating_project" bool
	,"module_project_forecast" bool
	,"group_subtask_project" bool
	,"use_sale_note" bool
	,"group_discount_per_so_line" bool
	,"module_sale_margin" bool
	,"group_sale_layout" bool
	,"group_warning_sale" bool
	,"portal_confirmation" bool
	,"portal_confirmation_options" varchar
	,"module_sale_payment" bool
	,"module_website_quote" bool
	,"group_sale_delivery_address" bool
	,"multi_sales_price" bool
	,"multi_sales_price_method" varchar
	,"sale_pricelist_setting" varchar
	,"group_show_price_subtotal" bool
	,"group_show_price_total" bool
	,"group_proforma_sales" bool
	,"sale_show_tax" varchar NOT NULL
	,"default_invoice_policy" varchar
	,"default_deposit_product_id" int4
	,"auto_done_setting" bool
	,"module_website_sale_digital" bool
	,"auth_signup_uninvited" varchar
	,"module_delivery" bool
	,"module_delivery_dhl" bool
	,"module_delivery_fedex" bool
	,"module_delivery_ups" bool
	,"module_delivery_usps" bool
	,"module_delivery_bpost" bool
	,"module_product_email_template" bool
	,"module_sale_coupon" bool
	,"module_procurement_jit" int4
	,"module_product_expiry" bool
	,"group_stock_production_lot" bool
	,"group_stock_tracking_lot" bool
	,"group_stock_tracking_owner" bool
	,"group_stock_adv_location" bool
	,"group_warning_stock" bool
	,"use_propagation_minimum_delta" bool
	,"module_stock_picking_batch" bool
	,"module_stock_barcode" bool
	,"group_stock_multi_locations" bool
	,"group_stock_multi_warehouses" bool
	,"group_multi_company" bool
	,"company_id" int4 NOT NULL
	,"default_user_rights" bool
	,"default_external_email_server" bool
	,"module_base_import" bool
	,"module_google_calendar" bool
	,"module_google_drive" bool
	,"module_google_spreadsheet" bool
	,"module_auth_oauth" bool
	,"module_auth_ldap" bool
	,"module_base_gengo" bool
	,"module_inter_company_rules" bool
	,"module_pad" bool
	,"module_voip" bool
	,"company_share_partner" bool
	,"default_custom_report_footer" bool
	,"group_multi_currency" bool
	,"module_stock_landed_costs" bool
	,"lock_confirmed_po" bool
	,"po_order_approval" bool
	,"default_purchase_method" varchar
	,"module_purchase_requisition" bool
	,"group_warning_purchase" bool
	,"module_stock_dropshipping" bool
	,"group_manage_vendor_price" bool
	,"module_account_3way_match" bool
	,"is_installed_sale" bool
	,"group_analytic_account_for_purchases" bool
	,"use_po_lead" bool
	,"group_route_so_lines" bool
	,"module_sale_order_dates" bool
	,"group_display_incoterm" bool
	,"use_security_lead" bool
	,"default_picking_policy" varchar NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sommobilitat_sm_config');
drop foreign table if exists sm_sommobilitat_sm_member_import;	create FOREIGN TABLE sm_sommobilitat_sm_member_import("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"ibin_filename" varchar
	,"ibin" bytea NOT NULL
	,"write_uid" int4
	,"write_date" timestamp
	,"name" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sommobilitat_sm_member_import');
drop foreign table if exists sm_sommobilitat_sm_member_signup_wizard;	create FOREIGN TABLE sm_sommobilitat_sm_member_signup_wizard("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_date" timestamp
	,"write_uid" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sommobilitat_sm_member_signup_wizard');
drop foreign table if exists sm_sparse_fields_test;	create FOREIGN TABLE sm_sparse_fields_test("id" int4 NOT NULL
	,"data" text
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'sparse_fields_test');
drop foreign table if exists sm_stock_backorder_confirmation;	create FOREIGN TABLE sm_stock_backorder_confirmation("id" int4 NOT NULL
	,"create_uid" int4
	,"write_uid" int4
	,"pick_id" int4
	,"write_date" timestamp
	,"create_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'stock_backorder_confirmation');
drop foreign table if exists sm_stock_change_product_qty;	create FOREIGN TABLE sm_stock_change_product_qty("id" int4 NOT NULL
	,"create_uid" int4
	,"product_id" int4 NOT NULL
	,"write_uid" int4
	,"product_tmpl_id" int4 NOT NULL
	,"create_date" timestamp
	,"write_date" timestamp
	,"lot_id" int4
	,"new_quantity" numeric NOT NULL
	,"location_id" int4 NOT NULL
	,"analytic_account_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'stock_change_product_qty');
drop foreign table if exists sm_stock_change_standard_price;	create FOREIGN TABLE sm_stock_change_standard_price("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"new_price" numeric NOT NULL
	,"write_uid" int4
	,"write_date" timestamp
	,"counterpart_account_id" int4
	,"counterpart_account_id_required" bool)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'stock_change_standard_price');
drop foreign table if exists sm_stock_config_settings;	create FOREIGN TABLE sm_stock_config_settings("id" int4 NOT NULL
	,"create_date" timestamp
	,"module_stock_calendar" int4
	,"module_stock_barcode" bool
	,"module_stock_picking_wave" int4
	,"company_id" int4 NOT NULL
	,"group_stock_tracking_lot" int4
	,"group_product_variant" int4
	,"create_uid" int4
	,"group_warning_stock" int4
	,"module_delivery_temando" bool
	,"group_stock_production_lot" int4
	,"group_stock_multi_warehouses" bool
	,"group_stock_tracking_owner" int4
	,"module_delivery_usps" bool
	,"module_stock_dropshipping" int4
	,"module_quality" bool
	,"module_procurement_jit" int4
	,"group_stock_packaging" int4
	,"write_date" timestamp
	,"module_delivery_fedex" bool
	,"write_uid" int4
	,"decimal_precision" int4
	,"group_uom" int4
	,"warehouse_and_location_usage_level" int4
	,"module_delivery_ups" bool
	,"module_product_expiry" int4
	,"group_stock_multi_locations" bool
	,"group_stock_adv_location" int4
	,"module_delivery_dhl" bool
	,"module_stock_landed_costs" int4
	,"group_stock_inventory_valuation" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'stock_config_settings');
drop foreign table if exists sm_stock_fixed_putaway_strat;	create FOREIGN TABLE sm_stock_fixed_putaway_strat("id" int4 NOT NULL
	,"create_uid" int4
	,"fixed_location_id" int4 NOT NULL
	,"putaway_id" int4 NOT NULL
	,"sequence" int4
	,"write_uid" int4
	,"write_date" timestamp
	,"create_date" timestamp
	,"category_id" int4
	,"product_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'stock_fixed_putaway_strat');
drop foreign table if exists sm_stock_history;	create FOREIGN TABLE sm_stock_history("id" int4
	,"move_id" int4
	,"location_id" int4
	,"company_id" int4
	,"product_id" int4
	,"product_categ_id" int4
	,"product_template_id" int4
	,"quantity" float8
	,"date" timestamp
	,"price_unit_on_quant" float8
	,"source" varchar
	,"serial_number" text)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'stock_history');
drop foreign table if exists sm_stock_immediate_transfer;	create FOREIGN TABLE sm_stock_immediate_transfer("id" int4 NOT NULL
	,"create_uid" int4
	,"write_uid" int4
	,"pick_id" int4
	,"write_date" timestamp
	,"create_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'stock_immediate_transfer');
drop foreign table if exists sm_stock_inventory;	create FOREIGN TABLE sm_stock_inventory("id" int4 NOT NULL
	,"create_date" timestamp
	,"exhausted" bool
	,"write_uid" int4
	,"package_id" int4
	,"lot_id" int4
	,"partner_id" int4
	,"create_uid" int4
	,"location_id" int4 NOT NULL
	,"company_id" int4 NOT NULL
	,"state" varchar
	,"write_date" timestamp
	,"date" timestamp NOT NULL
	,"product_id" int4
	,"name" varchar NOT NULL
	,"filter" varchar NOT NULL
	,"category_id" int4
	,"accounting_date" date)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'stock_inventory');
drop foreign table if exists sm_stock_inventory_line;	create FOREIGN TABLE sm_stock_inventory_line("id" int4 NOT NULL
	,"theoretical_qty" numeric
	,"write_uid" int4
	,"package_id" int4
	,"product_qty" numeric
	,"create_date" timestamp
	,"product_code" varchar
	,"partner_id" int4
	,"create_uid" int4
	,"location_name" varchar
	,"location_id" int4 NOT NULL
	,"company_id" int4
	,"product_name" varchar
	,"prodlot_name" varchar
	,"inventory_id" int4
	,"write_date" timestamp
	,"product_id" int4 NOT NULL
	,"product_uom_id" int4 NOT NULL
	,"prod_lot_id" int4
	,"analytic_account_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'stock_inventory_line');
drop foreign table if exists sm_stock_location;	create FOREIGN TABLE sm_stock_location("id" int4 NOT NULL
	,"parent_left" int4
	,"parent_right" int4
	,"comment" text
	,"putaway_strategy_id" int4
	,"create_date" timestamp
	,"write_date" timestamp
	,"write_uid" int4
	,"partner_id" int4
	,"removal_strategy_id" int4
	,"scrap_location" bool
	,"location_id" int4
	,"company_id" int4
	,"complete_name" varchar
	,"usage" varchar NOT NULL
	,"create_uid" int4
	,"barcode" varchar
	,"posz" int4
	,"posx" int4
	,"posy" int4
	,"active" bool
	,"name" varchar NOT NULL
	,"return_location" bool
	,"valuation_in_account_id" int4
	,"valuation_out_account_id" int4
	,"parent_path" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'stock_location');
drop foreign table if exists sm_stock_location_path;	create FOREIGN TABLE sm_stock_location_path("id" int4 NOT NULL
	,"location_from_id" int4 NOT NULL
	,"create_uid" int4
	,"route_sequence" int4
	,"name" varchar NOT NULL
	,"picking_type_id" int4 NOT NULL
	,"auto" varchar NOT NULL
	,"sequence" int4
	,"company_id" int4
	,"warehouse_id" int4
	,"delay" int4
	,"route_id" int4 NOT NULL
	,"create_date" timestamp
	,"location_dest_id" int4 NOT NULL
	,"write_date" timestamp
	,"active" bool
	,"propagate" bool
	,"write_uid" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'stock_location_path');
drop foreign table if exists sm_stock_location_route;	create FOREIGN TABLE sm_stock_location_route("id" int4 NOT NULL
	,"supplier_wh_id" int4
	,"create_uid" int4
	,"name" varchar NOT NULL
	,"sequence" int4
	,"warehouse_selectable" bool
	,"write_date" timestamp
	,"company_id" int4
	,"supplied_wh_id" int4
	,"product_selectable" bool
	,"product_categ_selectable" bool
	,"active" bool
	,"create_date" timestamp
	,"write_uid" int4
	,"sale_selectable" bool)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'stock_location_route');
drop foreign table if exists sm_stock_location_route_categ;	create FOREIGN TABLE sm_stock_location_route_categ("categ_id" int4 NOT NULL
	,"route_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'stock_location_route_categ');
drop foreign table if exists sm_stock_location_route_move;	create FOREIGN TABLE sm_stock_location_route_move("move_id" int4 NOT NULL
	,"route_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'stock_location_route_move');
drop foreign table if exists sm_stock_location_route_procurement;	create FOREIGN TABLE sm_stock_location_route_procurement("procurement_id" int4 NOT NULL
	,"route_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'stock_location_route_procurement');
drop foreign table if exists sm_stock_location_route_stock_rules_report_rel;	create FOREIGN TABLE sm_stock_location_route_stock_rules_report_rel("stock_rules_report_id" int4 NOT NULL
	,"stock_location_route_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'stock_location_route_stock_rules_report_rel');
drop foreign table if exists sm_stock_move;	create FOREIGN TABLE sm_stock_move("id" int4 NOT NULL
	,"origin" varchar
	,"create_date" timestamp
	,"restrict_partner_id" int4
	,"product_uom" int4 NOT NULL
	,"price_unit" float8
	,"product_uom_qty" numeric NOT NULL
	,"procure_method" varchar NOT NULL
	,"product_qty" numeric
	,"partner_id" int4
	,"priority" varchar
	,"picking_type_id" int4
	,"location_id" int4 NOT NULL
	,"sequence" int4
	,"company_id" int4 NOT NULL
	,"note" text
	,"state" varchar
	,"ordered_qty" numeric
	,"origin_returned_move_id" int4
	,"product_packaging" int4
	,"restrict_lot_id" int4
	,"date_expected" timestamp NOT NULL
	,"procurement_id" int4
	,"create_uid" int4
	,"warehouse_id" int4
	,"inventory_id" int4
	,"partially_available" bool
	,"propagate" bool
	,"move_dest_id" int4
	,"date" timestamp NOT NULL
	,"scrapped" bool
	,"write_uid" int4
	,"product_id" int4 NOT NULL
	,"openupgrade_legacy_12_0_push_rule_id" int4
	,"name" varchar NOT NULL
	,"split_from" int4
	,"rule_id" int4
	,"location_dest_id" int4 NOT NULL
	,"write_date" timestamp
	,"group_id" int4
	,"picking_id" int4
	,"purchase_line_id" int4
	,"to_refund" bool
	,"reference" varchar
	,"additional" bool
	,"value" float8
	,"remaining_qty" float8
	,"remaining_value" float8
	,"created_purchase_line_id" int4
	,"sale_line_id" int4
	,"analytic_account_id" int4
	,"package_level_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'stock_move');
drop foreign table if exists sm_stock_move_line;	create FOREIGN TABLE sm_stock_move_line("id" int4 NOT NULL
	,"picking_id" int4
	,"move_id" int4
	,"product_id" int4
	,"product_uom_id" int4 NOT NULL
	,"product_qty" numeric
	,"product_uom_qty" numeric NOT NULL
	,"ordered_qty" numeric
	,"qty_done" numeric
	,"package_id" int4
	,"lot_id" int4
	,"lot_name" varchar
	,"result_package_id" int4
	,"date" timestamp NOT NULL
	,"owner_id" int4
	,"location_id" int4 NOT NULL
	,"location_dest_id" int4 NOT NULL
	,"state" varchar
	,"reference" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"package_level_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'stock_move_line');
drop foreign table if exists sm_stock_move_line_consume_rel;	create FOREIGN TABLE sm_stock_move_line_consume_rel("consume_line_id" int4 NOT NULL
	,"produce_line_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'stock_move_line_consume_rel');
drop foreign table if exists sm_stock_move_move_rel;	create FOREIGN TABLE sm_stock_move_move_rel("move_orig_id" int4 NOT NULL
	,"move_dest_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'stock_move_move_rel');
drop foreign table if exists sm_stock_move_operation_link;	create FOREIGN TABLE sm_stock_move_operation_link("id" int4 NOT NULL
	,"reserved_quant_id" int4
	,"create_uid" int4
	,"qty" float8
	,"create_date" timestamp
	,"write_date" timestamp
	,"operation_id" int4 NOT NULL
	,"write_uid" int4
	,"move_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'stock_move_operation_link');
drop foreign table if exists sm_stock_overprocessed_transfer;	create FOREIGN TABLE sm_stock_overprocessed_transfer("id" int4 NOT NULL
	,"picking_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'stock_overprocessed_transfer');
drop foreign table if exists sm_stock_pack_operation;	create FOREIGN TABLE sm_stock_pack_operation("id" int4 NOT NULL
	,"create_date" timestamp
	,"result_package_id" int4
	,"write_uid" int4
	,"package_id" int4
	,"product_qty" numeric NOT NULL
	,"location_id" int4 NOT NULL
	,"ordered_qty" numeric
	,"qty_done" numeric
	,"owner_id" int4
	,"create_uid" int4
	,"fresh_record" bool
	,"write_date" timestamp
	,"date" timestamp NOT NULL
	,"product_id" int4
	,"product_uom_id" int4
	,"location_dest_id" int4 NOT NULL
	,"picking_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'stock_pack_operation');
drop foreign table if exists sm_stock_pack_operation_lot;	create FOREIGN TABLE sm_stock_pack_operation_lot("id" int4 NOT NULL
	,"create_uid" int4
	,"lot_name" varchar
	,"qty_todo" numeric
	,"qty" numeric
	,"create_date" timestamp
	,"write_date" timestamp
	,"operation_id" int4
	,"lot_id" int4
	,"write_uid" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'stock_pack_operation_lot');
drop foreign table if exists sm_stock_package_destination;	create FOREIGN TABLE sm_stock_package_destination("id" int4 NOT NULL
	,"picking_id" int4 NOT NULL
	,"location_dest_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'stock_package_destination');
drop foreign table if exists sm_stock_package_level;	create FOREIGN TABLE sm_stock_package_level("id" int4 NOT NULL
	,"package_id" int4 NOT NULL
	,"picking_id" int4
	,"location_dest_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'stock_package_level');
drop foreign table if exists sm_stock_picking;	create FOREIGN TABLE sm_stock_picking("id" int4 NOT NULL
	,"origin" varchar
	,"date_done" timestamp
	,"write_uid" int4
	,"recompute_pack_op" bool
	,"launch_pack_operations" bool
	,"location_id" int4 NOT NULL
	,"priority" varchar
	,"picking_type_id" int4 NOT NULL
	,"partner_id" int4
	,"move_type" varchar NOT NULL
	,"message_last_post" timestamp
	,"company_id" int4 NOT NULL
	,"note" text
	,"state" varchar
	,"owner_id" int4
	,"backorder_id" int4
	,"create_uid" int4
	,"min_date" timestamp
	,"printed" bool
	,"write_date" timestamp
	,"date" timestamp
	,"group_id" int4
	,"name" varchar
	,"create_date" timestamp
	,"location_dest_id" int4 NOT NULL
	,"max_date" timestamp
	,"openupgrade_legacy_11_0_state" varchar
	,"scheduled_date" timestamp
	,"is_locked" bool
	,"openupgrade_legacy_12_0_activity_date_deadline" date
	,"sale_id" int4
	,"message_main_attachment_id" int4
	,"immediate_transfer" bool)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'stock_picking');
drop foreign table if exists sm_stock_picking_backorder_rel;	create FOREIGN TABLE sm_stock_picking_backorder_rel("stock_backorder_confirmation_id" int4 NOT NULL
	,"stock_picking_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'stock_picking_backorder_rel');
drop foreign table if exists sm_stock_picking_transfer_rel;	create FOREIGN TABLE sm_stock_picking_transfer_rel("stock_immediate_transfer_id" int4 NOT NULL
	,"stock_picking_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'stock_picking_transfer_rel');
drop foreign table if exists sm_stock_picking_type;	create FOREIGN TABLE sm_stock_picking_type("id" int4 NOT NULL
	,"code" varchar NOT NULL
	,"create_date" timestamp
	,"sequence" int4
	,"color" int4
	,"write_uid" int4
	,"use_create_lots" bool
	,"create_uid" int4
	,"default_location_dest_id" int4
	,"show_entire_packs" bool
	,"barcode_nomenclature_id" int4
	,"use_existing_lots" bool
	,"warehouse_id" int4
	,"sequence_id" int4 NOT NULL
	,"write_date" timestamp
	,"active" bool
	,"name" varchar NOT NULL
	,"return_picking_type_id" int4
	,"default_location_src_id" int4
	,"show_operations" bool
	,"show_reserved" bool
	,"barcode" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'stock_picking_type');
drop foreign table if exists sm_stock_production_lot;	create FOREIGN TABLE sm_stock_production_lot("id" int4 NOT NULL
	,"create_date" timestamp
	,"write_uid" int4
	,"create_uid" int4
	,"message_last_post" timestamp
	,"ref" varchar
	,"write_date" timestamp
	,"product_id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"product_uom_id" int4
	,"message_main_attachment_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'stock_production_lot');
drop foreign table if exists sm_stock_quant;	create FOREIGN TABLE sm_stock_quant("id" int4 NOT NULL
	,"create_date" timestamp
	,"quantity" float8 NOT NULL
	,"propagated_from_id" int4
	,"package_id" int4
	,"cost" float8
	,"lot_id" int4
	,"location_id" int4 NOT NULL
	,"create_uid" int4
	,"reservation_id" int4
	,"company_id" int4
	,"owner_id" int4
	,"write_date" timestamp
	,"write_uid" int4
	,"product_id" int4 NOT NULL
	,"packaging_type_id" int4
	,"negative_move_id" int4
	,"in_date" timestamp
	,"reserved_quantity" float8 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'stock_quant');
drop foreign table if exists sm_stock_quant_move_rel;	create FOREIGN TABLE sm_stock_quant_move_rel("move_id" int4 NOT NULL
	,"quant_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'stock_quant_move_rel');
drop foreign table if exists sm_stock_quant_package;	create FOREIGN TABLE sm_stock_quant_package("id" int4 NOT NULL
	,"parent_left" int4
	,"parent_right" int4
	,"create_uid" int4
	,"name" varchar
	,"parent_id" int4
	,"write_date" timestamp
	,"packaging_id" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"location_id" int4
	,"company_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'stock_quant_package');
drop foreign table if exists sm_stock_quantity_history;	create FOREIGN TABLE sm_stock_quantity_history("id" int4 NOT NULL
	,"compute_at_date" int4
	,"date" timestamp
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'stock_quantity_history');
drop foreign table if exists sm_stock_return_picking;	create FOREIGN TABLE sm_stock_return_picking("id" int4 NOT NULL
	,"move_dest_exists" bool
	,"original_location_id" int4
	,"create_uid" int4
	,"parent_location_id" int4
	,"write_uid" int4
	,"write_date" timestamp
	,"create_date" timestamp
	,"location_id" int4
	,"picking_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'stock_return_picking');
drop foreign table if exists sm_stock_return_picking_line;	create FOREIGN TABLE sm_stock_return_picking_line("id" int4 NOT NULL
	,"create_uid" int4
	,"product_id" int4 NOT NULL
	,"wizard_id" int4
	,"write_uid" int4
	,"write_date" timestamp
	,"create_date" timestamp
	,"move_id" int4
	,"quantity" numeric NOT NULL
	,"to_refund_so" bool
	,"to_refund" bool)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'stock_return_picking_line');
drop foreign table if exists sm_stock_route_product;	create FOREIGN TABLE sm_stock_route_product("product_id" int4 NOT NULL
	,"route_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'stock_route_product');
drop foreign table if exists sm_stock_route_warehouse;	create FOREIGN TABLE sm_stock_route_warehouse("route_id" int4 NOT NULL
	,"warehouse_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'stock_route_warehouse');
drop foreign table if exists sm_stock_rule;	create FOREIGN TABLE sm_stock_rule("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"name" varchar NOT NULL
	,"sequence" int4
	,"company_id" int4
	,"write_uid" int4
	,"action" varchar NOT NULL
	,"write_date" timestamp
	,"active" bool
	,"group_id" int4
	,"group_propagation_option" varchar
	,"partner_address_id" int4
	,"location_id" int4 NOT NULL
	,"location_src_id" int4
	,"picking_type_id" int4 NOT NULL
	,"delay" int4
	,"warehouse_id" int4
	,"propagate" bool
	,"procure_method" varchar NOT NULL
	,"route_sequence" int4
	,"route_id" int4 NOT NULL
	,"propagate_warehouse_id" int4
	,"openupgrade_legacy_12_0_action" varchar
	,"openupgrade_legacy_12_0_loc_path_id" int4
	,"auto" varchar NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'stock_rule');
drop foreign table if exists sm_stock_rules_report;	create FOREIGN TABLE sm_stock_rules_report("id" int4 NOT NULL
	,"product_id" int4 NOT NULL
	,"product_tmpl_id" int4 NOT NULL
	,"product_has_variants" bool NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'stock_rules_report');
drop foreign table if exists sm_stock_rules_report_stock_warehouse_rel;	create FOREIGN TABLE sm_stock_rules_report_stock_warehouse_rel("stock_rules_report_id" int4 NOT NULL
	,"stock_warehouse_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'stock_rules_report_stock_warehouse_rel');
drop foreign table if exists sm_stock_scheduler_compute;	create FOREIGN TABLE sm_stock_scheduler_compute("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'stock_scheduler_compute');
drop foreign table if exists sm_stock_scrap;	create FOREIGN TABLE sm_stock_scrap("id" int4 NOT NULL
	,"origin" varchar
	,"create_date" timestamp
	,"write_uid" int4
	,"package_id" int4
	,"scrap_qty" float8 NOT NULL
	,"lot_id" int4
	,"location_id" int4 NOT NULL
	,"create_uid" int4
	,"scrap_location_id" int4 NOT NULL
	,"state" varchar
	,"owner_id" int4
	,"date_expected" timestamp
	,"write_date" timestamp
	,"move_id" int4
	,"product_id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"product_uom_id" int4 NOT NULL
	,"picking_id" int4
	,"analytic_account_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'stock_scrap');
drop foreign table if exists sm_stock_traceability_report;	create FOREIGN TABLE sm_stock_traceability_report("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'stock_traceability_report');
drop foreign table if exists sm_stock_track_confirmation;	create FOREIGN TABLE sm_stock_track_confirmation("id" int4 NOT NULL
	,"inventory_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'stock_track_confirmation');
drop foreign table if exists sm_stock_track_line;	create FOREIGN TABLE sm_stock_track_line("id" int4 NOT NULL
	,"product_id" int4
	,"tracking" varchar
	,"wizard_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'stock_track_line');
drop foreign table if exists sm_stock_warehouse;	create FOREIGN TABLE sm_stock_warehouse("id" int4 NOT NULL
	,"code" varchar(5) NOT NULL
	,"create_date" timestamp
	,"lot_stock_id" int4 NOT NULL
	,"wh_pack_stock_loc_id" int4
	,"reception_route_id" int4
	,"pick_type_id" int4
	,"crossdock_route_id" int4
	,"partner_id" int4
	,"create_uid" int4
	,"delivery_route_id" int4
	,"wh_input_stock_loc_id" int4
	,"company_id" int4 NOT NULL
	,"reception_steps" varchar NOT NULL
	,"delivery_steps" varchar NOT NULL
	,"view_location_id" int4 NOT NULL
	,"wh_qc_stock_loc_id" int4
	,"default_resupply_wh_id" int4
	,"pack_type_id" int4
	,"wh_output_stock_loc_id" int4
	,"write_uid" int4
	,"write_date" timestamp
	,"active" bool
	,"mto_pull_id" int4
	,"name" varchar NOT NULL
	,"in_type_id" int4
	,"out_type_id" int4
	,"int_type_id" int4
	,"buy_pull_id" int4
	,"buy_to_resupply" bool)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'stock_warehouse');
drop foreign table if exists sm_stock_warehouse_orderpoint;	create FOREIGN TABLE sm_stock_warehouse_orderpoint("id" int4 NOT NULL
	,"product_max_qty" numeric NOT NULL
	,"create_uid" int4
	,"qty_multiple" numeric NOT NULL
	,"name" varchar NOT NULL
	,"write_uid" int4
	,"warehouse_id" int4 NOT NULL
	,"company_id" int4 NOT NULL
	,"lead_days" int4
	,"lead_type" varchar NOT NULL
	,"product_min_qty" numeric NOT NULL
	,"write_date" timestamp
	,"active" bool
	,"create_date" timestamp
	,"group_id" int4
	,"location_id" int4 NOT NULL
	,"product_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'stock_warehouse_orderpoint');
drop foreign table if exists sm_stock_warn_insufficient_qty_scrap;	create FOREIGN TABLE sm_stock_warn_insufficient_qty_scrap("id" int4 NOT NULL
	,"scrap_id" int4
	,"product_id" int4 NOT NULL
	,"location_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'stock_warn_insufficient_qty_scrap');
drop foreign table if exists sm_stock_wh_resupply_table;	create FOREIGN TABLE sm_stock_wh_resupply_table("supplied_wh_id" int4 NOT NULL
	,"supplier_wh_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'stock_wh_resupply_table');
drop foreign table if exists sm_subscription_register;	create FOREIGN TABLE sm_subscription_register("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"register_number_operation" int4 NOT NULL
	,"partner_id" int4 NOT NULL
	,"partner_id_to" int4
	,"date" date NOT NULL
	,"quantity" int4
	,"share_unit_price" numeric
	,"share_product_id" int4 NOT NULL
	,"share_to_product_id" int4
	,"quantity_to" int4
	,"share_to_unit_price" numeric
	,"type" varchar
	,"company_id" int4 NOT NULL
	,"user_id" int4
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'subscription_register');
drop foreign table if exists sm_subscription_request;	create FOREIGN TABLE sm_subscription_request("id" int4 NOT NULL
	,"already_cooperator" bool
	,"name" varchar NOT NULL
	,"firstname" varchar
	,"lastname" varchar
	,"birthdate" date
	,"gender" varchar
	,"type" varchar
	,"state" varchar NOT NULL
	,"email" varchar NOT NULL
	,"iban" varchar
	,"partner_id" int4
	,"share_product_id" int4 NOT NULL
	,"ordered_parts" int4 NOT NULL
	,"address" varchar NOT NULL
	,"city" varchar NOT NULL
	,"zip_code" varchar NOT NULL
	,"country_id" int4 NOT NULL
	,"phone" varchar
	,"user_id" int4
	,"skip_control_ng" bool
	,"lang" varchar NOT NULL
	,"date" date NOT NULL
	,"company_id" int4 NOT NULL
	,"is_company" bool
	,"is_operation" bool
	,"company_name" varchar
	,"company_email" varchar
	,"company_register_number" varchar
	,"company_type" varchar
	,"same_address" bool
	,"activities_address" varchar
	,"activities_city" varchar
	,"activities_zip_code" varchar
	,"activities_country_id" int4
	,"contact_person_function" varchar
	,"operation_request_id" int4
	,"capital_release_request_date" date
	,"source" varchar
	,"data_policy_approved" bool
	,"internal_rules_approved" bool
	,"financial_risk_approved" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"_api_external_id" int4
	,"external_id_sequence_id" int4
	,"vat" varchar
	,"message_main_attachment_id" int4
	,"state_id" int4
	,"mobile" varchar
	,"must_register_in_cs" bool
	,"driving_license_expiration_date" varchar
	,"image_dni" varchar
	,"image_driving_license" varchar
	,"external_obj_id" int4
	,"representative_vat" varchar
	,"validation_cron_executed" bool
	,"related_reward_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'subscription_request');
drop foreign table if exists sm_summary_dept_rel;	create FOREIGN TABLE sm_summary_dept_rel("sum_id" int4 NOT NULL
	,"dept_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'summary_dept_rel');
drop foreign table if exists sm_summary_emp_rel;	create FOREIGN TABLE sm_summary_emp_rel("sum_id" int4 NOT NULL
	,"emp_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'summary_emp_rel');
drop foreign table if exists sm_tax_adjustments_wizard;	create FOREIGN TABLE sm_tax_adjustments_wizard("id" int4 NOT NULL
	,"create_uid" int4
	,"reason" varchar NOT NULL
	,"credit_account_id" int4 NOT NULL
	,"journal_id" int4 NOT NULL
	,"write_uid" int4
	,"company_currency_id" int4
	,"amount" numeric NOT NULL
	,"write_date" timestamp
	,"date" date NOT NULL
	,"create_date" timestamp
	,"debit_account_id" int4 NOT NULL
	,"tax_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'tax_adjustments_wizard');
drop foreign table if exists sm_team_favorite_user_rel;	create FOREIGN TABLE sm_team_favorite_user_rel("team_id" int4 NOT NULL
	,"user_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'team_favorite_user_rel');
drop foreign table if exists sm_trial_balance_report_wizard;	create FOREIGN TABLE sm_trial_balance_report_wizard("id" int4 NOT NULL
	,"company_id" int4
	,"date_range_id" int4
	,"date_from" date NOT NULL
	,"date_to" date NOT NULL
	,"target_move" varchar NOT NULL
	,"hierarchy_on" varchar NOT NULL
	,"limit_hierarchy_level" bool
	,"show_hierarchy_level" int4
	,"hide_parent_hierarchy_level" bool
	,"hide_account_at_0" bool
	,"receivable_accounts_only" bool
	,"payable_accounts_only" bool
	,"show_partner_details" bool
	,"not_only_one_unaffected_earnings_account" bool
	,"foreign_currency" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'trial_balance_report_wizard');
drop foreign table if exists sm_unece_code_list;	create FOREIGN TABLE sm_unece_code_list("id" int4 NOT NULL
	,"code" varchar NOT NULL
	,"name" varchar NOT NULL
	,"display_name" varchar
	,"type" varchar NOT NULL
	,"description" text
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'unece_code_list');
drop foreign table if exists sm_uom_category;	create FOREIGN TABLE sm_uom_category("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"name" varchar NOT NULL
	,"write_uid" int4
	,"write_date" timestamp
	,"measure_type" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'uom_category');
drop foreign table if exists sm_uom_uom;	create FOREIGN TABLE sm_uom_uom("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"name" varchar NOT NULL
	,"rounding" numeric NOT NULL
	,"write_uid" int4
	,"active" bool
	,"write_date" timestamp
	,"factor" numeric NOT NULL
	,"uom_type" varchar NOT NULL
	,"category_id" int4 NOT NULL
	,"unece_code" varchar
	,"measure_type" varchar
	,"timesheet_widget" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'uom_uom');
drop foreign table if exists sm_utm_campaign;	create FOREIGN TABLE sm_utm_campaign("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'utm_campaign');
drop foreign table if exists sm_utm_medium;	create FOREIGN TABLE sm_utm_medium("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"active" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'utm_medium');
drop foreign table if exists sm_utm_source;	create FOREIGN TABLE sm_utm_source("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'utm_source');
drop foreign table if exists sm_validate_account_move;	create FOREIGN TABLE sm_validate_account_move("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_date" timestamp
	,"write_uid" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'validate_account_move');
drop foreign table if exists sm_validate_subscription_request;	create FOREIGN TABLE sm_validate_subscription_request("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'validate_subscription_request');
drop foreign table if exists sm_vat_report_wizard;	create FOREIGN TABLE sm_vat_report_wizard("id" int4 NOT NULL
	,"company_id" int4
	,"date_range_id" int4
	,"date_from" date NOT NULL
	,"date_to" date NOT NULL
	,"based_on" varchar NOT NULL
	,"tax_detail" bool
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'vat_report_wizard');
drop foreign table if exists sm_web_editor_converter_test;	create FOREIGN TABLE sm_web_editor_converter_test("id" int4 NOT NULL
	,"create_uid" int4
	,"selection" int4
	,"create_date" timestamp
	,"binary" bytea
	,"text" text
	,"float" float8
	,"numeric" numeric
	,"datetime" timestamp
	,"char" varchar
	,"html" text
	,"selection_str" varchar
	,"write_date" timestamp
	,"many2one" int4
	,"date" date
	,"integer" int4
	,"write_uid" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'web_editor_converter_test');
drop foreign table if exists sm_web_editor_converter_test_sub;	create FOREIGN TABLE sm_web_editor_converter_test_sub("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"name" varchar
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'web_editor_converter_test_sub');
drop foreign table if exists sm_web_planner;	create FOREIGN TABLE sm_web_planner("id" int4 NOT NULL
	,"menu_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"name" varchar NOT NULL
	,"planner_application" varchar NOT NULL
	,"view_id" int4 NOT NULL
	,"write_uid" int4
	,"write_date" timestamp
	,"active" bool
	,"progress" int4
	,"data" text
	,"tooltip_planner" text)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'web_planner');
drop foreign table if exists sm_web_tip;	create FOREIGN TABLE sm_web_tip("id" int4 NOT NULL
	,"create_uid" int4
	,"placement" varchar
	,"description" text NOT NULL
	,"end_selector" varchar
	,"end_event" varchar
	,"title" varchar
	,"write_uid" int4
	,"mode" varchar
	,"write_date" timestamp
	,"trigger_selector" varchar
	,"create_date" timestamp
	,"model" varchar
	,"type" varchar
	,"highlight_selector" varchar
	,"action_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'web_tip');
drop foreign table if exists sm_web_tour_tour;	create FOREIGN TABLE sm_web_tour_tour("id" int4 NOT NULL
	,"user_id" int4
	,"name" varchar NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'web_tour_tour');
drop foreign table if exists sm_wiz_account_asset_report;	create FOREIGN TABLE sm_wiz_account_asset_report("id" int4 NOT NULL
	,"asset_group_id" int4
	,"date_from" date NOT NULL
	,"date_to" date NOT NULL
	,"draft" bool
	,"company_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'wiz_account_asset_report');
drop foreign table if exists sm_wizard_account_matching;	create FOREIGN TABLE sm_wizard_account_matching("id" int4 NOT NULL
	,"update_chart_wizard_id" int4 NOT NULL
	,"sequence" int4 NOT NULL
	,"matching_value" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'wizard_account_matching');
drop foreign table if exists sm_wizard_document_page_history_show_diff;	create FOREIGN TABLE sm_wizard_document_page_history_show_diff("id" int4 NOT NULL
	,"diff" text
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'wizard_document_page_history_show_diff');
drop foreign table if exists sm_wizard_fp_matching;	create FOREIGN TABLE sm_wizard_fp_matching("id" int4 NOT NULL
	,"update_chart_wizard_id" int4 NOT NULL
	,"sequence" int4 NOT NULL
	,"matching_value" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'wizard_fp_matching');
drop foreign table if exists sm_wizard_ir_model_menu_create;	create FOREIGN TABLE sm_wizard_ir_model_menu_create("id" int4 NOT NULL
	,"menu_id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"name" varchar NOT NULL
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'wizard_ir_model_menu_create');
drop foreign table if exists sm_wizard_matching;	create FOREIGN TABLE sm_wizard_matching("id" int4 NOT NULL
	,"update_chart_wizard_id" int4 NOT NULL
	,"sequence" int4 NOT NULL
	,"matching_value" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'wizard_matching');
drop foreign table if exists sm_wizard_multi_charts_accounts;	create FOREIGN TABLE sm_wizard_multi_charts_accounts("id" int4 NOT NULL
	,"only_one_chart_template" bool
	,"bank_account_code_prefix" varchar
	,"cash_account_code_prefix" varchar
	,"code_digits" int4 NOT NULL
	,"create_date" timestamp
	,"chart_template_id" int4 NOT NULL
	,"create_uid" int4
	,"transfer_account_id" int4 NOT NULL
	,"purchase_tax_id" int4
	,"complete_tax_set" bool
	,"sale_tax_id" int4
	,"write_uid" int4
	,"currency_id" int4 NOT NULL
	,"company_id" int4 NOT NULL
	,"purchase_tax_rate" float8
	,"write_date" timestamp
	,"sale_tax_rate" float8)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'wizard_multi_charts_accounts');
drop foreign table if exists sm_wizard_open_tax_balances;	create FOREIGN TABLE sm_wizard_open_tax_balances("id" int4 NOT NULL
	,"company_id" int4 NOT NULL
	,"from_date" date NOT NULL
	,"to_date" date NOT NULL
	,"date_range_id" int4
	,"target_move" varchar NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'wizard_open_tax_balances');
drop foreign table if exists sm_wizard_tax_matching;	create FOREIGN TABLE sm_wizard_tax_matching("id" int4 NOT NULL
	,"update_chart_wizard_id" int4 NOT NULL
	,"sequence" int4 NOT NULL
	,"matching_value" varchar
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'wizard_tax_matching');
drop foreign table if exists sm_wizard_update_charts_account_fields_rel;	create FOREIGN TABLE sm_wizard_update_charts_account_fields_rel("wizard_update_charts_accounts_id" int4 NOT NULL
	,"ir_model_fields_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'wizard_update_charts_account_fields_rel');
drop foreign table if exists sm_wizard_update_charts_accounts;	create FOREIGN TABLE sm_wizard_update_charts_accounts("id" int4 NOT NULL
	,"state" varchar
	,"company_id" int4 NOT NULL
	,"chart_template_id" int4 NOT NULL
	,"code_digits" int4
	,"lang" varchar NOT NULL
	,"update_tax" bool
	,"update_account" bool
	,"update_fiscal_position" bool
	,"continue_on_errors" bool
	,"recreate_xml_ids" bool
	,"rejected_new_account_number" int4
	,"rejected_updated_account_number" int4
	,"log" text
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'wizard_update_charts_accounts');
drop foreign table if exists sm_wizard_update_charts_accounts_account;	create FOREIGN TABLE sm_wizard_update_charts_accounts_account("id" int4 NOT NULL
	,"account_id" int4 NOT NULL
	,"update_chart_wizard_id" int4 NOT NULL
	,"type" varchar
	,"update_account_id" int4
	,"notes" text
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'wizard_update_charts_accounts_account');
drop foreign table if exists sm_wizard_update_charts_accounts_fiscal_position;	create FOREIGN TABLE sm_wizard_update_charts_accounts_fiscal_position("id" int4 NOT NULL
	,"fiscal_position_id" int4 NOT NULL
	,"update_chart_wizard_id" int4 NOT NULL
	,"type" varchar NOT NULL
	,"update_fiscal_position_id" int4
	,"notes" text
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'wizard_update_charts_accounts_fiscal_position');
drop foreign table if exists sm_wizard_update_charts_accounts_tax;	create FOREIGN TABLE sm_wizard_update_charts_accounts_tax("id" int4 NOT NULL
	,"tax_id" int4
	,"update_chart_wizard_id" int4 NOT NULL
	,"type" varchar
	,"update_tax_id" int4
	,"notes" text
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'wizard_update_charts_accounts_tax');
drop foreign table if exists sm_wizard_update_charts_fp_fields_rel;	create FOREIGN TABLE sm_wizard_update_charts_fp_fields_rel("wizard_update_charts_accounts_id" int4 NOT NULL
	,"ir_model_fields_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'wizard_update_charts_fp_fields_rel');
drop foreign table if exists sm_wizard_update_charts_tax_fields_rel;	create FOREIGN TABLE sm_wizard_update_charts_tax_fields_rel("wizard_update_charts_accounts_id" int4 NOT NULL
	,"ir_model_fields_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'wizard_update_charts_tax_fields_rel');
drop foreign table if exists sm_wizard_valuation_history;	create FOREIGN TABLE sm_wizard_valuation_history("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"write_uid" int4
	,"write_date" timestamp
	,"date" timestamp NOT NULL
	,"choose_date" bool)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'wizard_valuation_history');
drop foreign table if exists sm_wkf;	create FOREIGN TABLE sm_wkf("id" int4 NOT NULL
	,"name" varchar NOT NULL
	,"osv" varchar NOT NULL
	,"on_create" bool
	,"create_date" timestamp
	,"create_uid" int4
	,"write_uid" int4
	,"write_date" timestamp)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'wkf');
drop foreign table if exists sm_wkf_activity;	create FOREIGN TABLE sm_wkf_activity("id" int4 NOT NULL
	,"create_uid" int4
	,"kind" varchar NOT NULL
	,"create_date" timestamp
	,"name" varchar NOT NULL
	,"join_mode" varchar(3) NOT NULL
	,"wkf_id" int4 NOT NULL
	,"flow_stop" bool
	,"write_uid" int4
	,"subflow_id" int4
	,"split_mode" varchar(3) NOT NULL
	,"write_date" timestamp
	,"action" text
	,"signal_send" varchar
	,"flow_start" bool
	,"action_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'wkf_activity');
drop foreign table if exists sm_wkf_instance;	create FOREIGN TABLE sm_wkf_instance("id" int4 NOT NULL
	,"res_type" varchar
	,"uid" int4
	,"wkf_id" int4
	,"state" varchar
	,"res_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'wkf_instance');
drop foreign table if exists sm_wkf_transition;	create FOREIGN TABLE sm_wkf_transition("id" int4 NOT NULL
	,"create_uid" int4
	,"create_date" timestamp
	,"sequence" int4
	,"signal" varchar
	,"trigger_model" varchar
	,"write_uid" int4
	,"act_from" int4 NOT NULL
	,"act_to" int4 NOT NULL
	,"write_date" timestamp
	,"trigger_expr_id" varchar
	,"group_id" int4
	,"condition" varchar NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'wkf_transition');
drop foreign table if exists sm_wkf_triggers;	create FOREIGN TABLE sm_wkf_triggers("id" int4 NOT NULL
	,"instance_id" int4
	,"workitem_id" int4 NOT NULL
	,"model" varchar
	,"res_id" int4)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'wkf_triggers');
drop foreign table if exists sm_wkf_witm_trans;	create FOREIGN TABLE sm_wkf_witm_trans("inst_id" int4 NOT NULL
	,"trans_id" int4 NOT NULL)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'wkf_witm_trans');
drop foreign table if exists sm_wkf_workitem;	create FOREIGN TABLE sm_wkf_workitem("id" int4 NOT NULL
	,"act_id" int4 NOT NULL
	,"inst_id" int4 NOT NULL
	,"subflow_id" int4
	,"state" varchar)SERVER postgres_fdw_sm OPTIONS (schema_name 'public', table_name 'wkf_workitem');

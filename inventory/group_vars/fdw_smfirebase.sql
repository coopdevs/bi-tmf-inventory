drop foreign table if exists smfirebase_carconfigs;	create FOREIGN TABLE smfirebase_carconfigs("id" text NOT NULL
	,"group" text
	,"name" text
	,"car" text
	,"home" text
	,"ownergroup" text)SERVER postgres_fdw_smfirebase OPTIONS (schema_name 'public', table_name 'carconfigs');
drop foreign table if exists smfirebase_cars;	create FOREIGN TABLE smfirebase_cars("id" text NOT NULL
	,"inuse" bool
	,"latitude" float8
	,"longitude" float8
	,"fuel_level" float8
	,"charge_connected" bool
	,"charge_inprogress" bool
	,"status" json
	,"licenseplate" text
	,"model" text
	,"active_reservation" text
	,"ownergroup" text)SERVER postgres_fdw_smfirebase OPTIONS (schema_name 'public', table_name 'cars');
drop foreign table if exists smfirebase_devices;	create FOREIGN TABLE smfirebase_devices("id" text NOT NULL
	,"platform" text
	,"model" text
	,"manufacturer" text
	,"iscordova" bool
	,"lastuse" timestamptz
	,"useragent" text
	,"appversion" text
	,"flavor" text
	,"person" text)SERVER postgres_fdw_smfirebase OPTIONS (schema_name 'public', table_name 'devices');
drop foreign table if exists smfirebase_groups;	create FOREIGN TABLE smfirebase_groups("id" text
	,"name" text
	,"ownergroup" text)SERVER postgres_fdw_smfirebase OPTIONS (schema_name 'public', table_name 'groups');
drop foreign table if exists smfirebase_persons;	create FOREIGN TABLE smfirebase_persons("id" text NOT NULL
	,"group" text NOT NULL
	,"role" text
	,"created" timestamptz
	,"ownergroup" text)SERVER postgres_fdw_smfirebase OPTIONS (schema_name 'public', table_name 'persons');
drop foreign table if exists smfirebase_reservations;	create FOREIGN TABLE smfirebase_reservations("id" text
	,"starttime" timestamptz
	,"endtime" timestamptz
	,"effectivestarttime" timestamptz
	,"effectiveendtime" timestamptz
	,"iscancelled" bool
	,"created" timestamptz
	,"resource" text
	,"group" text
	,"person" text
	,"distance" float8
	,"endsoc" float8)SERVER postgres_fdw_smfirebase OPTIONS (schema_name 'public', table_name 'reservations');
drop foreign table if exists smfirebase_transactions;	create FOREIGN TABLE smfirebase_transactions("billingaccount" text
	,"group" text
	,"time" timestamptz
	,"type" text
	,"credits" float8
	,"creditsleft" float8
	,"price" float8
	,"product" text)SERVER postgres_fdw_smfirebase OPTIONS (schema_name 'public', table_name 'transactions');
drop foreign table if exists smfirebase_tripinfo;	create FOREIGN TABLE smfirebase_tripinfo("id" text
	,"group" text
	,"starttime" timestamptz
	,"endtime" timestamptz
	,"distance" float8
	,"energycharged" float8
	,"energydischarged" float8)SERVER postgres_fdw_smfirebase OPTIONS (schema_name 'public', table_name 'tripinfo');
drop foreign table if exists smfirebase_usages;	create FOREIGN TABLE smfirebase_usages("id" text
	,"starttime" timestamptz
	,"endtime" timestamptz
	,"resource" text
	,"group" text
	,"person" text
	,"distance" float8
	,"endsoc" float8)SERVER postgres_fdw_smfirebase OPTIONS (schema_name 'public', table_name 'usages');

import logging
import os
from datetime import timedelta
from typing import Optional

from cachelib.file import FileSystemCache
from celery.schedules import crontab

logger = logging.getLogger()


def get_env_variable(var_name: str, default: Optional[str] = None) -> str:
    """Get the environment variable or raise exception."""
    try:
        return os.environ[var_name]
    except KeyError:
        if default is not None:
            return default
        else:
            error_msg = "The environment variable {} was missing, abort...".format(
                var_name
            )
            raise EnvironmentError(error_msg)


SECRET_KEY = "XXXSECRET_KEY"

REDIS_HOST = "redis"
REDIS_PORT = 6379
REDIS_CELERY_DB = "0"
REDIS_RESULTS_DB = "1"

RESULTS_BACKEND = FileSystemCache("/app/superset_home/sqllab")


class CeleryConfig(object):
    BROKER_URL = f"redis://{REDIS_HOST}:{REDIS_PORT}/{REDIS_CELERY_DB}"
    CELERY_IMPORTS = ("superset.sql_lab", "superset.tasks", "superset.tasks.thumbnails", )
    CELERY_RESULT_BACKEND = f"redis://{REDIS_HOST}:{REDIS_PORT}/{REDIS_RESULTS_DB}"
    CELERYD_LOG_LEVEL = "DEBUG"
    CELERYD_PREFETCH_MULTIPLIER = 10
    CELERY_ACKS_LATE = True
    CELERY_ANNOTATIONS = {
        "tasks.add": {
            "rate_limit": "10/s",
        },
        "sql_lab.get_sql_results": {
            "rate_limit": "100/s",
        },
        "email_reports.send": {
            "rate_limit": "1/s",
            "time_limit": 120,
            "soft_time_limit": 150,
            "ignore_result": True,
        },
    }
    CELERYBEAT_SCHEDULE = {
        "reports.scheduler": {
            "task": "reports.scheduler",
            "schedule": crontab(minute="*", hour="*"),
        },
        "reports.prune_log": {
            "task": "reports.prune_log",
            "schedule": crontab(minute=10, hour=0),
        },
        'email_reports.schedule_hourly': {
            'task': 'email_reports.schedule_hourly',
            'schedule': crontab(minute=1, hour='*'),
        },
    }
    CELERY_TASK_PROTOCOL = 1

ROW_LIMIT = 5000
SUPERSET_WEBSERVER_PORT = 8088

SQLALCHEMY_DATABASE_URI = 'postgresql://superset:superset@db:5432/superset'

WTF_CSRF_ENABLED = True
WTF_CSRF_EXEMPT_LIST = []
WTF_CSRF_TIME_LIMIT = 60 * 60 * 24 * 365


MAPBOX_API_KEY = ''


CYPRESS_CONFIG = True


# Email configuration
SMTP_HOST = "XXXSMTP_HOST"
SMTP_STARTTLS = XXXSMTP_STARTTLS
SMTP_SSL = XXXSMTP_SSL
SMTP_USER = "XXXSMTP_USER"
SMTP_PORT = XXXSMTP_PORT
SMTP_PASSWORD = "XXXSMTP_PASSWORD"
SMTP_MAIL_FROM = "XXXSMTP_MAIL_FROM"
SCHEDULED_EMAIL_DEBUG_MODE = False


CELERY_CONFIG = CeleryConfig
SCREENSHOT_LOCATE_WAIT = 100
SCREENSHOT_LOAD_WAIT = 600
WEBDRIVER_TYPE = "firefox"


FEATURE_FLAGS = {"ALERT_REPORTS": True, "ENABLE_TEMPLATE_PROCESSING": True}
ALERT_REPORTS_NOTIFICATION_DRY_RUN = False
WEBDRIVER_BASEURL = "http://superset:8088/"
WEBDRIVER_BASEURL_USER_FRIENDLY = "XXXWEBDRIVER_BASEURL_USER_FRIENDLY"

SQLLAB_CTAS_NO_LIMIT = True

try:
    import superset_config_docker
    from superset_config_docker import *  # noqa

    logger.info(
        f"Loaded your Docker configuration at " f"[{superset_config_docker.__file__}]"
    )
except ImportError:
    logger.info("Using default Docker config...")
